function initialize() {
    var latitude =  parseFloat($('#latitude').val());
    var longitude = parseFloat($('#longitude').val());
    var zoom = parseInt($('#zoom').val());
    var map_canvas = document.getElementById('map_contact');
    var location = new google.maps.LatLng(latitude, longitude);
    var map_options = {
        center: location,
        zoom: zoom,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    var map = new google.maps.Map(map_canvas, map_options);
    new google.maps.Marker({
        position: location,
        map: map
    });
}

$(document).ready(function(){

    $("#signupForm").validate({
        rules: {
            firstname: "required",
            lastname: "required",
            username: {
                required: true,
                minlength: 2
            },
            password: {
                required: true,
                minlength: 5
            },
            confirm_password: {
                required: true,
                minlength: 5,
                equalTo: "#password"
            },
            email: {
                required: true,
                email: true
            }
        },
        messages: {
            firstname: "Please enter your first name",
            lastname: "Please enter your last name",
            username: {
                required: "Please enter a username",
                minlength: "Your username must consist of at least 2 characters"
            },
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long"
            },
            confirm_password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long",
                equalTo: "Please enter the same password as above"
            },
            email: "Please enter a valid email address",
        }
    });

    $("#checkoutForm").validate({
        rules: {
            bfirstname: "required",
            blastname: "required",
            bcompany: "required",
            baddr: "required",
            bcity: "required",
            bcounty: "required",
            bpostcode: "required",
            bemail: "required",
            bphone: "required",
            sfirstname: "required",
            slastname: "required",
            scompany: "required",
            saddr: "required",
            scity: "required",
            scounty: "required",
            spostcode: "required",
            semail: "required",
            sphone: "required",

        },
        messages: {
            bfirstname: "Please enter your first name",
            blastname: "Please enter your Last name",
            bcompany: "Please enter your company",
            baddr: "Please enter your address",
            bcity: "Please enter your town/city",
            bcounty: "Please enter your county",
            bpostcode: "Please enter your post code",
            bemail: "Please enter your email",
            bphone: "Please enter your phone",
            sfirstname: "Please enter your first name",
            slastname: "Please enter your last name",
            scompany: "Please enter your company",
            saddr: "Please enter your address",
            scity: "Please enter your town/city",
            scounty: "Please enter your county",
            spostcode: "Please enter your post code",
            semail: "Please enter your email",
            sphone: "Please enter your phone",

        }
    });

    $("#contactForm").validate({
        rules: {
            name: "required",

            email: {
                required: true,
                email: true
            }
        },
        messages: {
            name: "Please enter your name",
            email: "Please enter a valid email address",
        }
    });

   // google.maps.event.addDomListener(window, 'load', initialize);

})