/**
 * Created by R41m3L on 07/17/2015.
 */
$(function(){

    //resize menu on page scroll...
    var pageNav = $(".page-navigation");
    if ($(document).scrollTop() > 110) {
        pageNav.addClass("small-nav");
        //$("body").css("padding-top", "60px");
    } else {
        pageNav.removeClass("small-nav");
       // $("body").css("padding-top", "110px");
    }
    $(document).on("scroll", function() {
        if ($(document).scrollTop() > 110) {
            pageNav.addClass("small-nav");
            //$("body").css("padding-top", "60px");
        } else {
            pageNav.removeClass("small-nav");
           // $("body").css("padding-top", "110px");
        }
    });

    //menu trigger
    var navMenu = $(".navigation");
    $(".menu-trigger").on("click", function (e) {
        var t = $(this);
        e.preventDefault();
        navMenu.slideToggle(200, function () {
            if (navMenu.is(":visible")) {
                t.addClass("open");
            } else {
                t.removeClass("open");
            }
        });
    });

    //back to top
    $(".back-top").on("click", function (e) {
        e.preventDefault();
        $("html, body").animate({
            scrollTop: 0
        }, 1000);
    });

    //easyzoom effect (Overlay option)
    $(".easyzoom").easyZoom();


    //cart list trigger
    var cartList = $(".cart-content");
    $(".shop-cart").on("click", function () {
        var t = $(this);
        cartList.stop(true,true).toggle("slide", { direction: "up" }, function (){
            if (cartList.is(":visible")) {
                t.addClass("open");
            } else {
                t.removeClass("open");
            }
        });
    });

    //user options
    var uOpt = $(".user-options");
    $(".signed").on("click", function(e) {
        var t = $(this);
        e.preventDefault();
        uOpt.stop(true,true).toggle("slide", {direction: "right"}, function (){
            if (uOpt.is(":visible")) {
                t.addClass("open");
            } else {
                t.removeClass("open");
            }
        });
    });

    //search panel trigger
    var searchPanel = $(".search-panel");
    $(".search-btn").on("click", function (e) {
        var t = $(this);
        e.preventDefault();
        searchPanel.slideToggle(200);
    });
    $("#search-close").on("click", function (e) {
        e.preventDefault();
        searchPanel.slideToggle(200);
    });

    //accordion
    var collapsible = $('.panel-collapse');

    collapsible.on("show.bs.collapse", function () {
        var t = $(this);
        var icon = $("i.fa");
        if (t.parent().find(icon).hasClass("fa-plus")) {
            t.parent().find(icon).removeClass("fa-plus").addClass("fa-minus")
        }
    });
    collapsible.on("hide.bs.collapse", function () {
        var t = $(this);
        var icon = $("i.fa");
        if (t.parent().find(icon).hasClass("fa-minus")) {
            t.parent().find(icon).removeClass("fa-minus").addClass("fa-plus")
        }
    });

    //sizes list ...
    var sizes = $(".sizes").find("span");
    sizes.on("click", function () {
        sizes.each(function () {
            $(this).removeClass("selected");
        });
        $(this).addClass("selected");
    });

    //tabs
    /*
    var pList = $(".product-list");
    $.each(pList, function () {
        if ($(this).children().length > 4) {
            $(this).prepend('<span class="arrows arrow-left"><img src="/static/imgs/thin-left.png"></span><span class="arrows arrow-right"><img src="/static/imgs/thin-right.png"></span>')
        }
    });
    */

    var tabs = $("a[data-toggle='tab']");

    var arrowLeft = $(".arrow-left");
    var arrowRight = $(".arrow-right");
    var arrows = $(".arrows");


    //carousels
    $("#main-carousel").slick({
        arrows: false,
        dots: false,
        slidesToShow: 1,
        slidesToScroll: 1
    });

    var bigTrend = $(".big-trend-carousel");
    bigTrend.slick({
        arrows: false,
        dots: false,
        slide: 'div',
        pauseOnHover: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        asNavFor: '.small-trend-carousel'
    });

    $(".small-trend-carousel").slick({
        arrows: false,
        dots: false,
        slide: 'div',
        pauseOnHover: true,
        focusOnSelect: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.big-trend-carousel'
    });

    $(".related-products").slick({
        arrows: true,
        dots: false,
        slide: 'div',
        prevArrow: '<span class="arrows arrow-left"><img src="/static/imgs/thin-left.png"></span>',
        nextArrow: '<span class="arrows arrow-right"><img src="/static/imgs/thin-right.png"></span>',
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 991,
                settings: {
                    arrows: true,
                    dots: false,
                    slide: 'div',
                    prevArrow: '<span class="arrows arrow-left"><img src="/static/imgs/thin-left.png"></span>',
                    nextArrow: '<span class="arrows arrow-right"><img src="/static/imgs/thin-right.png"></span>',
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 767,
                settings: {
                    arrows: true,
                    dots: false,
                    slide: 'div',
                    prevArrow: '<span class="arrows arrow-left"><img src="/static/imgs/thin-left.png"></span>',
                    nextArrow: '<span class="arrows arrow-right"><img src="/static/imgs/thin-right.png"></span>',
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 460,
                settings: {
                    arrows: true,
                    dots: false,
                    slide: 'div',
                    prevArrow: '<span class="arrows arrow-left"><img src="/static/imgs/thin-left.png"></span>',
                    nextArrow: '<span class="arrows arrow-right"><img src="/static/imgs/thin-right.png"></span>',
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });


    var mostReview = $(".most-reviews");
    var newArrivals = $(".new-arrivals");
    var popularProducts = $(".popular-products");

    mostReview.slick({
        arrows: true,
        dots: false,
        prevArrow: '<span class="arrows arrow-left"><img src="/static/imgs/thin-left.png"></span>',
        nextArrow: '<span class="arrows arrow-right"><img src="/static/imgs/thin-right.png"></span>',
        slide: 'div',
        slidesToShow: 4,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 991,
                settings: {
                    arrows: true,
                    dots: false,
                    prevArrow: '<span class="arrows arrow-left"><img src="/static/imgs/thin-left.png"></span>',
                    nextArrow: '<span class="arrows arrow-right"><img src="/static/imgs/thin-right.png"></span>',
                    slide: 'div',
                    slidesToShow: 3,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 768,
                settings: {
                    arrows: true,
                    prevArrow: '<span class="arrows arrow-left"><img src="/static/imgs/thin-left.png"></span>',
                    nextArrow: '<span class="arrows arrow-right"><img src="/static/imgs/thin-right.png"></span>',
                    dots: false,
                    slide: 'div',
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 590,
                settings: {
                    arrows: true,
                    prevArrow: '<span class="arrows arrow-left"><img src="/static/imgs/thin-left.png"></span>',
                    nextArrow: '<span class="arrows arrow-right"><img src="/static/imgs/thin-right.png"></span>',
                    dots: false,
                    slide: 'div',
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
        ]
    });

    tabs.on("shown.bs.tab", function() {
        $.each(tabs, function () {
            var t = $(this);
            if (t.parent().hasClass("active")) {
                t.removeClass("inverse");
            } else {
                t.addClass("inverse");
            }
        });

        if (mostReview.is(":visible")) {
            mostReview.slick({
                arrows: true,
                dots: false,
                prevArrow: '<span class="arrows arrow-left"><img src="/static/imgs/thin-left.png"></span>',
                nextArrow: '<span class="arrows arrow-right"><img src="/static/imgs/thin-right.png"></span>',
                slide: 'div',
                slidesToShow: 4,
                slidesToScroll: 1,
                responsive: [
                    {
                        breakpoint: 991,
                        settings: {
                            arrows: true,
                            prevArrow: '<span class="arrows arrow-left"><img src="/static/imgs/thin-left.png"></span>',
                            nextArrow: '<span class="arrows arrow-right"><img src="/static/imgs/thin-right.png"></span>',
                            dots: false,
                            slide: 'div',
                            slidesToShow: 3,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            arrows: true,
                            prevArrow: '<span class="arrows arrow-left"><img src="/static/imgs/thin-left.png"></span>',
                            nextArrow: '<span class="arrows arrow-right"><img src="/static/imgs/thin-right.png"></span>',
                            dots: false,
                            slide: 'div',
                            slidesToShow: 2,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 590,
                        settings: {
                            arrows: true,
                            prevArrow: '<span class="arrows arrow-left"><img src="/static/imgs/thin-left.png"></span>',
                            nextArrow: '<span class="arrows arrow-right"><img src="/static/imgs/thin-right.png"></span>',
                            dots: false,
                            slide: 'div',
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            });
        } else if (newArrivals.is(":visible")) {
            newArrivals.slick({
                arrows: true,
                dots: false,
                prevArrow: '<span class="arrows arrow-left"><img src="/static/imgs/thin-left.png"></span>',
                nextArrow: '<span class="arrows arrow-right"><img src="/static/imgs/thin-right.png"></span>',
                slide: 'div',
                slidesToShow: 4,
                slidesToScroll: 1,
                responsive: [
                    {
                        breakpoint: 991,
                        settings: {
                            arrows: true,
                            prevArrow: '<span class="arrows arrow-left"><img src="/static/imgs/thin-left.png"></span>',
                            nextArrow: '<span class="arrows arrow-right"><img src="/static/imgs/thin-right.png"></span>',
                            dots: false,
                            slide: 'div',
                            slidesToShow: 3,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            arrows: true,
                            prevArrow: '<span class="arrows arrow-left"><img src="/static/imgs/thin-left.png"></span>',
                            nextArrow: '<span class="arrows arrow-right"><img src="/static/imgs/thin-right.png"></span>',
                            dots: false,
                            slide: 'div',
                            slidesToShow: 2,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 590,
                        settings: {
                            arrows: true,
                            prevArrow: '<span class="arrows arrow-left"><img src="/static/imgs/thin-left.png"></span>',
                            nextArrow: '<span class="arrows arrow-right"><img src="/static/imgs/thin-right.png"></span>',
                            dots: false,
                            slide: 'div',
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            });
        } else if (popularProducts.is(":visible")) {
            popularProducts.slick({
                arrows: true,
                dots: false,
                prevArrow: '<span class="arrows arrow-left"><img src="/static/imgs/thin-left.png"></span>',
                nextArrow: '<span class="arrows arrow-right"><img src="/static/imgs/thin-right.png"></span>',
                slide: 'div',
                slidesToShow: 4,
                slidesToScroll: 1,
                responsive: [
                    {
                        breakpoint: 991,
                        settings: {
                            arrows: true,
                            dots: false,
                            prevArrow: '<span class="arrows arrow-left"><img src="/static/imgs/thin-left.png"></span>',
                            nextArrow: '<span class="arrows arrow-right"><img src="/static/imgs/thin-right.png"></span>',
                            slide: 'div',
                            slidesToShow: 3,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            arrows: true,
                            prevArrow: '<span class="arrows arrow-left"><img src="/static/imgs/thin-left.png"></span>',
                            nextArrow: '<span class="arrows arrow-right"><img src="/static/imgs/thin-right.png"></span>',
                            dots: false,
                            slide: 'div',
                            slidesToShow: 2,
                            slidesToScroll: 1
                        }
                    },
                    {
                        breakpoint: 590,
                        settings: {
                            arrows: true,
                            prevArrow: '<span class="arrows arrow-left"><img src="/static/imgs/thin-left.png"></span>',
                            nextArrow: '<span class="arrows arrow-right"><img src="/static/imgs/thin-right.png"></span>',
                            dots: false,
                            slide: 'div',
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                ]
            })
        }
    });

    $(".chosen-select").chosen({disable_search_threshold: 5});

    bigTrend.height($(".wrapper").height());

    var pItem = $(".product-item");
    var hOverContent = $(".hover-content");
    pItem.hover(
        function () {
            $(this).find(".hover-content").stop(true,true).slideToggle();
        }, function () {
            $(this).find(".hover-content").stop(true,true).slideToggle();
        }
    );

    var tItem = $(".trend-item");
    var hOverContent = $(".hover-content");
    tItem.hover(
        function () {
            $(this).find(".hover-content").stop(true,true).slideToggle();
        }, function () {
            $(this).find(".hover-content").stop(true,true).slideToggle();
        }
    );

    var nItem = $(".nav-item");
    var hOverContent = $(".hover-content");
    nItem.hover(
        function () {
            $(this).find(".hover-content").stop(true,true).slideToggle();
        }, function () {
            $(this).find(".hover-content").stop(true,true).slideToggle();
        }
    );

    var starFavorite = $(".fa.favorite");
    starFavorite.on("click", function () {
        var t = $(this);
        if (t.hasClass("fa-star-o")) {
            t.removeClass("fa-star-o").addClass("fa-star")
        } else if (t.hasClass("fa-star")) {
            t.removeClass("fa-star").addClass("fa-star-o")
        }
    });

    var rangeSlider = $(".slider");
    var rangeLimits = $(".range");
    var rangeValues = $(".price-range");
    var min = parseInt($('#b').val());
    var max = parseInt($('#f').val());

    rangeSlider.slider({
        range: true,
        min: parseInt(rangeLimits.attr("data-min"),10),
        max: parseInt(rangeLimits.attr("data-max"),10),
        values: [min, max],
        slide: function (event, ui) {
            rangeValues.html("Price: <strong>$"+ui.values[0]+"</strong> - <strong>$"+ui.values[1]+"</strong>");
            $('#b').val(rangeSlider.slider("values",0));
            $('#f').val(rangeSlider.slider("values",1));
        }
    });
    rangeValues.html("Price: <strong>$"+rangeSlider.slider("values",0)+"</strong> - <strong>$"+rangeSlider.slider("values",1)+"</strong>");
    $('#b').val(rangeSlider.slider("values",0));
    $('#f').val(rangeSlider.slider("values",1));

    //set columns to same height
    var row = $('.equalize');
    $.each(row, function() {
        var maxh=0;
        $.each($(this).find('div[class^="col-"]'), function() {
            if($(this).height() > maxh)
                maxh=$(this).height();
        });
        $.each($(this).find('div[class^="col-"]'), function() {
            $(this).height(maxh);
        });
    });

    var box = $('.box-equalized');
    var maxh=0;
    $.each(box, function() {
        if ($(this).height() > maxh) {
            maxh=$(this).height();
        }
    });
    $.each(box, function() {
        $(this).height(maxh);
    });

    $('#sort_select').change(function(){
        var url = $(location).attr('href');
        var array = url.split('?')
        var domain = array[0];
        if (array.length > 1){
            array = array[1].split('&');
            console.log(array);
            if (array.length >= 3){
                var params = '?';
                for(var i = 0; i < array.length - 1; i++){
                    params += array[i]+'&';
                }
                $(location).attr('href', domain+params+'o='+$(this).val());
            }
            else
                $(location).attr('href', domain+'?o='+$(this).val());
        }
        else{
            $(location).attr('href', domain+'?o='+$(this).val());
        }

    });


    $('#diferent_addr').click(function(){
        if($('#diferent_addr').get(0).checked){
            $('#shipping_addr').show();
            $('.ship_cmp').attr('required','required');
        }
        else{
            $('#shipping_addr').hide();
            $('.ship_cmp').removeAttr('required');
        }
    });

    if($('#diferent_addr').length > 0 && $('#diferent_addr').get(0).checked){
        $('#shipping_addr').show();
    }
    else{
        $('#shipping_addr').hide();
    }


    $('#accept_terms').click(function(){
        if($('#accept_terms').get(0).checked){
            $('#btn_send').removeAttr('disabled');
        }
        else{
            $('#btn_send').attr('disabled','disabled');
        }
    });

    if($('#accept_terms').length > 0 && $('#accept_terms').get(0).checked){
        $('#btn_send').removeAttr('disabled');
    }
    else{
        $('#btn_send').attr('disabled','disabled');
    }

    $('#signin').click(function(){
       $('#form-signin').submit();
    });

});
