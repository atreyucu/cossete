function show_add_message(message){
    if($('#not_line').html()){
        $("html, body").animate({
            scrollTop: 90
        },1000);
        $('#not_line').children('strong').html(message);
        $('#not_line').fadeIn(2000).delay(8000).fadeOut();
        //$('#not_line').fadeOut(10000);
        $('#error_message').hide();

    }
}

function show_error_message(message){
    //if($('#error_message').html()){
    $("html, body").animate({
        scrollTop: 90
    },1000);
    $('#error_message').children('strong').html(message);
    $('#error_message').fadeIn(2000).delay(4000).fadeOut();
    //}
}

function disable_up_btns(){
    $('#btn_up_go_checkout').attr('disabled','disabled');
    $('#btn_up_view_cart').attr('disabled','disabled');
}


function enable_up_btns(){
    $('#btn_up_go_checkout').removeAttr('disabled');
    $('#btn_up_view_cart').removeAttr('disabled');
}

function update_row_qty(total,key){
    if($('#total-' + key).html()){
        $('#total-' + key).html('$' + total);
    }
}

function update_order_subtotal(value){
    if($('#subtotal_price').html()){
        $('#subtotal_price').html('$' + value);
    }
}

function update_order_total(value){
    if($('#total_price').html()){
        $('#total_price').html('$' + value);
    }
}

function update_subtotal_menu(value){
    if($('#subtotal_span').html()){
        $('#subtotal_span').html('$' + value);
    }
}

function update_menu_cart(){
    $.get('/frontend/default/updatecarmenu',function(data){
        $('#cart_list').html(data);
    });
}


function add2Cart(productid){
    var color = '';
    if($('.colors>select').length != 0 && $('.colors>select option:selected').attr('value')){
        color = $('.colors>select option:selected').attr('value');
    }
    else if($('.colors>select').length != 0 && !$('.colors>select option:selected').attr('value')){
        color = ''
    }
    else{
        color = 'no_color';
    }

    var size = '';

    if($('.sizes>ul>li').length != 0 && $('.sizes>ul>li>span.selected').attr('id')){
        size = $('.sizes>ul>li>span.selected').attr('id');
    }
    if($('.sizes>ul>li').length != 0 && !$('.sizes>ul>li>span.selected').attr('id')){
        size = '';
    }
    else{
        size = 'no_size';
    }

    $.post('/frontend/default/add2cart',{
        'product_id':productid,
        'color_id': color,
        'size_id': size,
        'quantity': $('.amount>input').get(0).value
    },function(data){
        if(color && size){
            if(data['success']){
                $('.shop-cart>p>strong').html(data['count'] + ' item');
                $('.shop-cart>p>span').html(data['rtotal']);
                show_add_message(data['message']);
            } else
                show_error_message('Sorry, We have had an error, please fill your data again');

            if(data['total'] > 0){
                enable_up_btns();
            }
            else{
                disable_up_btns();
            }

            update_subtotal_menu(data['total']);

            update_menu_cart();
        }
        else{
            show_error_message('You should to select one size and color');
        }

    },"json");
}

function removeFromCart(productid){
    if (confirm('Are you sure you want to delete this item?')) {
        $.post('/frontend/default/removeproductcart',{
            'product_id':productid
        },function(data){
            if(data['success']){
                $('.shop-cart>p>strong').html(data['count'] + ' item');
                $('.shop-cart>p>span').html(data['rtotal']);
            }

            show_add_message(data['message']);

            update_order_subtotal(data['total']);

            update_order_total(data['rtotal']);

            update_subtotal_menu(data['total']);

            update_menu_cart();

            if(data['total'] > 0){
                enable_up_btns();
            }
            else{
                disable_up_btns();
            }

            if(data['count'] > 0){
                $('#tr'+productid).remove();
                $('#div'+productid).remove();
            }
            else{
                $('#div_cart_list').html('<div style="font-weight: bold; text-align: center; margin-top: 30px;"><h3>Your cart is empty.</h3></div>');
            }

        },"json");
    }
}


function updateqtyCart(productid,qty_elem){
    $.post('/frontend/default/updateqtyproductcart',{
        'product_id':productid,
        'qty':$('#' + qty_elem).get(0).value
    },function(data){
        if(data['success']){
            $('.shop-cart>p>strong').html(data['count'] + ' item');
            $('.shop-cart>p>span').html(data['rtotal']);
        }

        show_add_message(data['message']);

        update_row_qty(data['row_total'],productid);

        update_order_subtotal(data['total']);

        update_order_total(data['rtotal']);

        update_subtotal_menu(data['total']);

        update_menu_cart();

        if(data['total'] > 0){
            enable_up_btns();
        }
        else{
            disable_up_btns();
        }

    },"json");
}

function updateShiping(){
    $.post('/frontend/default/updateordertotal',{
        'shipping_id': $('#shipping_select option:selected').attr('id')
    },function(data){
        if(data['success']){
            $('.shop-cart>p>strong').html(data['count'] + ' item');
            $('.shop-cart>p>span').html(data['rtotal']);
        }

        update_order_total(data['rtotal']);

        update_subtotal_menu(data['total']);

        update_menu_cart();
    }, "json");
}


function updateShippingRealOrder(order_id){
    $.post('/frontend/default/updateordertotal',{
        'shipping_id': $('#shipping_select option:selected').attr('id'),
        'order_id': order_id
    },function(data){
        if(data['success']){
            $('.shop-cart>p>strong').html(data['count'] + ' item');
            $('.shop-cart>p>span').html(data['rtotal']);
        }

        update_order_total(data['rtotal']);

        update_order_subtotal(data['total']);
    }, "json");

}

function updateCart(){
    $.get('/frontend/default/updatecart',function(data){
        if(data['success']){
            $('.shop-cart>p>strong').html(data['count'] + ' item');
            $('.shop-cart>p>span').html(data['rtotal']);
        }
    },"json");

    update_menu_cart();

    setTimeout("updateCart()", 20000);
}


$(document).ready(function(){
    updateCart();
});