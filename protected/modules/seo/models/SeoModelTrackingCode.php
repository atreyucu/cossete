<?php

Yii::import('seo.models._base.BaseSeoModelTrackingCode');

class SeoModelTrackingCode extends BaseSeoModelTrackingCode
{
    /**
     * @param string $className
     * @return SeoModelTrackingCode
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}