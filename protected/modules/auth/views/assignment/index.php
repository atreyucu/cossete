<?php
/* @var $this AssignmentController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs = array(
    Yii::t('AuthModule.main', 'Assignments'),
);

/*$atras = app()->request->urlReferrer?array(
    array(
        'icon' => 'arrow-left',
        'label'=>t('Atras'),
        'url'=>app()->request->urlReferrer,
    ),
):array();

$this->beginWidget('ext.unicorn.widgets.IRBox', array(
    'title' => Yii::t('AuthModule.main', 'Assignments'),
    'headerIcon' => auth()->getAuthItemIcon('assignment'),
    //'headerButtons' => CMap::mergeArray($atras,$this->menu)
));*/

$this->widget(
    'application.extensions.bootstrap.widgets.TbGridView',
    array(
        'type' => 'striped hover',
        'dataProvider' => $dataProvider,
        'emptyText' => Yii::t('AuthModule.main', 'No assignments found.'),
        'template' => "{items}\n{pager}",
        'columns' => array(
            array(
                'header' => Yii::t('AuthModule.main', 'User'),
                'class' => 'AuthAssignmentNameColumn',
            ),
            array(
                'header' => Yii::t('AuthModule.main', 'Assigned items'),
                'class' => 'AuthAssignmentItemsColumn',
            ),
            array(
                'class' => 'AuthAssignmentViewColumn',
            ),
        ),
    )
);


//$this->endWidget();

?>
