<?php
/* @var $this OperationController|TaskController|RoleController */
/* @var $model AuthItemForm */
/* @var $form TbActiveForm */

$this->breadcrumbs = array(
    $this->capitalize($this->getTypeText(true)) => array('index'),
    Yii::t('AuthModule.main', 'New {type}', array('{type}' => $this->getTypeText())),
);
?>

    <h1><?php echo Yii::t('AuthModule.main', 'New {type}', array('{type}' => $this->getTypeText())); ?></h1>

<?php $form = $this->beginWidget(
    'application.extensions.bootstrap.widgets.TbActiveForm',
    array(
        'type' => TbHtml::FORM_LAYOUT_HORIZONTAL,
    )
); ?>

<?php echo $form->hiddenField($model, 'type'); ?>
<?php echo $form->textFieldGroup($model, 'name'); ?>
<?php echo $form->textFieldGroup($model, 'description'); ?>

    <div class="form-actions">
        <a href='<?php echo app()->request->urlReferrer; ?>' class='btn btn-default'><span
                class='glyphicon glyphicon-arrow-left'></span><?php echo t('Back'); ?></a>
        <?php echo TbHtml::submitButton(
            Yii::t('AuthModule.main', 'Create'),
            array(
                'color' => TbHtml::BUTTON_COLOR_PRIMARY,
            )
        ); ?>


        <!--        --><?php /*echo TbHtml::linkButton(
            Yii::t('AuthModule.main', 'Cancel'),
            array(
                'color' => TbHtml::BUTTON_COLOR_LINK,
                'url' => array('index'),
                'class'=>'btn',
            )
        ); */
        ?>
    </div>

<?php $this->endWidget(); ?>