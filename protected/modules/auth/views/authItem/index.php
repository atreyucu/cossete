<?php
/* @var $this OperationController|TaskController|RoleController */
/* @var $dataProvider AuthItemDataProvider */

$this->breadcrumbs = array(
    $this->capitalize($this->getTypeText(true)),
);

/*$this->menu = array(
    array(
        'icon' => 'plus',
        'label'=>Yii::t('AuthModule.main', 'Add {type}', array('{type}' => $this->getTypeText())),
        'url'=>array('create'),
    ),
);*/

/*$atras = app()->request->urlReferrer?array(
    array(
        'icon' => 'arrow-left',
        'label'=>t('Atras'),
        'url'=>app()->request->urlReferrer,
    ),
):array();*/

/*$this->beginWidget('ext.unicorn.widgets.IRBox', array(
    'title' => $this->capitalize($this->getTypeText(true)),
    'headerIcon' => auth()->getAuthItemIcon($this->type),
    //'headerButtons' => CMap::mergeArray($atras,$this->menu)
));*/

$this->widget(
    'application.extensions.bootstrap.widgets.TbGridView',
    array(
        'type' => 'striped hover',
        'dataProvider' => $dataProvider,
        'emptyText' => Yii::t('AuthModule.main', 'No {type} found.', array('{type}' => $this->getTypeText(true))),
        'template' => "{items}\n{pager}",
        'columns' => array(
            array(
                'name' => 'name',
                'type' => 'raw',
                'header' => Yii::t('AuthModule.main', 'System name'),
                'htmlOptions' => array('class' => 'item-name-column'),
                'value' => "CHtml::link(\$data->name, array('view', 'name'=>\$data->name))",
            ),
            array(
                'name' => 'description',
                'header' => Yii::t('AuthModule.main', 'Description'),
                'htmlOptions' => array('class' => 'item-description-column'),
            ),
            array(
                'class' => 'application.extensions.bootstrap.widgets.TbButtonColumn',
                'viewButtonLabel' => Yii::t('AuthModule.main', 'View'),
                'viewButtonUrl' => "Yii::app()->controller->createUrl('view', array('name'=>\$data->name))",
                'updateButtonLabel' => Yii::t('AuthModule.main', 'Edit'),
                'updateButtonUrl' => "Yii::app()->controller->createUrl('update', array('name'=>\$data->name))",
                'deleteButtonLabel' => Yii::t('AuthModule.main', 'Delete'),
                'deleteButtonUrl' => "Yii::app()->controller->createUrl('delete', array('name'=>\$data->name))",
                'deleteConfirmation' => Yii::t('AuthModule.main', 'Are you sure you want to delete this item?'),
            ),
        ),
    )
);


//$this->endWidget();
?>

<div class="form-actions">
    <?php echo CHtml::link(t(TbHtml::icon('glyphicon glyphicon-plus') . 'Add item'), array('create'), array('class' => 'btn btn-default')); ?>
</div>

