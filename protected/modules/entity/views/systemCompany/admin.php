<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
?>

<?php

$this->title = $model->adminNames[3];
$this->breadcrumbs = array(
    $model->adminNames[3],
);

?>

<?php $this->widget('application.extensions.bootstrap.widgets.TbGridView', array(
    'id' => 'system-company-grid',
    'dataProvider' => $model->search(),
//	'filter' => $model,
    'columns' => array(
        array(
            'class' => 'application.extensions.bootstrap.widgets.TbEditableColumn',
            'name' => 'name',
            'sortable' => true,
            'editable' => array(
                'type' => 'text',
                'url' => array('/ycm/model/updateAttribute', 'model' => get_class($model)),
                'placement' => 'right',
                'inputclass' => 'span3',
                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
            )
        ),
        array(
            'class' => 'application.extensions.bootstrap.widgets.TbEditableColumn',
            'name' => 'logo_image_alt',
            'sortable' => true,
            'editable' => array(
                'type' => 'text',
                'url' => array('/ycm/model/updateAttribute', 'model' => get_class($model)),
                'placement' => 'right',
                'inputclass' => 'span3',
                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
            )
        ),
        array(
            'class' => 'application.extensions.bootstrap.widgets.TbEditableColumn',
            'name' => 'description',
            'sortable' => true,
            'editable' => array(
                'type' => 'textarea',
                'url' => array('/ycm/model/updateAttribute', 'model' => get_class($model)),
                'placement' => 'right',
                'inputclass' => 'span3',
                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
            )
        ),
        array(
            'class' => 'application.extensions.bootstrap.widgets.TbEditableColumn',
            'name' => 'mision',
            'sortable' => true,
            'editable' => array(
                'type' => 'textarea',
                'url' => array('/ycm/model/updateAttribute', 'model' => get_class($model)),
                'placement' => 'right',
                'inputclass' => 'span3',
                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
            )
        ),
        array(
            'class' => 'application.extensions.bootstrap.widgets.TbEditableColumn',
            'name' => 'vision',
            'sortable' => true,
            'editable' => array(
                'type' => 'textarea',
                'url' => array('/ycm/model/updateAttribute', 'model' => get_class($model)),
                'placement' => 'right',
                'inputclass' => 'span3',
                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
            )
        ),
        array(
            'class' => 'application.extensions.bootstrap.widgets.TbButtonColumn',
            'buttons' => array('delete' => array('visible' => (user()->isAdmin) ? 'true' : 'false')),
            'htmlOptions' => array('class' => 'action-column'),
            'headerHtmlOptions' => array('class' => 'action-column'),
            'footerHtmlOptions' => array('class' => 'action-column'),
            'deleteConfirmation' => Yii::t('admin', 'Are you sure want to delete this item'),
            'header' => Yii::t('admin', 'Actions'),
        ),
    ),
)); ?>

<div class="form-actions">
    <?php echo CHtml::link(t(TbHtml::icon('glyphicon glyphicon-plus') . 'Add item'), array('create'), array('class' => 'btn btn-default')); ?></div>

<div id="success" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p><?php echo Yii::t('msg','The data was save with success'); ?></p>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>