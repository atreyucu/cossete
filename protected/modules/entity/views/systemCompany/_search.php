<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
?>
<?php $form = $this->beginWidget('application.extensions.bootstrap.widgets.TbActiveForm', array(
    'action' => Yii::app()->createUrl($this->route),
    'method' => 'get',
)); ?>

<?php echo $form->textFieldGroup($model, 'id', array(
        'maxlength' => 50,
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-5',
        ),
        'hint' => Yii::t('Hint',  'Please, insert id'),
        'append' => 'Text'
    )
); ?>
<?php echo $form->textFieldGroup($model, 'name', array(
        'maxlength' => 255,
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-5',
        ),
        'hint' => Yii::t('Hint',  'Please, insert name'),
        'append' => 'Text'
    )
); ?>
<?php echo $form->fileFieldGroup($model, 'recipeImg', array(
    'wrapperHtmlOptions' => array(
        'class' => 'col-sm-5',
    ),
    'hint' => Yii::t('Hint',  'Please, select the picture')
));
echo $form->textFieldGroup($model, 'logo_image_alt',
    array(
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-5',
        ),
        'hint' => Yii::t('Hint',  'Please, insert logo_image_alt'),
        'append' => 'Text',
    )
); ?>
<?php echo $form->ckEditorGroup($model, 'description',
    array(
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-5',
        ),
        'widgetOptions' => array(
            'editorOptions' => array(
                'fullpage' => 'js:true',
                //'width' => '640',
                //'resize_maxWidth' => '640',
                '//resize_minWidth' => '320'
            )
        )
    )
); ?>
<?php echo $form->redactorGroup($model, 'mision',
    array(
        'widgetOptions' => array(
            'editorOptions' => array(
                'class' => 'span4',
                'rows' => 10,
                'options' => array('plugins' => array('clips', 'fontfamily', 'fullscreen'), 'lang' => app()->getLanguage())
            )
        )
    )
); ?>
<?php echo $form->redactorGroup($model, 'vision',
    array(
        'widgetOptions' => array(
            'editorOptions' => array(
                'class' => 'span4',
                'rows' => 10,
                'options' => array('plugins' => array('clips', 'fontfamily', 'fullscreen'), 'lang' => app()->getLanguage())
            )
        )
    )
); ?>
<?php echo $form->textFieldGroup($model, 'phone', array(
        'maxlength' => 125,
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-5',
        ),
        'hint' => Yii::t('Hint',  'Please, insert phone'),
        'append' => 'Text'
    )
); ?>
<?php echo $form->textFieldGroup($model, 'fax', array(
        'maxlength' => 125,
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-5',
        ),
        'hint' => Yii::t('Hint',  'Please, insert fax'),
        'append' => 'Text'
    )
); ?>
<?php echo $form->textFieldGroup($model, 'url', array(
        'maxlength' => 255,
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-5',
        ),
        'hint' => Yii::t('Hint',  'Please, insert url'),
        'append' => 'Text'
    )
); ?>
<?php echo $form->textFieldGroup($model, 'email', array(
        'maxlength' => 255,
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-5',
        ),
        'hint' => Yii::t('Hint',  'Please, insert email'),
        'append' => 'Text'
    )
); ?>
<?php echo $form->html5EditorGroup($model, 'full_adress',
    array(
        'widgetOptions' => array(
            'editorOptions' => array(
                'class' => 'span4',
                'rows' => 10,
                //'height' => '200',
                'options' => array('color' => true, 'lang' => app()->getLanguage())
            ),
        )
    )
); ?>
<?php echo $form->textFieldGroup($model, 'longitude',
    array(
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-5',
        ),
        'hint' => Yii::t('Hint',  'Please, insert longitude'),
        'append' => '.00'
    )
); ?>
<?php echo $form->textFieldGroup($model, 'latitude',
    array(
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-5',
        ),
        'hint' => Yii::t('Hint',  'Please, insert latitude'),
        'append' => '.00'
    )
); ?>
<?php echo $form->ckEditorGroup($model, 'terms_conditions',
    array(
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-5',
        ),
        'widgetOptions' => array(
            'editorOptions' => array(
                'fullpage' => 'js:true',
                //'width' => '640',
                //'resize_maxWidth' => '640',
                '//resize_minWidth' => '320'
            )
        )
    )
); ?>
<?php echo $form->textFieldGroup($model, 'shared_facebook', array(
        'maxlength' => 200,
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-5',
        ),
        'hint' => Yii::t('Hint',  'Please, insert shared_facebook'),
        'append' => 'Text'
    )
); ?>
<?php echo $form->textFieldGroup($model, 'shared_google', array(
        'maxlength' => 200,
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-5',
        ),
        'hint' => Yii::t('Hint',  'Please, insert shared_google'),
        'append' => 'Text'
    )
); ?>
<?php echo $form->textFieldGroup($model, 'shared_twitter', array(
        'maxlength' => 200,
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-5',
        ),
        'hint' => Yii::t('Hint',  'Please, insert shared_twitter'),
        'append' => 'Text'
    )
); ?>
<?php echo $form->textFieldGroup($model, 'shared_youtube', array(
        'maxlength' => 200,
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-5',
        ),
        'hint' => Yii::t('Hint',  'Please, insert shared_youtube'),
        'append' => 'Text'
    )
); ?>
<?php echo $form->redactorGroup($model, 'msg_newUser',
    array(
        'widgetOptions' => array(
            'editorOptions' => array(
                'class' => 'span4',
                'rows' => 10,
                'options' => array('plugins' => array('clips', 'fontfamily', 'fullscreen'), 'lang' => app()->getLanguage())
            )
        )
    )
); ?>
<?php echo $form->redactorGroup($model, 'msg_contactUs',
    array(
        'widgetOptions' => array(
            'editorOptions' => array(
                'class' => 'span4',
                'rows' => 10,
                'options' => array('plugins' => array('clips', 'fontfamily', 'fullscreen'), 'lang' => app()->getLanguage())
            )
        )
    )
); ?>
<?php echo $form->redactorGroup($model, 'msg_newsletter',
    array(
        'widgetOptions' => array(
            'editorOptions' => array(
                'class' => 'span4',
                'rows' => 10,
                'options' => array('plugins' => array('clips', 'fontfamily', 'fullscreen'), 'lang' => app()->getLanguage())
            )
        )
    )
); ?>

<div class="form-actions">
    <?php $this->widget('application.extensions.bootstrap.widgets.TbButton',
        array(
            'buttonType' => 'submit',
            'context' => 'success',
            'icon' => 'glyphicon glyphicon-saved',
            'label' => Yii::t('Label', 'Buscar ' . mod('ycm')->getPluralName($model))
        ));
    ?></div>

<?php $this->endWidget(); ?>
