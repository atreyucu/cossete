<?php

class DefaultController extends Controller
{
    public function actions()
    {
        return array(
            'addHomeGallRow' => 'backend.controllers.actions.AddHomeGallRowAction',
            'addTrendRow' => 'backend.controllers.actions.AddTrendRowAction',
            'refreshColors' => 'backend.controllers.actions.RefreshColorByCategoryAction',
            'refreshSizes' => 'backend.controllers.actions.RefreshSizeByCategoryAction',
        );
    }
}