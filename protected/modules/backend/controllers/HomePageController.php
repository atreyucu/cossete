<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
?>

<?php

class HomePageController extends GxController
{

    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    public function accessRules()
    {
        return array(
            array('allow',
                'actions' => array('index', 'view', 'updateAttribute'),
                'users' => array('@'),
            ),
            /*array('allow',
	            'actions'=>array('index', 'view','update','admin'),
	            'users'=>user()->getPermissionbyRoles(array('Level-1','Level-2','Level-3')),
	        ),*/
            array('allow',
                'actions' => array('minicreate', 'create', 'update', 'admin', 'delete'),
                'users' => mod('user')->getAdmins(),
            ),
            array('deny',
                'users' => array('*'),
            ),
        );
    }

    public function actionView($id)
    {
        $model = $this->loadModel($id, 'HomePage');

        $home_banner_gallery_manager = HomeBannerGalleryManager::load($model);
        $trend_manager = TrendManager::load($model);

        $this->render('view', array(
            'model' => $model,
            'banner_manager' => $home_banner_gallery_manager,
            'trend_manager' => $trend_manager,
        ));
    }

    public function actionCreate()
    {
        $model = new HomePage;


        $this->performAjaxValidation($model, 'home-page-form');

        if ($model->count() >= 1)
            $this->redirect(array('admin'));
        if (isset($_POST['HomePage'])) {
            $model->setAttributes($_POST['HomePage']);
            if ($model->save()) {

                $galls = (array)$_POST['HomeBannerGallery'];

                $gall_upload_dir = Yii::getPathOfAlias('webroot').DIRECTORY_SEPARATOR.'images/HomeBannerGallery/';

                foreach($galls as $key=>$gall) {
                    $temp_gall = new HomeBannerGallery;
                    $temp_gall->setAttributes($gall);
                    $temp_gall->home_page = $model->id;
                    $temp_gall->save();
                    $temp_extension = explode('.', $_FILES['HomeBannerGallery']["name"][$key]['recipeImg1']);
                    $img_path = $gall_upload_dir . 'img1_' . $temp_gall->id . '_normal.' . $temp_extension[1];

                    move_uploaded_file($_FILES['HomeBannerGallery']["tmp_name"][$key]['recipeImg1'],$img_path);

                    Yii::import('application.extensions.image.Image');

                    $image = new Image($img_path, array('driver' => 'GD', 'params' => array()));
                    $image->save();
                }


                $trends = (array)$_POST['Trend'];

                $trend_upload_dir = Yii::getPathOfAlias('webroot').DIRECTORY_SEPARATOR.'images/Trend/';

                foreach($trends as $key=>$trend) {
                    $temp_trend = new Trend;
                    $temp_trend->setAttributes($trend);
                    $temp_trend->home_page = $model->id;
                    $temp_trend->save();
                    $temp_extension = explode('.', $_FILES['Trend']["name"][$key]['recipeImg1']);
                    $img_path = $trend_upload_dir . 'img1_' . $temp_trend->id . '_normal.' . $temp_extension[1];

                    move_uploaded_file($_FILES['Trend']["tmp_name"][$key]['recipeImg1'],$img_path);

                    Yii::import('application.extensions.image.Image');

                    $image = new Image($img_path, array('driver' => 'GD', 'params' => array()));
                    $image->save();
                }

                if (Yii::app()->getRequest()->getIsAjaxRequest())
                    Yii::app()->end();
                else {
                    Yii::app()->user->setFlash('success', Yii::t('admin', 'Success, item was saved.'));
                    if (Yii::app()->request->getParam('id')) {
                        if (Yii::app()->request->getParam('action') == 'create')
                            $this->redirect(array('/backend/' . Yii::app()->request->getParam('controller') . '/' . Yii::app()->request->getParam('action')));
                        else
                            $this->redirect(array('/backend/' . Yii::app()->request->getParam('controller') . '/' . Yii::app()->request->getParam('action'), 'id' => Yii::app()->request->getParam('id')));
                    } else
                        $this->redirect(array('admin'));
                }
            } else {
                Yii::app()->user->setFlash('error', Yii::t('admin', 'Error, had been an error saving item.'));
            }
        }

        $home_banner_gallery_manager = HomeBannerGalleryManager::load($model);
        $trend_manager = TrendManager::load($model);

        $this->render('create', array('model' => $model, 'banner_manager'=> $home_banner_gallery_manager, 'trend_manager' => $trend_manager));
    }

    public function actionUpdate($id)
    {
        $model = $this->loadModel($id, 'HomePage');

        $this->performAjaxValidation($model, 'home-page-form');

        if (isset($_POST['HomePage'])) {
            $model->setAttributes($_POST['HomePage']);
            if ($model->save()) {

                $galls = (array)$_POST['HomeBannerGallery'];

                $gall_upload_dir = Yii::getPathOfAlias('webroot').DIRECTORY_SEPARATOR.'images/HomeBannerGallery/';


                $del_gall_criteria = new CDbCriteria();
                $del_gall_criteria->addNotInCondition('id',array_keys($galls));
                $del_gall_criteria->compare('home_page',$model->id);

                HomeBannerGallery::model()->deleteAll($del_gall_criteria);

                foreach($galls as $key=>$gall){
                    if(substr_count($key,'newgall')){
                        $temp_gall = new HomeBannerGallery;
                        $temp_gall->setAttributes($gall);
                        $temp_gall->home_page = $model->id;
                        $temp_gall->save();
                        $temp_extension = explode('.',$_FILES['HomeBannerGallery']["name"][$key]['recipeImg1']);
                        $img_path = $gall_upload_dir.'img1_'.$temp_gall->id.'_normal.'.$temp_extension[1];

                        move_uploaded_file($_FILES['HomeBannerGallery']["tmp_name"][$key]['recipeImg1'],$img_path);

                        Yii::import('application.extensions.image.Image');

                        $image = new Image($img_path,array('driver' => 'GD', 'params' => array()));
                        $image->save();
                    }
                    else{
                        $gall_obj = HomeBannerGallery::model()->findByPk($key);
                        $gall_obj->setAttributes($gall);
                        $gall_obj->save();

                        if($_FILES['HomeBannerGallery']["tmp_name"][$key]['recipeImg1']){
                            $temp_extension = explode('.',$_FILES['HomeBannerGallery']["name"][$key]['recipeImg1']);
                            $img_path = $gall_upload_dir.'img1_'.$gall_obj->id.'_normal.'.$temp_extension[1];


                            move_uploaded_file($_FILES['HomeBannerGallery']["tmp_name"][$key]['recipeImg1'],$img_path);

                            Yii::import('application.extensions.image.Image');

                            $image = new Image($img_path,array('driver' => 'GD', 'params' => array()));
                            $image->save();
                        }
                    }
                }


                $trends = (array)$_POST['Trend'];

                $trend_upload_dir = Yii::getPathOfAlias('webroot').DIRECTORY_SEPARATOR.'images/Trend/';

                $del_trend_criteria = new CDbCriteria();
                $del_trend_criteria->addNotInCondition('id',array_keys($trends));
                $del_trend_criteria->compare('home_page',$model->id);

                Trend::model()->deleteAll($del_trend_criteria);

                foreach($trends as $key=>$trend){
                    if(substr_count($key,'newtrend')){
                        $temp_trend = new Trend;
                        $temp_trend->setAttributes($trend);
                        $temp_trend->tag_id = $trend['tag_id'];
                        $temp_trend->home_page = $model->id;
                        $temp_trend->save();

                        $temp_extension = explode('.',$_FILES['Trend']["name"][$key]['recipeImg1']);

                        $img_path = $trend_upload_dir.'img1_'.$temp_trend->id.'_normal.'.$temp_extension[1];

                        move_uploaded_file($_FILES['Trend']["tmp_name"][$key]['recipeImg1'],$img_path);

                        Yii::import('application.extensions.image.Image');

                        $image = new Image($img_path,array('driver' => 'GD', 'params' => array()));
                        $image->save();
                    }
                    else{
                        $trend_obj = Trend::model()->findByPk($key);
                        $trend_obj->setAttributes($trend);
                        $trend_obj->tag_id = $trend['tag_id'];
                        $trend_obj->save();

                        if($_FILES['Trend']["tmp_name"][$key]['recipeImg1']){
                            $temp_extension = explode('.',$_FILES['Trend']["name"][$key]['recipeImg1']);
                            $img_path = $trend_upload_dir.'img1_'.$trend_obj->id.'_normal.'.$temp_extension[1];


                            move_uploaded_file($_FILES['Trend']["tmp_name"][$key]['recipeImg1'],$img_path);

                            Yii::import('application.extensions.image.Image');

                            $image = new Image($img_path,array('driver' => 'GD', 'params' => array()));
                            $image->save();
                        }
                    }
                }

                Yii::app()->user->setFlash('success', Yii::t('admin', 'Success, the changes were saved.'));
                $this->redirect(array('admin'));
            } else {
                Yii::app()->user->setFlash('error', Yii::t('admin', 'Error, had been an error saving the item.'));
            }
        }

        $home_banner_gallery_manager = HomeBannerGalleryManager::load($model);
        $trend_manager = TrendManager::load($model);

        $this->render('update', array(
            'model' => $model,
            'banner_manager'=> $home_banner_gallery_manager,
            'trend_manager'=> $trend_manager
        ));
    }

    public function actionDelete($id)
    {
        if (isset($id)) {
            if ($this->loadModel($id, "HomePage")->delete()) {
                Yii::app()->user->setFlash('success', Yii::t('admin', 'Success, the item was deleted.'));
                $this->redirect(array('admin'));
            } else {
                Yii::app()->user->setFlash('error', Yii::t('admin', 'Error, exist a native error to delete the item: ' . $id . ', to resolve this problem, please contact with the database administrator.'));
            }
        } else {
            Yii::app()->user->setFlash('error', Yii::t('admin', 'Error, the item ' . $id . ' is not defined.'));
        }
    }

    public function actionAdmin()
    {
        $model = new HomePage('search');
        $model->unsetAttributes();

        if (isset($_GET['HomePage']))
            $model->setAttributes($_GET['HomePage']);

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    public function actionUpdateAttribute($model)
    {
        if (app()->request->isAjaxRequest && app()->request->isPostRequest) {
            Yii::import("bootstrap.widgets.TbEditableSaver");
            $editableSaver = new TbEditableSaver($model);
            $editableSaver->update();
            app()->end();
        }
    }

}