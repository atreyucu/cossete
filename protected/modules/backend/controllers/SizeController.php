

<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
 ?>

<?php

class SizeController extends GxController {

public function filters() {
	return array(
			'accessControl', 
			);
}

public function accessRules() {
	return array(
			array('allow', 
				'actions'=>array('index', 'view','updateAttribute'),
				'users'=>array('@'),
				),
            /*array('allow',
	            'actions'=>array('index', 'view','update','admin'),
	            'users'=>user()->getPermissionbyRoles(array('Level-1','Level-2','Level-3')),
	        ),*/
			array('allow', 
				'actions'=>array('minicreate', 'create', 'update', 'admin', 'delete'),
				'users'=>mod('user')->getAdmins(),
				),
			array('deny', 
				'users'=>array('*'),
				),
			);
}

public function actionView($id) {
    $model = $this->loadModel($id, 'Size');
    $this->render('view', array(
    'model' => $model,
    ));
}

public function actionCreate() {
    $model = new Size;


    $this->performAjaxValidation($model, 'size-form');

    if (isset($_POST['Size'])) {
        $model->setAttributes($_POST['Size']);
        if ($model->save()) {

            $cats = (array)$_POST['SizeCategory'];

            foreach($cats as $cat){
                $temp_cat = new SizeCategory();
                $temp_cat->category = $cat;
                $temp_cat->size = $model->id;
                $temp_cat->save();
            }

            if (Yii::app()->getRequest()->getIsAjaxRequest())
                Yii::app()->end();
            else{
                Yii::app()->user->setFlash('success',Yii::t('admin','Success, item was saved.'));
                if (Yii::app()->request->getParam('id'))
                {
                    if(Yii::app()->request->getParam('action')=='create')
                    $this->redirect(array('/backend/' . Yii::app()->request->getParam('controller') . '/' . Yii::app()->request->getParam('action')));
                else
                    $this->redirect(array('/backend/' . Yii::app()->request->getParam('controller') . '/' . Yii::app()->request->getParam('action'), 'id' => Yii::app()->request->getParam('id')));
                }
                else
                    $this->redirect(array('admin'));
            }
        }
        else {
            Yii::app()->user->setFlash('error',Yii::t('admin','Error, had been an error saving item.'));
        }
    }

    $cat_arr = array();

    $cat_criteria = new CDbCriteria();
    $cat_criteria->compare('is_active',1);

    foreach(ProductCategory::model()->findAll($cat_criteria) as $cat){
        $cat_arr[$cat->id] = array('name'=>$cat->name);
    }


    $check_all_cats = false;

    $this->render('create', array( 'model' => $model,
        'cats' => $cat_arr,
        'ccats' => array(),
        'check_all_cats' => $check_all_cats
    ));
}

public function actionUpdate($id) {
    $model = $this->loadModel($id, 'Size');

    $this->performAjaxValidation($model, 'size-form');

    if (isset($_POST['Size'])) {
        $model->setAttributes($_POST['Size']);
        if ($model->save()) {
            $cats = (array)$_POST['SizeCategory'];

            $del_criteria = new CDbCriteria();
            $del_criteria->compare('size',$model->id);

            SizeCategory::model()->deleteAll($del_criteria);

            foreach($cats as $cat){
                $temp_cat = new SizeCategory();
                $temp_cat->category = $cat;
                $temp_cat->size = $model->id;
                $temp_cat->save();
            }

            Yii::app()->user->setFlash('success',Yii::t('admin','Success, the changes were saved.'));
            $this->redirect(array('admin'));
        }
        else {
            Yii::app()->user->setFlash('error',Yii::t('admin','Error, had been an error saving the item.'));
        }
    }
    $cat_arr = array();

    $cat_criteria = new CDbCriteria();
    $cat_criteria->compare('is_active',1);

    foreach(ProductCategory::model()->findAll($cat_criteria) as $cat){
        $cat_arr[$cat->id] = array('name'=>$cat->name);
    }

    $ccat_arr = array();

    $ccat_criteria = new CDbCriteria();
    $ccat_criteria->compare('size',$model->id);

    $cat_index = 1;

    foreach(SizeCategory::model()->findAll($ccat_criteria) as $cat){
        $ccat_arr[$cat_index++] = $cat->category;
    }


    $check_all_cats = count($cat_arr) == count($ccat_arr);


    $this->render('update', array(
        'model' => $model,
        'cats' => $cat_arr,
        'ccats' => $ccat_arr,
        'check_all_cats' => $check_all_cats
    ));
}

public function actionDelete($id)
{
    if(isset($id)){
        if($this->loadModel($id,"Size")->delete()){
            Yii::app()->user->setFlash('success',Yii::t('admin','Success, the item was deleted.'));
            $this->redirect(array('admin'));
        }
        else{
            Yii::app()->user->setFlash('error',Yii::t('admin','Error, exist a native error to delete the item: '.$id.', to resolve this problem, please contact with the database administrator.'));
        }
    }
    else {
        Yii::app()->user->setFlash('error',Yii::t('admin','Error, the item '.$id.' is not defined.'));
    }
}

public function actionAdmin() {
    $model = new Size('search');
    $model->unsetAttributes();

    if (isset($_GET['Size']))
        $model->setAttributes($_GET['Size']);

    $this->render('admin', array(
        'model' => $model,
    ));
}

public function actionUpdateAttribute($model)
{
    if (app()->request->isAjaxRequest && app()->request->isPostRequest) {
        Yii::import("bootstrap.widgets.TbEditableSaver");
        $editableSaver = new TbEditableSaver($model);
        $editableSaver->update();
        app()->end();
    }
}

}