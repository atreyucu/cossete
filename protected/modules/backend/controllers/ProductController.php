

<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
 ?>

<?php

class ProductController extends GxController {

public function filters() {
	return array(
			'accessControl', 
			);
}

public function accessRules() {
	return array(
			array('allow', 
				'actions'=>array('index', 'view','updateAttribute'),
				'users'=>array('@'),
				),
            /*array('allow',
	            'actions'=>array('index', 'view','update','admin'),
	            'users'=>user()->getPermissionbyRoles(array('Level-1','Level-2','Level-3')),
	        ),*/
			array('allow', 
				'actions'=>array('minicreate', 'create', 'update', 'admin', 'delete'),
				'users'=>mod('user')->getAdmins(),
				),
			array('deny', 
				'users'=>array('*'),
				),
			);
}

public function actionView($id) {
    $model = $this->loadModel($id, 'Product');

    $tags_arr_id = array();

    $product_tag_criteria = new CDbCriteria();
    $product_tag_criteria->compare('product',$model->id);

    $products_tags = ProductTag::model()->findAll($product_tag_criteria);

    foreach($products_tags as $tag){
        $tags_arr_id[] = $tag->tag;
    }


    $tags_arr = array();

    $tag_criteria = new CDbCriteria();
    $tag_criteria->compare('is_active',1);
    $tag_criteria->addInCondition('id',$tags_arr_id);

    foreach(Tag::model()->findAll($tag_criteria) as $tag){
        $tags_arr[$tag->id] = array('name'=>$tag->name);
    }

    $colors_arr_id = array();

    $product_color_criteria = new CDbCriteria();
    $product_color_criteria->compare('product',$model->id);

    $products_colors = ProductColor::model()->findAll($product_color_criteria);

    foreach($products_colors as $color){
        $colors_arr_id[] = $color->color;
    }

    $color_arr = array();

    $color_criteria = new CDbCriteria();
    $color_criteria->compare('is_active',1);
    $color_criteria->addInCondition('id',$colors_arr_id);

    foreach(Color::model()->findAll($color_criteria) as $color){
        $color_arr[$color->id] = array('name'=>$color->name);
    }

    $sizes_arr_id = array();

    $product_size_criteria = new CDbCriteria();
    $product_size_criteria->compare('product',$model->id);

    $products_sizes = ProductSize::model()->findAll($product_size_criteria);

    foreach($products_sizes as $size){
        $sizes_arr_id[] = $size->size;
    }

    $sizes_arr = array();

    $sizes_criteria = new CDbCriteria();
    $sizes_criteria->compare('is_active',1);
    $sizes_criteria->addInCondition('id',$sizes_arr_id);

    foreach(Size::model()->findAll($sizes_criteria) as $size){
        $sizes_arr[$size->id] = array('name'=>$size->name);
    }

    $this->render('view', array(
    'model' => $model,
     'tags' => $tags_arr,
     'colors' => $color_arr,
     'sizes' => $sizes_arr
    ));
}

public function actionCreate() {
    $model = new Product;


    $this->performAjaxValidation($model, 'product-form');

    if (isset($_POST['Product'])) {
        $model->setAttributes($_POST['Product']);
        if ($model->save()) {
            $tags = (array)$_POST['ProductTag'];

            foreach($tags as $tag){
                $temp_tag = new ProductTag();
                $temp_tag->tag = $tag;
                $temp_tag->product= $model->id;
                $temp_tag->save();
            }

            $colors = (array)$_POST['ProductColor'];

            foreach($colors as $color){
                $temp_col = new ProductColor();
                $temp_col->color = $color;
                $temp_col->product= $model->id;
                $temp_col->save();
            }

            $sizes = (array)$_POST['ProductSize'];

            foreach($sizes as $size){
                $temp_size = new ProductSize();
                $temp_size->size = $size;
                $temp_size->product= $model->id;
                $temp_size->save();
            }

            if (Yii::app()->getRequest()->getIsAjaxRequest())
                Yii::app()->end();
            else{
                Yii::app()->user->setFlash('success',Yii::t('admin','Success, item was saved.'));
                if (Yii::app()->request->getParam('id'))
                {
                    if(Yii::app()->request->getParam('action')=='create')
                    $this->redirect(array('/backend/' . Yii::app()->request->getParam('controller') . '/' . Yii::app()->request->getParam('action')));
                else
                    $this->redirect(array('/backend/' . Yii::app()->request->getParam('controller') . '/' . Yii::app()->request->getParam('action'), 'id' => Yii::app()->request->getParam('id')));
                }
                else
                    $this->redirect(array('admin'));
            }
        }
        else {
            Yii::app()->user->setFlash('error',Yii::t('admin','Error, had been an error saving item.'));
        }
    }

    $tags_arr = array();

    $tag_criteria = new CDbCriteria();
    $tag_criteria->compare('is_active',1);

    foreach(Tag::model()->findAll($tag_criteria) as $tag){
        $tags_arr[$tag->id] = array('name'=>$tag->name);
    }


    $color_arr = array();

    $color_criteria = new CDbCriteria();
    $color_criteria->compare('is_active',1);

    foreach(Color::model()->findAll($color_criteria) as $color){
        $color_arr[$color->id] = array('name'=>$color->name);
    }

    $sizes_arr = array();

    $sizes_criteria = new CDbCriteria();
    $sizes_criteria->compare('is_active',1);

    foreach(Size::model()->findAll($sizes_criteria) as $size){
        $sizes_arr[$size->id] = array('name'=>$size->name);
    }

    $check_all_tags = false;
    $check_all_colors = false;
    $check_all_sizes = false;

    $this->render('create', array( 'model' => $model,
        'tags' => $tags_arr,
        'sizes' => $sizes_arr,
        'colors' => $color_arr,
        'ctags' => array(),
        'csizes' => array(),
        'ccolors' => array(),
        'check_all_tags' => $check_all_tags,
        'check_all_colors' => $check_all_colors,
        'check_all_sizes' => $check_all_sizes
    ));
}

public function actionUpdate($id) {
    $model = $this->loadModel($id, 'Product');

    $this->performAjaxValidation($model, 'product-form');

    if (isset($_POST['Product'])) {
        $model->setAttributes($_POST['Product']);
        if ($model->save()) {
            $tags = (array)$_POST['ProductTag'];


            $del_criteria = new CDbCriteria();
            $del_criteria->compare('product',$model->id);

            ProductTag::model()->deleteAll($del_criteria);

            foreach($tags as $tag){
                $temp_tag = new ProductTag();
                $temp_tag->tag = $tag;
                $temp_tag->product= $model->id;
                $temp_tag->save();
            }

            $colors = (array)$_POST['ProductColor'];

            $del_criteria = new CDbCriteria();
            $del_criteria->compare('product',$model->id);

            ProductColor::model()->deleteAll($del_criteria);

            foreach($colors as $color){
                $temp_col = new ProductColor();
                $temp_col->color = $color;
                $temp_col->product= $model->id;
                $temp_col->save();
            }

            $sizes = (array)$_POST['ProductSize'];

            $del_criteria = new CDbCriteria();
            $del_criteria->compare('product',$model->id);

            ProductSize::model()->deleteAll($del_criteria);

            foreach($sizes as $size){
                $temp_size = new ProductSize();
                $temp_size->size = $size;
                $temp_size->product= $model->id;
                $temp_size->save();
            }

            Yii::app()->user->setFlash('success',Yii::t('admin','Success, the changes were saved.'));
            $this->redirect(array('admin'));
        }
        else {
            Yii::app()->user->setFlash('error',Yii::t('admin','Error, had been an error saving the item.'));
        }
    }

    $tags_arr = array();

    $tag_criteria = new CDbCriteria();
    $tag_criteria->compare('is_active',1);

    foreach(Tag::model()->findAll($tag_criteria) as $tag){
        $tags_arr[$tag->id] = array('name'=>$tag->name);
    }


    $ctags_arr = array();

    $ctag_criteria = new CDbCriteria();
    $ctag_criteria->compare('product',$model->id);

    $tag_index = 1;

    foreach(ProductTag::model()->findAll($ctag_criteria) as $tag){
        $ctags_arr[$tag_index++] = $tag->tag;
    }


    $color_arr = array();

    $ids_criteria = new CDbCriteria();
    $ids_criteria->compare('category',$model->category);

    $color_cat_list = ColorCategory::model()->findAll($ids_criteria);

    $color_ids = array();

    foreach($color_cat_list as $color_cat){
        $color_ids[] = $color_cat->color;
    }

    $color_criteria = new CDbCriteria();
    $color_criteria->addInCondition('id',$color_ids);
    $color_criteria->compare('is_active',1);

    foreach(Color::model()->findAll($color_criteria) as $color){
        $color_arr[$color->id] = array('name'=>$color->name);
    }

    $ccolor_arr = array();

    $ccolor_criteria = new CDbCriteria();
    $ccolor_criteria->compare('product',$model->id);

    $color_index = 1;

    foreach(ProductColor::model()->findAll($ccolor_criteria) as $color){
        $ccolor_arr[$color_index++] = $color->color;
    }

    $sizes_arr = array();

    $ids_criteria = new CDbCriteria();
    $ids_criteria->compare('category',$model->category);

    $size_cat_list = SizeCategory::model()->findAll($ids_criteria);

    $size_ids = array();

    foreach($size_cat_list as $size_cat){
        $size_ids[] = $size_cat->size;
    }

    $sizes_criteria = new CDbCriteria();
    $sizes_criteria->addInCondition('id',$size_ids);
    $sizes_criteria->compare('is_active',1);

    foreach(Size::model()->findAll($sizes_criteria) as $size){
        $sizes_arr[$size->id] = array('name'=>$size->name);
    }

    $csize_arr = array();

    $csize_criteria = new CDbCriteria();
    $csize_criteria->compare('product',$model->id);

    $size_index = 1;

    foreach(ProductSize::model()->findAll($csize_criteria) as $size){
        $csize_arr[$size_index++] = $size->size;
    }

    $check_all_tags = count($tags_arr) == count($ctags_arr);
    $check_all_colors = count($color_arr) == count($ccolor_arr);
    $check_all_sizes = count($sizes_arr) == count($csize_arr);


    $this->render('update', array(
        'model' => $model,
        'tags' => $tags_arr,
        'sizes' => $sizes_arr,
        'colors' => $color_arr,
        'ctags' => $ctags_arr,
        'csizes' => $csize_arr,
        'ccolors' => $ccolor_arr,
        'check_all_tags' => $check_all_tags,
        'check_all_colors' => $check_all_colors,
        'check_all_sizes' => $check_all_sizes
    ));
}

public function actionDelete($id)
{
    if(isset($id)){
        if($this->loadModel($id,"Product")->delete()){
            Yii::app()->user->setFlash('success',Yii::t('admin','Success, the item was deleted.'));
            $this->redirect(array('admin'));
        }
        else{
            Yii::app()->user->setFlash('error',Yii::t('admin','Error, exist a native error to delete the item: '.$id.', to resolve this problem, please contact with the database administrator.'));
        }
    }
    else {
        Yii::app()->user->setFlash('error',Yii::t('admin','Error, the item '.$id.' is not defined.'));
    }
}

public function actionAdmin() {
    $model = new Product('search');
    $model->unsetAttributes();

    if (isset($_GET['Product']))
        $model->setAttributes($_GET['Product']);

    $this->render('admin', array(
        'model' => $model,
    ));
}

public function actionUpdateAttribute($model)
{
    if (app()->request->isAjaxRequest && app()->request->isPostRequest) {
        Yii::import("bootstrap.widgets.TbEditableSaver");
        $editableSaver = new TbEditableSaver($model);
        $editableSaver->update();
        app()->end();
    }
}

}