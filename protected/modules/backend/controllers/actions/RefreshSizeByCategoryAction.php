<?php

class RefreshSizeByCategoryAction extends CAction
{
    public function run()
    {
        if(Yii::app()->request->isAjaxRequest){
            $cat = $_GET['cat'];
            $product = $_GET['prod'];

            $select_criteria = new CDbCriteria();
            $select_criteria->compare('category',$cat);

            $size_cat_list = SizeCategory::model()->findAll($select_criteria);

            $sizes_id = array();

            foreach($size_cat_list as $size_cat){
                $sizes_id[] = $size_cat->size;
            }

            $select_criteria = new CDbCriteria();
            $select_criteria->addInCondition('id',$sizes_id);

            $sizes = array();

            foreach(Size::model()->findAll($select_criteria) as $size){
                $sizes[$size->id] = array('name'=>$size->name);
            }

            $csize_arr = array();

            $csize_criteria = new CDbCriteria();
            $csize_criteria->compare('product',$product);

            $size_index = 1;

            foreach(ProductSize::model()->findAll($csize_criteria) as $size){
                $csize_arr[$size_index++] = $size->size;
            }

            return $this->controller->renderPartial('backend.views.product._sizeForm', array('sizes' => $sizes, 'csizes'=>$csize_arr));
        }
        else{
            $this->controller->renderText('');
        }
    }
} 