<?php

class RefreshColorByCategoryAction extends CAction
{
    public function run()
    {
        if(Yii::app()->request->isAjaxRequest){
            $cat = $_GET['cat'];
            $product = $_GET['prod'];

            $select_criteria = new CDbCriteria();
            $select_criteria->compare('category',$cat);

            $color_cat_list = ColorCategory::model()->findAll($select_criteria);

            $colors_id = array();

            foreach($color_cat_list as $col_cat){
                $colors_id[] = $col_cat->color;
            }

            $select_criteria = new CDbCriteria();
            $select_criteria->addInCondition('id',$colors_id);

            $colors = array();

            foreach(Color::model()->findAll($select_criteria) as $color){
                $colors[$color->id] = array('name'=>$color->name);
            }

            $ccolor_arr = array();

            $ccolor_criteria = new CDbCriteria();
            $ccolor_criteria->compare('product',$product);

            $color_index = 1;

            foreach(ProductColor::model()->findAll($ccolor_criteria) as $color){
                $ccolor_arr[$color_index++] = $color->color;
            }


            return $this->controller->renderPartial('backend.views.product._colorForm', array('colors' => $colors, 'ccolors'=>$ccolor_arr));
        }
        else{
            return $this->controller->renderText('');
        }
    }
} 