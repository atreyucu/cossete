<?php

class AddHomeGallRowAction extends CAction
{
    public function run()
    {
        Yii::import('bootstrap.widgets.TbActiveForm');
        $form = new TbActiveForm();
        $form->showErrors = false;
        $form->type = 'inline';
        $data = new HomeBannerGallery();
        return $this->controller->renderPartial('backend.views.homePage._galleryForm', array('form' => $form, 'data' => $data));
    }
} 