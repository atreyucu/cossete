<?php

class AddTrendRowAction extends CAction
{
    public function run()
    {
        Yii::import('bootstrap.widgets.TbActiveForm');
        $form = new TbActiveForm();
        $form->showErrors = false;
        $form->type = 'inline';
        $data = new Trend();
        return $this->controller->renderPartial('backend.views.homePage._trendsForm', array('form' => $form, 'data' => $data));
    }
} 