
<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
 ?>

<?php

$this->title = $model->adminNames[3];
$this->breadcrumbs = array(
    $model->adminNames[3],Yii::t('sideMenu', 'Transaction information'));

?>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id' => 'trans-info-grid',
	'dataProvider' => $model->search(),
	//'filter' => $model,
	'columns' => array(
		array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => 'first_name',
					'sortable' => true,
					'editable' => array(
                            'type'=>'text',
                            'apply'=>true,
                            'url' => array('updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
		array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => 'last_name',
					'sortable' => true,
					'editable' => array(
                            'type'=>'text',
                            'apply'=>true,
                            'url' => array('updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
		array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => 'company_name',
					'sortable' => true,
					'editable' => array(
                            'type'=>'text',
                            'apply'=>true,
                            'url' => array('updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
		array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => 'address',
					'sortable' => true,
					'editable' => array(
                            'type'=>'text',
                            'apply'=>true,
                            'url' => array('updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
		array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => 'city',
					'sortable' => true,
					'editable' => array(
                            'type'=>'text',
                            'apply'=>true,
                            'url' => array('updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
		/*
		array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => 'state',
					'sortable' => true,
					'editable' => array(
                            'type'=>'text',
                            'apply'=>true,
                            'url' => array('updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
		array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => 'zip_code',
					'sortable' => true,
					'editable' => array(
                            'type'=>'text',
                            'apply'=>true,
                            'url' => array('updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
		array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => 'email',
					'sortable' => true,
					'editable' => array(
                            'type'=>'text',
                            'apply'=>true,
                            'url' => array('updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
		array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => 'phone',
					'sortable' => true,
					'editable' => array(
                            'type'=>'text',
                            'apply'=>true,
                            'url' => array('updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
		array(
                    'class' => 'bootstrap.widgets.TbEditableColumn',
                    'name' => 'type',
                    'filter' => array(1=>Yii::t('admin','Shipping'),2=>Yii::t('admin','Billing'),),
					'sortable' => true,
					'editable' => array(
                        'type'=>'select',
                        'apply'=>true,
                        'placement' => 'right',
                        'inputclass' => 'span3',
                        'url' => array('updateAttribute', 'model' => get_class($model)),
                        'source' => array(1=>Yii::t('admin','Shipping'),2=>Yii::t('admin','Billing'),),
                        'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                            )
					),
		array(
                'class' => 'bootstrap.widgets.TbEditableColumn',
                'name' => 'country',
                'filter' => GxHtml::listDataEx(Country::model()->findAll()),
                'sortable' => true,
                'editable' => array(
                    'type'=>'select',
                    'apply'=>true,
                    'placement' => 'right',
                    'inputclass' => 'span3',
                    'url' => array('updateAttribute', 'model' => get_class($model)),
                    'source' => GxHtml::listDataEx(Country::model()->findAll()),
                         'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
		*/
    array(
        'class' => 'application.extensions.bootstrap.widgets.TbButtonColumn',
        'buttons' => array('delete' => array('visible' => (user()->isAdmin) ? 'true' : 'false')),
        'htmlOptions'=>array('class'=>'action-column'),
        'headerHtmlOptions'=>array('class'=>'action-column'),
        'footerHtmlOptions'=>array('class'=>'action-column'),
        'deleteConfirmation'=>Yii::t('admin','Are you sure want to delete this item'),
        'header'=>Yii::t('admin','Actions'),
	),
    ),
)); ?>

<div class="form-actions">















<?php if(user()->isAdmin):?>

    <?php echo CHtml::link(TbHtml::icon('glyphicon glyphicon-plus'). Yii::t('admin','Add item'),array('create'),array('class'=>'btn btn-default'));?>
<?php endif?>
</div>
<div id="success"  class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p><?php echo Yii::t('admin','The data was save with success');?></p>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

