

<?php


$this->breadcrumbs = array(
    $model->adminNames[3] , Yii::t('sideMenu', 'Product labels')  => array('admin'),
    Yii::t('admin',
        'Edit ',
        array('{name}'=>$model->adminNames[1])
    ),
);

$this->title = Yii::t('admin',
    'Edit {name}',
    array('{name}'=> Yii::t('sideMenu',"Product labels"))
);

$this->renderPartial('_form', array(
		'model' => $model));

