
<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
 ?>

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'shipping-option-form',
    'enableClientValidation'=>true,

));
?>
        
            
    
                        <?php echo $form->textFieldGroup($model, 'name', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Option name',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
                    
    
                        <?php echo $form->checkBoxGroup($model, 'use_percent'); ?>
                    
    
                        <?php echo $form->textFieldGroup($model,'fixed_amount',
                   array(
                       'wrapperHtmlOptions' => array(
                           'class' => 'col-sm-5 currency',
                           'id' => 'id_fixed_amount',
                       ),
                       //'hint' => Yii::t('admin','Please, insert').' Fixed amount',
				 'append' => '$'
			)
		);
                        $this->widget('ext.currency.CurrencyWidget', array('selector' => '#ShippingOption_fixed_amount','options'=>array (
  'symbol' => '$',
  'showSymbol' => false,
  'symbolStay' => false,
  'thousands' => '',
  'decimal' => '.',
  'precision' => 2,
  'defaultZero' => true,
  'allowZero' => true,
  'allowNegative' => false,
))); ?>
                    
    
                        <?php echo $form->textFieldGroup($model,'percent_amount',
			array(
				'wrapperHtmlOptions' => array(
					'class' => 'col-sm-5',
                    'id' => 'id_percent_amount',
				),
				 //'hint' => Yii::t('admin','Please, insert').' Percent',
				 'append' => '.00'
			)
		); ?>
                    
    
                        <?php echo $form->dropDownListGroup($model,'condition',
                            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                'widgetOptions' => array(
                                    'data' => array(1=>Yii::t('admin','By order'),2=>Yii::t('admin','By product'),),
                                   // 'htmlOptions' => array('multiple' => true),
                                ),
                                 //'hint' => Yii::t('admin','Please, select').' Apply?',
                                 'prepend' => Yii::t('admin','Select')
                            )
                        ); ?>
                    
    
                        <?php echo $form->checkBoxGroup($model, 'is_active'); ?>
                    
    
                        <?php echo $form->checkBoxGroup($model, 'is_default'); ?>
                    
            
            
    

<div class='form-actions'>   <a href='<?php echo app()->request->urlReferrer;?>' class='btn btn-default'><span class='glyphicon glyphicon-arrow-left'></span><?php echo Yii::t('admin','Back');?></a>   <?php $this->widget(
        'bootstrap.widgets.TbButton',
        array(
            'buttonType' => 'submit',
            'context' => 'primary',
            'icon'=> 'glyphicon glyphicon-saved',
            'label' => Yii::t('admin','Save item')
        )
    ); ?>
    <?php
    if(Yii::app()->controller->action->id!='update') {
        $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'buttonType' => 'reset',
                'context' => 'warning',
                'icon'=> 'glyphicon glyphicon-remove',
                'label' => Yii::t('admin','Reset form')
            )
        );
    } ?>
    <?php $this->endWidget(); ?>

    <?php if(isset($model->id)){
       // echo CHtml::link(Yii::t('admin',TbHtml::icon('glyphicon glyphicon-remove'). 'Remove item'),array('delete','id'=>$model->id),array('class'=>'btn btn-danger'));
    }?>
</div>

<script type="text/javascript">
    $(document).ready(function(){
        if($('#ShippingOption_use_percent').attr('checked')){
            $('#id_fixed_amount').parent().hide();
            $('#id_percent_amount').parent().show();
        }
        else{
            $('#id_fixed_amount').parent().show();
            $('#id_percent_amount').parent().hide();
        }


        $('#ShippingOption_use_percent').click(function(){
            if($('#ShippingOption_use_percent').attr('checked')){
                $('#id_fixed_amount').parent().hide();
                $('#id_percent_amount').parent().show();
            }
            else{
                $('#id_fixed_amount').parent().show();
                $('#id_percent_amount').parent().hide();
            }
        });

    });
</script>





