
<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
 ?>

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'checkout-order-page-form',
    'enableClientValidation'=>true,

));
?>
        
            
    
                        <?php echo $form->textFieldGroup($model, 'breadcrumb_title', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Breadcrumb title',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
                    
    
                        <?php echo $form->textFieldGroup($model, 'order_list_header', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Order list header',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
                    
    
<!--                        --><?php //echo $form->textFieldGroup($model, 'order_column_label', array(
//                'maxlength' => 255,
//                'wrapperHtmlOptions' => array(
//                                    'class' => 'col-sm-5',
//                                ),
//
//                                //'hint' => Yii::t('admin','Please, insert ').' Order column label',
//                                'append' => Yii::t('admin','Text')
//                                )
//                      ); ?>
<!--                    -->
<!--    -->
<!--                        --><?php //echo $form->textFieldGroup($model, 'day_column_label', array(
//                'maxlength' => 255,
//                'wrapperHtmlOptions' => array(
//                                    'class' => 'col-sm-5',
//                                ),
//
//                                //'hint' => Yii::t('admin','Please, insert ').' Day column label',
//                                'append' => Yii::t('admin','Text')
//                                )
//                      ); ?>
<!--                    -->
<!--    -->
<!--                        --><?php //echo $form->textFieldGroup($model, 'total_column_label', array(
//                'maxlength' => 255,
//                'wrapperHtmlOptions' => array(
//                                    'class' => 'col-sm-5',
//                                ),
//
//                                //'hint' => Yii::t('admin','Please, insert ').' Total column label',
//                                'append' => Yii::t('admin','Text')
//                                )
//                      ); ?>
<!--                    -->
    
                        <?php echo $form->textFieldGroup($model, 'cart_details_header', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Cart details header',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
                    
    
                        <?php echo $form->textFieldGroup($model, 'subtotal_label', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Subtotal label',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
                    
    
                        <?php echo $form->textFieldGroup($model, 'shipping_label', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Shipping label',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
                    
    
                        <?php echo $form->textFieldGroup($model, 'total_label', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Total label',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
                    
    
                        <?php echo $form->textFieldGroup($model, 'have_question_link_text', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Have question link text',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
                    
    
                        <?php echo $form->textFieldGroup($model, 'call_us_text', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Call us text',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
                    
    
                        <?php echo $form->textFieldGroup($model, 'order_detail_header', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Order details header',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>

<?php echo $form->textAreaGroup($model,'product_column_label',
    array(
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-12',
        ),
        'widgetOptions' => array(
            'htmlOptions' => array('rows' => 4),
        ),
    )
); ?>
                    
<!--    -->
<!--                        --><?php //echo $form->textareaFieldGroup($model, 'product_column_label', array(
//                'maxlength' => 255,
//                'wrapperHtmlOptions' => array(
//                                    'class' => 'col-sm-5',
//                                ),
//
//                                //'hint' => Yii::t('admin','Please, insert ').' Product column label',
//                                'append' => Yii::t('admin','Text')
//                                )
//                      ); ?>
                    
    
<!--                        --><?php //echo $form->textFieldGroup($model, 'product_total_column_label', array(
//                'maxlength' => 255,
//                'wrapperHtmlOptions' => array(
//                                    'class' => 'col-sm-5',
//                                ),
//
//                                //'hint' => Yii::t('admin','Please, insert ').' Product total column label',
//                                'append' => Yii::t('admin','Text')
//                                )
//                      ); ?>
                    
    
                        <?php echo $form->textFieldGroup($model, 'customer_detail_header', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Customer details header',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
                    
<!--    -->
<!--                        --><?php //echo $form->textFieldGroup($model, 'name_label', array(
//                'maxlength' => 255,
//                'wrapperHtmlOptions' => array(
//                                    'class' => 'col-sm-5',
//                                ),
//
//                                //'hint' => Yii::t('admin','Please, insert ').' Name label',
//                                'append' => Yii::t('admin','Text')
//                                )
//                      ); ?>
<!--                    -->
<!--    -->
<!--                        --><?php //echo $form->textFieldGroup($model, 'email_label', array(
//                'maxlength' => 255,
//                'wrapperHtmlOptions' => array(
//                                    'class' => 'col-sm-5',
//                                ),
//
//                                //'hint' => Yii::t('admin','Please, insert ').' Email label',
//                                'append' => Yii::t('admin','Text')
//                                )
//                      ); ?>
<!--                    -->
<!--    -->
<!--                        --><?php //echo $form->textFieldGroup($model, 'phone_label', array(
//                'maxlength' => 255,
//                'wrapperHtmlOptions' => array(
//                                    'class' => 'col-sm-5',
//                                ),
//
//                                //'hint' => Yii::t('admin','Please, insert ').' Phone label',
//                                'append' => Yii::t('admin','Text')
//                                )
//                      ); ?>
                    
    
                        <?php echo $form->textFieldGroup($model, 'billing_address', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Billing address header text',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
                    
    
                        <?php echo $form->textFieldGroup($model, 'shipping_address', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Shipping address header text',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
                    
            
            
    

<div class='form-actions'>   <a href='<?php echo app()->request->urlReferrer;?>' class='btn btn-default'><span class='glyphicon glyphicon-arrow-left'></span><?php echo Yii::t('admin','Back');?></a>   <?php $this->widget(
        'bootstrap.widgets.TbButton',
        array(
            'buttonType' => 'submit',
            'context' => 'primary',
            'icon'=> 'glyphicon glyphicon-saved',
            'label' => Yii::t('admin','Save item')
        )
    ); ?>
    <?php
    if(Yii::app()->controller->action->id!='update') {
        $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'buttonType' => 'reset',
                'context' => 'warning',
                'icon'=> 'glyphicon glyphicon-remove',
                'label' => Yii::t('admin','Reset form')
            )
        );
    } ?>
    <?php $this->endWidget(); ?>

    <?php if(isset($model->id)){
       // echo CHtml::link(Yii::t('admin',TbHtml::icon('glyphicon glyphicon-remove'). 'Remove item'),array('delete','id'=>$model->id),array('class'=>'btn btn-danger'));
    }?>
</div>





