
<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
 ?>

<?php

$this->title = $model->adminNames[3];
$this->breadcrumbs = array(
    $model->adminNames[3],Yii::t('sideMenu', 'Order Page'));

?>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id' => 'checkout-order-page-grid',
	'dataProvider' => $model->search(),
	//'filter' => $model,
	'columns' => array(
		array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => 'breadcrumb_title',
					'sortable' => true,
					'editable' => array(
                            'type'=>'text',
                            'apply'=>true,
                            'url' => array('updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
		array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => 'order_list_header',
					'sortable' => true,
					'editable' => array(
                            'type'=>'text',
                            'apply'=>true,
                            'url' => array('updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
//		array(
//				    'class' => 'bootstrap.widgets.TbEditableColumn',
//					'name' => 'order_column_label',
//					'sortable' => true,
//					'editable' => array(
//                            'type'=>'text',
//                            'apply'=>true,
//                            'url' => array('updateAttribute', 'model' => get_class($model)),
//                            'placement' => 'right',
//                            'inputclass' => 'span3',
//                                'success' => "js: function(response, newValue) {
//                    if (!response.success)
//                        $('#success').modal('toggle');
//                        setTimeout(function(){
//                            $('#success').modal('toggle');
//                        }, 2000);
//                }",
//                        )
//					),
//		array(
//				    'class' => 'bootstrap.widgets.TbEditableColumn',
//					'name' => 'day_column_label',
//					'sortable' => true,
//					'editable' => array(
//                            'type'=>'text',
//                            'apply'=>true,
//                            'url' => array('updateAttribute', 'model' => get_class($model)),
//                            'placement' => 'right',
//                            'inputclass' => 'span3',
//                                'success' => "js: function(response, newValue) {
//                    if (!response.success)
//                        $('#success').modal('toggle');
//                        setTimeout(function(){
//                            $('#success').modal('toggle');
//                        }, 2000);
//                }",
//                        )
//					),
//		array(
//				    'class' => 'bootstrap.widgets.TbEditableColumn',
//					'name' => 'total_column_label',
//					'sortable' => true,
//					'editable' => array(
//                            'type'=>'text',
//                            'apply'=>true,
//                            'url' => array('updateAttribute', 'model' => get_class($model)),
//                            'placement' => 'right',
//                            'inputclass' => 'span3',
//                                'success' => "js: function(response, newValue) {
//                    if (!response.success)
//                        $('#success').modal('toggle');
//                        setTimeout(function(){
//                            $('#success').modal('toggle');
//                        }, 2000);
//                }",
//                        )
//					),
		/*
		array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => 'cart_details_header',
					'sortable' => true,
					'editable' => array(
                            'type'=>'text',
                            'apply'=>true,
                            'url' => array('updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
		array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => 'subtotal_label',
					'sortable' => true,
					'editable' => array(
                            'type'=>'text',
                            'apply'=>true,
                            'url' => array('updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
		array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => 'shipping_label',
					'sortable' => true,
					'editable' => array(
                            'type'=>'text',
                            'apply'=>true,
                            'url' => array('updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
		array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => 'total_label',
					'sortable' => true,
					'editable' => array(
                            'type'=>'text',
                            'apply'=>true,
                            'url' => array('updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
		array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => 'have_question_link_text',
					'sortable' => true,
					'editable' => array(
                            'type'=>'text',
                            'apply'=>true,
                            'url' => array('updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
		array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => 'call_us_text',
					'sortable' => true,
					'editable' => array(
                            'type'=>'text',
                            'apply'=>true,
                            'url' => array('updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
		array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => 'order_detail_header',
					'sortable' => true,
					'editable' => array(
                            'type'=>'text',
                            'apply'=>true,
                            'url' => array('updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
		array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => 'product_column_label',
					'sortable' => true,
					'editable' => array(
                            'type'=>'text',
                            'apply'=>true,
                            'url' => array('updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
		array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => 'product_total_column_label',
					'sortable' => true,
					'editable' => array(
                            'type'=>'text',
                            'apply'=>true,
                            'url' => array('updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
		array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => 'customer_detail_header',
					'sortable' => true,
					'editable' => array(
                            'type'=>'text',
                            'apply'=>true,
                            'url' => array('updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
		array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => 'name_label',
					'sortable' => true,
					'editable' => array(
                            'type'=>'text',
                            'apply'=>true,
                            'url' => array('updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
		array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => 'email_label',
					'sortable' => true,
					'editable' => array(
                            'type'=>'text',
                            'apply'=>true,
                            'url' => array('updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
		array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => 'phone_label',
					'sortable' => true,
					'editable' => array(
                            'type'=>'text',
                            'apply'=>true,
                            'url' => array('updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
		array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => 'billing_address',
					'sortable' => true,
					'editable' => array(
                            'type'=>'text',
                            'apply'=>true,
                            'url' => array('updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
		array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => 'shipping_address',
					'sortable' => true,
					'editable' => array(
                            'type'=>'text',
                            'apply'=>true,
                            'url' => array('updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
		*/
    array(
        'class' => 'application.extensions.bootstrap.widgets.TbButtonColumn',
        'buttons' => array('delete' => array('visible' => (user()->isAdmin) ? 'true' : 'false')),
        'htmlOptions'=>array('class'=>'action-column'),
        'headerHtmlOptions'=>array('class'=>'action-column'),
        'footerHtmlOptions'=>array('class'=>'action-column'),
        'deleteConfirmation'=>Yii::t('admin','Are you sure want to delete this item'),
        'header'=>Yii::t('admin','Actions'),
	),
    ),
)); ?>

<div class="form-actions">
<?php if($model->count() < 1): ?>
    <p>Up to 1 row(s).</p>
<?php if(user()->isAdmin):?>

    <?php echo CHtml::link(TbHtml::icon('glyphicon glyphicon-plus'). Yii::t('admin','Add item'),array('create'),array('class'=>'btn btn-default'));?>
<?php endif?>
<?php endif?>
</div>
<div id="success"  class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p><?php echo Yii::t('admin','The data was save with success');?></p>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

