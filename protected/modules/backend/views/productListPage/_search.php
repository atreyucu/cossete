
<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
 ?>
<?php $form = $this->beginWidget('application.extensions.bootstrap.widgets.TbActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

        <?php echo $form->textFieldGroup($model, 'id', array(
                'maxlength' => 50,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' id',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
        <?php echo $form->textFieldGroup($model, 'breadcrumb_title', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Breadcrumb title',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
        <?php echo $form->textFieldGroup($model, 'items_per_page',
				            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                //'hint' =>Yii::t('admin','Please, insert ').' Items per page',
                                'append' => Yii::t('admin','Text')
                            )
                        ); ?>
        <?php echo $form->textFieldGroup($model, 'categories_title', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Categories tree header label',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
        <?php echo $form->textFieldGroup($model, 'filter_title', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Filter by header label',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
        <?php echo $form->textFieldGroup($model, 'filter_button_label', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Filter button label',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
        <?php echo $form->fileFieldGroup($model, 'recipeImg1',array(
                            'wrapperHtmlOptions' => array(
                                'class' => 'col-sm-5',
                            ),
                            'append' => CHtml::image($model->_litle_banner_image1->getFileUrl('normal'), '', array('width' => '100px')),
                            'hint' => Yii::t('admin','The image dimensions are 275x200px')
                                    )); 

                 echo $form->textFieldGroup($model, 'litle_banner_image1',
            				            array(
                                            'wrapperHtmlOptions' => array(
                                                'class' => 'col-sm-5',
                                            ),//'hint' =>Yii::t('admin','Please, insert ').' Right banner image 1',
                                            'append' => 'text',
                                        )
                                    ); ?>
        <?php echo $form->fileFieldGroup($model, 'recipeImg2',array(
                            'wrapperHtmlOptions' => array(
                                'class' => 'col-sm-5',
                            ),
                            'append' => CHtml::image($model->_litle_banner_image2->getFileUrl('normal'), '', array('width' => '100px')),
                            'hint' => Yii::t('admin','The image dimensions are 275x200px')
                                    )); 

                 echo $form->textFieldGroup($model, 'litle_banner_image2',
            				            array(
                                            'wrapperHtmlOptions' => array(
                                                'class' => 'col-sm-5',
                                            ),//'hint' =>Yii::t('admin','Please, insert ').' Right banner image 2',
                                            'append' => 'text',
                                        )
                                    ); ?>
        <?php echo $form->fileFieldGroup($model, 'recipeImg3',array(
                            'wrapperHtmlOptions' => array(
                                'class' => 'col-sm-5',
                            ),
                            'append' => CHtml::image($model->_main_category_image->getFileUrl('normal'), '', array('width' => '100px')),
                            'hint' => Yii::t('admin','The image dimensions are 1600x562px')
                                    )); 

                 echo $form->textFieldGroup($model, 'main_category_image',
            				            array(
                                            'wrapperHtmlOptions' => array(
                                                'class' => 'col-sm-5',
                                            ),//'hint' =>Yii::t('admin','Please, insert ').' Main category image',
                                            'append' => 'text',
                                        )
                                    ); ?>
        <?php echo $form->textFieldGroup($model, 'main_category_title', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Main category title',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
        <?php echo $form->textFieldGroup($model, 'main_category_subtitle', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Main category subtitle',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
        <?php echo $form->redactorGroup($model, 'main_category_description_text',
                        array(
                            'wrapperHtmlOptions' => array(
                                'class' => 'col-sm-8',
                            ),
                            'widgetOptions' => array(
                                'editorOptions' => array(
                                    'rows' => 10,
                                    'options' => array('plugins' => array('clips', 'fontfamily', 'fullscreen'), 'lang' => app()->getLanguage()),
                                    'minHeight' => 60
                                )
                            )
                        )
                    ); ?>
        <?php echo $form->textFieldGroup($model, 'main_category_button', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Main category button text',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
        <?php echo $form->dropDownListGroup($model, 'category', GxHtml::listDataEx(ProductCategory::model()->findAll()), array('prompt' => Yii::t('admin','All'))); ?>
        <?php echo $form->textFieldGroup($model, 'created',
				            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                //'hint' =>Yii::t('admin','Please, insert ').' created',
                                'append' => Yii::t('admin','Text')
                            )
                        ); ?>
        <?php echo $form->textFieldGroup($model, 'updated',
				            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                //'hint' =>Yii::t('admin','Please, insert ').' updated',
                                'append' => Yii::t('admin','Text')
                            )
                        ); ?>
        <?php echo $form->textFieldGroup($model, 'owner', array(
                'maxlength' => 100,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' owner',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>

<div class="form-actions">
    		<?php $this->widget('application.extensions.bootstrap.widgets.TbButton',
    array(
            'buttonType' => 'submit',
            'context' => 'success',
            'icon'=> 'glyphicon glyphicon-saved',
            'label' => Yii::t('admin','Buscar '.$model->adminNames[2])
        ));
 ?></div>

<?php $this->endWidget(); ?>
