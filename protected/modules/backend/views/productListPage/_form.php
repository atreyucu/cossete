<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
?>

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'product-list-page-form',
    'enableClientValidation' => true,

));
?>



<?php echo $form->textFieldGroup($model, 'breadcrumb_title', array(
        'maxlength' => 255,
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-5',
        ),

        //'hint' => Yii::t('admin','Please, insert ').' Breadcrumb title',
        'append' => Yii::t('admin', 'Text')
    )
); ?>


<?php echo $form->textFieldGroup($model, 'items_per_page',
    array(
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-5',
        ),
        //'hint' =>Yii::t('admin','Please, insert ').' Items per page',
        'append' => Yii::t('admin', 'Text')
    )
); ?>


<?php echo $form->textFieldGroup($model, 'categories_title', array(
        'maxlength' => 255,
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-5',
        ),

        //'hint' => Yii::t('admin','Please, insert ').' Categories tree header label',
        'append' => Yii::t('admin', 'Text')
    )
); ?>


<?php echo $form->textFieldGroup($model, 'filter_title', array(
        'maxlength' => 255,
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-5',
        ),

        //'hint' => Yii::t('admin','Please, insert ').' Filter by header label',
        'append' => Yii::t('admin', 'Text')
    )
); ?>


<?php echo $form->textFieldGroup($model, 'filter_button_label', array(
        'maxlength' => 255,
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-5',
        ),

        //'hint' => Yii::t('admin','Please, insert ').' Filter button label',
        'append' => Yii::t('admin', 'Text')
    )
); ?>


<?php echo $form->fileFieldGroup($model, 'recipeImg1', array(
    'wrapperHtmlOptions' => array(
        'class' => 'col-sm-5',
    ),
    'append' => CHtml::image($model->_litle_banner_image1->getFileUrl('normal'), '', array('width' => '100px')),
    'hint' => Yii::t('admin', 'The image dimensions are 275x200px')
));

echo $form->textFieldGroup($model, 'litle_banner_image1',
    array(
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-5',
        ),//'hint' =>Yii::t('admin','Please, insert ').' Right banner image 1',
        'append' => 'text',
    )
); ?>


<?php echo $form->fileFieldGroup($model, 'recipeImg2', array(
    'wrapperHtmlOptions' => array(
        'class' => 'col-sm-5',
    ),
    'append' => CHtml::image($model->_litle_banner_image2->getFileUrl('normal'), '', array('width' => '100px')),
    'hint' => Yii::t('admin', 'The image dimensions are 275x200px')
));

echo $form->textFieldGroup($model, 'litle_banner_image2',
    array(
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-5',
        ),//'hint' =>Yii::t('admin','Please, insert ').' Right banner image 2',
        'append' => 'text',
    )
); ?>


<?php echo $form->fileFieldGroup($model, 'recipeImg3', array(
    'wrapperHtmlOptions' => array(
        'class' => 'col-sm-5',
    ),
    'append' => CHtml::image($model->_main_category_image->getFileUrl('normal'), '', array('width' => '100px')),
    'hint' => Yii::t('admin', 'The image dimensions are 1600x562px')
));

echo $form->textFieldGroup($model, 'main_category_image',
    array(
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-5',
        ),//'hint' =>Yii::t('admin','Please, insert ').' Main category image',
        'append' => 'text',
    )
); ?>


<?php echo $form->textFieldGroup($model, 'main_category_title', array(
        'maxlength' => 255,
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-5',
        ),

        //'hint' => Yii::t('admin','Please, insert ').' Main category title',
        'append' => Yii::t('admin', 'Text')
    )
); ?>


<?php echo $form->textFieldGroup($model, 'main_category_subtitle', array(
        'maxlength' => 255,
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-5',
        ),

        //'hint' => Yii::t('admin','Please, insert ').' Main category subtitle',
        'append' => Yii::t('admin', 'Text')
    )
); ?>


<?php echo $form->redactorGroup($model, 'main_category_description_text',
    array(
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-8',
        ),
        'widgetOptions' => array(
            'editorOptions' => array(
                'rows' => 10,
                'options' => array('plugins' => array('clips', 'fontfamily', 'fullscreen'), 'lang' => app()->getLanguage()),
                'minHeight' => 60
            )
        )
    )
); ?>


<?php echo $form->textFieldGroup($model, 'main_category_button', array(
        'maxlength' => 255,
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-5',
        ),

        //'hint' => Yii::t('admin','Please, insert ').' Main category button text',
        'append' => Yii::t('admin', 'Text')
    )
); ?>


<?php echo $form->dropDownListGroup($model, 'category',
    array(
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-5',
        ),
        'widgetOptions' => array(
            'data' => GxHtml::listDataEx(ProductCategory::model()->findAll()),
            // 'htmlOptions' => array('multiple' => true),
        ),
        //'hint' => Yii::t('admin','Please, select').' category',
        'prepend' => Yii::t('admin', 'Select')
    )
); ?>


<div class='form-actions'><a href='<?php echo app()->request->urlReferrer; ?>' class='btn btn-default'><span
            class='glyphicon glyphicon-arrow-left'></span><?php echo Yii::t('admin', 'Back'); ?>
    </a> <?php $this->widget(
        'bootstrap.widgets.TbButton',
        array(
            'buttonType' => 'submit',
            'context' => 'primary',
            'icon' => 'glyphicon glyphicon-saved',
            'label' => Yii::t('admin', 'Save item')
        )
    ); ?>
    <?php
    if (Yii::app()->controller->action->id != 'update') {
        $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'buttonType' => 'reset',
                'context' => 'warning',
                'icon' => 'glyphicon glyphicon-remove',
                'label' => Yii::t('admin', 'Reset form')
            )
        );
    } ?>
    <?php $this->endWidget(); ?>

    <?php if (isset($model->id)) {
        // echo CHtml::link(Yii::t('admin',TbHtml::icon('glyphicon glyphicon-remove'). 'Remove item'),array('delete','id'=>$model->id),array('class'=>'btn btn-danger'));
    } ?>


</div>





