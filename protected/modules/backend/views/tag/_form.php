
<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
 ?>

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'tag-form',
    'enableClientValidation'=>true,

));
?>
        
            
    
                        <?php echo $form->textFieldGroup($model, 'name', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Name',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
                    
    
                        <?php echo $form->checkBoxGroup($model, 'is_active'); ?>
                    
    
                        <?php echo $form->checkBoxGroup($model, 'show_in_home'); ?>
                    
    
                        <?php echo $form->dropDownListGroup($model,'position',
                            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                'widgetOptions' => array(
                                    'data' => array(1=>Yii::t('admin','Position 1'),2=>Yii::t('admin','Position 2'),3=>Yii::t('admin','Position 3'),),
                                   // 'htmlOptions' => array('multiple' => true),
                                ),
                                 //'hint' => Yii::t('admin','Please, select').' Position in the home',
                                 'prepend' => Yii::t('admin','Select')
                            )
                        ); ?>
                    
    
<!--                        --><?php //echo $form->textFieldGroup($model, 'priority',
//				            array(
//                                'wrapperHtmlOptions' => array(
//                                    'class' => 'col-sm-5',
//                                ),
//                                //'hint' =>Yii::t('admin','Please, insert ').' priority',
//                                'append' => Yii::t('admin','Text')
//                            )
//                        ); ?>

                        <?php echo $form->numberFieldGroup($model, 'priority',
                            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                'widgetOptions' => array(
                                    'htmlOptions' => array('min' => 1),

                                ),
                                //'hint' => Yii::t('admin','Please, insert ').' Number of Buildings',
                                //'append' => '#'
                            )
                        ); ?>
                    
            
            
    

<div class='form-actions'>   <a href='<?php echo app()->request->urlReferrer;?>' class='btn btn-default'><span class='glyphicon glyphicon-arrow-left'></span><?php echo Yii::t('admin','Back');?></a>   <?php $this->widget(
        'bootstrap.widgets.TbButton',
        array(
            'buttonType' => 'submit',
            'context' => 'primary',
            'icon'=> 'glyphicon glyphicon-saved',
            'label' => Yii::t('admin','Save item')
        )
    ); ?>
    <?php
    if(Yii::app()->controller->action->id!='update') {
        $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'buttonType' => 'reset',
                'context' => 'warning',
                'icon'=> 'glyphicon glyphicon-remove',
                'label' => Yii::t('admin','Reset form')
            )
        );
    } ?>
    <?php $this->endWidget(); ?>

    <?php if(isset($model->id)){
       // echo CHtml::link(Yii::t('admin',TbHtml::icon('glyphicon glyphicon-remove'). 'Remove item'),array('delete','id'=>$model->id),array('class'=>'btn btn-danger'));
    }?>
</div>





