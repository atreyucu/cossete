
<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
 ?>

<?php
$this->breadcrumbs = array(
        $model->adminNames[3] ,Yii::t('sideMenu', 'Product')   => array('admin'),
    Yii::t('admin','View'),
);


$this->title = Yii::t('admin',
    'View {name}',
    array('{name}'=>Yii::t('sideMenu',"Product"))
);

?>

<div class="widget-box">
    <div class="widget-title">
        <span class="icon">
            <i class="fa fa-file"></i>
        </span>
        <h5>Product</h5>
    </div>
    <div class="widget-content nopadding">
        <?php $this->widget('bootstrap.widgets.TbEditableDetailView', array(
            'data' => $model,
            'id'=>'view-table',
            'attributes' => array(
                array(
                    'label' => Yii::t('admin','Detail image'),
                    'type' => 'raw',
                    'value' => CHtml::image($model->_detail_image->getFileUrl('normal'), $model->detail_image , array('width'=> '100px')),
                ),
                array(
                    'class' => 'bootstrap.widgets.TbEditableColumn',
                    'name' => 'detail_image',
                    'sortable' => false,
                    'editable' => array(
                        'type'=>'text',
                        'apply'=>true,
                        'url' => array('updateAttribute', 'model' => get_class($model)),
                        'placement' => 'right',
                        'inputclass' => 'span3',
                        'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                    )
                ),
                array(
                    'label' => Yii::t('admin','Detail zoom image'),
                    'type' => 'raw',
                    'value' => CHtml::image($model->_detail_zoom_image->getFileUrl('normal'), $model->detail_zoom_image , array('width'=> '100px')),
                ),
                array(
                    'class' => 'bootstrap.widgets.TbEditableColumn',
                    'name' => 'detail_zoom_image',
                    'sortable' => false,
                    'editable' => array(
                        'type'=>'text',
                        'apply'=>true,
                        'url' => array('updateAttribute', 'model' => get_class($model)),
                        'placement' => 'right',
                        'inputclass' => 'span3',
                        'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                    )
                ),
                array(
                    'label' => Yii::t('admin','Listing image'),
                    'type' => 'raw',
                    'value' => CHtml::image($model->_listing_image->getFileUrl('normal'), $model->listing_image , array('width'=> '100px')),
                ),
                array(
                    'class' => 'bootstrap.widgets.TbEditableColumn',
                    'name' => 'listing_image',
                    'sortable' => false,
                    'editable' => array(
                        'type'=>'text',
                        'apply'=>true,
                        'url' => array('updateAttribute', 'model' => get_class($model)),
                        'placement' => 'right',
                        'inputclass' => 'span3',
                        'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                    )
                ),
                array(
                    'class' => 'bootstrap.widgets.TbEditableColumn',
                    'name' => 'name',
                    'sortable' => false,
                    'editable' => array(
                        'type'=>'text',
                        'apply'=>true,
                        'url' => array('updateAttribute', 'model' => get_class($model)),
                        'placement' => 'right',
                        'inputclass' => 'span3',
                        'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                    )
                ),
                array(
                    'class' => 'bootstrap.widgets.TbEditableColumn',
                    'name' => 'code',
                    'sortable' => false,
                    'editable' => array(
                        'type'=>'text',
                        'apply'=>true,
                        'url' => array('updateAttribute', 'model' => get_class($model)),
                        'placement' => 'right',
                        'inputclass' => 'span3',
                        'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                    )
                ),
                array(
                    'class' => 'bootstrap.widgets.TbEditableColumn',
                    'name' => 'description',
                    'sortable' => false,
                    'editable' => array(
                        'type'=>'textarea',
                        'apply'=>true,
                        'url' => array('updateAttribute', 'model' => get_class($model)),
                        'placement' => 'right',
                        'inputclass' => 'span3',
                        'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                    )
                ),
                array(
                    'class' => 'bootstrap.widgets.TbEditableColumn',
                    'name' => 'price',
                    'sortable' => false,
                    'editable' => array(
                        'type'=>'text',
                        'apply'=>true,
                        'url' => array('updateAttribute', 'model' => get_class($model)),
                        'placement' => 'right',
                        'inputclass' => 'span3',
                        'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                    )
                ),
                array(
                    'class' => 'bootstrap.widgets.TbEditableColumn',
                    'name' => 'visit_count',
                    'sortable' => false,
                    'editable' => array(
                        'type'=>'text',
                        'apply'=>true,
                        'url' => array('updateAttribute', 'model' => get_class($model)),
                        'placement' => 'right',
                        'inputclass' => 'span3',
                        'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                    )
                ),
                array(
                    'label' => Yii::t('admin','category0'),
                    'type' => 'raw',
                    'value' => $model->category0 !== null ? CHtml::link(CHtml::encode(GxHtml::valueEx($model->category0)), array('productCategory/view', 'id' => GxActiveRecord::extractPkValue($model->category0, true)),array('class'=> 'btn btn-primary btn-small')) : null,
                ),
                array(
                    'label' => Yii::t('admin','label0'),
                    'type' => 'raw',
                    'value' => $model->label0 !== null ? CHtml::link(CHtml::encode(GxHtml::valueEx($model->label0)), array('label/view', 'id' => GxActiveRecord::extractPkValue($model->label0, true)),array('class'=> 'btn btn-primary btn-small')) : null,
                )
            ),
        )); ?>

    </div>
</div>

<div class="tabbable inline">
    <ul id="myTab" class="nav nav-tabs tab-bricky">
        <li class="active">
            <a href="#tour_panel_tab1" data-toggle="tab">
                Tags
            </a>
        </li>
        <li>
            <a href="#tour_panel_tab2" data-toggle="tab">
                Colors
            </a>
        </li>
        <li>
            <a href="#tour_panel_tab3" data-toggle="tab">
                Sizes
            </a>
        </li>
        <li>
            <a href="#tour_panel_tab4" data-toggle="tab">
                Seo
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane in active" id="tour_panel_tab1">
            <table class="table table-bordered table-striped table-hover with-check">
                <thead>
                <tr>
                    <th>Tag</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $this->renderPartial('_tagView', array('tags' => $tags));
                ?>
                </tbody>
            </table>
            <br>
            <br>

            <div class="widget-box">
                <div class="widget-title">
                <span class="icon">
                    <i class="fa fa-file"></i>
                </span>
                    <h5>Home Visibility</h5>
                </div>
                <div class="widget-content nopadding">
                    <?php $this->widget('bootstrap.widgets.TbEditableDetailView', array(
                        'data' => $model,
                        'id'=>'view-table2',
                        'attributes' => array(
                             array(
                                'label' => Yii::t('admin','Home image(Tag section)'),
                                'type' => 'raw',
                                'value' => CHtml::image($model->_tag_home_image->getFileUrl('normal'), $model->tag_home_image , array('width'=> '100px')),
                            ),
                            array(
                                'class' => 'bootstrap.widgets.TbEditableColumn',
                                'name' => 'tag_home_image',
                                'sortable' => false,
                                'editable' => array(
                                    'type'=>'text',
                                    'apply'=>true,
                                    'url' => array('updateAttribute', 'model' => get_class($model)),
                                    'placement' => 'right',
                                    'inputclass' => 'span3',
                                    'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                                )
                            ),
                        ),
                    )); ?>
                </div>
            </div>



        </div>

        <div class="tab-pane" id="tour_panel_tab2">
            <table class="table table-bordered table-striped table-hover with-check">
                <thead>
                <tr>
                    <th>Color</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $this->renderPartial('_colorView', array('colors' => $colors));
                ?>
                </tbody>
            </table>
        </div>

        <div class="tab-pane" id="tour_panel_tab3">
            <table class="table table-bordered table-striped table-hover with-check">
                <thead>
                <tr>
                    <th>Size</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $this->renderPartial('_sizeView', array('sizes' => $sizes));
                ?>
                </tbody>
            </table>
        </div>

        <div class="tab-pane" id="tour_panel_tab4">
            <?php $model->seoViewWidget(); ?>
        </div>


    </div>
</div>



<div class="form-actions" style="text-align: right">
    <a href='<?php echo app()->request->urlReferrer?>' class='btn btn-default'><span class='glyphicon glyphicon-arrow-left'></span><?php echo Yii::t('admin','Back');?></a><?php // echo CHtml::link(TbHtml::icon('glyphicon glyphicon-briefcase'). Yii::t('admin','Manage item'),array('admin'),array('class'=>'btn btn-default'));?><?php echo CHtml::link(TbHtml::icon('glyphicon glyphicon-saved'). Yii::t('admin','Update item'),array('update','id'=>$model->id),array('class'=>'btn btn-primary'));?><?php if(user()->isAdmin):?>
<?php echo CHtml::link(TbHtml::icon('glyphicon glyphicon-remove'). Yii::t('admin','Remove item'),array('delete','id'=>$model->id),array('class'=>'btn btn-danger delete'));?><?php endif?></div>

<div id="success"  class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p><?php echo Yii::t('admin','Data was saved with success')?></p>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<?php
$text = Yii::t('admin', 'Are you sure you want to delete this item?');
$js = <<< JS
    $('.delete').on('click',function(){
        if(confirm("$text"))
            return true;
        else
            return false;
        })
JS;
cs()->registerScript('delete_confirm', $js, CClientScript::POS_READY);
?>