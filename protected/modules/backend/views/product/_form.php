
<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
 ?>

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'product-form',
    'enableClientValidation'=>true,

));
?>
<div class="widget-box">
    <div class="widget-title">
        <span class="icon">
            <i class="fa fa-file"></i>
        </span>
        <h5>Product</h5>
    </div>
    <div class="widget-content nopadding">
        <?php echo $form->fileFieldGroup($model, 'recipeImg2',array(
            'wrapperHtmlOptions' => array(
                'class' => 'col-sm-5',
            ),
            'append' => CHtml::image($model->_detail_image->getFileUrl('normal'), '', array('width' => '100px')),
            'hint' => Yii::t('admin','The image dimensions are 360x540px')
        ));

        echo $form->textFieldGroup($model, 'detail_image',
            array(
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-5',
                ),//'hint' =>Yii::t('admin','Please, insert ').' Detail image',
                'append' => 'text',
            )
        ); ?>

        <?php echo $form->fileFieldGroup($model, 'recipeImg4',array(
            'wrapperHtmlOptions' => array(
                'class' => 'col-sm-5',
            ),
            'append' => CHtml::image($model->_detail_zoom_image->getFileUrl('normal'), '', array('width' => '100px')),
            'hint' => Yii::t('admin','The image dimensions are 600x900px')
        ));

        echo $form->textFieldGroup($model, 'detail_zoom_image',
            array(
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-5',
                ),//'hint' =>Yii::t('admin','Please, insert ').' Detail image',
                'append' => 'text',
            )
        ); ?>



        <?php echo $form->fileFieldGroup($model, 'recipeImg1',array(
            'wrapperHtmlOptions' => array(
                'class' => 'col-sm-5',
            ),
            'append' => CHtml::image($model->_listing_image->getFileUrl('normal'), '', array('width' => '100px')),
            'hint' => Yii::t('admin','The image dimensions are 260x390px')
        ));

        echo $form->textFieldGroup($model, 'listing_image',
            array(
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-5',
                ),//'hint' =>Yii::t('admin','Please, insert ').' Listing image',
                'append' => 'text',
            )
        ); ?>


        <?php echo $form->textFieldGroup($model, 'name', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-5',
                ),

                //'hint' => Yii::t('admin','Please, insert ').' Product name',
                'append' => Yii::t('admin','Text')
            )
        ); ?>


        <?php echo $form->textFieldGroup($model, 'code', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-5',
                ),

                //'hint' => Yii::t('admin','Please, insert ').' Product code',
                'append' => Yii::t('admin','Text')
            )
        ); ?>

        <?php echo $form->redactorGroup($model, 'description',
            array(
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-8',
                ),
                'widgetOptions' => array(
                    'editorOptions' => array(
                        'rows' => 10,
                        'options' => array('plugins' => array('clips', 'fontfamily', 'fullscreen'), 'lang' => app()->getLanguage()),
                        'minHeight' => 60
                    )
                )
            )
        ); ?>


        <?php echo $form->textFieldGroup($model,'price',
            array(
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-5 currency',
                    'id' => 'id_price',
                ),
                //'hint' => Yii::t('admin','Please, insert').' Price',
                'append' => '$'
            )
        );
        $this->widget('ext.currency.CurrencyWidget', array('selector' => '#Product_price','options'=>array (
            'symbol' => '$',
            'showSymbol' => false,
            'symbolStay' => false,
            'thousands' => '',
            'decimal' => '.',
            'precision' => 2,
            'defaultZero' => true,
            'allowZero' => true,
            'allowNegative' => false,
        ))); ?>

        <?php echo $form->dropDownListGroup($model,'category',
            array(
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-5',
                ),
                'widgetOptions' => array(
                    'data' => GxHtml::listDataEx(ProductCategory::model()->findAll()),
                    // 'htmlOptions' => array('multiple' => true),
                ),
                //'hint' => Yii::t('admin','Please, select').' category',
                'prepend' => Yii::t('admin','Select')
            )
        ); ?>

        <?php echo $form->dropDownListGroup($model,'label',
            array(
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-5',
                ),
                'widgetOptions' => array(
                    'data' => GxHtml::listDataEx(Label::model()->findAll()),
                    // 'htmlOptions' => array('multiple' => true),
                ),
                //'hint' => Yii::t('admin','Please, select').' label',
                'prepend' => Yii::t('admin','Select')
            )
        ); ?>


    </div>
</div>


<div class="tabbable inline">
    <ul id="myTab" class="nav nav-tabs tab-bricky">
        <li class="active">
            <a href="#tour_panel_tab1" data-toggle="tab">
                Tags
            </a>
        </li>
        <li>
            <a href="#tour_panel_tab2" data-toggle="tab">
                Colors
            </a>
        </li>
        <li>
            <a href="#tour_panel_tab3" data-toggle="tab">
                Sizes
            </a>
        </li>
        <li>
            <a href="#tour_panel_tab4" data-toggle="tab">
                Seo
            </a>
        </li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane in active" id="tour_panel_tab1">
            <table class="table table-bordered table-striped table-hover with-check">
                <thead>
                <tr>
                    <th><div class="checkbox"><input type="hidden" name="tagchechk" value="0" id="tagchechk"><input style="cursor: pointer" type="checkbox" value="1" id="tagmaincheck" name="tagmaincheck" <?php if($check_all_tags):?>checked="checked"<?php endif;?>></div></th>
                    <th>Tag</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $this->renderPartial('_tagForm', array('tags' => $tags, 'ctags'=>$ctags));
                ?>
                </tbody>
            </table>
            <br>
            <br>

            <div class="widget-box">
                <div class="widget-title">
                <span class="icon">
                    <i class="fa fa-file"></i>
                </span>
                    <h5>Home Visibility</h5>
                </div>
                <div class="widget-content nopadding">
                    <?php echo $form->fileFieldGroup($model, 'recipeImg3',array(
                        'wrapperHtmlOptions' => array(
                            'class' => 'col-sm-5',
                        ),
                        'append' => CHtml::image($model->_tag_home_image->getFileUrl('normal'), '', array('width' => '100px')),
                        'hint' => Yii::t('admin','The image dimensions are 275x412px')
                    ));

                    echo $form->textFieldGroup($model, 'tag_home_image',
                        array(
                            'wrapperHtmlOptions' => array(
                                'class' => 'col-sm-5',
                            ),//'hint' =>Yii::t('admin','Please, insert ').' Home image(Tag section)',
                            'append' => 'text',
                        )
                    ); ?>
                </div>
            </div>



        </div>

        <div class="tab-pane" id="tour_panel_tab2">
            <table class="table table-bordered table-striped table-hover with-check">
                <thead>
                <tr>
                    <th><div class="checkbox"><input type="hidden" name="colormainchechk" value="0" id="colormainchechk"><input style="cursor: pointer" type="checkbox" value="1" id="colormaincheck" name="colormaincheck" <?php if($check_all_colors):?>checked="checked"<?php endif;?>></div></th>
                    <th>Color</th>

                </tr>
                </thead>
                <tbody>
                <?php
                $this->renderPartial('_colorForm', array('colors' => $colors, 'ccolors'=>$ccolors));
                ?>
                </tbody>
            </table>
        </div>

        <div class="tab-pane" id="tour_panel_tab3">
            <table class="table table-bordered table-striped table-hover with-check">
                <thead>
                <tr>
                    <th><div class="checkbox"><input type="hidden" name="sizemainchechk" value="0" id="sizemainchechk"><input style="cursor: pointer" type="checkbox" value="1" id="sizemaincheck" name="sizemaincheck" <?php if($check_all_sizes):?>checked="checked"<?php endif;?>></div></th>
                    <th>Size</th>

                </tr>
                </thead>
                <tbody>
                <?php
                $this->renderPartial('_sizeForm', array('sizes' => $sizes, 'csizes'=>$csizes));
                ?>
                </tbody>
            </table>
        </div>

        <div class="tab-pane" id="tour_panel_tab4">
            <?php $model->seoFormWidget($form); ?>
        </div>


    </div>
</div>

<div class='form-actions'>   <a href='<?php echo app()->request->urlReferrer;?>' class='btn btn-default'><span class='glyphicon glyphicon-arrow-left'></span><?php echo Yii::t('admin','Back');?></a>   <?php $this->widget(
        'bootstrap.widgets.TbButton',
        array(
            'buttonType' => 'submit',
            'context' => 'primary',
            'icon'=> 'glyphicon glyphicon-saved',
            'label' => Yii::t('admin','Save item')
        )
    ); ?>
    <?php
    if(Yii::app()->controller->action->id!='update') {
        $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'buttonType' => 'reset',
                'context' => 'warning',
                'icon'=> 'glyphicon glyphicon-remove',
                'label' => Yii::t('admin','Reset form')
            )
        );
    } ?>
    <?php $this->endWidget(); ?>

    <?php if(isset($model->id)){
       // echo CHtml::link(Yii::t('admin',TbHtml::icon('glyphicon glyphicon-remove'). 'Remove item'),array('delete','id'=>$model->id),array('class'=>'btn btn-danger'));
    }?>
    <div class='btn-group'>
        <a href='#' data-toggle='dropdown' class='btn btn-info dropdown-toggle'>
            <i class='fa fa-plus icon-white'></i>

            <?php echo Yii::t('admin', 'Add'); ?>            <span class='caret'></span>
        </a>
        <ul class='dropdown-menu dropdown-primary'>
                            <li>
                    <?php echo CHtml::link(Yii::t('admin', TbHtml::icon('glyphicon glyphicon-plus-sign') . 'ProductCategory'), array('/backend/category0/create', 'id' => $model->primaryKey, 'controller' => Yii::app()->controller->id, 'action' => Yii::app()->controller->action->id), array()); ?>                </li>
                            <li>
                    <?php echo CHtml::link(Yii::t('admin', TbHtml::icon('glyphicon glyphicon-plus-sign') . 'Label'), array('/backend/label0/create', 'id' => $model->primaryKey, 'controller' => Yii::app()->controller->id, 'action' => Yii::app()->controller->action->id), array()); ?>                </li>
                    </ul>
    </div>


</div>


<script type="text/javascript">
    $('.tagcheck').each(function(){
        $(this).click(function(){
            var checked_count = 0;
            $('.tagcheck').each(function(){
                if($(this).attr('checked')){
                    checked_count+=1;
                }
            });

            if(checked_count == $('.tagcheck').length){
                $('#tagmaincheck').attr('checked','checked');
            }
            else{
                $('#tagmaincheck').removeAttr('checked');
            }
        })
    });



    $('#tagmaincheck').click(function(){
        if($('#tagmaincheck').attr('checked')){
            $('.tagcheck').each(function(){
                $(this).attr('checked','checked');
            });
        }
        else{
            $('.tagcheck').each(function(){
                $(this).removeAttr('checked');
            });
        }
    });

    $('.colorcheck').each(function(){
        $(this).click(function(){
            var checked_count = 0;
            $('.colorcheck').each(function(){
                if($(this).attr('checked')){
                    checked_count+=1;
                }
            });

            if(checked_count == $('.colorcheck').length){
                $('#colormaincheck').attr('checked','checked');
            }
            else{
                $('#colormaincheck').removeAttr('checked');
            }
        })
    });



    $('#colormaincheck').click(function(){
        if($('#colormaincheck').attr('checked')){
            $('.colorcheck').each(function(){
                $(this).attr('checked','checked');
            });
        }
        else{
            $('.colorcheck').each(function(){
                $(this).removeAttr('checked');
            });
        }
    });

    $('.sizecheck').each(function(){
        $(this).click(function(){
            var checked_count = 0;
            $('.sizecheck').each(function(){
                if($(this).attr('checked')){
                    checked_count+=1;
                }
            });

            if(checked_count == $('.sizecheck').length){
                $('#sizemaincheck').attr('checked','checked');
            }
            else{
                $('#sizemaincheck').removeAttr('checked');
            }
        })
    });



    $('#sizemaincheck').click(function(){
        if($('#sizemaincheck').attr('checked')){
            $('.sizecheck').each(function(){
                $(this).attr('checked','checked');
            });
        }
        else{
            $('.sizecheck').each(function(){
                $(this).removeAttr('checked');
            });
        }
    });

    $('#Product_category').change(function(){
        var cat = $('#Product_category option:selected').attr('value');
        $.get('/backend/default/refreshColors?cat=' + cat + '&prod=<?php echo $model->id?>'
            ,function(data){
                $('#tour_panel_tab2 table tbody').html(data);
                if($('.colorcheck').length > 0){
                    var checked_count = 0;
                    $('.colorcheck').each(function(){
                        if($(this).attr('checked')){
                            checked_count+=1;
                        }
                    });

                    if(checked_count == $('.colorcheck').length){
                        $('#colormaincheck').attr('checked','checked');
                    }
                    else{
                        $('#colormaincheck').removeAttr('checked');
                    }
                }
                else{
                    $('#colormaincheck').removeAttr('checked');
                }

        });

        $.get('/backend/default/refreshSizes?cat=' + cat + '&prod=<?php echo $model->id?>'
            ,function(data){
                $('#tour_panel_tab3 table tbody').html(data);
                if($('.sizecheck').length > 0){
                    var checked_count = 0;
                    $('.sizecheck').each(function(){
                        if($(this).attr('checked')){
                            checked_count+=1;
                        }
                    });

                    if(checked_count == $('.sizecheck').length){
                        $('#sizemaincheck').attr('checked','checked');
                    }
                    else{
                        $('#sizemaincheck').removeAttr('checked');
                    }
                }
                else{
                    $('#sizemaincheck').removeAttr('checked');
                }
            });
    });
</script>





