
<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
 ?>

<?php

$this->title = $model->adminNames[3];
$this->breadcrumbs = array(
    $model->adminNames[3],Yii::t('sideMenu', 'Product'));

?>

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id' => 'product-grid',
	'dataProvider' => $model->search(),
	//'filter' => $model,
	'columns' => array(
		array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => 'name',
					'sortable' => true,
					'editable' => array(
                            'type'=>'text',
                            'apply'=>true,
                            'url' => array('updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
		array(
                        'class' => 'bootstrap.widgets.TbEditableColumn',
                        'name' => 'description',
                        'sortable' => true,
                        'editable' => array(
                                'type'=>'textarea',
                                'apply'=>true,
                                'url' => array('updateAttribute', 'model' => get_class($model)),
                                'placement' => 'right',
                                'inputclass' => 'span3',
                                 'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                            )
                        ),
		array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => 'price',
					'sortable' => true,
					'editable' => array(
                            'type'=>'text',
                            'apply'=>true,
                            'url' => array('updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
		array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => 'visit_count',
					'sortable' => true,
					'editable' => array(
                            'type'=>'text',
                            'apply'=>true,
                            'url' => array('updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
		array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => 'code',
					'sortable' => true,
					'editable' => array(
                            'type'=>'text',
                            'apply'=>true,
                            'url' => array('updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
		/*
		array(
                'class' => 'bootstrap.widgets.TbEditableColumn',
                'name' => 'category',
                'filter' => GxHtml::listDataEx(ProductCategory::model()->findAll()),
                'sortable' => true,
                'editable' => array(
                    'type'=>'select',
                    'apply'=>true,
                    'placement' => 'right',
                    'inputclass' => 'span3',
                    'url' => array('updateAttribute', 'model' => get_class($model)),
                    'source' => GxHtml::listDataEx(ProductCategory::model()->findAll()),
                         'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
		array(
                'class' => 'bootstrap.widgets.TbEditableColumn',
                'name' => 'label',
                'filter' => GxHtml::listDataEx(Label::model()->findAll()),
                'sortable' => true,
                'editable' => array(
                    'type'=>'select',
                    'apply'=>true,
                    'placement' => 'right',
                    'inputclass' => 'span3',
                    'url' => array('updateAttribute', 'model' => get_class($model)),
                    'source' => GxHtml::listDataEx(Label::model()->findAll()),
                         'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
		array(
                                'class' => 'bootstrap.widgets.TbImageColumn',
                                'header' => 'Listing image',
                                'imagePathExpression' => '$data->_listing_image->getFileUrl("normal")',
                                'imageOptions'=>array('width'=>'100px')
                            ),
		array(
                                'class' => 'bootstrap.widgets.TbImageColumn',
                                'header' => 'Detail image',
                                'imagePathExpression' => '$data->_detail_image->getFileUrl("normal")',
                                'imageOptions'=>array('width'=>'100px')
                            ),
		array(
                                'class' => 'bootstrap.widgets.TbImageColumn',
                                'header' => 'Home image(Tag section)',
                                'imagePathExpression' => '$data->_tag_home_image->getFileUrl("normal")',
                                'imageOptions'=>array('width'=>'100px')
                            ),
		*/
    array(
        'class' => 'application.extensions.bootstrap.widgets.TbButtonColumn',
        'buttons' => array('delete' => array('visible' => (user()->isAdmin) ? 'true' : 'false')),
        'htmlOptions'=>array('class'=>'action-column'),
        'headerHtmlOptions'=>array('class'=>'action-column'),
        'footerHtmlOptions'=>array('class'=>'action-column'),
        'deleteConfirmation'=>Yii::t('admin','Are you sure want to delete this item'),
        'header'=>Yii::t('admin','Actions'),
	),
    ),
)); ?>

<div class="form-actions">














<?php if(user()->isAdmin):?>

    <?php echo CHtml::link(TbHtml::icon('glyphicon glyphicon-plus'). Yii::t('admin','Add item'),array('create'),array('class'=>'btn btn-default'));?>
<?php endif?>
</div>
<div id="success"  class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p><?php echo Yii::t('admin','The data was save with success');?></p>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

