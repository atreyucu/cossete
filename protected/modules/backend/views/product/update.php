

<?php


$this->breadcrumbs = array(
    $model->adminNames[3] , Yii::t('sideMenu', 'Product')  => array('admin'),
    Yii::t('admin',
        'Edit ',
        array('{name}'=>$model->adminNames[1])
    ),
);

$this->title = Yii::t('admin',
    'Edit {name}',
    array('{name}'=> Yii::t('sideMenu',"Product"))
);

$this->renderPartial('_form', array(
		'model' => $model,
        'tags' => $tags,
        'colors'=> $colors,
        'sizes' => $sizes,
        'ctags' => $ctags,
        'ccolors'=> $ccolors,
        'csizes' => $csizes,
        'check_all_tags' => $check_all_tags,
        'check_all_colors' => $check_all_colors,
        'check_all_sizes' => $check_all_sizes,
));

