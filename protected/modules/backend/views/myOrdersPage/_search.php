
<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
 ?>
<?php $form = $this->beginWidget('application.extensions.bootstrap.widgets.TbActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

        <?php echo $form->textFieldGroup($model, 'id', array(
                'maxlength' => 50,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' id',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
        <?php echo $form->textFieldGroup($model, 'breadcrumb_title', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Breadcrumb title',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
        <?php echo $form->textFieldGroup($model, 'order_column_label', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Order column label',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
        <?php echo $form->textFieldGroup($model, 'date_column_label', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Date column label',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
        <?php echo $form->textFieldGroup($model, 'status_column_label', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Status column label',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
        <?php echo $form->textFieldGroup($model, 'total_column_label', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Total column label',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
        <?php echo $form->textFieldGroup($model, 'billing_address', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Billing address header text',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
        <?php echo $form->textFieldGroup($model, 'shipping_address', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Shipping address header text',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
        <?php echo $form->textFieldGroup($model, 'view_button_text', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Checkout button text',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
        <?php echo $form->textFieldGroup($model, 'created',
				            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                //'hint' =>Yii::t('admin','Please, insert ').' created',
                                'append' => Yii::t('admin','Text')
                            )
                        ); ?>
        <?php echo $form->textFieldGroup($model, 'updated',
				            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                //'hint' =>Yii::t('admin','Please, insert ').' updated',
                                'append' => Yii::t('admin','Text')
                            )
                        ); ?>
        <?php echo $form->textFieldGroup($model, 'owner', array(
                'maxlength' => 100,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' owner',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>

<div class="form-actions">
    		<?php $this->widget('application.extensions.bootstrap.widgets.TbButton',
    array(
            'buttonType' => 'submit',
            'context' => 'success',
            'icon'=> 'glyphicon glyphicon-saved',
            'label' => Yii::t('admin','Buscar '.$model->adminNames[2])
        ));
 ?></div>

<?php $this->endWidget(); ?>
