
<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
 ?>

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'checkout-page-form',
    'enableClientValidation'=>true,

));
?>
        
            
    
                        <?php echo $form->textFieldGroup($model, 'breadcrumb_title', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Breadcrumb title',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
                    
    
                        <?php echo $form->textFieldGroup($model, 'billing_form_header', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Billing form header text',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
                    
    
                        <?php echo $form->textFieldGroup($model, 'shipping_form_header', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Shipping form header text',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
                    
    
                        <?php echo $form->textFieldGroup($model, 'your_order_header', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Your order header text',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
                    
    
                        <?php echo $form->textFieldGroup($model, 'cart_totals_header', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Cart total header text',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
                    
    
                        <?php echo $form->textFieldGroup($model, 'subtotal_label', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Subtotal label',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
                    
    
                        <?php echo $form->textFieldGroup($model, 'shipping_label', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Shipping label',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
                    
    
                        <?php echo $form->textFieldGroup($model, 'total_label', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Total label',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
                    
    
                        <?php echo $form->textFieldGroup($model, 'accept_terms_text', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Accept terms text',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
                    
    
                        <?php echo $form->textFieldGroup($model, 'terms_cond_link_text', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Terms & conditions link text',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
                    
    
                        <?php echo $form->textFieldGroup($model, 'checkout_button_text', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Checkout button text',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
                    
    
<!--                        --><?php //echo $form->textFieldGroup($model, 'country_label', array(
//                'maxlength' => 255,
//                'wrapperHtmlOptions' => array(
//                                    'class' => 'col-sm-5',
//                                ),
//
//                                //'hint' => Yii::t('admin','Please, insert ').' Country label',
//                                'append' => Yii::t('admin','Text')
//                                )
//                      ); ?>
<!--                    -->
<!--    -->
<!--                        --><?php //echo $form->textFieldGroup($model, 'firstname_label', array(
//                'maxlength' => 255,
//                'wrapperHtmlOptions' => array(
//                                    'class' => 'col-sm-5',
//                                ),
//
//                                //'hint' => Yii::t('admin','Please, insert ').' Firstname label',
//                                'append' => Yii::t('admin','Text')
//                                )
//                      ); ?>
<!--                    -->
<!--    -->
<!--                        --><?php //echo $form->textFieldGroup($model, 'lastname_label', array(
//                'maxlength' => 255,
//                'wrapperHtmlOptions' => array(
//                                    'class' => 'col-sm-5',
//                                ),
//
//                                //'hint' => Yii::t('admin','Please, insert ').' Lastname label',
//                                'append' => Yii::t('admin','Text')
//                                )
//                      ); ?>
<!--                    -->
<!--    -->
<!--                        --><?php //echo $form->textFieldGroup($model, 'city_label', array(
//                'maxlength' => 255,
//                'wrapperHtmlOptions' => array(
//                                    'class' => 'col-sm-5',
//                                ),
//
//                                //'hint' => Yii::t('admin','Please, insert ').' Town/City label',
//                                'append' => Yii::t('admin','Text')
//                                )
//                      ); ?>
<!--                    -->
<!--    -->
<!--                        --><?php //echo $form->textFieldGroup($model, 'state_label', array(
//                'maxlength' => 255,
//                'wrapperHtmlOptions' => array(
//                                    'class' => 'col-sm-5',
//                                ),
//
//                                //'hint' => Yii::t('admin','Please, insert ').' State/County label',
//                                'append' => Yii::t('admin','Text')
//                                )
//                      ); ?>
<!--                    -->
<!--    -->
<!--                        --><?php //echo $form->textFieldGroup($model, 'zipcode_label', array(
//                'maxlength' => 255,
//                'wrapperHtmlOptions' => array(
//                                    'class' => 'col-sm-5',
//                                ),
//
//                                //'hint' => Yii::t('admin','Please, insert ').' Zip code label',
//                                'append' => Yii::t('admin','Text')
//                                )
//                      ); ?>
<!--                    -->
<!--    -->
<!--                        --><?php //echo $form->textFieldGroup($model, 'email_label', array(
//                'maxlength' => 255,
//                'wrapperHtmlOptions' => array(
//                                    'class' => 'col-sm-5',
//                                ),
//
//                                //'hint' => Yii::t('admin','Please, insert ').' Email label',
//                                'append' => Yii::t('admin','Text')
//                                )
//                      ); ?>
<!--                    -->
<!--    -->
<!--                        --><?php //echo $form->textFieldGroup($model, 'phone_label', array(
//                'maxlength' => 255,
//                'wrapperHtmlOptions' => array(
//                                    'class' => 'col-sm-5',
//                                ),
//
//                                //'hint' => Yii::t('admin','Please, insert ').' Phone label',
//                                'append' => Yii::t('admin','Text')
//                                )
//                      ); ?>
<!--                    -->
<!--    -->
<!--                        --><?php //echo $form->textFieldGroup($model, 'use_shipping_question_text', array(
//                'maxlength' => 255,
//                'wrapperHtmlOptions' => array(
//                                    'class' => 'col-sm-5',
//                                ),
//
//                                //'hint' => Yii::t('admin','Please, insert ').' Use different shipping address question text',
//                                'append' => Yii::t('admin','Text')
//                                )
//                      ); ?>
<!--                    -->
<!--    -->
<!--                        --><?php //echo $form->textFieldGroup($model, 'checkbox_text', array(
//                'maxlength' => 255,
//                'wrapperHtmlOptions' => array(
//                                    'class' => 'col-sm-5',
//                                ),
//
//                                //'hint' => Yii::t('admin','Please, insert ').' Checkbox text',
//                                'append' => Yii::t('admin','Text')
//                                )
//                      ); ?>
<!--                    -->
<!--    -->
<!--                        --><?php //echo $form->textFieldGroup($model, 'other_notes_text', array(
//                'maxlength' => 255,
//                'wrapperHtmlOptions' => array(
//                                    'class' => 'col-sm-5',
//                                ),
//
//                                //'hint' => Yii::t('admin','Please, insert ').' Other notest text',
//                                'append' => Yii::t('admin','Text')
//                                )
//                      ); ?>
                    
            
            
    

<div class='form-actions'>   <a href='<?php echo app()->request->urlReferrer;?>' class='btn btn-default'><span class='glyphicon glyphicon-arrow-left'></span><?php echo Yii::t('admin','Back');?></a>   <?php $this->widget(
        'bootstrap.widgets.TbButton',
        array(
            'buttonType' => 'submit',
            'context' => 'primary',
            'icon'=> 'glyphicon glyphicon-saved',
            'label' => Yii::t('admin','Save item')
        )
    ); ?>
    <?php
    if(Yii::app()->controller->action->id!='update') {
        $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'buttonType' => 'reset',
                'context' => 'warning',
                'icon'=> 'glyphicon glyphicon-remove',
                'label' => Yii::t('admin','Reset form')
            )
        );
    } ?>
    <?php $this->endWidget(); ?>

    <?php if(isset($model->id)){
       // echo CHtml::link(Yii::t('admin',TbHtml::icon('glyphicon glyphicon-remove'). 'Remove item'),array('delete','id'=>$model->id),array('class'=>'btn btn-danger'));
    }?>
</div>





