
<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
 ?>

<?php
$this->breadcrumbs = array(
        $model->adminNames[3]   => array('admin'),
    Yii::t('admin','View'),
);


$this->title = Yii::t('admin',
    'View {name}',
    array('{name}'=>Yii::t('sideMenu',""))
);

?>

<?php $this->widget('bootstrap.widgets.TbEditableDetailView', array(
	'data' => $model,
    'id'=>'view-table',
	'attributes' => array(
array(
			'label' => Yii::t('admin','color0'),
			'type' => 'raw',
			'value' => $model->color0 !== null ? CHtml::link(CHtml::encode(GxHtml::valueEx($model->color0)), array('color/view', 'id' => GxActiveRecord::extractPkValue($model->color0, true)),array('class'=> 'btn btn-primary btn-small')) : null,
			),
array(
			'label' => Yii::t('admin','category0'),
			'type' => 'raw',
			'value' => $model->category0 !== null ? CHtml::link(CHtml::encode(GxHtml::valueEx($model->category0)), array('productCategory/view', 'id' => GxActiveRecord::extractPkValue($model->category0, true)),array('class'=> 'btn btn-primary btn-small')) : null,
			),
	),
)); ?>

<div class="form-actions" style="text-align: right">
    <a href='<?php echo app()->request->urlReferrer?>' class='btn btn-default'><span class='glyphicon glyphicon-arrow-left'></span><?php echo Yii::t('admin','Back');?></a><?php // echo CHtml::link(TbHtml::icon('glyphicon glyphicon-briefcase'). Yii::t('admin','Manage item'),array('admin'),array('class'=>'btn btn-default'));?><?php echo CHtml::link(TbHtml::icon('glyphicon glyphicon-saved'). Yii::t('admin','Update item'),array('update','id'=>$model->id),array('class'=>'btn btn-primary'));?><?php if(user()->isAdmin):?>
<?php echo CHtml::link(TbHtml::icon('glyphicon glyphicon-remove'). Yii::t('admin','Remove item'),array('delete','id'=>$model->id),array('class'=>'btn btn-danger delete'));?><?php endif?></div>

<div id="success"  class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p><?php echo Yii::t('admin','Data was saved with success')?></p>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<?php
$text = Yii::t('admin', 'Are you sure you want to delete this item?');
$js = <<< JS
    $('.delete').on('click',function(){
        if(confirm("$text"))
            return true;
        else
            return false;
        })
JS;
cs()->registerScript('delete_confirm', $js, CClientScript::POS_READY);
?>