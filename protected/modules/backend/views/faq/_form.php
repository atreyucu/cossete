<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
?>

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'faq-form',
    'enableClientValidation' => true,

));
?>



<?php echo $form->textFieldGroup($model, 'title', array(
        'maxlength' => 255,
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-5',
        ),

        //'hint' => Yii::t('admin','Please, insert ').' Title',
        'append' => Yii::t('admin', 'Text')
    )
); ?>


<?php echo $form->textFieldGroup($model, 'question_text',
    array(
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-5',
        ),
        //'hint' =>Yii::t('admin','Please, insert ').' Question text',
        'append' => Yii::t('admin', 'Text')
    )
); ?>

<?php echo $form->redactorGroup($model, 'reply_text',
    array(
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-8',
        ),
        'widgetOptions' => array(
            'editorOptions' => array(
                'rows' => 10,
                'options' => array('plugins' => array('clips', 'fontfamily', 'fullscreen'), 'lang' => app()->getLanguage()),
                'minHeight' => 60
            )
        )
    )
); ?>


<?php echo $form->dropDownListGroup($model, 'category',
    array(
        'wrapperHtmlOptions' => array(
            'class' => 'col-sm-5',
        ),
        'widgetOptions' => array(
            'data' => GxHtml::listDataEx(FaqCategory::model()->findAll()),
            // 'htmlOptions' => array('multiple' => true),
        ),
        //'hint' => Yii::t('admin','Please, select').' category',
        'prepend' => Yii::t('admin', 'Select')
    )
); ?>


<div class='form-actions'><a href='<?php echo app()->request->urlReferrer; ?>' class='btn btn-default'><span
            class='glyphicon glyphicon-arrow-left'></span><?php echo Yii::t('admin', 'Back'); ?>
    </a> <?php $this->widget(
        'bootstrap.widgets.TbButton',
        array(
            'buttonType' => 'submit',
            'context' => 'primary',
            'icon' => 'glyphicon glyphicon-saved',
            'label' => Yii::t('admin', 'Save item')
        )
    ); ?>
    <?php
    if (Yii::app()->controller->action->id != 'update') {
        $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'buttonType' => 'reset',
                'context' => 'warning',
                'icon' => 'glyphicon glyphicon-remove',
                'label' => Yii::t('admin', 'Reset form')
            )
        );
    } ?>
    <?php $this->endWidget(); ?>

    <?php if (isset($model->id)) {
        // echo CHtml::link(Yii::t('admin',TbHtml::icon('glyphicon glyphicon-remove'). 'Remove item'),array('delete','id'=>$model->id),array('class'=>'btn btn-danger'));
    } ?>
    <div class='btn-group'>
        <a href='#' data-toggle='dropdown' class='btn btn-info dropdown-toggle'>
            <i class='fa fa-plus icon-white'></i>

            <?php echo Yii::t('admin', 'Add'); ?> <span class='caret'></span>
        </a>
        <ul class='dropdown-menu dropdown-primary'>
            <li>
                <?php echo CHtml::link(Yii::t('admin', TbHtml::icon('glyphicon glyphicon-plus-sign') . 'FaqCategory'), array('/backend/category0/create', 'id' => $model->primaryKey, 'controller' => Yii::app()->controller->id, 'action' => Yii::app()->controller->action->id), array()); ?>                </li>
        </ul>
    </div>


</div>





