
<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
 ?>

<?php
$this->breadcrumbs = array(
        $model->adminNames[3] ,Yii::t('sideMenu', 'Order')   => array('admin'),
    Yii::t('admin','View'),
);


$this->title = Yii::t('admin',
    'View {name}',
    array('{name}'=>Yii::t('sideMenu',"Order"))
);

?>

<?php $this->widget('bootstrap.widgets.TbEditableDetailView', array(
	'data' => $model,
    'id'=>'view-table',
	'attributes' => array(
array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => 'order_number',
					'sortable' => false,
					'editable' => array(
                            'type'=>'text',
                            'apply'=>true,
                            'url' => array('updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => 'order_subtotal',
					'sortable' => false,
					'editable' => array(
                            'type'=>'text',
                            'apply'=>true,
                            'url' => array('updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => 'shipping_cost',
					'sortable' => false,
					'editable' => array(
                            'type'=>'text',
                            'apply'=>true,
                            'url' => array('updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => 'order_total',
					'sortable' => false,
					'editable' => array(
                            'type'=>'text',
                            'apply'=>true,
                            'url' => array('updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
array(
                    'class' => 'bootstrap.widgets.TbEditableColumn',
                    'name' => 'status',
                    'sortable' => false,
					'editable' => array(
                        'type'=>'select',
                        'apply'=>true,
                        'placement' => 'right',
                        'inputclass' => 'span3',
                        'url' => array('updateAttribute', 'model' => get_class($model)),
                        'source' => array('1'=>Yii::t('admin','Pending'),'2'=>Yii::t('admin','Completed'),),
                        'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                            )
					),
array(
			'label' => Yii::t('admin','shippingInfo'),
			'type' => 'raw',
			'value' => $model->shippingInfo !== null ? CHtml::link(CHtml::encode(GxHtml::valueEx($model->shippingInfo)), array('transInfo/view', 'id' => GxActiveRecord::extractPkValue($model->shippingInfo, true)),array('class'=> 'btn btn-primary btn-small')) : null,
			),
array(
			'label' => Yii::t('admin','billingInfo'),
			'type' => 'raw',
			'value' => $model->billingInfo !== null ? CHtml::link(CHtml::encode(GxHtml::valueEx($model->billingInfo)), array('transInfo2/view', 'id' => GxActiveRecord::extractPkValue($model->billingInfo, true)),array('class'=> 'btn btn-primary btn-small')) : null,
			),
array(
			'label' => Yii::t('admin','client0'),
			'type' => 'raw',
			'value' => $model->client0 !== null ? CHtml::link(CHtml::encode(GxHtml::valueEx($model->client0)), array('users/view', 'id' => GxActiveRecord::extractPkValue($model->client0, true)),array('class'=> 'btn btn-primary btn-small')) : null,
			),
array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => 'notes',
					'sortable' => false,
					'editable' => array(
                            'type'=>'text',
                            'apply'=>true,
                            'url' => array('updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
	),
)); ?>

<div class="form-actions" style="text-align: right">
    <a href='<?php echo app()->request->urlReferrer?>' class='btn btn-default'><span class='glyphicon glyphicon-arrow-left'></span><?php echo Yii::t('admin','Back');?></a><?php // echo CHtml::link(TbHtml::icon('glyphicon glyphicon-briefcase'). Yii::t('admin','Manage item'),array('admin'),array('class'=>'btn btn-default'));?><?php echo CHtml::link(TbHtml::icon('glyphicon glyphicon-saved'). Yii::t('admin','Update item'),array('update','id'=>$model->id),array('class'=>'btn btn-primary'));?><?php if(user()->isAdmin):?>
<?php echo CHtml::link(TbHtml::icon('glyphicon glyphicon-remove'). Yii::t('admin','Remove item'),array('delete','id'=>$model->id),array('class'=>'btn btn-danger delete'));?><?php endif?></div>

<div id="success"  class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p><?php echo Yii::t('admin','Data was saved with success')?></p>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<?php
$text = Yii::t('admin', 'Are you sure you want to delete this item?');
$js = <<< JS
    $('.delete').on('click',function(){
        if(confirm("$text"))
            return true;
        else
            return false;
        })
JS;
cs()->registerScript('delete_confirm', $js, CClientScript::POS_READY);
?>