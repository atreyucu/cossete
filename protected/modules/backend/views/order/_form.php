
<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
 ?>

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'order-form',
    'enableClientValidation'=>true,

));
?>
        
            
    
                        <?php echo $form->textFieldGroup($model, 'order_number', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Order number',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
                    
    
                        <?php echo $form->textFieldGroup($model,'order_subtotal',
                   array(
                       'wrapperHtmlOptions' => array(
                           'class' => 'col-sm-5 currency',
                           'id' => 'id_order_subtotal',
                       ),
                       //'hint' => Yii::t('admin','Please, insert').' Order Subtotal',
				 'append' => '$'
			)
		);
                        $this->widget('ext.currency.CurrencyWidget', array('selector' => '#Order_order_subtotal','options'=>array (
  'symbol' => '$',
  'showSymbol' => false,
  'symbolStay' => false,
  'thousands' => '',
  'decimal' => '.',
  'precision' => 2,
  'defaultZero' => true,
  'allowZero' => true,
  'allowNegative' => false,
))); ?>
                    
    
                        <?php echo $form->textFieldGroup($model,'shipping_cost',
                   array(
                       'wrapperHtmlOptions' => array(
                           'class' => 'col-sm-5 currency',
                           'id' => 'id_shipping_cost',
                       ),
                       //'hint' => Yii::t('admin','Please, insert').' Shipping cost',
				 'append' => '$'
			)
		);
                        $this->widget('ext.currency.CurrencyWidget', array('selector' => '#Order_shipping_cost','options'=>array (
  'symbol' => '$',
  'showSymbol' => false,
  'symbolStay' => false,
  'thousands' => '',
  'decimal' => '.',
  'precision' => 2,
  'defaultZero' => true,
  'allowZero' => true,
  'allowNegative' => false,
))); ?>
                    
    
                        <?php echo $form->textFieldGroup($model,'order_total',
                   array(
                       'wrapperHtmlOptions' => array(
                           'class' => 'col-sm-5 currency',
                           'id' => 'id_order_total',
                       ),
                       //'hint' => Yii::t('admin','Please, insert').' Order Total',
				 'append' => '$'
			)
		);
                        $this->widget('ext.currency.CurrencyWidget', array('selector' => '#Order_order_total','options'=>array (
  'symbol' => '$',
  'showSymbol' => false,
  'symbolStay' => false,
  'thousands' => '',
  'decimal' => '.',
  'precision' => 2,
  'defaultZero' => true,
  'allowZero' => true,
  'allowNegative' => false,
))); ?>
                    
    
                        <?php echo $form->dropDownListGroup($model,'status',
                            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                'widgetOptions' => array(
                                    'data' => array(1=>Yii::t('admin','Pending'),2=>Yii::t('admin','Completed'),),
                                   // 'htmlOptions' => array('multiple' => true),
                                ),
                                 //'hint' => Yii::t('admin','Please, select').' Order status',
                                 'prepend' => Yii::t('admin','Select')
                            )
                        ); ?>
                    
    
                        <?php echo $form->dropDownListGroup($model,'shipping_info',
                            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                'widgetOptions' => array(
                                    'data' => GxHtml::listDataEx(TransInfo::model()->findAll()),
                                   // 'htmlOptions' => array('multiple' => true),
                                ),
                                 //'hint' => Yii::t('admin','Please, select').' shipping_info',
                                 'prepend' => Yii::t('admin','Select')
                            )
                        ); ?>
                    
    
                        <?php echo $form->dropDownListGroup($model,'billing_info',
                            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                'widgetOptions' => array(
                                    'data' => GxHtml::listDataEx(TransInfo::model()->findAll()),
                                   // 'htmlOptions' => array('multiple' => true),
                                ),
                                 //'hint' => Yii::t('admin','Please, select').' billing_info',
                                 'prepend' => Yii::t('admin','Select')
                            )
                        ); ?>

                        <?php
                        $client_criteria = new CDbCriteria();
                        $client_criteria->compare('is_client',1);
                        ?>
                    
    
                        <?php echo $form->dropDownListGroup($model,'client',
                            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                'widgetOptions' => array(
                                    'data' => GxHtml::listDataEx(Users::model()->findAll($client_criteria)),
                                   // 'htmlOptions' => array('multiple' => true),
                                ),
                                 //'hint' => Yii::t('admin','Please, select').' client',
                                 'prepend' => Yii::t('admin','Select')
                            )
                        ); ?>
                    
    
                        <?php echo $form->textFieldGroup($model, 'notes',
				            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                //'hint' =>Yii::t('admin','Please, insert ').' Order status',
                                'append' => Yii::t('admin','Text')
                            )
                        ); ?>
                    
            
            
    

<div class='form-actions'>   <a href='<?php echo app()->request->urlReferrer;?>' class='btn btn-default'><span class='glyphicon glyphicon-arrow-left'></span><?php echo Yii::t('admin','Back');?></a>   <?php $this->widget(
        'bootstrap.widgets.TbButton',
        array(
            'buttonType' => 'submit',
            'context' => 'primary',
            'icon'=> 'glyphicon glyphicon-saved',
            'label' => Yii::t('admin','Save item')
        )
    ); ?>
    <?php
    if(Yii::app()->controller->action->id!='update') {
        $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'buttonType' => 'reset',
                'context' => 'warning',
                'icon'=> 'glyphicon glyphicon-remove',
                'label' => Yii::t('admin','Reset form')
            )
        );
    } ?>
    <?php $this->endWidget(); ?>

    <?php if(isset($model->id)){
       // echo CHtml::link(Yii::t('admin',TbHtml::icon('glyphicon glyphicon-remove'). 'Remove item'),array('delete','id'=>$model->id),array('class'=>'btn btn-danger'));
    }?>
    <div class='btn-group'>
        <a href='#' data-toggle='dropdown' class='btn btn-info dropdown-toggle'>
            <i class='fa fa-plus icon-white'></i>

            <?php echo Yii::t('admin', 'Add'); ?>            <span class='caret'></span>
        </a>
        <ul class='dropdown-menu dropdown-primary'>
                            <li>
                    <?php echo CHtml::link(Yii::t('admin', TbHtml::icon('glyphicon glyphicon-plus-sign') . 'TransInfo'), array('/backend/shippingInfo/create', 'id' => $model->primaryKey, 'controller' => Yii::app()->controller->id, 'action' => Yii::app()->controller->action->id), array()); ?>                </li>
                            <li>
                    <?php echo CHtml::link(Yii::t('admin', TbHtml::icon('glyphicon glyphicon-plus-sign') . 'TransInfo2'), array('/backend/billingInfo/create', 'id' => $model->primaryKey, 'controller' => Yii::app()->controller->id, 'action' => Yii::app()->controller->action->id), array()); ?>                </li>
                            <li>
                    <?php echo CHtml::link(Yii::t('admin', TbHtml::icon('glyphicon glyphicon-plus-sign') . 'Users'), array('/backend/client0/create', 'id' => $model->primaryKey, 'controller' => Yii::app()->controller->id, 'action' => Yii::app()->controller->action->id), array()); ?>                </li>
                    </ul>
    </div>


</div>





