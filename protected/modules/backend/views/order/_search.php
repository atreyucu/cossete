
<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
 ?>
<?php $form = $this->beginWidget('application.extensions.bootstrap.widgets.TbActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

        <?php echo $form->textFieldGroup($model, 'id', array(
                'maxlength' => 50,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' id',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
        <?php echo $form->textFieldGroup($model, 'order_number', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Order number',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
        <?php echo $form->textFieldGroup($model,'order_subtotal',
                   array(
                       'wrapperHtmlOptions' => array(
                           'class' => 'col-sm-5 currency',
                           'id' => 'id_order_subtotal',
                       ),
                       //'hint' => Yii::t('admin','Please, insert').' Order Subtotal',
				 'append' => '$'
			)
		);
                        $this->widget('ext.currency.CurrencyWidget', array('selector' => '#Order_order_subtotal','options'=>array (
  'symbol' => '$',
  'showSymbol' => false,
  'symbolStay' => false,
  'thousands' => '',
  'decimal' => '.',
  'precision' => 2,
  'defaultZero' => true,
  'allowZero' => true,
  'allowNegative' => false,
))); ?>
        <?php echo $form->textFieldGroup($model,'shipping_cost',
                   array(
                       'wrapperHtmlOptions' => array(
                           'class' => 'col-sm-5 currency',
                           'id' => 'id_shipping_cost',
                       ),
                       //'hint' => Yii::t('admin','Please, insert').' Shipping cost',
				 'append' => '$'
			)
		);
                        $this->widget('ext.currency.CurrencyWidget', array('selector' => '#Order_shipping_cost','options'=>array (
  'symbol' => '$',
  'showSymbol' => false,
  'symbolStay' => false,
  'thousands' => '',
  'decimal' => '.',
  'precision' => 2,
  'defaultZero' => true,
  'allowZero' => true,
  'allowNegative' => false,
))); ?>
        <?php echo $form->textFieldGroup($model,'order_total',
                   array(
                       'wrapperHtmlOptions' => array(
                           'class' => 'col-sm-5 currency',
                           'id' => 'id_order_total',
                       ),
                       //'hint' => Yii::t('admin','Please, insert').' Order Total',
				 'append' => '$'
			)
		);
                        $this->widget('ext.currency.CurrencyWidget', array('selector' => '#Order_order_total','options'=>array (
  'symbol' => '$',
  'showSymbol' => false,
  'symbolStay' => false,
  'thousands' => '',
  'decimal' => '.',
  'precision' => 2,
  'defaultZero' => true,
  'allowZero' => true,
  'allowNegative' => false,
))); ?>
        <?php echo $form->dropDownListGroup($model,'status',
                            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                'widgetOptions' => array(
                                    'data' => array(1=>Yii::t('admin','Pending'),2=>Yii::t('admin','Completed'),),
                                   // 'htmlOptions' => array('multiple' => true),
                                ),
                                 //'hint' => Yii::t('admin','Please, select').' Order status',
                                 'prepend' => Yii::t('admin','Select')
                            )
                        ); ?>
        <?php echo $form->dropDownListGroup($model, 'shipping_info', GxHtml::listDataEx(TransInfo::model()->findAll()), array('prompt' => Yii::t('admin','All'))); ?>
        <?php echo $form->dropDownListGroup($model, 'billing_info', GxHtml::listDataEx(TransInfo::model()->findAll()), array('prompt' => Yii::t('admin','All'))); ?>
        <?php echo $form->dropDownListGroup($model, 'client', GxHtml::listDataEx(Users::model()->findAll()), array('prompt' => Yii::t('admin','All'))); ?>
        <?php echo $form->textFieldGroup($model, 'notes',
				            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                //'hint' =>Yii::t('admin','Please, insert ').' Order status',
                                'append' => Yii::t('admin','Text')
                            )
                        ); ?>
        <?php echo $form->textFieldGroup($model, 'created',
				            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                //'hint' =>Yii::t('admin','Please, insert ').' created',
                                'append' => Yii::t('admin','Text')
                            )
                        ); ?>
        <?php echo $form->textFieldGroup($model, 'updated',
				            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                //'hint' =>Yii::t('admin','Please, insert ').' updated',
                                'append' => Yii::t('admin','Text')
                            )
                        ); ?>
        <?php echo $form->textFieldGroup($model, 'owner', array(
                'maxlength' => 100,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' owner',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>

<div class="form-actions">
    		<?php $this->widget('application.extensions.bootstrap.widgets.TbButton',
    array(
            'buttonType' => 'submit',
            'context' => 'success',
            'icon'=> 'glyphicon glyphicon-saved',
            'label' => Yii::t('admin','Buscar '.$model->adminNames[2])
        ));
 ?></div>

<?php $this->endWidget(); ?>
