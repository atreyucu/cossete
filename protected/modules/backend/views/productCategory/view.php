
<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
 ?>

<?php
$this->breadcrumbs = array(
        $model->adminNames[3] ,Yii::t('sideMenu', 'Product categories')   => array('admin'),
    Yii::t('admin','View'),
);


$this->title = Yii::t('admin',
    'View {name}',
    array('{name}'=>Yii::t('sideMenu',"Product categories"))
);

?>

<?php $this->widget('bootstrap.widgets.TbEditableDetailView', array(
	'data' => $model,
    'id'=>'view-table',
	'attributes' => array(
array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => 'name',
					'sortable' => false,
					'editable' => array(
                            'type'=>'text',
                            'apply'=>true,
                            'url' => array('updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => 'is_active',
					'sortable' => false,
					'editable' => array(
                            'type'=>'select',
                            'apply'=>true,
                            'url' => array('updateAttribute', 'model' => get_class($model)),
                            'source' => array('0' => Yii::t('admin','No'), '1' => Yii::t('admin','Yes')),
                             'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => 'show_in_menu',
					'sortable' => false,
					'editable' => array(
                            'type'=>'select',
                            'apply'=>true,
                            'url' => array('updateAttribute', 'model' => get_class($model)),
                            'source' => array('0' => Yii::t('admin','No'), '1' => Yii::t('admin','Yes')),
                             'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
array(
                    'class' => 'bootstrap.widgets.TbEditableColumn',
                    'name' => 'position',
                    'sortable' => false,
					'editable' => array(
                        'type'=>'select',
                        'apply'=>true,
                        'placement' => 'right',
                        'inputclass' => 'span3',
                        'url' => array('updateAttribute', 'model' => get_class($model)),
                        'source' => array('1'=>Yii::t('admin','Position 1'),'2'=>Yii::t('admin','Position 2'),'3'=>Yii::t('admin','Position 3'),),
                        'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                            )
					),
array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => 'priority',
					'sortable' => false,
					'editable' => array(
                            'type'=>'text',
                            'apply'=>true,
                            'url' => array('updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => 'parent_id',
					'sortable' => false,
					'editable' => array(
                            'type'=>'text',
                            'apply'=>true,
                            'url' => array('updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
array(
				    'class' => 'bootstrap.widgets.TbEditableColumn',
					'name' => 'show_in_home',
					'sortable' => false,
					'editable' => array(
                            'type'=>'select',
                            'apply'=>true,
                            'url' => array('updateAttribute', 'model' => get_class($model)),
                            'source' => array('0' => Yii::t('admin','No'), '1' => Yii::t('admin','Yes')),
                             'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),

                    array(
                            'label' => Yii::t('admin','Home image'),
                            'type' => 'raw',
                            'value' => CHtml::image($model->_home_image->getFileUrl('normal'), $model->home_image , array('width'=> '100px')),
                        ),
                    array(
                    'class' => 'bootstrap.widgets.TbEditableColumn',
                    'name' => 'home_image',
                    'sortable' => false,
                    'editable' => array(
                        'type'=>'text',
                        'apply'=>true,
                            'url' => array('updateAttribute', 'model' => get_class($model)),
                            'placement' => 'right',
                            'inputclass' => 'span3',
                                'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                        )
					),
	),
)); ?>

<div class="form-actions" style="text-align: right">
    <a href='<?php echo app()->request->urlReferrer?>' class='btn btn-default'><span class='glyphicon glyphicon-arrow-left'></span><?php echo Yii::t('admin','Back');?></a><?php // echo CHtml::link(TbHtml::icon('glyphicon glyphicon-briefcase'). Yii::t('admin','Manage item'),array('admin'),array('class'=>'btn btn-default'));?><?php echo CHtml::link(TbHtml::icon('glyphicon glyphicon-saved'). Yii::t('admin','Update item'),array('update','id'=>$model->id),array('class'=>'btn btn-primary'));?><?php if(user()->isAdmin):?>
<?php echo CHtml::link(TbHtml::icon('glyphicon glyphicon-remove'). Yii::t('admin','Remove item'),array('delete','id'=>$model->id),array('class'=>'btn btn-danger delete'));?><?php endif?></div>

<div id="success"  class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p><?php echo Yii::t('admin','Data was saved with success')?></p>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<?php
$text = Yii::t('admin', 'Are you sure you want to delete this item?');
$js = <<< JS
    $('.delete').on('click',function(){
        if(confirm("$text"))
            return true;
        else
            return false;
        })
JS;
cs()->registerScript('delete_confirm', $js, CClientScript::POS_READY);
?>