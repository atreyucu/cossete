
<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
 ?>

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'product-category-form',
    'enableClientValidation'=>true,

));
?>


<div class="widget-box">
<div class="widget-title">
        <span class="icon">
            <i class="fa fa-file"></i>
        </span>
    <h5>Product category</h5>
</div>
<div class="widget-content nopadding">
    <?php echo $form->textFieldGroup($model, 'name', array(
            'maxlength' => 255,
            'wrapperHtmlOptions' => array(
                'class' => 'col-sm-5',
            ),

            //'hint' => Yii::t('admin','Please, insert ').' Name',
            'append' => Yii::t('admin','Text')
        )
    ); ?>

    <?php echo $form->checkBoxGroup($model, 'is_active'); ?>

    <?php echo $form->numberFieldGroup($model, 'priority',
        array(
            'wrapperHtmlOptions' => array(
                'class' => 'col-sm-5',
            ),
            'widgetOptions' => array(
                'htmlOptions' => array('min' => 1),

            ),
            //'hint' => Yii::t('admin','Please, insert ').' Number of Buildings',
            //'append' => '#'
        )
    ); ?>

    <?php echo $form->dropDownListGroup($model, 'parent_id',
        array(
            'wrapperHtmlOptions' => array(
                'class' => 'col-sm-5',
            ),
            'widgetOptions' => array(
                'data' => array($model->getCategories()),
                // 'htmlOptions' => array('multiple' => true),
            ),
            //'hint' =>Yii::t('admin','Please, insert ').' Parent category',
            'prepend' => Yii::t('admin','Select')
        )
    );

    ?>
</div>
</div>

<div class="tabbable inline">
<ul id="myTab" class="nav nav-tabs tab-bricky">
    <li class="active">
        <a href="#tour_panel_tab1" data-toggle="tab">
            Home page visibility
        </a>
    </li>
    <li>
        <a href="#tour_panel_tab2" data-toggle="tab">
            Main menu visibility
        </a>
    </li>

    <li>
        <a href="#tour_panel_tab3" data-toggle="tab">
            Seo
        </a>
    </li>
</ul>
<div class="tab-content">
    <div class="tab-pane in active" id="tour_panel_tab1">

        <?php echo $form->checkBoxGroup($model, 'show_in_home'); ?>

        <div id="home_page_div">
            <?php echo $form->dropDownListGroup($model,'position',
                array(
                    'wrapperHtmlOptions' => array(
                        'class' => 'col-sm-5',
                    ),
                    'widgetOptions' => array(
                        'data' => array(1=>Yii::t('admin','Position 1'),2=>Yii::t('admin','Position 2'),3=>Yii::t('admin','Position 3'),),
                        // 'htmlOptions' => array('multiple' => true),
                    ),
                    //'hint' => Yii::t('admin','Please, select').' Position in the home',
                    'prepend' => Yii::t('admin','Select')
                )
            ); ?>

            <?php echo $form->fileFieldGroup($model, 'recipeImg1',array(
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-5',
                ),
                'append' => CHtml::image($model->_home_image->getFileUrl('normal'), '', array('width' => '100px')),
                'hint' => Yii::t('admin','The image dimensions are 528x206px')
            ));

            echo $form->textFieldGroup($model, 'home_image',
                array(
                    'wrapperHtmlOptions' => array(
                        'class' => 'col-sm-5',
                    ),//'hint' =>Yii::t('admin','Please, insert ').' Home image',
                    'append' => 'text',
                )
            ); ?>
        </div>


    </div>



    <div class="tab-pane" id="tour_panel_tab2">
        <?php echo $form->checkBoxGroup($model, 'show_in_menu'); ?>
    </div>


<div class="tab-pane" id="tour_panel_tab3">
    <?php $model->seoFormWidget($form); ?>
</div>

</div>
</div>


<script type="text/javascript">
    $(document).ready(function(){
        if($('#ProductCategory_show_in_home').attr('checked')){
            $('#home_page_div').show();
        }
        else{
            $('#home_page_div').hide();
        }


        $('#ProductCategory_show_in_home').click(function(){
            if($('#ProductCategory_show_in_home').attr('checked')){
                $('#home_page_div').show();
            }
            else{
                $('#home_page_div').hide();
            }
        });

    });
</script>
        
            
    

                    
    

                    
    

                    
    

                    
    

                    
    

                    
    

                    
    

                    
    

                    
            
            
    

<div class='form-actions'>   <a href='<?php echo app()->request->urlReferrer;?>' class='btn btn-default'><span class='glyphicon glyphicon-arrow-left'></span><?php echo Yii::t('admin','Back');?></a>   <?php $this->widget(
        'bootstrap.widgets.TbButton',
        array(
            'buttonType' => 'submit',
            'context' => 'primary',
            'icon'=> 'glyphicon glyphicon-saved',
            'label' => Yii::t('admin','Save item')
        )
    ); ?>
    <?php
    if(Yii::app()->controller->action->id!='update') {
        $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'buttonType' => 'reset',
                'context' => 'warning',
                'icon'=> 'glyphicon glyphicon-remove',
                'label' => Yii::t('admin','Reset form')
            )
        );
    } ?>
    <?php $this->endWidget(); ?>

    <?php if(isset($model->id)){
       // echo CHtml::link(Yii::t('admin',TbHtml::icon('glyphicon glyphicon-remove'). 'Remove item'),array('delete','id'=>$model->id),array('class'=>'btn btn-danger'));
    }?>
</div>





