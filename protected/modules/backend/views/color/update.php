

<?php


$this->breadcrumbs = array(
    $model->adminNames[3] , Yii::t('sideMenu', 'Colors')  => array('admin'),
    Yii::t('admin',
        'Edit ',
        array('{name}'=>$model->adminNames[1])
    ),
);

$this->title = Yii::t('admin',
    'Edit {name}',
    array('{name}'=> Yii::t('sideMenu',"Colors"))
);

$this->renderPartial('_form', array(
	'model' => $model,
    'cats' => $cats,
    'ccats' => $ccats,
    'check_all_cats' => $check_all_cats
));

