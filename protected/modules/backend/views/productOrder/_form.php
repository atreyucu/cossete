
<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
 ?>

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'product-order-form',
    'enableClientValidation'=>true,

));
?>
        
            
    
                        <?php echo $form->textFieldGroup($model, 'pcolor', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' pcolor',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
                    
    
                        <?php echo $form->textFieldGroup($model, 'psize', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' psize',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
                    
    
                        <?php echo $form->textFieldGroup($model,'price',
			array(
				'wrapperHtmlOptions' => array(
					'class' => 'col-sm-5',
				),
				 //'hint' => Yii::t('admin','Please, insert').' price',
				 'append' => '.00'
			)
		); ?>
                    
    
                        <?php echo $form->textFieldGroup($model, 'quantity',
				            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                //'hint' =>Yii::t('admin','Please, insert ').' quantity',
                                'append' => Yii::t('admin','Text')
                            )
                        ); ?>
                    
    
                        <?php echo $form->dropDownListGroup($model,'product',
                            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                'widgetOptions' => array(
                                    'data' => GxHtml::listDataEx(Product::model()->findAll()),
                                   // 'htmlOptions' => array('multiple' => true),
                                ),
                                 //'hint' => Yii::t('admin','Please, select').' product',
                                 'prepend' => Yii::t('admin','Select')
                            )
                        ); ?>
                    
    
                        <?php echo $form->dropDownListGroup($model,'ord',
                            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                'widgetOptions' => array(
                                    'data' => GxHtml::listDataEx(Order::model()->findAll()),
                                   // 'htmlOptions' => array('multiple' => true),
                                ),
                                 //'hint' => Yii::t('admin','Please, select').' ord',
                                 'prepend' => Yii::t('admin','Select')
                            )
                        ); ?>
                    
            
            
    

<div class='form-actions'>   <a href='<?php echo app()->request->urlReferrer;?>' class='btn btn-default'><span class='glyphicon glyphicon-arrow-left'></span><?php echo Yii::t('admin','Back');?></a>   <?php $this->widget(
        'bootstrap.widgets.TbButton',
        array(
            'buttonType' => 'submit',
            'context' => 'primary',
            'icon'=> 'glyphicon glyphicon-saved',
            'label' => Yii::t('admin','Save item')
        )
    ); ?>
    <?php
    if(Yii::app()->controller->action->id!='update') {
        $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'buttonType' => 'reset',
                'context' => 'warning',
                'icon'=> 'glyphicon glyphicon-remove',
                'label' => Yii::t('admin','Reset form')
            )
        );
    } ?>
    <?php $this->endWidget(); ?>

    <?php if(isset($model->id)){
       // echo CHtml::link(Yii::t('admin',TbHtml::icon('glyphicon glyphicon-remove'). 'Remove item'),array('delete','id'=>$model->id),array('class'=>'btn btn-danger'));
    }?>
    <div class='btn-group'>
        <a href='#' data-toggle='dropdown' class='btn btn-info dropdown-toggle'>
            <i class='fa fa-plus icon-white'></i>

            <?php echo Yii::t('admin', 'Add'); ?>            <span class='caret'></span>
        </a>
        <ul class='dropdown-menu dropdown-primary'>
                            <li>
                    <?php echo CHtml::link(Yii::t('admin', TbHtml::icon('glyphicon glyphicon-plus-sign') . 'Product'), array('/backend/product0/create', 'id' => $model->primaryKey, 'controller' => Yii::app()->controller->id, 'action' => Yii::app()->controller->action->id), array()); ?>                </li>
                            <li>
                    <?php echo CHtml::link(Yii::t('admin', TbHtml::icon('glyphicon glyphicon-plus-sign') . 'Order'), array('/backend/ord0/create', 'id' => $model->primaryKey, 'controller' => Yii::app()->controller->id, 'action' => Yii::app()->controller->action->id), array()); ?>                </li>
                    </ul>
    </div>


</div>





