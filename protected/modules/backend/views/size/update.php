

<?php


$this->breadcrumbs = array(
    $model->adminNames[3] , Yii::t('sideMenu', 'Sizes')  => array('admin'),
    Yii::t('admin',
        'Edit ',
        array('{name}'=>$model->adminNames[1])
    ),
);

$this->title = Yii::t('admin',
    'Edit {name}',
    array('{name}'=> Yii::t('sideMenu',"Sizes"))
);

$this->renderPartial('_form', array(
        'model' => $model,
        'cats' => $cats,
        'ccats' => $ccats,
        'check_all_cats' => $check_all_cats,
		'model' => $model));

