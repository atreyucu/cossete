

<?php





$this->breadcrumbs = array(
    $model->adminNames[3] ,Yii::t('sideMenu', 'Sizes')  => array('admin'),
	Yii::t('admin','Add'),
);

$this->title = Yii::t('admin',
    'Create {name}',
    array('{name}'=>Yii::t('sideMenu',"Sizes"))
);

$this->renderPartial('_form', array(
    'model' => $model,
    'cats' => $cats,
    'ccats' => $ccats,
    'check_all_cats' => $check_all_cats,
    'buttons' => 'create'));

