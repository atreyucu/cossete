
<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
 ?>

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'size-form',
    'enableClientValidation'=>true,

));
?>


<div class="widget-box">
    <div class="widget-title">
        <span class="icon">
            <i class="fa fa-file"></i>
        </span>
        <h5>Color</h5>
    </div>
    <div class="widget-content nopadding">

        <?php echo $form->textFieldGroup($model, 'name', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-5',
                ),

                //'hint' => Yii::t('admin','Please, insert ').' Name',
                'append' => Yii::t('admin','Text')
            )
        ); ?>


        <?php echo $form->checkBoxGroup($model, 'is_active'); ?>

    </div>
</div>

<div class="widget-box">
    <div class="widget-title">
        <span class="icon">
            <i class="fa fa-file"></i>
        </span>
        <h5>Categories</h5>
    </div>
    <div class="widget-content nopadding">
        <table class="table table-bordered table-striped table-hover with-check">
            <thead>
            <tr>
                <th><div class="checkbox"><input type="hidden" name="catmainchechk" value="0" id="catmainchechk"><input style="cursor: pointer" type="checkbox" value="1" id="catmaincheck" name="catmaincheck" <?php if($check_all_cats):?>checked="checked"<?php endif;?>></div></th>
                <th>Category</th>

            </tr>
            </thead>
            <tbody>
            <?php
            $this->renderPartial('_catForm', array('cats' => $cats, 'ccats'=>$ccats));
            ?>
            </tbody>
        </table>
    </div>
</div>

<div class='form-actions'>   <a href='<?php echo app()->request->urlReferrer;?>' class='btn btn-default'><span class='glyphicon glyphicon-arrow-left'></span><?php echo Yii::t('admin','Back');?></a>   <?php $this->widget(
        'bootstrap.widgets.TbButton',
        array(
            'buttonType' => 'submit',
            'context' => 'primary',
            'icon'=> 'glyphicon glyphicon-saved',
            'label' => Yii::t('admin','Save item')
        )
    ); ?>
    <?php
    if(Yii::app()->controller->action->id!='update') {
        $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'buttonType' => 'reset',
                'context' => 'warning',
                'icon'=> 'glyphicon glyphicon-remove',
                'label' => Yii::t('admin','Reset form')
            )
        );
    } ?>
    <?php $this->endWidget(); ?>

    <?php if(isset($model->id)){
       // echo CHtml::link(Yii::t('admin',TbHtml::icon('glyphicon glyphicon-remove'). 'Remove item'),array('delete','id'=>$model->id),array('class'=>'btn btn-danger'));
    }?>
</div>



<script type="text/javascript">
    $('.catcheck').each(function(){
        $(this).click(function(){
            var checked_count = 0;
            $('.catcheck').each(function(){
                if($(this).attr('checked')){
                    checked_count+=1;
                }
            });

            if(checked_count == $('.catcheck').length){
                $('#catmaincheck').attr('checked','checked');
            }
            else{
                $('#catmaincheck').removeAttr('checked');
            }
        })
    });



    $('#catmaincheck').click(function(){
        if($('#catmaincheck').attr('checked')){
            $('.catcheck').each(function(){
                $(this).attr('checked','checked');
            });
        }
        else{
            $('.catcheck').each(function(){
                $(this).removeAttr('checked');
            });
        }
    });
</script>





