
<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
 ?>

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'home-banner-gallery-form',
    'enableClientValidation'=>true,

));
?>
        
            
    
                        <?php echo $form->textFieldGroup($model, 'bottom_title',
				            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                //'hint' =>Yii::t('admin','Please, insert ').' Slide bottom title',
                                'append' => Yii::t('admin','Text')
                            )
                        ); ?>
                    
    
                        <?php echo $form->textFieldGroup($model, 'middle_title',
				            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                //'hint' =>Yii::t('admin','Please, insert ').' Slide middle title',
                                'append' => Yii::t('admin','Text')
                            )
                        ); ?>
                    
    
                        <?php echo $form->textFieldGroup($model, 'title',
				            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                //'hint' =>Yii::t('admin','Please, insert ').' Slide Title',
                                'append' => Yii::t('admin','Text')
                            )
                        ); ?>
                    
    
                        <?php echo $form->fileFieldGroup($model, 'recipeImg1',array(
                            'wrapperHtmlOptions' => array(
                                'class' => 'col-sm-5',
                            ),
                            'append' => CHtml::image($model->_gallery_image->getFileUrl('normal'), '', array('width' => '100px')),
                            'hint' => Yii::t('admin','The image dimensions are 1600x802px')
                                    )); 

                 echo $form->textFieldGroup($model, 'gallery_image',
            				            array(
                                            'wrapperHtmlOptions' => array(
                                                'class' => 'col-sm-5',
                                            ),//'hint' =>Yii::t('admin','Please, insert ').' Image alt',
                                            'append' => 'text',
                                        )
                                    ); ?>
                    
    
                        <?php echo $form->dropDownListGroup($model,'home_page',
                            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                'widgetOptions' => array(
                                    'data' => GxHtml::listDataEx(HomePage::model()->findAll()),
                                   // 'htmlOptions' => array('multiple' => true),
                                ),
                                 //'hint' => Yii::t('admin','Please, select').' home_page',
                                 'prepend' => Yii::t('admin','Select')
                            )
                        ); ?>
                    
            
            
    

<div class='form-actions'>   <a href='<?php echo app()->request->urlReferrer;?>' class='btn btn-default'><span class='glyphicon glyphicon-arrow-left'></span><?php echo Yii::t('admin','Back');?></a>   <?php $this->widget(
        'bootstrap.widgets.TbButton',
        array(
            'buttonType' => 'submit',
            'context' => 'primary',
            'icon'=> 'glyphicon glyphicon-saved',
            'label' => Yii::t('admin','Save item')
        )
    ); ?>
    <?php
    if(Yii::app()->controller->action->id!='update') {
        $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'buttonType' => 'reset',
                'context' => 'warning',
                'icon'=> 'glyphicon glyphicon-remove',
                'label' => Yii::t('admin','Reset form')
            )
        );
    } ?>
    <?php $this->endWidget(); ?>

    <?php if(isset($model->id)){
       // echo CHtml::link(Yii::t('admin',TbHtml::icon('glyphicon glyphicon-remove'). 'Remove item'),array('delete','id'=>$model->id),array('class'=>'btn btn-danger'));
    }?>
    <div class='btn-group'>
        <a href='#' data-toggle='dropdown' class='btn btn-info dropdown-toggle'>
            <i class='fa fa-plus icon-white'></i>

            <?php echo Yii::t('admin', 'Add'); ?>            <span class='caret'></span>
        </a>
        <ul class='dropdown-menu dropdown-primary'>
                            <li>
                    <?php echo CHtml::link(Yii::t('admin', TbHtml::icon('glyphicon glyphicon-plus-sign') . 'HomePage'), array('/backend/homePage/create', 'id' => $model->primaryKey, 'controller' => Yii::app()->controller->id, 'action' => Yii::app()->controller->action->id), array()); ?>                </li>
                    </ul>
    </div>


</div>





