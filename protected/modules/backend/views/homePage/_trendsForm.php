<?php $index = $data->isNewRecord ? ("newtrend" . str_replace('.','',str_replace(' ', '' ,microtime()))) : $data->primaryKey ?>

<tr id="<?php echo $index?>"  class="homeTrend <?php echo $data->hasErrors() ? 'error' : '' ?>">
    <td>
        <?php echo $form->fileFieldGroup($data, "[$index]recipeImg1",array(
            'wrapperHtmlOptions' => array(
                'class' => 'col-sm-5',
            ),
            'append' => CHtml::image($data->_home_image->getFileUrl('normal'), '', array('width' => '100px')),
            'hint' => Yii::t('admin','The image dimensions are 600x900px'),
            'widgetOptions' => array(
                'htmlOptions' => array('placeholder' => 'Trend image'),
            ),
        ));
        ?>
        <?php echo Yii::t('admin','The image dimensions are 600x900px')?>
    </td>

    <td>
        <?php echo $form->textFieldGroup($data, "[$index]home_image",
            array(
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-5',
                ),//'hint' =>Yii::t('admin','Please, insert ').' Image alt',
                'append' => 'text',
                'widgetOptions' => array(
                    'htmlOptions' => array('placeholder' => 'Image alt'),
                ),
            )

        ); ?>

    </td>
    <td>
        <?php echo $form->dropDownListGroup($data,"[$index]tag_id",
            array(
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-5',
                ),
                'widgetOptions' => array(
                    'data' => GxHtml::listDataEx(Tag::model()->findAll()),
                    // 'htmlOptions' => array('multiple' => true),
                ),
                //'hint' => Yii::t('admin','Please, select').' Tag',
                'prepend' => Yii::t('admin','Select')
            )
        ); ?>
    </td>

    <td>

        <?php echo $form->textFieldGroup($data, "[$index]title", array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-5',
                ),

                //'hint' => Yii::t('admin','Please, insert ').' Image caption',
                'append' => Yii::t('admin','Text'),
                'widgetOptions' => array(
                    'htmlOptions' => array('placeholder' => 'Title'),
                ),
            )
        ); ?>

        <?php echo $form->hiddenField($data, "[$index]img_order",
            array(
            )

        ); ?>
    </td>

    <td width="100">
        <?php if (substr_count($index, 'newtrend') > 0){
            echo CHtml::link(
                CHtml::tag('i', array('class' => 'fa fa-arrow-up', 'title' => Yii::t('admin', 'Up')), ''),
                'javascript:void(0)',
                array(
                    'class' => 'del-meta',
                    'onclick' => 'js:swap_contents($(this).parents("tr").prev(),$(this).parents("tr"));swap'
                ));

            echo CHtml::link(
                CHtml::tag('i', array('class' => 'fa fa-arrow-down', 'title' => Yii::t('admin', 'Down')), ''),
                'javascript:void(0)',
                array(
                    'class' => 'del-meta',
                    'onclick' => 'js:swap_contents($(this).parents("tr").next(),$(this).parents("tr"));'
                ));

            echo CHtml::link(
                CHtml::tag('i', array('class' => 'glyphicon glyphicon-remove', 'title' => Yii::t('admin', 'Remove')), ''),
                'javascript:void(0)',
                array(
                    'class' => 'del-meta',
                    'onclick' => 'js:$(this).parents("tr").remove();'
                ));
        }
        else{
            echo CHtml::link(
                CHtml::tag('i', array('class' => 'fa fa-arrow-up', 'title' => Yii::t('admin', 'Up')), ''),
                'javascript:void(0)',
                array(
                    'class' => 'del-meta',
                    'style' => 'margin-left:2px;margin-right:2px',
                    'onclick' => 'js:swap_contents($(this).parents("tr").prev(),$(this).parents("tr"));'
                ));

            echo CHtml::link(
                CHtml::tag('i', array('class' => 'fa fa-arrow-down', 'title' => Yii::t('admin', 'Down')), ''),
                'javascript:void(0)',
                array(
                    'class' => 'del-meta',
                    'style' => 'margin-left:2px;margin-right:2px',
                    'onclick' => 'js:swap_contents($(this).parents("tr").next(),$(this).parents("tr"));'
                ));

            echo CHtml::link(
                CHtml::tag('i', array('class' => 'glyphicon glyphicon-remove', 'title' => Yii::t('admin', 'Remove')), ''),
                'javascript:void(0)',
                array(
                    'class' => 'del-meta',
                    'onclick' => "js:$(this).parents('tr').remove();$.get('/backend/trend/delete?id=$index',function(data){
                    })"
                ));
        }

        ?>
    </td>
</tr>