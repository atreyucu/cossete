<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
?>

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'home-page-form',
    'enableClientValidation' => true,

));
?>

<div class="widget-box">
    <div class="widget-title">
        <span class="icon">
            <i class="fa fa-file"></i>
        </span>
        <h5>Home Banner</h5>
    </div>
    <div class="widget-content nopadding">
        <?php $tlabel = CActiveRecord::model($banner_manager->class) ?>
        <table id="homeGallTable" class="offset1 table table-bordered table-stripped"
               style="width: 1300px; padding: 5px 5px 5px 5px;">
            <thead>
            <tr>
                <th><?php echo $tlabel->getAttributeLabel('image') ?></th>
                <th><?php echo $tlabel->getAttributeLabel('gallery_image') ?></th>
                <th><?php echo $tlabel->getAttributeLabel('title') ?></th>
                <th><?php echo $tlabel->getAttributeLabel('middle_title') ?></th>
                <th><?php echo $tlabel->getAttributeLabel('bottom_title') ?></th>
                <th>
                    <?php echo CHtml::ajaxLink(
                        CHtml::tag('i', array('class' => 'glyphicon glyphicon-plus', 'title' => Yii::t('transfer', 'Add new gallery slide')), ''),
                        array('//backend/default/addHomeGallRow'),
                        array('type' => 'POST', 'success' => 'function(data){ $("#homeGallTable tbody").append(data);add_gall(); $("#metaTable .error").tooltip(); }')
                    );
                    ?>
                </th>
            </tr>
            </thead>
            <tbody>
            <?php
            $form->type = 'inline';
            foreach ($banner_manager->getItems() as $id => $data) {
                $this->renderPartial('_galleryForm', array('form' => $form, 'id' => $id, 'data' => $data));
            }
            ?>
            </tbody>
        </table>
    </div>
</div>

<div class="tabbable inline">
    <ul id="myTab" class="nav nav-tabs tab-bricky">
        <li class="active">
            <a href="#tour_panel_tab1" data-toggle="tab">
                Trends section
            </a>
        </li>
        <li>
            <a href="#tour_panel_tab2" data-toggle="tab">
                Main category section
            </a>
        </li>

    </ul>
    <div class="tab-content">
        <div class="tab-pane in active" id="tour_panel_tab1">
            <?php $tlabel = CActiveRecord::model($trend_manager->class) ?>
            <table id="homeTrendTable" class="offset1 table table-bordered table-stripped"
                   style="width: 1300px; padding: 5px 5px 5px 5px;">
                <thead>
                <tr>
                    <th><?php echo $tlabel->getAttributeLabel('recipeImg1') ?></th>
                    <th><?php echo $tlabel->getAttributeLabel('home_image') ?></th>
                    <th><?php echo $tlabel->getAttributeLabel('tag_id') ?></th>
                    <th><?php echo $tlabel->getAttributeLabel('title') ?></th>
                    <th>
                        <?php echo CHtml::ajaxLink(
                            CHtml::tag('i', array('class' => 'glyphicon glyphicon-plus', 'title' => Yii::t('transfer', 'Add new trend')), ''),
                            array('//backend/default/addTrendRow'),
                            array('type' => 'POST', 'success' => 'function(data){ $("#homeTrendTable tbody").append(data);add_trend(); $("#metaTable .error").tooltip(); }')
                        );
                        ?>
                    </th>
                </tr>
                </thead>
                <tbody>
                <?php
                $form->type = 'inline';
                foreach ($trend_manager->getItems() as $id => $data) {
                    $this->renderPartial('_trendsForm', array('form' => $form, 'id' => $id, 'data' => $data));
                }
                ?>
                </tbody>
            </table>


        </div>


        <div class="tab-pane" id="tour_panel_tab2">


            <?php echo $form->fileFieldGroup($model, 'recipeImg1', array(
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-5',
                ),
                'append' => CHtml::image($model->_main_category_image->getFileUrl('normal'), '', array('width' => '100px')),
                'hint' => Yii::t('admin', 'The image dimensions are 100x562px')
            ));

            echo $form->textFieldGroup($model, 'main_category_image',
                array(
                    'wrapperHtmlOptions' => array(
                        'class' => 'col-sm-5',
                    ),//'hint' =>Yii::t('admin','Please, insert ').' Main category image',
                    'append' => 'text',
                )
            ); ?>


            <?php echo $form->textFieldGroup($model, 'main_category_title', array(
                    'maxlength' => 255,
                    'wrapperHtmlOptions' => array(
                        'class' => 'col-sm-5',
                    ),

                    //'hint' => Yii::t('admin','Please, insert ').' Main category title',
                    'append' => Yii::t('admin', 'Text')
                )
            ); ?>


            <?php echo $form->textFieldGroup($model, 'main_category_subtitle', array(
                    'maxlength' => 255,
                    'wrapperHtmlOptions' => array(
                        'class' => 'col-sm-5',
                    ),

                    //'hint' => Yii::t('admin','Please, insert ').' Main category subtitle',
                    'append' => Yii::t('admin', 'Text')
                )
            ); ?>


            <?php echo $form->redactorGroup($model, 'main_category_description_text',
                array(
                    'wrapperHtmlOptions' => array(
                        'class' => 'col-sm-8',
                    ),
                    'widgetOptions' => array(
                        'editorOptions' => array(
                            'rows' => 10,
                            'options' => array('plugins' => array('clips', 'fontfamily', 'fullscreen'), 'lang' => app()->getLanguage()),
                            'minHeight' => 60
                        )
                    )
                )
            ); ?>


            <?php echo $form->textFieldGroup($model, 'main_category_button', array(
                    'maxlength' => 255,
                    'wrapperHtmlOptions' => array(
                        'class' => 'col-sm-5',
                    ),

                    //'hint' => Yii::t('admin','Please, insert ').' Main category button text',
                    'append' => Yii::t('admin', 'Text')
                )
            ); ?>


            <?php echo $form->dropDownListGroup($model, 'category',
                array(
                    'wrapperHtmlOptions' => array(
                        'class' => 'col-sm-5',
                    ),
                    'widgetOptions' => array(
                        'data' => GxHtml::listDataEx(ProductCategory::model()->findAll()),
                        // 'htmlOptions' => array('multiple' => true),
                    ),
                    //'hint' => Yii::t('admin','Please, select').' category',
                    'prepend' => Yii::t('admin', 'Target category')
                )
            ); ?>

        </div>


    </div>
</div>


<div class='form-actions'><a href='<?php echo app()->request->urlReferrer; ?>' class='btn btn-default'><span
            class='glyphicon glyphicon-arrow-left'></span><?php echo Yii::t('admin', 'Back'); ?>
    </a> <?php $this->widget(
        'bootstrap.widgets.TbButton',
        array(
            'buttonType' => 'submit',
            'context' => 'primary',
            'icon' => 'glyphicon glyphicon-saved',
            'label' => Yii::t('admin', 'Save item')
        )
    ); ?>
    <?php
    if (Yii::app()->controller->action->id != 'update') {
        $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'buttonType' => 'reset',
                'context' => 'warning',
                'icon' => 'glyphicon glyphicon-remove',
                'label' => Yii::t('admin', 'Reset form')
            )
        );
    } ?>
    <?php $this->endWidget(); ?>

    <?php if (isset($model->id)) {
        // echo CHtml::link(Yii::t('admin',TbHtml::icon('glyphicon glyphicon-remove'). 'Remove item'),array('delete','id'=>$model->id),array('class'=>'btn btn-danger'));
    } ?>



</div>





