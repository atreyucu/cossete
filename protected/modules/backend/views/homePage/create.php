

<?php





$this->breadcrumbs = array(
    $model->adminNames[3] ,Yii::t('sideMenu', 'Home Page')  => array('admin'),
	Yii::t('admin','Add'),
);

$this->title = Yii::t('admin',
    'Create {name}',
    array('{name}'=>Yii::t('sideMenu',"Home Page"))
);

$this->renderPartial('_form', array(
    'model' => $model,
    'banner_manager' => $banner_manager,
    'trend_manager' => $trend_manager,
    'buttons' => 'create'));

