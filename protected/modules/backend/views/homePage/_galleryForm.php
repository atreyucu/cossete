<?php $index = $data->isNewRecord ? ("newgall" . str_replace('.','',str_replace(' ', '' ,microtime()))) : $data->primaryKey ?>

<tr id="<?php echo $index?>"  class="homeGall <?php echo $data->hasErrors() ? 'error' : '' ?>">
    <td>
        <?php echo $form->fileFieldGroup($data, "[$index]recipeImg1",array(
            'wrapperHtmlOptions' => array(
                'class' => 'col-sm-5',
            ),
            'append' => CHtml::image($data->_gallery_image->getFileUrl('normal'), '', array('width' => '100px')),
            'hint' => Yii::t('admin','The image dimensions are 1600x802px'),
            'widgetOptions' => array(
                'htmlOptions' => array('placeholder' => 'Slide Image'),
            ),
        ));
        ?>
        <?php echo Yii::t('admin','The image dimensions are 1600x802px')?>
    </td>
    <td>
        <?php echo $form->textFieldGroup($data, "[$index]gallery_image",
            array(
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-5',
                ),//'hint' =>Yii::t('admin','Please, insert ').' Image alt',
                'append' => 'text',
                'widgetOptions' => array(
                    'htmlOptions' => array('placeholder' => 'Slide Image alt'),
                ),
            )

        ); ?>

    </td>

    <td>

        <?php echo $form->textFieldGroup($data, "[$index]title", array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-5',
                ),

                //'hint' => Yii::t('admin','Please, insert ').' Image caption',
                'append' => Yii::t('admin','Text'),
                'widgetOptions' => array(
                    'htmlOptions' => array('placeholder' => 'Title'),
                ),
            )
        ); ?>



    </td>

    <td>

        <?php echo $form->textFieldGroup($data, "[$index]middle_title", array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-5',
                ),

                //'hint' => Yii::t('admin','Please, insert ').' Image caption',
                'append' => Yii::t('admin','Text'),
                'widgetOptions' => array(
                    'htmlOptions' => array('placeholder' => 'Middle title'),
                ),
            )
        ); ?>



    </td>

    <td>

        <?php echo $form->textFieldGroup($data, "[$index]bottom_title", array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-5',
                ),

                //'hint' => Yii::t('admin','Please, insert ').' Image caption',
                'append' => Yii::t('admin','Text'),
                'widgetOptions' => array(
                    'htmlOptions' => array('placeholder' => 'Bottom title'),
                ),
            )
        ); ?>

        <?php echo $form->hiddenField($data, "[$index]img_order",
            array(
            )

        ); ?>


    </td>
    <td width="100">
        <?php if (substr_count($index, 'newgall') > 0){
            echo CHtml::link(
                CHtml::tag('i', array('class' => 'fa fa-arrow-up', 'title' => Yii::t('admin', 'Up')), ''),
                'javascript:void(0)',
                array(
                    'class' => 'del-meta',
                    'onclick' => 'js:swap_contents($(this).parents("tr").prev(),$(this).parents("tr"));swap'
                ));

            echo CHtml::link(
                CHtml::tag('i', array('class' => 'fa fa-arrow-down', 'title' => Yii::t('admin', 'Down')), ''),
                'javascript:void(0)',
                array(
                    'class' => 'del-meta',
                    'onclick' => 'js:swap_contents($(this).parents("tr").next(),$(this).parents("tr"));'
                ));

            echo CHtml::link(
                CHtml::tag('i', array('class' => 'glyphicon glyphicon-remove', 'title' => Yii::t('admin', 'Remove')), ''),
                'javascript:void(0)',
                array(
                    'class' => 'del-meta',
                    'onclick' => 'js:$(this).parents("tr").remove();'
                ));
        }
        else{

            echo CHtml::link(
                CHtml::tag('i', array('class' => 'fa fa-arrow-up', 'title' => Yii::t('admin', 'Up')), ''),
                'javascript:void(0)',
                array(
                    'class' => 'del-meta',
                    'style' => 'margin-left:2px;margin-right:2px',
                    'onclick' => 'js:swap_contents($(this).parents("tr").prev(),$(this).parents("tr"));'
                ));

            echo CHtml::link(
                CHtml::tag('i', array('class' => 'fa fa-arrow-down', 'title' => Yii::t('admin', 'Down')), ''),
                'javascript:void(0)',
                array(
                    'class' => 'del-meta',
                    'style' => 'margin-left:2px;margin-right:2px',
                    'onclick' => 'js:swap_contents($(this).parents("tr").next(),$(this).parents("tr"));'
                ));

            echo CHtml::link(
                CHtml::tag('i', array('class' => 'glyphicon glyphicon-remove', 'title' => Yii::t('admin', 'Remove')), ''),
                'javascript:void(0)',
                array(
                    'class' => 'del-meta',
                    'onclick' => "js:$(this).parents('tr').remove();$.get('/backend/homeBannerGallery/delete?id=$index',function(data){
                    })"
                ));
        }

        ?>
    </td>
</tr>