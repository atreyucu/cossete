
<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
 ?>
<?php $form = $this->beginWidget('application.extensions.bootstrap.widgets.TbActiveForm', array(
	'action' => Yii::app()->createUrl($this->route),
	'method' => 'get',
)); ?>

        <?php echo $form->textFieldGroup($model, 'id', array(
                'maxlength' => 50,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' id',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
        <?php echo $form->textFieldGroup($model, 'breadcrumb_title', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Breadcrumb title',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
        <?php echo $form->redactorGroup($model, 'page_description',
                        array(
                            'wrapperHtmlOptions' => array(
                                'class' => 'col-sm-8',
                            ),
                            'widgetOptions' => array(
                                'editorOptions' => array(
                                    'rows' => 10,
                                    'options' => array('plugins' => array('clips', 'fontfamily', 'fullscreen'), 'lang' => app()->getLanguage()),
                                    'minHeight' => 60
                                )
                            )
                        )
                    ); ?>
        <?php echo $form->textFieldGroup($model, 'section1_title', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Section1 title',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
        <?php echo $form->textFieldGroup($model, 'section1_subtitle', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Section1 subtitle',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
        <?php echo $form->fileFieldGroup($model, 'recipeImg1',array(
                            'wrapperHtmlOptions' => array(
                                'class' => 'col-sm-5',
                            ),
                            'append' => CHtml::image($model->_section1_about_main_banner->getFileUrl('normal'), '', array('width' => '100px')),
                            'hint' => Yii::t('admin','The image dimensions are 800x600px')
                                    )); 

                 echo $form->textFieldGroup($model, 'section1_about_main_banner',
            				            array(
                                            'wrapperHtmlOptions' => array(
                                                'class' => 'col-sm-5',
                                            ),//'hint' =>Yii::t('admin','Please, insert ').' Section 1 about main banner image ',
                                            'append' => 'text',
                                        )
                                    ); ?>
        <?php echo $form->fileFieldGroup($model, 'recipeImg2',array(
                            'wrapperHtmlOptions' => array(
                                'class' => 'col-sm-5',
                            ),
                            'append' => CHtml::image($model->_section1_about_banner1->getFileUrl('normal'), '', array('width' => '100px')),
                            'hint' => Yii::t('admin','The image dimensions are 800x300px')
                                    )); 

                 echo $form->textFieldGroup($model, 'section1_about_banner1',
            				            array(
                                            'wrapperHtmlOptions' => array(
                                                'class' => 'col-sm-5',
                                            ),//'hint' =>Yii::t('admin','Please, insert ').' Section 1 about banner image1 ',
                                            'append' => 'text',
                                        )
                                    ); ?>
        <?php echo $form->fileFieldGroup($model, 'recipeImg3',array(
                            'wrapperHtmlOptions' => array(
                                'class' => 'col-sm-5',
                            ),
                            'append' => CHtml::image($model->_section1_about_banner2->getFileUrl('normal'), '', array('width' => '100px')),
                            'hint' => Yii::t('admin','The image dimensions are 800x300px')
                                    )); 

                 echo $form->textFieldGroup($model, 'section1_about_banner2',
            				            array(
                                            'wrapperHtmlOptions' => array(
                                                'class' => 'col-sm-5',
                                            ),//'hint' =>Yii::t('admin','Please, insert ').' Section 1 about banner image2 ',
                                            'append' => 'text',
                                        )
                                    ); ?>
        <?php echo $form->textFieldGroup($model, 'section2_title', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Section 2 title',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
        <?php echo $form->textFieldGroup($model, 'section2_subtitle', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' Section 2 subtitle',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>
        <?php echo $form->redactorGroup($model, 'section2_description',
                        array(
                            'wrapperHtmlOptions' => array(
                                'class' => 'col-sm-8',
                            ),
                            'widgetOptions' => array(
                                'editorOptions' => array(
                                    'rows' => 10,
                                    'options' => array('plugins' => array('clips', 'fontfamily', 'fullscreen'), 'lang' => app()->getLanguage()),
                                    'minHeight' => 60
                                )
                            )
                        )
                    ); ?>
        <?php echo $form->fileFieldGroup($model, 'recipeImg4',array(
                            'wrapperHtmlOptions' => array(
                                'class' => 'col-sm-5',
                            ),
                            'append' => CHtml::image($model->_section2_image->getFileUrl('normal'), '', array('width' => '100px')),
                            'hint' => Yii::t('admin','The image dimensions are 605x644px')
                                    )); 

                 echo $form->textFieldGroup($model, 'section2_image',
            				            array(
                                            'wrapperHtmlOptions' => array(
                                                'class' => 'col-sm-5',
                                            ),//'hint' =>Yii::t('admin','Please, insert ').' Section 2 image',
                                            'append' => 'text',
                                        )
                                    ); ?>
        <?php echo $form->textFieldGroup($model, 'created',
				            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                //'hint' =>Yii::t('admin','Please, insert ').' created',
                                'append' => Yii::t('admin','Text')
                            )
                        ); ?>
        <?php echo $form->textFieldGroup($model, 'updated',
				            array(
                                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                //'hint' =>Yii::t('admin','Please, insert ').' updated',
                                'append' => Yii::t('admin','Text')
                            )
                        ); ?>
        <?php echo $form->textFieldGroup($model, 'owner', array(
                'maxlength' => 100,
                'wrapperHtmlOptions' => array(
                                    'class' => 'col-sm-5',
                                ),
                                 
                                //'hint' => Yii::t('admin','Please, insert ').' owner',
                                'append' => Yii::t('admin','Text')
                                )
                      ); ?>

<div class="form-actions">
    		<?php $this->widget('application.extensions.bootstrap.widgets.TbButton',
    array(
            'buttonType' => 'submit',
            'context' => 'success',
            'icon'=> 'glyphicon glyphicon-saved',
            'label' => Yii::t('admin','Buscar '.$model->adminNames[2])
        ));
 ?></div>

<?php $this->endWidget(); ?>
