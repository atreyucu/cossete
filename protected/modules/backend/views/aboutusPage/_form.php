
<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
 ?>
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'aboutus-page-form',
    'enableClientValidation'=>true,

));
?>


<div class="widget-box">
    <div class="widget-title">
        <span class="icon">
            <i class="fa fa-file"></i>
        </span>
        <h5>About Us</h5>
    </div>
    <div class="widget-content nopadding">

        <?php echo $form->textFieldGroup($model, 'breadcrumb_title', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-5',
                ),

                //'hint' => Yii::t('admin','Please, insert ').' Breadcrumb title',
                'append' => Yii::t('admin','Text')
            )
        ); ?>

        <?php echo $form->textFieldGroup($model, 'description_title', array(
                'maxlength' => 255,
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-5',
                ),

                //'hint' => Yii::t('admin','Please, insert ').' Breadcrumb title',
                'append' => Yii::t('admin','Text')
            )
        ); ?>


        <?php echo $form->redactorGroup($model, 'page_description',
            array(
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-8',
                ),
                'widgetOptions' => array(
                    'editorOptions' => array(
                        'rows' => 10,
                        'options' => array('plugins' => array('clips', 'fontfamily', 'fullscreen'), 'lang' => app()->getLanguage()),
                        'minHeight' => 60
                    )
                )
            )
        ); ?>
    </div>
</div>

<div class="tabbable inline">
    <ul id="myTab" class="nav nav-tabs tab-bricky">
        <li class="active">
            <a href="#tour_panel_tab1" data-toggle="tab">
                Section 1
            </a>
        </li>
        <li>
            <a href="#tour_panel_tab2" data-toggle="tab">
                Section 2
            </a>
        </li>

    </ul>
    <div class="tab-content">
        <div class="tab-pane in active" id="tour_panel_tab1">
            <?php echo $form->textFieldGroup($model, 'section1_title', array(
                    'maxlength' => 255,
                    'wrapperHtmlOptions' => array(
                        'class' => 'col-sm-5',
                    ),

                    //'hint' => Yii::t('admin','Please, insert ').' Section1 title',
                    'append' => Yii::t('admin','Text')
                )
            ); ?>


            <?php echo $form->textFieldGroup($model, 'section1_subtitle', array(
                    'maxlength' => 255,
                    'wrapperHtmlOptions' => array(
                        'class' => 'col-sm-5',
                    ),

                    //'hint' => Yii::t('admin','Please, insert ').' Section1 subtitle',
                    'append' => Yii::t('admin','Text')
                )
            ); ?>




            <?php echo $form->fileFieldGroup($model, 'recipeImg1',array(
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-5',
                ),
                'append' => CHtml::image($model->_section1_about_main_banner->getFileUrl('normal'), '', array('width' => '100px')),
                'hint' => Yii::t('admin','The image dimensions are 800x600px')
            ));

            echo $form->textFieldGroup($model, 'section1_about_main_banner',
                array(
                    'wrapperHtmlOptions' => array(
                        'class' => 'col-sm-5',
                    ),//'hint' =>Yii::t('admin','Please, insert ').' Section 1 about main banner image ',
                    'append' => 'text',
                )
            ); ?>


            <?php echo $form->fileFieldGroup($model, 'recipeImg2',array(
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-5',
                ),
                'append' => CHtml::image($model->_section1_about_banner1->getFileUrl('normal'), '', array('width' => '100px')),
                'hint' => Yii::t('admin','The image dimensions are 800x300px')
            ));

            echo $form->textFieldGroup($model, 'section1_about_banner1',
                array(
                    'wrapperHtmlOptions' => array(
                        'class' => 'col-sm-5',
                    ),//'hint' =>Yii::t('admin','Please, insert ').' Section 1 about banner image1 ',
                    'append' => 'text',
                )
            ); ?>

            <?php echo $form->fileFieldGroup($model, 'recipeImg3',array(
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-5',
                ),
                'append' => CHtml::image($model->_section1_about_banner2->getFileUrl('normal'), '', array('width' => '100px')),
                'hint' => Yii::t('admin','The image dimensions are 800x300px')
            ));

            echo $form->textFieldGroup($model, 'section1_about_banner2',
                array(
                    'wrapperHtmlOptions' => array(
                        'class' => 'col-sm-5',
                    ),//'hint' =>Yii::t('admin','Please, insert ').' Section 1 about banner image2 ',
                    'append' => 'text',
                )
            ); ?>

        </div>



        <div class="tab-pane" id="tour_panel_tab2">


            <?php echo $form->textFieldGroup($model, 'section2_title', array(
                    'maxlength' => 255,
                    'wrapperHtmlOptions' => array(
                        'class' => 'col-sm-5',
                    ),

                    //'hint' => Yii::t('admin','Please, insert ').' Section 2 title',
                    'append' => Yii::t('admin','Text')
                )
            ); ?>


            <?php echo $form->textFieldGroup($model, 'section2_subtitle', array(
                    'maxlength' => 255,
                    'wrapperHtmlOptions' => array(
                        'class' => 'col-sm-5',
                    ),

                    //'hint' => Yii::t('admin','Please, insert ').' Section 2 subtitle',
                    'append' => Yii::t('admin','Text')
                )
            ); ?>


            <?php echo $form->redactorGroup($model, 'section2_description',
                array(
                    'wrapperHtmlOptions' => array(
                        'class' => 'col-sm-8',
                    ),
                    'widgetOptions' => array(
                        'editorOptions' => array(
                            'rows' => 10,
                            'options' => array('plugins' => array('clips', 'fontfamily', 'fullscreen'), 'lang' => app()->getLanguage()),
                            'minHeight' => 60
                        )
                    )
                )
            ); ?>


            <?php echo $form->textFieldGroup($model, 'section2_link1_text', array(
                    'maxlength' => 255,
                    'wrapperHtmlOptions' => array(
                        'class' => 'col-sm-5',
                    ),

                    //'hint' => Yii::t('admin','Please, insert ').' Breadcrumb title',
                    'append' => Yii::t('admin','Text')
                )
            ); ?>

            <?php echo $form->textFieldGroup($model, 'section2_link2_text', array(
                    'maxlength' => 255,
                    'wrapperHtmlOptions' => array(
                        'class' => 'col-sm-5',
                    ),

                    //'hint' => Yii::t('admin','Please, insert ').' Breadcrumb title',
                    'append' => Yii::t('admin','Text')
                )
            ); ?>


            <?php echo $form->fileFieldGroup($model, 'recipeImg4',array(
                'wrapperHtmlOptions' => array(
                    'class' => 'col-sm-5',
                ),
                'append' => CHtml::image($model->_section2_image->getFileUrl('normal'), '', array('width' => '100px')),
                'hint' => Yii::t('admin','The image dimensions are 605x644px')
            ));

            echo $form->textFieldGroup($model, 'section2_image',
                array(
                    'wrapperHtmlOptions' => array(
                        'class' => 'col-sm-5',
                    ),//'hint' =>Yii::t('admin','Please, insert ').' Section 2 image',
                    'append' => 'text',
                )
            ); ?>


        </div>


    </div>
</div>

<div class='form-actions'>   <a href='<?php echo app()->request->urlReferrer;?>' class='btn btn-default'><span class='glyphicon glyphicon-arrow-left'></span><?php echo Yii::t('admin','Back');?></a>   <?php $this->widget(
        'bootstrap.widgets.TbButton',
        array(
            'buttonType' => 'submit',
            'context' => 'primary',
            'icon'=> 'glyphicon glyphicon-saved',
            'label' => Yii::t('admin','Save item')
        )
    ); ?>
    <?php
    if(Yii::app()->controller->action->id!='update') {
        $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'buttonType' => 'reset',
                'context' => 'warning',
                'icon'=> 'glyphicon glyphicon-remove',
                'label' => Yii::t('admin','Reset form')
            )
        );
    } ?>
    <?php $this->endWidget(); ?>

    <?php if(isset($model->id)){
       // echo CHtml::link(Yii::t('admin',TbHtml::icon('glyphicon glyphicon-remove'). 'Remove item'),array('delete','id'=>$model->id),array('class'=>'btn btn-danger'));
    }?>
</div>





