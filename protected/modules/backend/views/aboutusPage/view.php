
<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
 ?>

<?php
$this->breadcrumbs = array(
        $model->adminNames[3] ,Yii::t('sideMenu', 'About Us')   => array('admin'),
    Yii::t('admin','View'),
);


$this->title = Yii::t('admin',
    'View {name}',
    array('{name}'=>Yii::t('sideMenu',"About Us"))
);

?>

<div class="widget-box">
    <div class="widget-title">
        <span class="icon">
            <i class="fa fa-file"></i>
        </span>
        <h5>About Us</h5>
    </div>
    <div class="widget-content nopadding">

    <?php $this->widget('bootstrap.widgets.TbEditableDetailView', array(
        'data' => $model,
        'id'=>'view-table',
        'attributes' => array(
            array(
                'class' => 'bootstrap.widgets.TbEditableColumn',
                'name' => 'breadcrumb_title',
                'sortable' => false,
                'editable' => array(
                    'type'=>'text',
                    'apply'=>true,
                    'url' => array('updateAttribute', 'model' => get_class($model)),
                    'placement' => 'right',
                    'inputclass' => 'span3',
                    'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                )
            ),

            array(
                'class' => 'bootstrap.widgets.TbEditableColumn',
                'name' => 'description_title',
                'sortable' => false,
                'editable' => array(
                    'type'=>'text',
                    'apply'=>true,
                    'url' => array('updateAttribute', 'model' => get_class($model)),
                    'placement' => 'right',
                    'inputclass' => 'span3',
                    'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                )
            ),
            array(
                'class' => 'bootstrap.widgets.TbEditableColumn',
                'name' => 'page_description',
                'sortable' => false,
                'editable' => array(
                    'type'=>'textarea',
                    'apply'=>true,
                    'url' => array('updateAttribute', 'model' => get_class($model)),
                    'placement' => 'right',
                    'inputclass' => 'span3',
                    'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                )
            ),
        ),
    )); ?>
    </div>
</div>

<div class="tabbable inline">
    <ul id="myTab" class="nav nav-tabs tab-bricky">
        <li class="active">
            <a href="#tour_panel_tab1" data-toggle="tab">
                Section 1
            </a>
        </li>
        <li>
            <a href="#tour_panel_tab2" data-toggle="tab">
                Section 2
            </a>
        </li>

    </ul>
    <div class="tab-content">
        <div class="tab-pane in active" id="tour_panel_tab1">
        <?php $this->widget('bootstrap.widgets.TbEditableDetailView', array(
            'data' => $model,
            'id'=>'view-table',
            'attributes' => array(
                array(
                    'class' => 'bootstrap.widgets.TbEditableColumn',
                    'name' => 'breadcrumb_title',
                    'sortable' => false,
                    'editable' => array(
                        'type'=>'text',
                        'apply'=>true,
                        'url' => array('updateAttribute', 'model' => get_class($model)),
                        'placement' => 'right',
                        'inputclass' => 'span3',
                        'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                    )
                ),

                array(
                    'class' => 'bootstrap.widgets.TbEditableColumn',
                    'name' => 'description_title',
                    'sortable' => false,
                    'editable' => array(
                        'type'=>'text',
                        'apply'=>true,
                        'url' => array('updateAttribute', 'model' => get_class($model)),
                        'placement' => 'right',
                        'inputclass' => 'span3',
                        'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                    )
                ),
                array(
                    'class' => 'bootstrap.widgets.TbEditableColumn',
                    'name' => 'page_description',
                    'sortable' => false,
                    'editable' => array(
                        'type'=>'textarea',
                        'apply'=>true,
                        'url' => array('updateAttribute', 'model' => get_class($model)),
                        'placement' => 'right',
                        'inputclass' => 'span3',
                        'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                    )
                ),
                array(
                    'class' => 'bootstrap.widgets.TbEditableColumn',
                    'name' => 'section1_title',
                    'sortable' => false,
                    'editable' => array(
                        'type'=>'text',
                        'apply'=>true,
                        'url' => array('updateAttribute', 'model' => get_class($model)),
                        'placement' => 'right',
                        'inputclass' => 'span3',
                        'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                    )
                ),
                array(
                    'class' => 'bootstrap.widgets.TbEditableColumn',
                    'name' => 'section1_subtitle',
                    'sortable' => false,
                    'editable' => array(
                        'type'=>'text',
                        'apply'=>true,
                        'url' => array('updateAttribute', 'model' => get_class($model)),
                        'placement' => 'right',
                        'inputclass' => 'span3',
                        'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                    )
                ),

                array(
                    'label' => Yii::t('admin','Section 1 about main banner image '),
                    'type' => 'raw',
                    'value' => CHtml::image($model->_section1_about_main_banner->getFileUrl('normal'), $model->section1_about_main_banner , array('width'=> '100px')),
                ),
                array(
                    'class' => 'bootstrap.widgets.TbEditableColumn',
                    'name' => 'section1_about_main_banner',
                    'sortable' => false,
                    'editable' => array(
                        'type'=>'text',
                        'apply'=>true,
                        'url' => array('updateAttribute', 'model' => get_class($model)),
                        'placement' => 'right',
                        'inputclass' => 'span3',
                        'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                    )
                ),

                array(
                    'label' => Yii::t('admin','Section 1 about banner image1 '),
                    'type' => 'raw',
                    'value' => CHtml::image($model->_section1_about_banner1->getFileUrl('normal'), $model->section1_about_banner1 , array('width'=> '100px')),
                ),
                array(
                    'class' => 'bootstrap.widgets.TbEditableColumn',
                    'name' => 'section1_about_banner1',
                    'sortable' => false,
                    'editable' => array(
                        'type'=>'text',
                        'apply'=>true,
                        'url' => array('updateAttribute', 'model' => get_class($model)),
                        'placement' => 'right',
                        'inputclass' => 'span3',
                        'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                    )
                ),

                array(
                    'label' => Yii::t('admin','Section 1 about banner image2 '),
                    'type' => 'raw',
                    'value' => CHtml::image($model->_section1_about_banner2->getFileUrl('normal'), $model->section1_about_banner2 , array('width'=> '100px')),
                ),
                array(
                    'class' => 'bootstrap.widgets.TbEditableColumn',
                    'name' => 'section1_about_banner2',
                    'sortable' => false,
                    'editable' => array(
                        'type'=>'text',
                        'apply'=>true,
                        'url' => array('updateAttribute', 'model' => get_class($model)),
                        'placement' => 'right',
                        'inputclass' => 'span3',
                        'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                    )
                ),
            ),
        )); ?>


        </div>



        <div class="tab-pane" id="tour_panel_tab2">
        <?php $this->widget('bootstrap.widgets.TbEditableDetailView', array(
            'data' => $model,
            'id'=>'view-table',
            'attributes' => array(
                array(
                    'class' => 'bootstrap.widgets.TbEditableColumn',
                    'name' => 'section2_title',
                    'sortable' => false,
                    'editable' => array(
                        'type'=>'text',
                        'apply'=>true,
                        'url' => array('updateAttribute', 'model' => get_class($model)),
                        'placement' => 'right',
                        'inputclass' => 'span3',
                        'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                    )
                ),
                array(
                    'class' => 'bootstrap.widgets.TbEditableColumn',
                    'name' => 'section2_subtitle',
                    'sortable' => false,
                    'editable' => array(
                        'type'=>'text',
                        'apply'=>true,
                        'url' => array('updateAttribute', 'model' => get_class($model)),
                        'placement' => 'right',
                        'inputclass' => 'span3',
                        'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                    )
                ),
                array(
                    'class' => 'bootstrap.widgets.TbEditableColumn',
                    'name' => 'section2_description',
                    'sortable' => false,
                    'editable' => array(
                        'type'=>'textarea',
                        'apply'=>true,
                        'url' => array('updateAttribute', 'model' => get_class($model)),
                        'placement' => 'right',
                        'inputclass' => 'span3',
                        'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                    )
                ),

                array(
                    'class' => 'bootstrap.widgets.TbEditableColumn',
                    'name' => 'section2_link1_text',
                    'sortable' => false,
                    'editable' => array(
                        'type'=>'text',
                        'apply'=>true,
                        'url' => array('updateAttribute', 'model' => get_class($model)),
                        'placement' => 'right',
                        'inputclass' => 'span3',
                        'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                    )
                ),

                array(
                    'class' => 'bootstrap.widgets.TbEditableColumn',
                    'name' => 'section2_link2_text',
                    'sortable' => false,
                    'editable' => array(
                        'type'=>'text',
                        'apply'=>true,
                        'url' => array('updateAttribute', 'model' => get_class($model)),
                        'placement' => 'right',
                        'inputclass' => 'span3',
                        'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                    )
                ),

                array(
                    'label' => Yii::t('admin','Section 2 image'),
                    'type' => 'raw',
                    'value' => CHtml::image($model->_section2_image->getFileUrl('normal'), $model->section2_image , array('width'=> '100px')),
                ),
                array(
                    'class' => 'bootstrap.widgets.TbEditableColumn',
                    'name' => 'section2_image',
                    'sortable' => false,
                    'editable' => array(
                        'type'=>'text',
                        'apply'=>true,
                        'url' => array('updateAttribute', 'model' => get_class($model)),
                        'placement' => 'right',
                        'inputclass' => 'span3',
                        'success' => "js: function(response, newValue) {
                    if (!response.success)
                        $('#success').modal('toggle');
                        setTimeout(function(){
                            $('#success').modal('toggle');
                        }, 2000);
                }",
                    )
                ),
            ),
        )); ?>

        </div>


    </div>
</div>



<div class="form-actions" style="text-align: right">
    <a href='<?php echo app()->request->urlReferrer?>' class='btn btn-default'><span class='glyphicon glyphicon-arrow-left'></span><?php echo Yii::t('admin','Back');?></a><?php // echo CHtml::link(TbHtml::icon('glyphicon glyphicon-briefcase'). Yii::t('admin','Manage item'),array('admin'),array('class'=>'btn btn-default'));?><?php echo CHtml::link(TbHtml::icon('glyphicon glyphicon-saved'). Yii::t('admin','Update item'),array('update','id'=>$model->id),array('class'=>'btn btn-primary'));?><?php if(user()->isAdmin):?>
<?php echo CHtml::link(TbHtml::icon('glyphicon glyphicon-remove'). Yii::t('admin','Remove item'),array('delete','id'=>$model->id),array('class'=>'btn btn-danger delete'));?><?php endif?></div>

<div id="success"  class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p><?php echo Yii::t('admin','Data was saved with success')?></p>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
<?php
$text = Yii::t('admin', 'Are you sure you want to delete this item?');
$js = <<< JS
    $('.delete').on('click',function(){
        if(confirm("$text"))
            return true;
        else
            return false;
        })
JS;
cs()->registerScript('delete_confirm', $js, CClientScript::POS_READY);
?>