<?php

class BackendModule extends CWebModule
{
    public $controllerMap = array(
        'gallery'=>array(
            'class'=>'application.extensions.GalleryManager.GalleryController'
        )
    );

    public function getName()
    {
        return 'Backend';
    }

    public function init()
    {
        // this method is called when the module is being created
        // you may place code here to customize the module or the application

        $this->attachBehavior('module', array(
            'class' => 'application.components.ModuleBehavior'
        ));

        // import the module-level models and components
        $this->setImport(array(
            'backend.models.*',
            'backend.components.*',
        ));
    }


    public function getAdminMenus()
    {
        parent::getAdminMenus();

        $menu = array(
            array(
                'url' => '#',
                'icon' => 'tags',
                'label' => Yii::t('sideMenu', 'General'),
                'visible' => user()->isAdmin,
                'items' => array(
                    array(
                        'icon' => 'list',
                        'label' => Yii::t('sideMenu', 'Menu'),
                        'url' => array('/backend/mainMenu/admin'),
                        'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'mainMenu',
                        'visible' => user()->isAdmin,
                    ),
                    array(
                        'icon' => 'file',
                        'label' => Yii::t('sideMenu', 'Footer'),
                        'url' => array('/backend/footer/admin'),
                        'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'footer',
                        'visible' => user()->isAdmin,
                    ),
                    array(
                        'icon' => 'link',
                        'label' => Yii::t('sideMenu', 'Social media'),
                        'url' => array('/backend/socialLinks/admin'),
                        'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'socialLinks',
                        'visible' => user()->isAdmin,
                    ),
                    array(
                        'icon' => 'paperclip',
                        'label' => Yii::t('sideMenu', 'Company Information'),
                        'url' => array('/backend/companyInfo/admin'),
                        'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'companyInfo',
                        'visible' => user()->isAdmin,
                    ),

                ),
            ),
            array(
                'url' => '#',
                'icon' => 'tags',
                'label' => Yii::t('sideMenu', 'Site Pages'),
                'visible' => user()->isAdmin,
                'items' => array(
                    array(
                        'icon' => 'file',
                        'label' => Yii::t('sideMenu', 'About us'),
                        'url' => array('/backend/aboutusPage/admin'),
                        'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'aboutusPage',
                        'visible' => user()->isAdmin,
                    ),
                    array(
                        'icon' => 'file',
                        'label' => Yii::t('sideMenu', 'Contact us'),
                        'url' => array('/backend/contactusPage/admin'),
                        'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'contactusPage',
                        'visible' => user()->isAdmin,
                    ),
                    array(
                        'icon' => 'file',
                        'label' => Yii::t('sideMenu', 'Error page'),
                        'url' => array('/backend/errorPage/admin'),
                        'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'errorPage',
                        'visible' => user()->isAdmin,
                    ),
                    array(
                        'icon' => 'file',
                        'label' => Yii::t('sideMenu', 'Thank page'),
                        'url' => array('/backend/thankPage/admin'),
                        'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'thankPage',
                        'visible' => user()->isAdmin,
                    ),
                    array(
                        'icon' => 'file',
                        'label' => Yii::t('sideMenu', 'Recover page'),
                        'url' => array('/backend/recoverPage/admin'),
                        'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'recoverPage',
                        'visible' => user()->isAdmin,
                    ),
                    array(
                        'icon' => 'file',
                        'label' => Yii::t('sideMenu', 'Register page'),
                        'url' => array('/backend/registerPage/admin'),
                        'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'registerPage',
                        'visible' => user()->isAdmin,
                    ),
                    array(
                        'icon' => 'file',
                        'label' => Yii::t('sideMenu', 'Terms & conditions'),
                        'url' => array('/backend/termConds/admin'),
                        'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'termConds',
                        'visible' => user()->isAdmin,
                    ),
                    array(
                        'icon' => 'file',
                        'label' => Yii::t('sideMenu', 'Checkout'),
                        'url' => array('/backend/checkoutPage/admin'),
                        'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'checkoutPage',
                        'visible' => user()->isAdmin,
                    ),
                    array(
                        'icon' => 'file',
                        'label' => Yii::t('sideMenu', 'Shopping cart'),
                        'url' => array('/backend/shoppingCartPage/admin'),
                        'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'shoppingCartPage',
                        'visible' => user()->isAdmin,
                    ),
                    array(
                        'icon' => 'file',
                        'label' => Yii::t('sideMenu', 'My orders'),
                        'url' => array('/backend/myOrdersPage/admin'),
                        'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'myOrdersPage',
                        'visible' => user()->isAdmin,
                    ),
                    array(
                        'icon' => 'file',
                        'label' => Yii::t('sideMenu', 'Order'),
                        'url' => array('/backend/checkoutOrderPage/admin'),
                        'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'checkoutOrderPage',
                        'visible' => user()->isAdmin,
                    ),
                    array(
                        'icon' => 'file',
                        'label' => Yii::t('sideMenu', 'Faqs'),
                        'url' => array('/backend/faqsPage/admin'),
                        'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'faqsPage',
                        'visible' => user()->isAdmin,
                    ),
                    array(
                        'icon' => 'file',
                        'label' => Yii::t('sideMenu', 'Home page'),
                        'url' => array('/backend/homePage/admin'),
                        'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'homePage',
                        'visible' => user()->isAdmin,
                    ),
                    array(
                        'icon' => 'file',
                        'label' => Yii::t('sideMenu', 'Login page'),
                        'url' => array('/backend/loginPage/admin'),
                        'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'loginPage',
                        'visible' => user()->isAdmin,
                    ),
                    array(
                        'icon' => 'file',
                        'label' => Yii::t('sideMenu', 'Product list page'),
                        'url' => array('/backend/productListPage/admin'),
                        'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'productListPage',
                        'visible' => user()->isAdmin,
                    ),
                    array(
                        'icon' => 'file',
                        'label' => Yii::t('sideMenu', 'Product page'),
                        'url' => array('/backend/productPage/admin'),
                        'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'productPage',
                        'visible' => user()->isAdmin,
                    ),

                ),
            ),
            array(
                'url' => '#',
                'icon' => 'tags',
                'label' => Yii::t('sideMenu', 'Faqs'),
                'visible' => user()->isAdmin,
                'items' => array(
                    array(
                        'icon' => 'question-sign',
                        'label' => Yii::t('sideMenu', 'Faqs'),
                        'url' => array('/backend/faq/admin'),
                        'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'faq',
                        'visible' => user()->isAdmin,
                    ),
                    array(
                        'icon' => 'question-sign',
                        'label' => Yii::t('sideMenu', 'Faq Categories'),
                        'url' => array('/backend/faqCategory/admin'),
                        'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'faqCategory',
                        'visible' => user()->isAdmin,
                    ),
                ),
            ),
            array(
                'url' => '#',
                'icon' => 'shopping-cart',
                'label' => Yii::t('sideMenu', 'Ecommerce'),
                'visible' => true,
                'items' => array(
                    array(
                        'icon' => 'globe',
                        'label' => Yii::t('sideMenu', 'Countries'),
                        'url' => array('/backend/country/admin'),
                        'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'country',
                    ),
//                    array(
//                        'icon' => 'usd',
//                        'label' => Yii::t('sideMenu', 'Tax'),
//                        'url' => array('/backend/tax/admin'),
//                        'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'tax',
//                    ),
                    array(
                        'icon' => 'plane',
                        'label' => Yii::t('sideMenu', 'Shipping Options'),
                        'url' => array('/backend/shippingOption/admin'),
                        'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'shippingOption',
                    ),
//                    array(
//                        'icon' => 'credit-card',
//                        'label' => Yii::t('sideMenu', 'Payment Methods'),
//                        'url' => array('/backend/paymentMethods/admin'),
//                        'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'paymentMethods',
//                    ),
//
//                    array(
//                        'icon' => 'list-alt',
//                        'label' => Yii::t('sideMenu', 'Product SubCategories'),
//                        'url' => array('/backend/productSubcategories/admin'),
//                        'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'productSubcategories',
//                    ),
//                    array(
//                        'icon' => 'book',
//                        'label' => Yii::t('sideMenu', 'Product Brand'),
//                        'url' => array('/backend/productBrand/admin'),
//                        'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'productBrand',
//                    ),
//                    array(
//                        'icon' => 'wrench',
//                        'label' => Yii::t('sideMenu', 'Product Condition'),
//                        'url' => array('/backend/productCondition/admin'),
//                        'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'productCondition',
//                    ),
                    array(
                        'icon' => 'picture',
                        'label' => Yii::t('sideMenu', 'Color'),
                        'url' => array('/backend/color/admin'),
                        'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'color',
                    ),
                    array(
                        'icon' => 'text-height',
                        'label' => Yii::t('sideMenu', 'Size'),
                        'url' => array('/backend/size/admin'),
                        'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'size',
                    ),
                    array(
                        'icon' => 'book',
                        'label' => Yii::t('sideMenu', 'Product Label'),
                        'url' => array('/backend/label/admin'),
                        'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'label',
                    ),
                    array(
                        'icon' => 'tasks',
                        'label' => Yii::t('sideMenu', 'Product Tags'),
                        'url' => array('/backend/tag/admin'),
                        'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'productTag',
                    ),
                    array(
                        'icon' => 'th-list',
                        'label' => Yii::t('sideMenu', 'Product Categories'),
                        'url' => array('/backend/productCategory/admin'),
                        'active' => Yii::app()->controller->module->id == 'backend' && Yii::app()->controller->id == 'productCategory',
                    ),
                    array(
                        'icon' => 'hand-right',
                        'label' => Yii::t('sideMenu', 'Products'),
                        'url' => array('/backend/product/admin'),
                        'active' => Yii::app()->controller->module->id == 'backend' && (Yii::app()->controller->id == 'product'),
                    ),
                    array(
                        'icon' => 'credit-card',
                        'label' => Yii::t('sideMenu', 'Orders'),
                        'url' => array('/backend/order/admin'),
                        'active' => Yii::app()->controller->module->id == 'backend' && (Yii::app()->controller->id == 'order'),
                    ),
                ),
            ),

        );
        return $menu;
    }

    public function beforeControllerAction($controller, $action)
    {
        if (parent::beforeControllerAction($controller, $action)) {
            // this method is called before any module controller action is performed
            // you may place customized code here
            return true;
        } else
            return false;
    }
}
