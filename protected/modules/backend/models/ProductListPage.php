
<?php

Yii::import('backend.models._base.BaseProductListPage');

class ProductListPage extends BaseProductListPage
{
    /**
    * @param string $className
    * @return ProductListPage    */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public static function choices(){
        return GxHtml::listDataEx(self::model()->findAllAttributes(null, true));
    }

    public function elementos($attr_id, $attr){
        $v = $this->$attr_id;
        unset($this->$attr_id);
        $c = $this->search()->criteria;
        //$c->select = $attr_id;
        $all = self::model()->findAll($c);
        $this->$attr_id = $v;

        $r = array();
        foreach($all as $p){
            $r[$p->$attr_id] = $p->$attr;
        }
        return $r;
    }


    /**
    * Es llamado para saber si se muestra o no la administracion de este modelo
    * usar en conjunto con los permisos
    * @return boolean
    */
    public function hasAdmin(){
        return false;
    }


    public function init(){
    $this->id = rand_uniqid();
    $this->owner= UserModule::user()->username;
    }

    public function beforeSave()
    {
        if(parent::beforeSave())
        {
            if($this->isNewRecord)
            {
                $this->created = date('Y-m-d G:i:s');
                                return true;
            }
            else
            {
                                return true;
            }
        }
        else{
            return false;
        }

    }


    /* Debe crear el metodo que aparece debajo para cada relacion
    public function afterSave(){
        parent::afterSave();
        if (!$this->isNewRecord) {

            CupOfferCupCity::model()->deleteAll('cup_offer_id='.$this->id);
        }

        if(is_array($this->city)) {
            foreach($this->city as $city_id) {
                $offerCity = new CupOfferCupCity();
                $offerCity->cup_city_id = $city_id;
                $offerCity->cup_offer_id = $this->id;
                $offerCity->save(false);
            }
        }
        return true;
    }

    public function afterFind() {
        parent::afterFind();
        if (!$this->isNewRecord) {
            $this->city = array_map(function($cupcity){return $cupcity->id;}, $this->cities);
        }
    }
    */

    /**
    * Admin variables (ycm module)
    */
    public $adminNames=array('ProductListPages','productlistpage','productlistpages','Pages'); // admin interface, singular, plural
    public $downloadExcel=false; // Download Excel
    public $downloadMsCsv=false; // Download MS CSV
    public $downloadCsv=false; // Download CSV

    public function categoryChoices(){
        return ProductCategory::choices();
    }


    /**
    * Config for attribute widgets (ycm module)
    *
    * @return array
    */
    public function attributeWidgets()
    {
        return array(
                array('id', 'textField'),
                    array('breadcrumb_title', 'textField'),
                    array('items_per_page', 'spinner'),
                    array('categories_title', 'textField'),
                    array('filter_title', 'textField'),
                    array('filter_button_label', 'textField'),
                    array('litle_banner_image1', 'textField'),
                    array('litle_banner_image2', 'textField'),
                    array('main_category_image', 'textField'),
                    array('main_category_title', 'textField'),
                    array('main_category_subtitle', 'textField'),
                    array('main_category_description_text', 'wysiwyg'),
                    array('main_category_button', 'textField'),
                    array('category', 'chosen'),
                    array('created', 'datetime'),
                    array('updated', 'datetime'),
                    array('owner', 'textField'),
            );
    }


    /**
    * Config for TbGridView class (ycm module)
    *
    * @return array
    */
    public function adminSearch()
    {
        return array(
            'columns'=>array(
				'breadcrumb_title',
				'items_per_page',
				'categories_title',
				'filter_title',
				'filter_button_label',
				'litle_banner_image1',
				/*
				'litle_banner_image2',
				'main_category_image',
				'main_category_title',
				'main_category_subtitle',
				'main_category_description_text',
				'main_category_button',
				array(
                    'class' => 'ext.yExt.YRelatedColumn',
					'relation'=>'category0',
				),
				'created',
				'updated',
				'owner',
			*/
            ),
        );
    }


    /**
    * Config for TbDetailView class (ycm module)
    *
    * @return array
    */
    public function details()
    {
        return array(
            'attributes'=>array(
                				'breadcrumb_title',
				'items_per_page',
				'categories_title',
				'filter_title',
				'filter_button_label',
				'litle_banner_image1',
				/*
				'litle_banner_image2',
				'main_category_image',
				'main_category_title',
				'main_category_subtitle',
				'main_category_description_text',
				'main_category_button',
				array(
                    'name'=>'category',
                    'type'=>'raw',
                    'value'=>CHtml::link($this->category0, ycm()->url_view($this->category0)),
				),
				'created',
				'updated',
				'owner',
			*/
            ),
        );
}



}