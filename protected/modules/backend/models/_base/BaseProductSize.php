<?php

/**
 * This is the model base class for the table "product_size".
 * It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "ProductSize".
 * This code was improve iReevo Team
 * Columns in table "product_size" available as properties of the model,
 * followed by relations of table "product_size" available as properties of the model.
 *
 * @property string $id
 * @property string $size
 * @property string $product
 * @property string $created
 * @property string $updated
 * @property string $owner
 *
 * @property Size $size0
 * @property Product $product0
 * @property ImageARBehavior $imageAR

 */
abstract class BaseProductSize extends I18NInTableAdapter {
// many to many relationship
            public $Size;
            public $Product;
    
/* si tiene una imagen pa subir con ImageARBehavior, descomente la linea siguiente
// public $recipeImg;

    /**
    * Behaviors.
    * @return array
    */
    function behaviors() {
        return CMap::mergeArray(parent::behaviors(), array(
                                                
            ));
    }

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'product_size';
	}

	public static function label($n = 1) {
		return self::model()->t_model('ProductSize|ProductSizes', $n);
	}

	public static function representingColumn() {
		return 'id';
	}

    public function i18nAttributes() {
        return array(
        );
    }

	public function rules() {
		return array(
			array('id', 'required'),
			array('id, size, product', 'length', 'max'=>50),
			array('owner', 'length', 'max'=>100),
			array('created, updated', 'safe'),
			array('size, product, created, updated, owner', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, size, product, created, updated, owner', 'safe', 'on'=>'search'),

    array('Size, Product', 'safe'),
		);
	}

	public function relations() {
		return array(
			'size0' => array(self::BELONGS_TO, 'Size', 'size'),
			'product0' => array(self::BELONGS_TO, 'Product', 'product'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('ProductSize','ID'),
			'size' => Yii::t('ProductSize','Size'),
			'product' => Yii::t('ProductSize','Product'),
			'created' => Yii::t('ProductSize','Created'),
			'updated' => Yii::t('ProductSize','Updated'),
			'owner' => Yii::t('ProductSize','Owner'),
								);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('size', $this->size);
		$criteria->compare('product', $this->product);
		$criteria->compare('created', $this->created, true);
		$criteria->compare('updated', $this->updated, true);
		$criteria->compare('owner', $this->owner, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
            		));
	}
}