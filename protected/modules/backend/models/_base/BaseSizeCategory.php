<?php

/**
 * This is the model base class for the table "size_category".
 * It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "SizeCategory".
 * This code was improve iReevo Team
 * Columns in table "size_category" available as properties of the model,
 * followed by relations of table "size_category" available as properties of the model.
 *
 * @property string $id
 * @property string $size
 * @property string $category
 * @property string $created
 * @property string $updated
 * @property string $owner
 *
 * @property Size $size0
 * @property ProductCategory $category0
 * @property ImageARBehavior $imageAR

 */
abstract class BaseSizeCategory extends I18NInTableAdapter {
// many to many relationship
            public $Size;
            public $ProductCategory;
    
/* si tiene una imagen pa subir con ImageARBehavior, descomente la linea siguiente
// public $recipeImg;

    /**
    * Behaviors.
    * @return array
    */
    function behaviors() {
        return CMap::mergeArray(parent::behaviors(), array(
                                                
            ));
    }

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'size_category';
	}

	public static function label($n = 1) {
		return self::model()->t_model('SizeCategory|SizeCategories', $n);
	}

	public static function representingColumn() {
		return 'id';
	}

    public function i18nAttributes() {
        return array(
        );
    }

	public function rules() {
		return array(
			array('id', 'required'),
			array('id, size, category', 'length', 'max'=>50),
			array('owner', 'length', 'max'=>100),
			array('created, updated', 'safe'),
			array('size, category, created, updated, owner', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, size, category, created, updated, owner', 'safe', 'on'=>'search'),

    array('Size, ProductCategory', 'safe'),
		);
	}

	public function relations() {
		return array(
			'size0' => array(self::BELONGS_TO, 'Size', 'size'),
			'category0' => array(self::BELONGS_TO, 'ProductCategory', 'category'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('SizeCategory','ID'),
			'size' => Yii::t('SizeCategory','Size'),
			'category' => Yii::t('SizeCategory','Category'),
			'created' => Yii::t('SizeCategory','Created'),
			'updated' => Yii::t('SizeCategory','Updated'),
			'owner' => Yii::t('SizeCategory','Owner'),
								);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('size', $this->size);
		$criteria->compare('category', $this->category);
		$criteria->compare('created', $this->created, true);
		$criteria->compare('updated', $this->updated, true);
		$criteria->compare('owner', $this->owner, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
            		));
	}
}