<?php

/**
 * This is the model base class for the table "company_info".
 * It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "CompanyInfo".
 * This code was improve iReevo Team
 * Columns in table "company_info" available as properties of the model,
 * and there are no model relations.
 *
 * @property string $id
 * @property string $company_logo
 * @property string $company_small_logo
 * @property string $name
 * @property string $email
 * @property string $website
 * @property string $phone1
 * @property string $phone2
 * @property string $addr
 * @property string $contact_person
 * @property string $created
 * @property string $updated
 * @property string $owner
 *
 * @property ImageARBehavior $imageAR

 */
abstract class BaseCompanyInfo extends I18NInTableAdapter {

/* si tiene una imagen pa subir con ImageARBehavior, descomente la linea siguiente
// public $recipeImg;

    /**
    * Behaviors.
    * @return array
    */
    public $recipeImg1;
    public $recipeImg2;
    function behaviors() {
        return CMap::mergeArray(parent::behaviors(), array(
                                                '_company_logo' => array(
                    'class' => 'ImageARBehavior',
                    'attribute' => 'recipeImg1', // this must exist
                    'extension' => 'jpg,gif,png', // possible extensions, comma separated
                    'prefix' => 'img1_',
                    'relativeWebRootFolder' => '/images/CompanyInfo',
                    'formats' => array(
                    // create a thumbnail for used in the view datails
                    'thumb' => array(
                    'suffix' => '_thumb',
                    'process' => array('resize' => array(50, 50)),
                    ),
                    'normal' => array(
                    'suffix' => '_normal',
                                        'process' => array('resize' => array(184,99, 1)),
                                        ),
                    // and override the default :
                    ),
                    'defaultName' => 'default', // when no file is associated, this one is used
                            // defaultName need to exist in the relativeWebRootFolder path, and prefixed by prefix,
                            // and with one of the possible extensions. if multiple formats are used, a default file must exist
                            // for each format. Name is constructed like this :
                            //     {prefix}{name of the default file}{suffix}{one of the extension}
                ),
                                '_company_small_logo' => array(
                    'class' => 'ImageARBehavior',
                    'attribute' => 'recipeImg2', // this must exist
                    'extension' => 'jpg,gif,png', // possible extensions, comma separated
                    'prefix' => 'img2_',
                    'relativeWebRootFolder' => '/images/CompanyInfo',
                    'formats' => array(
                    // create a thumbnail for used in the view datails
                    'thumb' => array(
                    'suffix' => '_thumb',
                    'process' => array('resize' => array(50, 50)),
                    ),
                    'normal' => array(
                    'suffix' => '_normal',
                                        'process' => array('resize' => array(114,61, 1)),
                                        ),
                    // and override the default :
                    ),
                    'defaultName' => 'default', // when no file is associated, this one is used
                            // defaultName need to exist in the relativeWebRootFolder path, and prefixed by prefix,
                            // and with one of the possible extensions. if multiple formats are used, a default file must exist
                            // for each format. Name is constructed like this :
                            //     {prefix}{name of the default file}{suffix}{one of the extension}
                ),
                                
            ));
    }

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'company_info';
	}

	public static function label($n = 1) {
		return self::model()->t_model('CompanyInfo|CompanyInfos', $n);
	}

	public static function representingColumn() {
		return 'id';
	}

    public function i18nAttributes() {
        return array(
        );
    }

	public function rules() {
		return array(
			array('id', 'required'),
			array('id', 'length', 'max'=>50),
			array('company_logo, company_small_logo, name, email, website, phone1, phone2, contact_person', 'length', 'max'=>255),
			array('owner', 'length', 'max'=>100),
			array('addr, created, updated', 'safe'),
			array('email', 'email','message'=>Yii::t('admin',"The email isn't correct")),
			array('email', 'unique','message'=>Yii::t('admin',"Email already exists!")),
			array('company_logo, company_small_logo, name, email, website, phone1, phone2, addr, contact_person, created, updated, owner', 'default', 'setOnEmpty' => true, 'value' => null),
            array('recipeImg1', 'file', 'on'=>'insert', 'allowEmpty'=>true, 'types'=>'jpg,jpeg,gif,png,JPG,GIF,JPEG,PNG', 'maxSize'=>1024*1024*6),
        array('recipeImg1', 'file', 'on'=>'update', 'allowEmpty'=>true, 'types'=>'jpg,jpeg,gif,png,JPG,GIF,JPEG,PNG', 'maxSize'=>1024*1024*6),
        array('recipeImg1', 'safe'),
            array('recipeImg2', 'file', 'on'=>'insert', 'allowEmpty'=>true, 'types'=>'jpg,jpeg,gif,png,JPG,GIF,JPEG,PNG', 'maxSize'=>1024*1024*6),
        array('recipeImg2', 'file', 'on'=>'update', 'allowEmpty'=>true, 'types'=>'jpg,jpeg,gif,png,JPG,GIF,JPEG,PNG', 'maxSize'=>1024*1024*6),
        array('recipeImg2', 'safe'),
			array('id, company_logo, company_small_logo, name, email, website, phone1, phone2, addr, contact_person, created, updated, owner', 'safe', 'on'=>'search'),

		);
	}

	public function relations() {
		return array(
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('CompanyInfo','ID'),
			'company_logo' => Yii::t('CompanyInfo','Company logo Alt'),
			'company_small_logo' => Yii::t('CompanyInfo','Company small logo Alt'),
			'name' => Yii::t('CompanyInfo','Company name'),
			'email' => Yii::t('CompanyInfo','Email address'),
			'website' => Yii::t('CompanyInfo','Company website'),
			'phone1' => Yii::t('CompanyInfo','phone1'),
			'phone2' => Yii::t('CompanyInfo','phone2'),
			'addr' => Yii::t('CompanyInfo','Address'),
			'contact_person' => Yii::t('CompanyInfo','Contact Person'),
			'created' => Yii::t('CompanyInfo','Created'),
			'updated' => Yii::t('CompanyInfo','Updated'),
			'owner' => Yii::t('CompanyInfo','Owner'),
    'recipeImg1' => Yii::t('CompanyInfo','Company logo'),
    'recipeImg2' => Yii::t('CompanyInfo','Company small logo'),
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('company_logo', $this->company_logo, true);
		$criteria->compare('company_small_logo', $this->company_small_logo, true);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('email', $this->email, true);
		$criteria->compare('website', $this->website, true);
		$criteria->compare('phone1', $this->phone1, true);
		$criteria->compare('phone2', $this->phone2, true);
		$criteria->compare('addr', $this->addr, true);
		$criteria->compare('contact_person', $this->contact_person, true);
		$criteria->compare('created', $this->created, true);
		$criteria->compare('updated', $this->updated, true);
		$criteria->compare('owner', $this->owner, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
            		));
	}
}