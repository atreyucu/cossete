<?php

/**
 * This is the model base class for the table "size".
 * It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Size".
 * This code was improve iReevo Team
 * Columns in table "size" available as properties of the model,
 * followed by relations of table "size" available as properties of the model.
 *
 * @property string $id
 * @property string $name
 * @property integer $is_active
 * @property string $created
 * @property string $updated
 * @property string $owner
 *
 * @property ProductSize[] $productSizes
 * @property SizeCategory[] $sizeCategories
 * @property ImageARBehavior $imageAR

 */
abstract class BaseSize extends I18NInTableAdapter {
// many to many relationship
            public $ProductSize;
            public $SizeCategory;
    
/* si tiene una imagen pa subir con ImageARBehavior, descomente la linea siguiente
// public $recipeImg;

    /**
    * Behaviors.
    * @return array
    */
    function behaviors() {
        return CMap::mergeArray(parent::behaviors(), array(
                                                
            ));
    }

	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return 'size';
	}

	public static function label($n = 1) {
		return self::model()->t_model('Size|Sizes', $n);
	}

	public static function representingColumn() {
		return 'id';
	}

    public function i18nAttributes() {
        return array(
        );
    }

	public function rules() {
		return array(
			array('id', 'required'),
			array('is_active', 'boolean'),
			array('id', 'length', 'max'=>50),
			array('name', 'length', 'max'=>255),
			array('owner', 'length', 'max'=>100),
			array('created, updated', 'safe'),
			array('name, is_active, created, updated, owner', 'default', 'setOnEmpty' => true, 'value' => null),
			array('id, name, is_active, created, updated, owner', 'safe', 'on'=>'search'),

    array('ProductSize, SizeCategory', 'safe'),
		);
	}

	public function relations() {
		return array(
			'productSizes' => array(self::HAS_MANY, 'ProductSize', 'size'),
			'sizeCategories' => array(self::HAS_MANY, 'SizeCategory', 'size'),
		);
	}

	public function pivotModels() {
		return array(
		);
	}

	public function attributeLabels() {
		return array(
			'id' => Yii::t('Size','ID'),
			'name' => Yii::t('Size','Name'),
			'is_active' => Yii::t('Size','Active'),
			'created' => Yii::t('Size','Created'),
			'updated' => Yii::t('Size','Updated'),
			'owner' => Yii::t('Size','Owner'),
								);
	}

	public function search() {
		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id, true);
		$criteria->compare('name', $this->name, true);
		$criteria->compare('is_active', $this->is_active);
		$criteria->compare('created', $this->created, true);
		$criteria->compare('updated', $this->updated, true);
		$criteria->compare('owner', $this->owner, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
            		));
	}
}