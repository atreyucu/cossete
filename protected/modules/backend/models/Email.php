
<?php

Yii::import('backend.models._base.BaseEmail');

class Email extends BaseEmail
{
    /**
    * @param string $className
    * @return Email    */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public static function choices(){
        return GxHtml::listDataEx(self::model()->findAllAttributes(null, true));
    }

    public function elementos($attr_id, $attr){
        $v = $this->$attr_id;
        unset($this->$attr_id);
        $c = $this->search()->criteria;
        //$c->select = $attr_id;
        $all = self::model()->findAll($c);
        $this->$attr_id = $v;

        $r = array();
        foreach($all as $p){
            $r[$p->$attr_id] = $p->$attr;
        }
        return $r;
    }


    /**
    * Es llamado para saber si se muestra o no la administracion de este modelo
    * usar en conjunto con los permisos
    * @return boolean
    */
    public function hasAdmin(){
        return false;
    }






    /**
    * Admin variables (ycm module)
    */
    public $adminNames=array('Emails','email','emails','Email'); // admin interface, singular, plural
    public $downloadExcel=false; // Download Excel
    public $downloadMsCsv=false; // Download MS CSV
    public $downloadCsv=false; // Download CSV


    /**
    * Config for attribute widgets (ycm module)
    *
    * @return array
    */
    public function attributeWidgets()
    {
        return array(
                    array('purpose', 'textField'),
                    array('from', 'textField'),
                    array('reply_to', 'textField'),
                    array('subject', 'textField'),
                    array('content', 'textField'),
                    array('internal', 'boolean'),
            );
    }


    /**
    * Config for TbGridView class (ycm module)
    *
    * @return array
    */
    public function adminSearch()
    {
        return array(
            'columns'=>array(
				'purpose',
				'from',
				'reply_to',
				'subject',
				'content',
				array(
                	'class' => 'ext.yExt.YBooleanColumn',
					'name' => 'internal',
				),
            ),
        );
    }


    /**
    * Config for TbDetailView class (ycm module)
    *
    * @return array
    */
    public function details()
    {
        return array(
            'attributes'=>array(
                				'purpose',
				'from',
				'reply_to',
				'subject',
				'content',
				'internal:boolean',
            ),
        );
}



}