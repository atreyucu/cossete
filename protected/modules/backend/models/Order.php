
<?php

Yii::import('backend.models._base.BaseOrder');

class Order extends BaseOrder
{
    /**
    * @param string $className
    * @return Order    */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public static function choices(){
        return GxHtml::listDataEx(self::model()->findAllAttributes(null, true));
    }

    public function elementos($attr_id, $attr){
        $v = $this->$attr_id;
        unset($this->$attr_id);
        $c = $this->search()->criteria;
        //$c->select = $attr_id;
        $all = self::model()->findAll($c);
        $this->$attr_id = $v;

        $r = array();
        foreach($all as $p){
            $r[$p->$attr_id] = $p->$attr;
        }
        return $r;
    }


    /**
    * Es llamado para saber si se muestra o no la administracion de este modelo
    * usar en conjunto con los permisos
    * @return boolean
    */
    public function hasAdmin(){
        return false;
    }


    public function init(){
    $this->id = rand_uniqid();
    $this->owner= UserModule::user()->username;
    }

    public function beforeSave()
    {
        if(parent::beforeSave())
        {
            if($this->isNewRecord)
            {
                $this->created = date('Y-m-d G:i:s');
                                return true;
            }
            else
            {
                                return true;
            }
        }
        else{
            return false;
        }

    }


    /* Debe crear el metodo que aparece debajo para cada relacion
    public function afterSave(){
        parent::afterSave();
        if (!$this->isNewRecord) {

            CupOfferCupCity::model()->deleteAll('cup_offer_id='.$this->id);
        }

        if(is_array($this->city)) {
            foreach($this->city as $city_id) {
                $offerCity = new CupOfferCupCity();
                $offerCity->cup_city_id = $city_id;
                $offerCity->cup_offer_id = $this->id;
                $offerCity->save(false);
            }
        }
        return true;
    }

    public function afterFind() {
        parent::afterFind();
        if (!$this->isNewRecord) {
            $this->city = array_map(function($cupcity){return $cupcity->id;}, $this->cities);
        }
    }
    */

    /**
    * Admin variables (ycm module)
    */
    public $adminNames=array('Orders','order','orders','Shop'); // admin interface, singular, plural
    public $downloadExcel=false; // Download Excel
    public $downloadMsCsv=false; // Download MS CSV
    public $downloadCsv=false; // Download CSV

    public function shipping_infoChoices(){
        return TransInfo::choices();
    }

    public function billing_infoChoices(){
        return TransInfo::choices();
    }

    public function clientChoices(){
        return Users::choices();
    }


    /**
    * Config for attribute widgets (ycm module)
    *
    * @return array
    */
    public function attributeWidgets()
    {
        return array(
                array('id', 'textField'),
                    array('order_number', 'textField'),
                    array('order_subtotal', 'currency'),
                    array('shipping_cost', 'currency'),
                    array('order_total', 'currency'),
                    array('status', 'textField'),
                    array('shipping_info', 'chosen'),
                    array('billing_info', 'chosen'),
                    array('client', 'chosen'),
                    array('notes', 'textField'),
                    array('created', 'datetime'),
                    array('updated', 'datetime'),
                    array('owner', 'textField'),
            );
    }


    /**
    * Config for TbGridView class (ycm module)
    *
    * @return array
    */
    public function adminSearch()
    {
        return array(
            'columns'=>array(
				'order_number',
				'order_subtotal',
				'shipping_cost',
				'order_total',
				'status',
				array(
                    'class' => 'ext.yExt.YRelatedColumn',
					'relation'=>'shippingInfo',
				),
				/*
				array(
                    'class' => 'ext.yExt.YRelatedColumn',
					'relation'=>'billingInfo',
				),
				array(
                    'class' => 'ext.yExt.YRelatedColumn',
					'relation'=>'client0',
				),
				'notes',
				'created',
				'updated',
				'owner',
			*/
            ),
        );
    }


    /**
    * Config for TbDetailView class (ycm module)
    *
    * @return array
    */
    public function details()
    {
        return array(
            'attributes'=>array(
                				'order_number',
				'order_subtotal',
				'shipping_cost',
				'order_total',
				'status',
				array(
                    'name'=>'shipping_info',
                    'type'=>'raw',
                    'value'=>CHtml::link($this->shippingInfo, ycm()->url_view($this->shippingInfo)),
				),
				/*
				array(
                    'name'=>'billing_info',
                    'type'=>'raw',
                    'value'=>CHtml::link($this->billingInfo, ycm()->url_view($this->billingInfo)),
				),
				array(
                    'name'=>'client',
                    'type'=>'raw',
                    'value'=>CHtml::link($this->client0, ycm()->url_view($this->client0)),
				),
				'notes',
				'created',
				'updated',
				'owner',
			*/
            ),
        );
}



}