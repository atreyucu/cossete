
<?php

Yii::import('backend.models._base.BaseAboutusPage');

class AboutusPage extends BaseAboutusPage
{
    /**
    * @param string $className
    * @return AboutusPage    */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public static function choices(){
        return GxHtml::listDataEx(self::model()->findAllAttributes(null, true));
    }

    public function elementos($attr_id, $attr){
        $v = $this->$attr_id;
        unset($this->$attr_id);
        $c = $this->search()->criteria;
        //$c->select = $attr_id;
        $all = self::model()->findAll($c);
        $this->$attr_id = $v;

        $r = array();
        foreach($all as $p){
            $r[$p->$attr_id] = $p->$attr;
        }
        return $r;
    }


    /**
    * Es llamado para saber si se muestra o no la administracion de este modelo
    * usar en conjunto con los permisos
    * @return boolean
    */
    public function hasAdmin(){
        return false;
    }


    public function init(){
    $this->id = rand_uniqid();
    $this->owner= UserModule::user()->username;
    }

    public function beforeSave()
    {
        if(parent::beforeSave())
        {
            if($this->isNewRecord)
            {
                $this->created = date('Y-m-d G:i:s');
                                return true;
            }
            else
            {
                                return true;
            }
        }
        else{
            return false;
        }

    }



    /**
    * Admin variables (ycm module)
    */
    public $adminNames=array('AboutusPages','aboutuspage','aboutuspages','Pages'); // admin interface, singular, plural
    public $downloadExcel=false; // Download Excel
    public $downloadMsCsv=false; // Download MS CSV
    public $downloadCsv=false; // Download CSV


    /**
    * Config for attribute widgets (ycm module)
    *
    * @return array
    */
    public function attributeWidgets()
    {
        return array(
                array('id', 'textField'),
                    array('breadcrumb_title', 'textField'),
                    array('page_description', 'wysiwyg'),
                    array('section1_title', 'textField'),
                    array('section1_subtitle', 'textField'),
                    array('section1_about_main_banner', 'textField'),
                    array('section1_about_banner1', 'textField'),
                    array('section1_about_banner2', 'textField'),
                    array('section2_title', 'textField'),
                    array('section2_subtitle', 'textField'),
                    array('section2_description', 'wysiwyg'),
                    array('section2_image', 'textField'),
                    array('created', 'datetime'),
                    array('updated', 'datetime'),
                    array('owner', 'textField'),
            );
    }


    /**
    * Config for TbGridView class (ycm module)
    *
    * @return array
    */
    public function adminSearch()
    {
        return array(
            'columns'=>array(
				'breadcrumb_title',
				'page_description',
				'section1_title',
				'section1_subtitle',
				'section1_about_main_banner',
				'section1_about_banner1',
				/*
				'section1_about_banner2',
				'section2_title',
				'section2_subtitle',
				'section2_description',
				'section2_image',
				'created',
				'updated',
				'owner',
			*/
            ),
        );
    }


    /**
    * Config for TbDetailView class (ycm module)
    *
    * @return array
    */
    public function details()
    {
        return array(
            'attributes'=>array(
                				'breadcrumb_title',
				'page_description',
				'section1_title',
				'section1_subtitle',
				'section1_about_main_banner',
				'section1_about_banner1',
				/*
				'section1_about_banner2',
				'section2_title',
				'section2_subtitle',
				'section2_description',
				'section2_image',
				'created',
				'updated',
				'owner',
			*/
            ),
        );
}



}