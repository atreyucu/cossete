<?php

Yii::import('ext.components.TabularInputManagerEx');

class TrendManager extends TabularInputManagerEx
{
    public $class = 'Trend';

    public $model;
    public $dependantAttr = 'home_page';

    /**
     * @static
     * @param $model
     * @return TrendManager
     */
    public static function load($model)
    {
        $instance = new self();
        $instance->model = $model;
        $instance->fetchItemsOrdered('img_order');
        return $instance;
    }
}