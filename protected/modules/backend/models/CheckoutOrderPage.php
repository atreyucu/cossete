
<?php

Yii::import('backend.models._base.BaseCheckoutOrderPage');

class CheckoutOrderPage extends BaseCheckoutOrderPage
{
    /**
    * @param string $className
    * @return CheckoutOrderPage    */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public static function choices(){
        return GxHtml::listDataEx(self::model()->findAllAttributes(null, true));
    }

    public function elementos($attr_id, $attr){
        $v = $this->$attr_id;
        unset($this->$attr_id);
        $c = $this->search()->criteria;
        //$c->select = $attr_id;
        $all = self::model()->findAll($c);
        $this->$attr_id = $v;

        $r = array();
        foreach($all as $p){
            $r[$p->$attr_id] = $p->$attr;
        }
        return $r;
    }


    /**
    * Es llamado para saber si se muestra o no la administracion de este modelo
    * usar en conjunto con los permisos
    * @return boolean
    */
    public function hasAdmin(){
        return false;
    }


    public function init(){
    $this->id = rand_uniqid();
    $this->owner= UserModule::user()->username;
    }

    public function beforeSave()
    {
        if(parent::beforeSave())
        {
            if($this->isNewRecord)
            {
                $this->created = date('Y-m-d G:i:s');
                                return true;
            }
            else
            {
                                return true;
            }
        }
        else{
            return false;
        }

    }



    /**
    * Admin variables (ycm module)
    */
    public $adminNames=array('CheckoutOrderPages','checkoutorderpage','checkoutorderpages','Pages'); // admin interface, singular, plural
    public $downloadExcel=false; // Download Excel
    public $downloadMsCsv=false; // Download MS CSV
    public $downloadCsv=false; // Download CSV


    /**
    * Config for attribute widgets (ycm module)
    *
    * @return array
    */
    public function attributeWidgets()
    {
        return array(
                array('id', 'textField'),
                    array('breadcrumb_title', 'textField'),
                    array('order_list_header', 'textField'),
                    array('order_column_label', 'textField'),
                    array('day_column_label', 'textField'),
                    array('total_column_label', 'textField'),
                    array('cart_details_header', 'textField'),
                    array('subtotal_label', 'textField'),
                    array('shipping_label', 'textField'),
                    array('total_label', 'textField'),
                    array('have_question_link_text', 'textField'),
                    array('call_us_text', 'textField'),
                    array('order_detail_header', 'textField'),
                    array('product_column_label', 'textField'),
                    array('product_total_column_label', 'textField'),
                    array('customer_detail_header', 'textField'),
                    array('name_label', 'textField'),
                    array('email_label', 'textField'),
                    array('phone_label', 'textField'),
                    array('billing_address', 'textField'),
                    array('shipping_address', 'textField'),
                    array('created', 'datetime'),
                    array('updated', 'datetime'),
                    array('owner', 'textField'),
            );
    }


    /**
    * Config for TbGridView class (ycm module)
    *
    * @return array
    */
    public function adminSearch()
    {
        return array(
            'columns'=>array(
				'breadcrumb_title',
				'order_list_header',
				'order_column_label',
				'day_column_label',
				'total_column_label',
				'cart_details_header',
				/*
				'subtotal_label',
				'shipping_label',
				'total_label',
				'have_question_link_text',
				'call_us_text',
				'order_detail_header',
				'product_column_label',
				'product_total_column_label',
				'customer_detail_header',
				'name_label',
				'email_label',
				'phone_label',
				'billing_address',
				'shipping_address',
				'created',
				'updated',
				'owner',
			*/
            ),
        );
    }


    /**
    * Config for TbDetailView class (ycm module)
    *
    * @return array
    */
    public function details()
    {
        return array(
            'attributes'=>array(
                				'breadcrumb_title',
				'order_list_header',
				'order_column_label',
				'day_column_label',
				'total_column_label',
				'cart_details_header',
				/*
				'subtotal_label',
				'shipping_label',
				'total_label',
				'have_question_link_text',
				'call_us_text',
				'order_detail_header',
				'product_column_label',
				'product_total_column_label',
				'customer_detail_header',
				'name_label',
				'email_label',
				'phone_label',
				'billing_address',
				'shipping_address',
				'created',
				'updated',
				'owner',
			*/
            ),
        );
}



}