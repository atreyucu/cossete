
<?php

Yii::import('backend.models._base.BaseCheckoutPage');

class CheckoutPage extends BaseCheckoutPage
{
    /**
    * @param string $className
    * @return CheckoutPage    */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public static function choices(){
        return GxHtml::listDataEx(self::model()->findAllAttributes(null, true));
    }

    public function elementos($attr_id, $attr){
        $v = $this->$attr_id;
        unset($this->$attr_id);
        $c = $this->search()->criteria;
        //$c->select = $attr_id;
        $all = self::model()->findAll($c);
        $this->$attr_id = $v;

        $r = array();
        foreach($all as $p){
            $r[$p->$attr_id] = $p->$attr;
        }
        return $r;
    }


    /**
    * Es llamado para saber si se muestra o no la administracion de este modelo
    * usar en conjunto con los permisos
    * @return boolean
    */
    public function hasAdmin(){
        return false;
    }


    public function init(){
    $this->id = rand_uniqid();
    $this->owner= UserModule::user()->username;
    }

    public function beforeSave()
    {
        if(parent::beforeSave())
        {
            if($this->isNewRecord)
            {
                $this->created = date('Y-m-d G:i:s');
                                return true;
            }
            else
            {
                                return true;
            }
        }
        else{
            return false;
        }

    }



    /**
    * Admin variables (ycm module)
    */
    public $adminNames=array('CheckoutPages','checkoutpage','checkoutpages','Pages'); // admin interface, singular, plural
    public $downloadExcel=false; // Download Excel
    public $downloadMsCsv=false; // Download MS CSV
    public $downloadCsv=false; // Download CSV


    /**
    * Config for attribute widgets (ycm module)
    *
    * @return array
    */
    public function attributeWidgets()
    {
        return array(
                array('id', 'textField'),
                    array('breadcrumb_title', 'textField'),
                    array('billing_form_header', 'textField'),
                    array('shipping_form_header', 'textField'),
                    array('your_order_header', 'textField'),
                    array('cart_totals_header', 'textField'),
                    array('subtotal_label', 'textField'),
                    array('shipping_label', 'textField'),
                    array('total_label', 'textField'),
                    array('accept_terms_text', 'textField'),
                    array('terms_cond_link_text', 'textField'),
                    array('checkout_button_text', 'textField'),
                    array('country_label', 'textField'),
                    array('firstname_label', 'textField'),
                    array('lastname_label', 'textField'),
                    array('city_label', 'textField'),
                    array('state_label', 'textField'),
                    array('zipcode_label', 'textField'),
                    array('email_label', 'textField'),
                    array('phone_label', 'textField'),
                    array('use_shipping_question_text', 'textField'),
                    array('checkbox_text', 'textField'),
                    array('other_notes_text', 'textField'),
                    array('created', 'datetime'),
                    array('updated', 'datetime'),
                    array('owner', 'textField'),
            );
    }


    /**
    * Config for TbGridView class (ycm module)
    *
    * @return array
    */
    public function adminSearch()
    {
        return array(
            'columns'=>array(
				'breadcrumb_title',
				'billing_form_header',
				'shipping_form_header',
				'your_order_header',
				'cart_totals_header',
				'subtotal_label',
				/*
				'shipping_label',
				'total_label',
				'accept_terms_text',
				'terms_cond_link_text',
				'checkout_button_text',
				'country_label',
				'firstname_label',
				'lastname_label',
				'city_label',
				'state_label',
				'zipcode_label',
				'email_label',
				'phone_label',
				'use_shipping_question_text',
				'checkbox_text',
				'other_notes_text',
				'created',
				'updated',
				'owner',
			*/
            ),
        );
    }


    /**
    * Config for TbDetailView class (ycm module)
    *
    * @return array
    */
    public function details()
    {
        return array(
            'attributes'=>array(
                				'breadcrumb_title',
				'billing_form_header',
				'shipping_form_header',
				'your_order_header',
				'cart_totals_header',
				'subtotal_label',
				/*
				'shipping_label',
				'total_label',
				'accept_terms_text',
				'terms_cond_link_text',
				'checkout_button_text',
				'country_label',
				'firstname_label',
				'lastname_label',
				'city_label',
				'state_label',
				'zipcode_label',
				'email_label',
				'phone_label',
				'use_shipping_question_text',
				'checkbox_text',
				'other_notes_text',
				'created',
				'updated',
				'owner',
			*/
            ),
        );
}



}