
<?php

Yii::import('backend.models._base.BaseUsers');

class Users extends BaseUsers
{
    /**
    * @param string $className
    * @return Users    */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public static function choices(){
        return GxHtml::listDataEx(self::model()->findAllAttributes(null, true));
    }

    public function elementos($attr_id, $attr){
        $v = $this->$attr_id;
        unset($this->$attr_id);
        $c = $this->search()->criteria;
        //$c->select = $attr_id;
        $all = self::model()->findAll($c);
        $this->$attr_id = $v;

        $r = array();
        foreach($all as $p){
            $r[$p->$attr_id] = $p->$attr;
        }
        return $r;
    }


    /**
    * Es llamado para saber si se muestra o no la administracion de este modelo
    * usar en conjunto con los permisos
    * @return boolean
    */
    public function hasAdmin(){
        return false;
    }





    /* Debe crear el metodo que aparece debajo para cada relacion
    public function afterSave(){
        parent::afterSave();
        if (!$this->isNewRecord) {

            CupOfferCupCity::model()->deleteAll('cup_offer_id='.$this->id);
        }

        if(is_array($this->city)) {
            foreach($this->city as $city_id) {
                $offerCity = new CupOfferCupCity();
                $offerCity->cup_city_id = $city_id;
                $offerCity->cup_offer_id = $this->id;
                $offerCity->save(false);
            }
        }
        return true;
    }

    public function afterFind() {
        parent::afterFind();
        if (!$this->isNewRecord) {
            $this->city = array_map(function($cupcity){return $cupcity->id;}, $this->cities);
        }
    }
    */

    /**
    * Admin variables (ycm module)
    */
    public $adminNames=array('Users','users','users','Users'); // admin interface, singular, plural
    public $downloadExcel=false; // Download Excel
    public $downloadMsCsv=false; // Download MS CSV
    public $downloadCsv=false; // Download CSV


    /**
    * Config for attribute widgets (ycm module)
    *
    * @return array
    */
    public function attributeWidgets()
    {
        return array(
                    array('is_client', 'boolean'),
                    array('last_name', 'textField'),
                    array('first_name', 'textField'),
                    array('username', 'textField'),
                    array('password', 'password'),
                    array('email', 'textField'),
                    array('activkey', 'textField'),
                    array('createtime', 'spinner'),
                    array('lastvisit', 'spinner'),
                    array('superuser', 'spinner'),
                    array('status', 'spinner'),
            );
    }


    /**
    * Config for TbGridView class (ycm module)
    *
    * @return array
    */
    public function adminSearch()
    {
        return array(
            'columns'=>array(
				array(
                	'class' => 'ext.yExt.YBooleanColumn',
					'name' => 'is_client',
				),
				'last_name',
				'first_name',
				'username',
				'password',
				'email',
				/*
				'activkey',
				'createtime',
				'lastvisit',
				'superuser',
				'status',
			*/
            ),
        );
    }


    /**
    * Config for TbDetailView class (ycm module)
    *
    * @return array
    */
    public function details()
    {
        return array(
            'attributes'=>array(
                				'is_client:boolean',
				'last_name',
				'first_name',
				'username',
				'password',
				'email',
				/*
				'activkey',
				'createtime',
				'lastvisit',
				'superuser',
				'status',
			*/
            ),
        );
}



}