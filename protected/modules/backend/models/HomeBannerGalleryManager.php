<?php

Yii::import('ext.components.TabularInputManagerEx');

class HomeBannerGalleryManager extends TabularInputManagerEx
{
    public $class = 'HomeBannerGallery';

    public $model;
    public $dependantAttr = 'home_page';

    /**
     * @static
     * @param $model
     * @return TourDateManager
     */
    public static function load($model)
    {
        $instance = new self();
        $instance->model = $model;
        $instance->fetchItemsOrdered('img_order');
        return $instance;
    }
}