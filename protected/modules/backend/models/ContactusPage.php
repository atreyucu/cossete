
<?php

Yii::import('backend.models._base.BaseContactusPage');

class ContactusPage extends BaseContactusPage
{
    /**
    * @param string $className
    * @return ContactusPage    */
	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

    public static function choices(){
        return GxHtml::listDataEx(self::model()->findAllAttributes(null, true));
    }

    public function elementos($attr_id, $attr){
        $v = $this->$attr_id;
        unset($this->$attr_id);
        $c = $this->search()->criteria;
        //$c->select = $attr_id;
        $all = self::model()->findAll($c);
        $this->$attr_id = $v;

        $r = array();
        foreach($all as $p){
            $r[$p->$attr_id] = $p->$attr;
        }
        return $r;
    }


    /**
    * Es llamado para saber si se muestra o no la administracion de este modelo
    * usar en conjunto con los permisos
    * @return boolean
    */
    public function hasAdmin(){
        return false;
    }


    public function init(){
    $this->id = rand_uniqid();
    $this->owner= UserModule::user()->username;
    }

    public function beforeSave()
    {
        if(parent::beforeSave())
        {
            if($this->isNewRecord)
            {
                $this->created = date('Y-m-d G:i:s');
                                return true;
            }
            else
            {
                                return true;
            }
        }
        else{
            return false;
        }

    }



    /**
    * Admin variables (ycm module)
    */
    public $adminNames=array('ContactusPages','contactuspage','contactuspages','Pages'); // admin interface, singular, plural
    public $downloadExcel=false; // Download Excel
    public $downloadMsCsv=false; // Download MS CSV
    public $downloadCsv=false; // Download CSV


    /**
    * Config for attribute widgets (ycm module)
    *
    * @return array
    */
    public function attributeWidgets()
    {
        return array(
                array('id', 'textField'),
                    array('breadcrumb_title', 'textField'),
                    array('form_title', 'textField'),
                    array('social_title', 'textField'),
                    array('address_title', 'textField'),
                    array('addr_description', 'wysiwyg'),
                    array('send_button_text', 'textField'),
                    array('also_like_title', 'textField'),
                    array('use_map', 'boolean'),
                    array('longitude', 'textField'),
                    array('latitude', 'textField'),
                    array('map_image', 'textField'),
                    array('created', 'datetime'),
                    array('updated', 'datetime'),
                    array('owner', 'textField'),
            );
    }


    /**
    * Config for TbGridView class (ycm module)
    *
    * @return array
    */
    public function adminSearch()
    {
        return array(
            'columns'=>array(
				'breadcrumb_title',
				'form_title',
				'social_title',
				'address_title',
				'addr_description',
				'send_button_text',
				/*
				'also_like_title',
				array(
                	'class' => 'ext.yExt.YBooleanColumn',
					'name' => 'use_map',
				),
				'longitude',
				'latitude',
				'map_image',
				'created',
				'updated',
				'owner',
			*/
            ),
        );
    }


    /**
    * Config for TbDetailView class (ycm module)
    *
    * @return array
    */
    public function details()
    {
        return array(
            'attributes'=>array(
                				'breadcrumb_title',
				'form_title',
				'social_title',
				'address_title',
				'addr_description',
				'send_button_text',
				/*
				'also_like_title',
				'use_map:boolean',
				'longitude',
				'latitude',
				'map_image',
				'created',
				'updated',
				'owner',
			*/
            ),
        );
}



}