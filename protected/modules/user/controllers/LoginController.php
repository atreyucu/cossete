<?php

class LoginController extends MainAdminController
{
    public $defaultAction = 'login';
    public $layout = '//layouts/login';

    /**
     * Displays the login page
     */
    public function actionLogin()
    {
        $session = Yii::app()->session;

        if (Yii::app()->user->isGuest) {
            $model = new UserLogin;
            // collect user input data
            if (isset($_POST['UserLogin'])) {
                $model->attributes = $_POST['UserLogin'];
                // validate user input and redirect to previous page if valid
                if ($model->validate()) {
                    $this->lastViset();
                    if($session['next_url']){
                        $next_url = $session['next_url'];
                        $session['next_url'] = '';
                        $this->redirect($next_url);
                    }
                    elseif ('/index.php')
                        $this->redirect(Yii::app()->controller->module->returnUrl);
                    else
                        $this->redirect(Yii::app()->user->returnUrl);
                }
            }

            if(strstr(Yii::app()->request->pathInfo,'admin') || strstr(Yii::app()->request->pathInfo,'backend') || strstr(Yii::app()->request->pathInfo,'site/index')){
                $session['front_frontend'] = false;
                $session['next_url'] = '';
                $this->render('/user/login', array('model' => $model,));
            }
            elseif($session['front_frontend']){
                $session['front_frontend'] = false;
                $this->redirect('/frontend/default/login?next='.$session['next_url']);
            }
            else{
                $this->render('/user/login', array('model' => $model,));
            }
        } else
            $this->redirect(Yii::app()->controller->module->returnUrl);
    }

    private function lastViset()
    {
        /*$lastVisit = User::model()->notsafe()->findByPk(Yii::app()->user->id);
        $lastVisit->lastvisit = time();
        $lastVisit->save();*/
    }

    public function actions()
    {
        return array(
            'oauth' => array(
                'class' => 'ext.hoauth.HOAuthAction',
            ),
            'oauthadmin' => array(
                'class' => 'ext.hoauth.HOAuthAdminAction',
            ),
        );
    }

    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl',
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions' => array('oauthadmin'),
                'users' => UserModule::getAdmins(),
            ),
            array('deny', // deny all users
                'actions' => array('oauthadmin'),
                'users' => array('*'),
            ),
        );
    }

}