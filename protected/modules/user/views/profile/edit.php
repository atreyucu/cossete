<?php $this->pageTitle = Yii::app()->name . ' - ' . UserModule::t("Profile");
$this->breadcrumbs = array(
    UserModule::t("Profile") => array('profile'),
    UserModule::t("Edit"),
);
$this->title = UserModule::t("Edit profile");
/** @var $form TbActiveForm */
$form = $this->beginWidget('application.extensions.bootstrap.widgets.TbActiveForm', array(
    'id' => 'edit-profile-form',
));

/** @var $this Controller */

//$this->profileEditFields($profile);

echo $form->textFieldGroup(
    $model,
    'username',
    array(
        'wrapperHtmlOptions' => array(
            'class' => 'col-xs-12 .col-sm-6 .col-md-8',
        ),
        'prepend' => '<i class="glyphicon glyphicon-user"></i>'
    )
);
echo $form->textFieldGroup(
    $model,
    'email',
    array(
        'wrapperHtmlOptions' => array(
            'class' => 'col-xs-12 .col-sm-6 .col-md-8',
        ),
        'prepend' => '<i class="glyphicon glyphicon-envelope"></i>'
    )
);


?>


<div class="form-actions">
  <span class="pull-right">
  <?php echo TbHtml::submitButton($model->isNewRecord ? UserModule::t('Create') : UserModule::t('Save'), array('class' => 'btn btn-primary')); ?>
        </span>
</div>

<?php $this->endWidget() ?>


