<?php $this->pageTitle = Yii::app()->name . ' - ' . UserModule::t("Profile");
$this->breadcrumbs = array(
    UserModule::t("Profile"),
);

$this->title = UserModule::t("Your profile");
/*$this->menu = array(

    array(
        'icon' => 'edit',
        'label' => t('Editar'),
        'url' => array('/user/profile/edit'),
    ),
    array(
        'icon' => 'ok',
        'label' => t('Cambiar contraseña'),
        'url' => array('/user/profile/changepassword'),
    ),
    array(
        'icon' => 'signout',
        'label' => t('Salir'),
        'url' => array('/user/logout'),
    ),
);*/
echo '<div style="clear: both;"></div>';
?>

<table class="dataGrid">
    <tr>
        <th><?php echo CHtml::encode($model->getAttributeLabel('username')); ?>
        </th>
        <td><?php echo CHtml::encode($model->username); ?>
        </td>
    </tr>
    <?php
    $profileFields = ProfileField::model()->forOwner()->sort()->findAll();
    if ($profileFields) {
        foreach ($profileFields as $field) {
            //echo "<pre>"; print_r($profile); die();
            ?>
            <tr>
                <th><?php echo CHtml::encode(UserModule::t($field->title)); ?>
                </th>
                <td><?php echo(($field->widgetView($profile)) ? $field->widgetView($profile) : CHtml::encode((($field->range) ? Profile::range($field->range, $profile->getAttribute($field->varname)) : $profile->getAttribute($field->varname)))); ?>
                </td>
            </tr>
        <?php
        }
        //$profile->getAttribute($field->varname)
    }
    ?>
    <tr>
        <th><?php echo CHtml::encode($model->getAttributeLabel('email')); ?>
        </th>
        <td><?php echo CHtml::encode($model->email); ?>
        </td>
    </tr>
    <tr>
        <th><?php echo CHtml::encode($model->getAttributeLabel('createtime')); ?>
        </th>
        <td><?php echo date("d.m.Y H:i:s", $model->createtime); ?>
        </td>
    </tr>
    <tr>
        <!--	<th>--><?php //echo CHtml::encode($model->getAttributeLabel('lastvisit')); ?>
        </th>
        <!--    <td>--><?php //echo date("d.m.Y H:i:s",$model->lastvisit); ?>
        </td>
    </tr>
    <tr>
        <th><?php echo CHtml::encode($model->getAttributeLabel('status')); ?>
        </th>
        <td><?php echo CHtml::encode(User::itemAlias("UserStatus", $model->status));
            ?>
        </td>
    </tr>

</table>
