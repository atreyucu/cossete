<?php
$this->breadcrumbs = array(
    UserModule::t('Users') => array('/user/admin'),
    $model->username,
);
$this->title = $model->username;
/*$this->menu = array();
$this->menu[] = array('icon'=>'pencil', 'label' => 'Editar', 'url' => array('/user/admin/update','id'=>$model->id));
$this->menu[] = array('icon'=>'trash', 'label' => 'Eliminar',
    'url' => array('/user/admin/delete','id'=>$model->id),
    'htmlOptions' => array(
        'submit'=>array('delete','id'=>$model->id),
        'confirm'=>UserModule::t('Are you sure to delete this item?')
    )
);
$this->boxButtons += array_merge($this->boxButtons, $this->menu);
	$attributes = array(
		'id',
		'username',
	);*/

$profileFields = ProfileField::model()->forOwner()->sort()->findAll();
/*if ($profileFields) {
    foreach($profileFields as $field) {
        array_push($attributes,array(
                'label' => UserModule::t($field->title),
                'name' => $field->varname,
                'type'=>'raw',
                'value' => (($field->widgetView($model->profile))?$field->widgetView($model->profile):(($field->range)?Profile::range($field->range,$model->profile->getAttribute($field->varname)):$model->profile->getAttribute($field->varname))),
            ));
    }
}*/

//	array_push($attributes,
$attributes = array(
    'password',
    'email',
    'activkey',
    array(
        'name' => 'createtime',
        'value' => date("d.m.Y H:i:s", $model->createtime),
    ),
    array(
        'name' => 'lastvisit',
        'value' => (($model->lastvisit) ? date("d.m.Y H:i:s", $model->lastvisit) : UserModule::t("Not visited")),
    ),
    array(
        'name' => 'superuser',
        'value' => User::itemAlias("AdminStatus", $model->superuser),
    ),
    array(
        'name' => 'status',
        'value' => User::itemAlias("UserStatus", $model->status),
    )
);

$this->widget('application.extensions.bootstrap.widgets.TbDetailView', array(
    'data' => $model,
    'attributes' => $attributes,
));


?>
<div class="form-actions" style="text-align: right">
    <a href="<?php echo app()->request->urlReferrer; ?>" class="btn btn-default"><span
            class="glyphicon glyphicon-arrow-left"></span> Atras</a>
    <?php echo CHtml::link(t(TbHtml::icon('glyphicon glyphicon-briefcase') . 'Manage item'), array('admin'), array('class' => 'btn btn-default')); ?><?php echo CHtml::link(t(TbHtml::icon('glyphicon glyphicon-saved') . 'Update item'), array('update', 'id' => $model->id), array('class' => 'btn btn-primary')); ?><?php echo CHtml::link(t(TbHtml::icon('glyphicon glyphicon-remove') . 'Remove item'), array('delete', 'id' => $model->id), array('class' => 'btn btn-danger')); ?>
</div>

