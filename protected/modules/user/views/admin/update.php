<?php
/** @var $this AdminController */
$this->breadcrumbs = array(
    (UserModule::t('Users')) => array('admin'),
    $model->username => array('view', 'id' => $model->id),
    (UserModule::t('Update')),
);
/*$this->title = UserModule::t('Update User')." ".$model->id;
$this->menu = array();
$this->menu[] = array('icon'=>'pencil', 'label' => 'Editar', 'url' => array('/user/admin/update','id'=>$model->id));
$this->menu[] = array('icon'=>'trash', 'label' => 'Eliminar',
    'url' => array('/user/admin/delete','id'=>$model->id),
    'htmlOptions' => array(
        'submit'=>array('delete','id'=>$model->id),
        'confirm'=>UserModule::t('Are you sure to delete this item?')
    )
);
$this->boxButtons += array_merge($this->boxButtons, $this->menu);*/
echo $this->renderPartial('_form', array('model' => $model, 'profile' => $profile));
