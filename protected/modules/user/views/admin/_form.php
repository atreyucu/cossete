<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
?>

<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
    'id' => 'product-form',
    'enableClientValidation'=>true,

));
?>
    <div class="widget-box">
        <div class="widget-title">
        <span class="icon">
            <i class="fa fa-file"></i>
        </span>
            <h5>User Information</h5>
        </div>
        <div class="widget-content nopadding">

            <?php echo $form->textFieldGroup($model, 'username', array(
                    'maxlength' => 20,
                    'wrapperHtmlOptions' => array(
                        'class' => 'col-sm-5',
                    ),

                    //'hint' => Yii::t('admin','Please, insert ').' Product name',
                    'append' => Yii::t('admin','Text')
                )
            ); ?>


            <?php echo $form->passwordFieldGroup($model, 'password', array(
                    'maxlength' => 60,
                    'wrapperHtmlOptions' => array(
                        'class' => 'col-sm-5',
                    ),

                    //'hint' => Yii::t('admin','Please, insert ').' Product code',
//                    'append' => Yii::t('admin','Text')
                )
            ); ?>

            <?php echo $form->emailFieldGroup($model,'email',
                array(
                    'wrapperHtmlOptions' => array(
                        'class' => 'col-sm-5',
                    ),
                    //'hint' => Yii::t('admin','Please, insert').' Email address',
                    'append' => '@'
                )
            ); ?>

            <?php echo $form->dropDownListGroup($model,'superuser',
                array(
                    'wrapperHtmlOptions' => array(
                        'class' => 'col-sm-5',
                    ),
                    'widgetOptions' => array(
                        'data' => array(0=>Yii::t('admin','No'),1=>Yii::t('admin','Yes')),
                        // 'htmlOptions' => array('multiple' => true),
                    ),
                    //'hint' => Yii::t('admin','Please, select').' Position in the home',
                    'prepend' => Yii::t('admin','Select')
                )
            ); ?>

<!--            --><?php // echo TbHtml::activeDropDownListControlGroup($model, 'superuser', User::itemAlias('AdminStatus'));?>

            <?php echo $form->dropDownListGroup($model,'status',
                array(
                    'wrapperHtmlOptions' => array(
                        'class' => 'col-sm-5',
                    ),
                    'widgetOptions' => array(
                        'data' => array(0=>Yii::t('admin','Not active'),1=>Yii::t('admin','Active'),-1=>Yii::t('admin','Banned')),
                        // 'htmlOptions' => array('multiple' => true),
                    ),
                    //'hint' => Yii::t('admin','Please, select').' Position in the home',
                    'prepend' => Yii::t('admin','Select')
                )
            ); ?>



<!--            --><?php //echo TbHtml::activeDropDownListControlGroup($model, 'status', User::itemAlias('UserStatus'));?>
            <div style="display: none">
            <?php $this->profileEditFields($profile);?>
            </div>

            <?php echo $form->checkBoxGroup($model, 'is_client'); ?>


        </div>
    </div>


    <div class="tabbable inline" id="client_info" style="display: none;">
        <ul id="myTab" class="nav nav-tabs tab-bricky">
            <li class="active">
                <a href="#tour_panel_tab1" data-toggle="tab">
                    Client information
                </a>
            </li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane in active" id="tour_panel_tab1">
                <?php echo $form->textFieldGroup($model, 'first_name', array(
                        'maxlength' => 20,
                        'wrapperHtmlOptions' => array(
                            'class' => 'col-sm-5',
                        ),

                        //'hint' => Yii::t('admin','Please, insert ').' Product name',
                        'append' => Yii::t('admin','Text')
                    )
                ); ?>

                <?php echo $form->textFieldGroup($model, 'last_name', array(
                        'maxlength' => 20,
                        'wrapperHtmlOptions' => array(
                            'class' => 'col-sm-5',
                        ),

                        //'hint' => Yii::t('admin','Please, insert ').' Product name',
                        'append' => Yii::t('admin','Text')
                    )
                ); ?>






            </div>
        </div>
    </div>

    <div class='form-actions'>   <a href='<?php echo app()->request->urlReferrer;?>' class='btn btn-default'><span class='glyphicon glyphicon-arrow-left'></span><?php echo Yii::t('admin','Back');?></a>   <?php $this->widget(
            'bootstrap.widgets.TbButton',
            array(
                'buttonType' => 'submit',
                'context' => 'primary',
                'icon'=> 'glyphicon glyphicon-saved',
                'label' => Yii::t('admin','Save item')
            )
        ); ?>
        <?php
        if(Yii::app()->controller->action->id!='update') {
            $this->widget(
                'bootstrap.widgets.TbButton',
                array(
                    'buttonType' => 'reset',
                    'context' => 'warning',
                    'icon'=> 'glyphicon glyphicon-remove',
                    'label' => Yii::t('admin','Reset form')
                )
            );
        } ?>
        <?php $this->endWidget(); ?>

        <?php if(isset($model->id)){
            // echo CHtml::link(Yii::t('admin',TbHtml::icon('glyphicon glyphicon-remove'). 'Remove item'),array('delete','id'=>$model->id),array('class'=>'btn btn-danger'));
        }?>
        <div class='btn-group'>
            <a href='#' data-toggle='dropdown' class='btn btn-info dropdown-toggle'>
                <i class='fa fa-plus icon-white'></i>

                <?php echo Yii::t('admin', 'Add'); ?>            <span class='caret'></span>
            </a>
            <ul class='dropdown-menu dropdown-primary'>
                <li>
                    <?php echo CHtml::link(Yii::t('admin', TbHtml::icon('glyphicon glyphicon-plus-sign') . 'ProductCategory'), array('/backend/category0/create', 'id' => $model->primaryKey, 'controller' => Yii::app()->controller->id, 'action' => Yii::app()->controller->action->id), array()); ?>                </li>
                <li>
                    <?php echo CHtml::link(Yii::t('admin', TbHtml::icon('glyphicon glyphicon-plus-sign') . 'Label'), array('/backend/label0/create', 'id' => $model->primaryKey, 'controller' => Yii::app()->controller->id, 'action' => Yii::app()->controller->action->id), array()); ?>                </li>
            </ul>
        </div>


    </div>

<script type="text/javascript">
    $(document).ready(function(){
        $('#Profile_firstname').attr('value','default');
        $('#Profile_lastname').attr('value','default');

        if($('#User_is_client').attr('checked')){
            $('#client_info').show();
        }
        else{
            $('#client_info').hide();
        }


        $('#User_is_client').click(function(){
            if($('#User_is_client').attr('checked')){
                $('#client_info').show();
            }
            else{
                $('#client_info').hide();
            }
        });

        $('#User_first_name').keyup(function(){
            $('#Profile_firstname').attr('value',$('#User_first_name').attr('value'));
        });

        $('#User_last_name').keyup(function(){
            $('#Profile_lastname').attr('value',$('#User_last_name').attr('value'));
        });

    });
</script>

