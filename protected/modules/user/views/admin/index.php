<?php
/** @var $this AdminController */
$this->breadcrumbs = array(
    UserModule::t('Users'),
);
$this->title = UserModule::t("Manage User");

/*$this->menu = array(
    array(
        'icon' => 'plus',
        'label' => t('Nuevo'),
        'url' => array('create'),
    ),
);*/

$this->widget('application.extensions.bootstrap.widgets.TbGridView', array(
    'dataProvider' => $model->search(),
    'filter' => $model,
    'columns' => array(
        /*array(
            'name' => 'id',
            'type'=>'raw',
            'value' => 'CHtml::link(CHtml::encode($data->id),array("admin/update","id"=>$data->id))',
        ),*/
        array(
            'name' => 'username',
            'type' => 'raw',
            'value' => 'CHtml::link(CHtml::encode($data->getFullName()),array("admin/view","id"=>$data->id))',
        ),
        /*array(
            'name'=>'email',
            'type'=>'raw',
            'value'=>'CHtml::link(CHtml::encode($data->email), "mailto:".$data->email)',
        ),*/
        array(
            'name' => 'status',
            'value' => 'User::itemAlias("UserStatus",$data->status)',
            'filter' => User::itemAlias('UserStatus'),
        ),
        array(
            'name' => 'superuser',
            'value' => 'User::itemAlias("AdminStatus",$data->superuser)',
            'filter' => User::itemAlias('AdminStatus'),
        ),
        array(
            'class' => 'application.extensions.bootstrap.widgets.TbButtonColumn',
        ),
    ),
));
?>


<div class="form-actions">
    <?php echo CHtml::link(t(TbHtml::icon('glyphicon glyphicon-plus') . 'Add item'), array('create'), array('class' => 'btn btn-default')); ?>
</div>

