<?php
/** @var $this AdminController */
$this->breadcrumbs = array(
    UserModule::t('Users') => array('admin'),
    UserModule::t('Create'),
);
$this->title = UserModule::t("Create User");
//$this->boxButtons = $this->menu;

echo $this->renderPartial('_form', array('model' => $model, 'profile' => $profile));
