<?php
$this->breadcrumbs = array(
    UserModule::t('Profile Fields') => array('admin'),
    UserModule::t($model->title),
);
$this->title = UserModule::t('View Profile Field # ') . " " . $model->id;
$this->boxButtons = array();
$this->boxButtons[] = array('icon' => 'eye-open', 'label' => 'Ver', 'url' => array('/user/profileField/view', 'id' => $model->id));
$this->boxButtons[] = array('icon' => 'pencil', 'label' => 'Editar', 'url' => array('/user/profileField/update', 'id' => $model->id));
$this->boxButtons[] = array('icon' => 'trash', 'label' => 'Eliminar',
    'url' => array('/user/profileField/delete', 'id' => $model->id),
    'linkOptions' => array(
        'submit' => array('/user/profileField/delete', 'id' => $model->id),
        'confirm' => UserModule::t('Are you sure to delete this item?')
    )
);
//$this->boxButtons += array_merge($this->boxButtons, $this->menu);
echo CHtml::openTag('div', array(
    'class' => 'span5'
));
$this->widget('application.extensions.bootstrap.widgets.TbDetailView', array(
    'data' => $model,
    'attributes' => array(
        'id',
        'varname',
        'title',
        'field_type',
        'field_size',
        'field_size_min',
        'required',
        'match',
        'range',
        'error_message',
        'other_validator',
        'widget',
        'widgetparams',
        'default',
        'position',
        'visible',
    ),
));

echo CHtml::closeTag('div');
?>
