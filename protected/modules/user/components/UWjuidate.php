<?php

Yii::import('application.extensions.bootstrap.widgets.TbDatePicker');
class UWjuidate
{
    /**
     * Initialization
     * @return array
     */
    public function init()
    {
        return array(
            'name' => __CLASS__,
            'label' => UserModule::t('jQueryUI datepicker'),
            'fieldType' => array('DATE', 'VARCHAR'),
            'params' => $this->params,
            'paramsLabels' => array(),
        );
    }

    /**
     * @param $value
     * @param $model
     * @param $field_varname
     * @return string
     */
    public function setAttributes($value, $model, $field_varname)
    {
        return $value;
    }

    /**
     * @param $model - profile model
     * @param $field - profile fields model item
     * @return string
     */
    public function viewAttribute($model, $field)
    {
        return $model->getAttribute($field->varname);
    }

    /**
     * @param $model - profile model
     * @param $field - profile fields model item
     * @param $params - htmlOptions
     * @return string
     */
    public function editAttribute($model, $field, $htmlOptions = array())
    {
        return Yii::app()->controller->widget('application.extensions.bootstrap.widgets.TbDatePicker', array(
            'model' => $model,
            'attribute' => $field->varname,
            'htmlOptions' => $htmlOptions,
        ), true);
    }

}