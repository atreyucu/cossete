<?php

class FrontendModule extends CWebModule
{
    public $hash = 'md5';

    public function getName(){
        return 'Frontend';
    }

    public function init()
    {
        // this method is called when the module is being created
        // you may place code here to customize the module or the application

        $this->attachBehavior('ycm', array(
            'class' => 'application.components.ModuleBehavior'
        ));

        // import the module-level models and components
        $this->setImport(array(
            'frontend.models.*',
            'frontend.components.*',
        ));
        /*Yii::app()->theme =  'site';
        $this->layout = 'main';*/
    }


    public function beforeControllerAction($controller, $action)
    {
        if(parent::beforeControllerAction($controller, $action))
        {
            // this method is called before any module controller action is performed
            // you may place customized code here
            $session = Yii::app()->session;

            if(!strstr(Yii::app()->request->pathInfo,'updatecart') && !strstr(Yii::app()->request->pathInfo,'add2cart') && !strstr(Yii::app()->request->pathInfo,'removeproductcart') && !strstr(Yii::app()->request->pathInfo,'updatecarmenu')){
                $session['front_frontend'] = true;
                $session['next_url'] = '/'.Yii::app()->request->pathInfo;
            }

//            ddump(!strstr(Yii::app()->request->pathInfo,'updatecart') && !strstr(Yii::app()->request->pathInfo,'add2cart'));
            $shpp_criteria = new CDbCriteria();
            $shpp_criteria->compare('is_default',1);

            $shpp = ShippingOption::model()->find($shpp_criteria);


           if(!$session['cart']){
                $session['cart'] = array('products'=>array(),'counts'=>0,'total'=>0, 'shipping'=>$shpp->id, 'rtotal'=> 0);
           }


            Yii::app()->theme = 'site';
            //$this->layout='main';

            return true;
        }
        else
            return false;
    }

    public static function encrypting($string = "")
    {
        $hash = Yii::app()->controller->module->hash;
        if ($hash == "md5")
            return md5($string);
        if ($hash == "sha1")
            return sha1($string);
        else
            return hash($hash, $string);
    }

}
