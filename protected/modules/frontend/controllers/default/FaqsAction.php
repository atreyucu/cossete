<?php

class FaqsAction extends CAction
{
    public function run()
    {
        $controller = $this->getController();
        $controller->common_data['view'] = 'faqs';

        $faq_page = FaqsPage::model()->find();
        $faq_dto = array();
        $faq_dto['page_title'] = 'FREQUENTLY ASKED QUESTIONS';
        $faq_dto['description'] = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed nisi sem. Nullam et lorem
        magna, in consectetur erat. Aliquam fermentum fringilla libero a vulputate. Curabitur non arcu non tortor semper
         dictum. Pellentesque pulvinar ornare quam, in faucibus tortor elementum vitae. Nam ut lacinia mi. Nullam et
         hendrerit tellus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.
         Sed nec pulvinar dolor. Aenean in velit non lectus semper hendrerit. Etiam ligula eros, aliquam ut ullamcorper
         quis, pulvinar in arcu. ';
        $faq_dto['contact_image'] = '/static/imgs/faqs-parallax.jpg';
        $faq_dto['contact_title'] = "Have any questions we didn't answer?";
        $faq_dto['contact_button'] = 'Contact Us';
        if (isset($faq_page)) {
            $faq_dto['page_title'] = $faq_page->breadcrumb_title;
            $faq_dto['description'] = strtr($faq_page->description_text, array('<p>'=>'', '</p>'=>''));
            $faq_dto['contact_image'] = $faq_page->_background_image->getFileUrl('normal');
            $faq_dto['contact_title'] = $faq_page->contact_title;
            $faq_dto['contact_button'] = $faq_page->contact_button;
        }

        $faqs_categories = FaqCategory::model()->findAll();
        $faqs_categories_dto = array();
        foreach($faqs_categories as $category){
            $temp1 = array();
            $temp1['id'] = $category->id;
            $temp1['name'] = $category->name;
            $temp1['faqs'] = array();
            $faqs = $category->faqs;
            foreach($faqs as $faq){
                $temp2 = array();
                $temp2['id'] = $faq->id;
                $temp2['question'] = $faq->question_text;
                $temp2['reply'] = $faq->reply_text;

                $temp1['faqs'][] = $temp2;
            }

            $faqs_categories_dto[] = $temp1;
        }

        return $controller->render('faqs',
            array(
                'faq_page' => $faq_dto,
                'faqs_categories' => $faqs_categories_dto
            )
        );

    }
}