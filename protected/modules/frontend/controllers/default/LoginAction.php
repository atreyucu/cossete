<?php

class LoginAction extends CAction
{
    public function run()
    {
        $controller = $this->getController();
        $controller->common_data['view'] = 'login';

        $login_page = LoginPage::model()->find();

        $session = Yii::app()->session;

        if(isset($_GET['next'])){
            $session['next_url'] = $_GET['next'];
        }

        if(isset($login_page)){
            $breadcrumb_title = $login_page->breadcrumb_title;
            $form_title = $login_page->form_title;
            $form_subtitle = $login_page->form_subtitle;
            $username_label = $login_page->username_label;
            $password_label = $login_page->password_label;
            $send_button_label = $login_page->send_button_label;
            $background_image = $login_page->_background_image->getFileUrl('normal');
            $contact_title = $login_page->contact_title;
            $contact_button = $login_page->contact_button;
        }
        else{
            $breadcrumb_title = 'My Account';
            $form_title = 'Login';
            $form_subtitle = 'Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu';
            $username_label = 'Username or email address *';
            $password_label = 'Password *';
            $send_button_label = 'send';
            $background_image = '/static/imgs/faqs-parallax.jpg';
            $contact_title = "Have any questions we didn't answer?";
            $contact_button = 'Contact Us';
        }

        return $controller->render('login',
            array(
                'breadcrumb_title' => $breadcrumb_title,
                'form_title' => $form_title,
                'form_subtitle' => $form_subtitle,
                'username_label' => $username_label,
                'password_label' => $password_label,
                'send_button_label' => $send_button_label,
                'background_image' => $background_image,
                'contact_title' => $contact_title,
                'contact_button' => $contact_button,
                'next' => $_GET['next'],
            )
        );

    }
}