<?php

class IndexAction extends CAction
{
    public function run()
    {
        $controller = $this->getController();
        $controller->common_data['view'] = 'home';

        $home = HomePage::model()->find();
        $home_galleries_dto = array();
        $home_category_dto = array();
        $home_category_dto['image'] = '/static/imgs/home-parallax.jpg';
        $home_category_dto['title'] = 'Authentic American Style';
        $home_category_dto['subtitle'] = 'THE NEW FASHION WAY';
        $home_category_dto['description'] = '<p>
                Duis sed nisi sem. Nullam et lorem magna, in consectetur erat. Aliquam fermentum fringilla
                libero a vulputate. Curabitur non arcu non tortor semper dictum. Pellente pulvinar ornare
                quam, in faucibus tortor elementum vitae. Nam ut lacini
            </p>';
        $home_category_dto['button'] = 'sales';
        $home_category_dto['button_url'] = '';
        $home_category_dto['logo'] = '/static/imgs/brand-logo-small.png';
        if (isset($home)) {
            $home_galleries = HomeBannerGallery::model()->findAll(array('condition' => 'home_page=:id_home',
                'params' => array(':id_home' => $home->id), 'order' => 'img_order'));

            foreach($home_galleries as $gallery) {
                $temp = array();
                $temp['id'] = $gallery->id;
                $temp['alt'] = $gallery->gallery_image;
                $temp['image'] = $gallery->_gallery_image->getFileUrl('normal');
                $temp['t_title'] = $gallery->title;
                $temp['m_title'] = $gallery->middle_title;
                $temp['b_title'] = $gallery->bottom_title;

                $home_galleries_dto[] = $temp;
            }

            $home_category_dto['image'] = $home->_main_category_image->getFileUrl('normal');
            $home_category_dto['alt'] = $home->main_category_image;
            $home_category_dto['title'] = $home->main_category_title;
            $home_category_dto['subtitle'] = $home->main_category_subtitle;
            $home_category_dto['description'] = $home->main_category_description_text;
            $home_category_dto['button'] = $home->main_category_button;
            $home_category_dto['button_url'] = $controller->createUrl('/frontend/default/category', array('id'=>$home->category0->id));
            $company = CompanyInfo::model()->find();
            $home_category_dto['logo'] = '/static/imgs/brand-logo-small.png';
            if (isset($company))
                $home_category_dto['logo'] = $company->_company_small_logo->getFileUrl('normal');
        }

        $trends = Trend::model()->findAll(array('order' => 'position'));
        $trends_dto = array();
        foreach($trends as $trend) {
            $temp = array();
            $temp['id'] = $trend->id;
            $temp['alt'] = $trend->home_image;
            $temp['image'] = $trend->_home_image->getFileUrl('normal');
            $temp['name'] = $trend->title;
            $temp['tag_id'] = $trend->tag_id;


            $trends_dto[] = $temp;
        }

        $categories_images = ProductCategory::model()->findAll(array('condition' => 'is_active=1 and show_in_home=1',
                                                                                                'order' => 'position'));
        $categories_images_dto = array();
        foreach($categories_images as $category) {
            $temp = array();
            $temp['id'] = $category->id;
            $temp['alt'] = $category->home_image;
            $temp['image'] = $category->_home_image->getFileUrl('normal');
            $temp['position'] = $category->position;

            $categories_images_dto[] = $temp;
        }

        $tags = Tag::model()->findAll(array('condition' => 'is_active=1 and show_in_home=1', 'order' => 'position'));
        $tags_dto = array();
        $temp_position = $tags[0]->position;
        $temp_tags = array();
        $count = 0;
        foreach($tags as $tag) {
            $count++;
            if ($tag->position == $temp_position){
                $temp_tags[] = $tag;
                if ($count == count($tags)) {
                    $temp_tag = $temp_tags[rand(0, count($temp_tags)-1)];
                    $temp = array();
                    $temp['id'] = $temp_tag->id;
                    $temp['name'] = $temp_tag->name;
                    $temp['position'] = $temp_tag->position;
                    $temp['products'] = array();
                    $products_tag = ProductTag::model()->findAll('tag=:id_tag', array(':id_tag' => $temp['id']));
                    foreach($products_tag as $product_tag){
                        $product = Product::model()->findByPk($product_tag->product);
                        $t_product = array();
                        $t_product['id'] = $product->id;
                        $t_product['image'] = $product->_tag_home_image->getFileUrl('normal');
                        $t_product['name'] = $product->name;
                        $t_product['price'] = $product->price;
                        $label = $product->label0;
                        if (isset($label)) {
                            $t_product['label'] = $product->label0->_icon->getFileUrl('normal');
                            $t_product['has_label'] = true;
                        }
                        else
                            $t_product['has_label'] = false;

                        $temp['products'][] = $t_product;
                    }
                    if (count($temp['products']) > 0)
                        $tags_dto[] = $temp;
                }
            }
            else {
                $temp_position = $tag->position;

                $temp_tag = $temp_tags[rand(0, count($temp_tags)-1)];
                $temp = array();
                $temp['id'] = $temp_tag->id;
                $temp['name'] = $temp_tag->name;
                $temp['position'] = $temp_tag->position;
                $temp['products'] = array();
                $products_tag = ProductTag::model()->findAll('tag=:id_tag', array(':id_tag' => $temp['id']));
                foreach($products_tag as $product_tag){
                    $product = Product::model()->findByPk($product_tag->product);
                    $t_product = array();
                    $t_product['id'] = $product->id;
                    $t_product['image'] = $product->_tag_home_image->getFileUrl('normal');
                    $t_product['name'] = $product->name;
                    $t_product['price'] = $product->price;
                    $label = $product->label0;
                    if (isset($label)) {
                        $t_product['label'] = $product->label0->_icon->getFileUrl('normal');
                        $t_product['has_label'] = true;
                    }
                    else
                        $t_product['has_label'] = false;

                    $temp['products'][] = $t_product;
                }
                if (count($temp['products']) > 0)
                    $tags_dto[] = $temp;

                $temp_tags = array();
                $temp_tags[] = $tag;

                if ($count == count($tags)) {
                    $temp['id'] = $tag->id;
                    $temp['name'] = $tag->name;
                    $temp['position'] = $tag->position;
                    $temp['products'] = array();
                    $products_tag = ProductTag::model()->findAll('tag=:id_tag', array(':id_tag' => $temp['id']));
                    foreach($products_tag as $product_tag){
                        $product = Product::model()->findByPk($product_tag->product);
                        $t_product = array();
                        $t_product['id'] = $product->id;
                        $t_product['image'] = $product->_tag_home_image->getFileUrl('normal');
                        $t_product['name'] = $product->name;
                        $t_product['price'] = $product->price;
                        $label = $product->label0;
                        if (isset($label)) {
                            $t_product['label'] = $product->label0->_icon->getFileUrl('normal');
                            $t_product['has_label'] = true;
                        }
                        else
                            $t_product['has_label'] = false;

                        $temp['products'][] = $t_product;
                    }
                    if (count($temp['products']) > 0)
                        $tags_dto[] = $temp;
                }
            }

        }
        $tag_classes = array('most-reviews', 'new-arrivals', 'popular-products');

        return $controller->render('index',
            array(
                'galleries' => $home_galleries_dto,
                'trends' => $trends_dto,
                'tags' => $tags_dto,
                'categories_images' => $categories_images_dto,
                'tags' => $tags_dto,
                'classes' => $tag_classes,
                'home_category' => $home_category_dto
            )
        );

    }
}