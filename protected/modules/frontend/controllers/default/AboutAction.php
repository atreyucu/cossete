<?php

class AboutAction extends CAction
{
    public function run()
    {
        $controller = $this->getController();
        $controller->common_data['view'] = 'about';

        $about_page = AboutusPage::model()->find();
        $about_dto = array();
        $about_dto['title'] = 'about cossette';
        $about_dto['description_title'] = 'who we are?';

        $about_dto['description'] = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed nisi sem. Nullam
        et lorem magna, in consectetur erat. Aliquam fermentum fringilla libero a vulputate. Curabitur non arcu non
        tortor semper dictum. Pellentesque pulvinar ornare quam, in faucibus tortor elementum vitae. Nam ut lacinia mi.
        Nullam et hendrerit tellus. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos
        himenaeos. Sed nec pulvinar dolor. Aenean in velit non lectus semper hendrerit. Etiam ligula eros, aliquam ut
        ullamcorper quis, pulvinar in arcu. ";

        $about_dto['section1_title'] = 'Authentic American Style';
        $about_dto['section1_subtitle'] = 'THE NEW FASHION WAY';
        $company = CompanyInfo::model()->find();
        $about_dto['section1_logo'] = '/static/imgs/brand-logo-small.png';
        $about_dto['section1_address'] = '4556 NT 2Th St. Miami, FL 33144.';
        $about_dto['section1_phone'] = '859.747.5466';
        if (isset($company)){
            $company = new CompanyInfo();
            $about_dto['section1_logo'] = $company->_company_small_logo->getFileUrl('nomal');
            $about_dto['section1_address'] = $company->addr;
            $about_dto['section1_phone'] = $company->phone1;
        }

        $about_dto['section1_main_banner'] = '/static/imgs/about-main-banner.jpg';
        $about_dto['section1_banner1'] = '/static/imgs/about-banner-1.jpg';
        $about_dto['section1_banner2'] = '/static/imgs/about-banner-2.jpg';
        $about_dto['section2_title'] = 'about cossette';
        $about_dto['section2_subtitle'] = 'about cossette';
        $about_dto['section2_description'] = '<p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis sed
        nisi sem. Nullam et lorem magna, in consectetur erat. Aliquam fermentum fringilla libero a vulputate. Curabitur
        non arcu non tortor semper dictum. Pellentesque pulvinar ornare quam, in faucibus tortor elementum vitae. Nam ut
         lacinia mi. </p>';
        $about_dto['section2_link1'] = 'Potus inciviliter ducunt';
        $about_dto['section2_link2'] = 'Lorem ipsum dolor sit';
        $about_dto['section2_image'] = '/static/imgs/pretty-woman.png';
        if (isset($about_page)) {
            $about_dto['title'] = $about_page->breadcrumb_title;
            $about_dto['description_title'] = strtr($about_page->description_title, array('<p>' => '', '</p>' => ''));
            $about_dto['description'] = $about_page->page_description;
            $about_dto['section1_title'] = $about_page->section1_title;
            $about_dto['section1_subtitle'] = $about_page->section1_subtitle;
            $about_dto['section1_main_banner'] = $about_page->_section1_about_main_banner->getFileUrl('normal');
            $about_dto['section1_banner1'] = $about_page->_section1_about_banner1->getFileUrl('normal');
            $about_dto['section1_banner2'] = $about_page->_section1_about_banner2->getFileUrl('normal');
            $about_dto['section2_title'] = $about_page->section2_title;
            $about_dto['section2_subtitle'] = $about_page->section2_subtitle;
            $about_dto['section2_description'] = $about_page->section2_description;
            $about_dto['section2_link1'] = $about_page->section2_link1_text;
            $about_dto['section2_link2'] = $about_page->section2_link2_text;
            $about_dto['section2_image'] = $about_page->_section2_image->getFileUrl('normal');
        }

        return $controller->render('about',
            array(
                'about' => $about_dto
            )
        );

    }
}