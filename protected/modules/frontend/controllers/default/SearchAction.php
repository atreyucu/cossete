<?php

class SearchAction extends CAction
{
    public function run()
    {
        $controller = $this->getController();
        $controller->common_data['view'] = 'search';

        $category_page = ProductListPage::model()->find();
        $category_page_dto = array();
        $category_page_dto['items'] = 9;
        $category_page_dto['tree_label'] = 'Product categories';
        $category_page_dto['filter_label'] = 'Filter by price';
        $category_page_dto['filter_button_label'] = 'Filter';
        $category_page_dto['right_image1'] = '/static/imgs/product-banner-02.jpg';
        $category_page_dto['right_image2'] = '/static/imgs/product-banner-01.jpg';
        $category_page_dto['main_category_image'] = '/static/imgs/product-parallax.jpg';
        $category_page_dto['main_category_title'] = 'Authentic American Style';
        $category_page_dto['main_category_subtitle'] = 'THE NEW FASHION WAY';
        $category_page_dto['main_category_description'] = "<p> Duis sed nisi sem. Nullam et lorem magna, in consectetur
        erat. Aliquam fermentum fringilla libero a vulputate. Curabitur non arcu non tortor semper dictum. Pellente
        pulvinar ornare quam, in faucibus tortor elementum vitae. Nam ut lacini </p>";
        $category_page_dto['main_category_button'] = 'Sales';
        $category_page_dto['main_category'] = '';
        if (isset($category_page)){
            $category_page_dto['items'] = $category_page->items_per_page;
            $category_page_dto['tree_label'] = $category_page->categories_title;
            $category_page_dto['filter_label'] = $category_page->filter_title;
            $category_page_dto['filter_button_label'] = $category_page->filter_button_label;
            $category_page_dto['right_image1'] = $category_page->_litle_banner_image1->getFileUrl('normal');
            $category_page_dto['right_image2'] = $category_page->_litle_banner_image2->getFileUrl('normal');
            $category_page_dto['main_category_image'] = $category_page->_main_category_image->getFileUrl('normal');
            $category_page_dto['main_category_title'] = $category_page->main_category_title;
            $category_page_dto['main_category_subtitle'] = $category_page->main_category_subtitle;
            $category_page_dto['main_category_description'] = $category_page->main_category_description_text;
            $category_page_dto['main_category_button'] = $category_page->main_category_button;
            $category_page_dto['main_category'] = $controller->createUrl('/frontend/default/category', array('id'=>$category_page->category0->id));

            $category_page_dto['small_logo'] = '/static/imgs/brand-logo-small.png';
            $company = CompanyInfo::model()->find();
            if (isset($company))
                $category_page_dto['small_logo'] = $company->_company_small_logo->getFileUrl('normal');
        }

        $criteria = new CDbCriteria();
        if ($_GET['b'] && $_GET['f']) {
            $criteria->addCondition('price >= :b and price <= :f');
            $criteria->params = array(':b' => $_GET['b'], ':f' => $_GET['f']);
            $b = $_GET['b'];
            $f = $_GET['f'];
        } else {
            $b = 120;
            $f = 375;
        }
        $order = 'ASC';
        if ($_GET['o'])
            $order = $_GET['o'];

        //Relacion con los tags
        $tag_ids = array();

        if ($_GET['t']) {
            $temp_criteria = new CDbCriteria();
            $temp_criteria->compare('tag',$_GET['t']);

            $tag_ids_obj = ProductTag::model()->findAll($temp_criteria);

            foreach($tag_ids_obj as $tag_id){
                $tag_ids[] = $tag_id->product;
            }
        }

        if($tag_ids){
            $criteria->addInCondition('id',$tag_ids);
        }

        $criteria->compare('name', $_GET['q'], true);
        $criteria->order = 'price '.$order;
        //$criteria->params = array(':match' => $_GET['q']);

        $count = Product::model()->count($criteria);
        $pages = new CPagination($count);
        //se define la cantidad de elementos por pagina
        $pages->pageSize = $category_page_dto['items'];
        $pages->applyLimit($criteria);

        $products = Product::model()->findAll($criteria);
        $products_dto = array();

        foreach($products as $product) {
            $temp = array();
            $temp['id'] = $product->id;
            $temp['image'] = $product->_listing_image->getFileUrl('normal');
            $temp['alt'] = $product->listing_image;
            $temp['name'] = $product->name;
            $label = $product->label0;
            if (isset($label)) {
                $temp['label'] = $product->label0->_icon->getFileUrl('normal');
                $temp['has_label'] = true;
            }
            else
                $temp['has_label'] = false;
            $temp['price'] = $product->price;

            $products_dto[] = $temp;
        }

        if (isset($_GET['page'])) {
            $initial = ($_GET['page']-1)*9;
            $end = (($count - $initial) > 9 ? $initial + 9 : $count);
        } elseif ($count > 0) {
            $initial = 0;
            $end = ($count > 9 ? 9 : $count);
        }

        $categories = ProductCategory::model()->roots()->findAll();
        $categories_dto = array();
        foreach($categories as $categ){
            //$categ = new ProductCategory();
            $temp = array();
            $temp['id'] = $categ->id;
            $temp['name'] = $categ->name;
            $temp['products'] = Product::model()->count('category=:category_id', array(':category_id' => $categ->id));
            $temp['children'] = array();
            $children = $categ->descendants(1)->findAll();
            foreach($children as $child){
                $temp2 = array();
                $temp2['id'] = $child->id;
                $temp2['name'] = $child->name;
                $temp2['products'] = Product::model()->count('category=:category_id', array(':category_id' => $child->id));
                $temp['products'] += $temp2['products'];

                $temp['children'][] = $temp2;
            }
            $categories_dto[] = $temp;
        }

        return $controller->render('search',
            array(
                'categories' => $categories_dto,
                'category_page' => $category_page_dto,
                'category_name' => 'Search',
                'products' => $products_dto,
                'pages' => $pages,
                'initial' => $initial,
                'end' => $end,
                'count' => $count,
                'f' => $f,
                'b' => $b,
                'o' => $order
            )
        );

    }
}