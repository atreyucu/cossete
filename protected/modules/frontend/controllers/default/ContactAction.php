<?php

class ContactAction extends CAction
{
    public function run()
    {
        $controller = $this->getController();
        $controller->common_data['view'] = 'contact';

        if (Yii::app()->request->isPostRequest){

            $email = $_POST['email'];
            $message = $_POST['message'];
            $name = $_POST['name'];

            $mail = new YiiMailer('contactus', array('message' => nl2br($message), 'from' => $name));
            //set properties
            $mail->setFrom($email, $name);
            $mail->setSubject($message);
            $company_info = CompanyInfo::model()->find();

            $mail->setTo($company_info->email);

            if ($mail->send()) {
                Yii::app()->user->setFlash('success',t('Thanks for contact us. We answers you soon as possible.'));
            } else {
                Yii::app()->user->setFlash('error',t('Error sending email: ').$mail->getError());
            }
        }

        $contact_page = ContactusPage::model()->find();
        $contact_dto = array();
        $contact_dto['title'] = 'Contact Us';
        $contact_dto['form_title'] = 'Send us and email';
        $contact_dto['social_title'] = 'Follow Us';
        $contact_dto['address_title'] = 'Address';
        $contact_dto['address_description'] = 'Proin tempor metus ut risus. Nulla dolor. Vesbulum diam nisl, aliquet
                                               tempor, mattis id, varius id, nibh. Quisque vel diam id urna dapibus
                                               sollicitudin. Cras eget neque eu leo sollicitudin dignissim.';
        $contact_dto['send_button'] = 'send';
        $contact_dto['map'] = '/static/imgs/contact-map.jpg';
        $contact_dto['use_map'] = 0;

        if (isset($contact_page)) {
            $contact_dto['title'] = $contact_page->breadcrumb_title;
            $contact_dto['form_title'] = $contact_page->form_title;
            $contact_dto['social_title'] = $contact_page->breadcrumb_title;
            $contact_dto['address_title'] = $contact_page->address_title;
            $contact_dto['address_description'] = strtr($contact_page->addr_description, array('<p>' => '', '</p>' => ''));
            $contact_dto['send_button'] = $contact_page->send_button_text;
            $contact_dto['use_map'] = $contact_page->use_map;
            if ($contact_page->use_map) {
                $contact_dto['longitud'] = $contact_page->longitude;
                $contact_dto['latitude'] = $contact_page->latitude;
            }
            else {
                $contact_dto['map'] = $contact_page->_map_image->getFileUrl('normal');
            }
        }

        $company = CompanyInfo::model()->find();
        $contact_dto['address'] = "4556 NT 2Th St. Miami, FL 33144.";
        $contact_dto['phone'] = "859.747.5466";
        if (isset($company)) {
            $contact_dto['address'] = $company->addr;
            $contact_dto['phone'] = $company->phone1;
        }

        $social = SocialLinks::model()->find();

        return $controller->render('contact',
            array(
                'contact' => $contact_dto,
                'social' => $social
            )
        );

    }
}