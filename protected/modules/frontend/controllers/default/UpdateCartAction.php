<?php

class UpdateCartAction extends CAction
{
    public function run()
    {
        $controller = $this->getController();
        $controller->layout = false;

        $session = Yii::app()->session;

        if(Yii::app()->request->isAjaxRequest){
            if($session['cart']['counts'] == 0){
                $response = array('success' => 1,'count'=> 0, 'total'=> 0, 'rtotal'=> 0);
            }
            $response = array('success' => 1,'count'=> $session['cart']['counts'], 'total'=> $session['cart']['total'], 'rtotal'=> $session['cart']['rtotal']);
            $json = json_encode($response);
            $controller->renderText($json);
        }
        else{
            $controller->renderText('');
        }

    }
}