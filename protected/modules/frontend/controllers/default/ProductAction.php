<?php

class ProductAction extends CAction
{
    public function run()
    {
        $controller = $this->getController();
        $product_id = $_GET['id'];

        $product = Product::model()->findByPk($product_id);
        $product_dto = array();
        if (!isset($product))
            throw new CHttpException(404, 'The specified post cannot be found.');
        //$product = new Product();
        $product_dto['id'] = $product->id;
        $product_dto['name'] = $product->name;
        $product_dto['description'] = $product->description;
        $product_dto['image'] = $product->_detail_image->getFileUrl('normal');
        $product_dto['zoom_image'] = $product->_detail_zoom_image->getFileUrl('normal');
        $product_dto['price'] = $product->price;
        $product_dto['code'] = $product->code;
        $product_dto['category'] = $product->category;
        $label = $product->label0;
        if (isset($label)) {
            $product_dto['label'] = $product->label0->_icon->getFileUrl('normal');
            $product_dto['has_label'] = true;
        } else
            $product_dto['has_label'] = false;

        $category = $product->category0;
        $categories = $this->getCategories($category, array());
        $categories_dto = array();

        foreach ($categories as $pcategory) {
            $temp = array();
            $temp['id'] = $pcategory->id;
            $temp['name'] = $pcategory->name;
            $categories_dto[] = $temp;
        }

        $colors = $product->productColors;
        $colors_dto = array();
        foreach($colors as $pcolor){
            $color = Color::model()->findByPk($pcolor->color);
            $temp = array();
            $temp['id'] = $color->id;
            $temp['name'] = $color->name;

            $colors_dto[] = $temp;
        }
        $sizes = $product->productSizes;
        $sizes_dto = array();
        foreach($sizes as $psize){
            $size = Size::model()->findByPk($psize->size);
            $temp = array();
            $temp['id'] = $size->id;
            $temp['name'] = $size->name;

            $sizes_dto[] = $temp;
        }

        $relates = Product::model()->findAll('category=:category and id<>:id', array(':category'=>$product->category, 'id'=>$product->id));
        $relates_dto = array();
        foreach($relates as $prelate){
            $temp = array();
            $temp['id'] = $prelate->id;
            $temp['image'] = $prelate->_listing_image->getFileUrl('normal');
            $label = $product->label0;
            if (isset($label)) {
                $temp['label'] = $prelate->label0->_icon->getFileUrl('normal');
                $temp['has_label'] = true;
            }
            else
                $temp['has_label'] = false;

            $relates_dto[] = $temp;
        }

        $product_page = ProductPage::model()->find();
        $product_page_dto = array();
        $product_page_dto['right_banner'] = '/static/imgs/product-banner-01.jpg';
        $product_page_dto['left_banner'] = '/static/imgs/product-banner-02.jpg';
        $product_page_dto['relates_text'] = 'related products';
        $product_page_dto['cart'] = 'add to cart';
        if (isset($product_page)){
            $product_page_dto['right_banner'] = $product_page->_litle_banner_image1->getFileUrl('normal');
            $product_page_dto['left_banner'] = $product_page->_litle_banner_image2->getFileUrl('normal');
            $product_page_dto['relates_text'] = $product_page->related_product_text;
            $product_page_dto['cart'] = $product_page->addcart_button_text;
        }

        return $controller->render('product',
            array(
                'product' => $product_dto,
                'categories' => $categories_dto,
                'colors' => $colors_dto,
                'sizes' => $sizes_dto,
                'relates' => $relates_dto,
                'product_page' => $product_page_dto
            )
        );
    }

    public function getCategories($category, $array)
    {
        $array[] = $category;
        //ddump($category->parent()->find()->parent()->find());
        if ($category == null || $category->isRoot())
            return $array;
        else
            return $this->getCategories($category->parent()->find(), $array);
    }

}