<?php

class CheckoutAction extends CAction
{
    public function run()
    {

        $controller = $this->getController();
        $controller->common_data['view'] = 'checkout';

        $session = Yii::app()->session;

        $cart = $session['cart'];

        if($cart['counts'] == 0){
            $controller->redirect('/');
        }

        if(Yii::app()->request->isPostRequest){
            $notes = $_POST['notes'];

            //ddump($_POST);

            $user = Yii::app()->user;

            $bfirstname = $_POST['bfirstname'];
            $blastname = $_POST['blastname'];
//            $bcompany = $_POST['bcompany'];
            $baddr = $_POST['baddr'];
            $bcity = $_POST['bcity'];
            $bcounty = $_POST['bcounty'];
            $bcountry = $_POST['bcountry'];
            $bpostcode = $_POST['bpostcode'];
            $bemail = $_POST['bemail'];
            $bphone = $_POST['bphone'];

            if($_POST['diferent_addr']){
                $sfirstname = $_POST['sfirstname'];
                $slastname = $_POST['slastname'];
//                $scompany = $_POST['scompany'];
                $saddr = $_POST['saddr'];
                $scity = $_POST['scity'];
                $scounty = $_POST['scounty'];
                $scountry = $_POST['scountry'];
                $spostcode = $_POST['spostcode'];
                $semail = $_POST['semail'];
                $sphone = $_POST['sphone'];
            }
            else{
                $sfirstname = $_POST['bfirstname'];
                $slastname = $_POST['blastname'];
//                $scompany = $_POST['bcompany'];
                $saddr = $_POST['baddr'];
                $scity = $_POST['bcity'];
                $scounty = $_POST['bcounty'];
                $scountry = $_POST['bcountry'];
                $spostcode = $_POST['bpostcode'];
                $semail = $_POST['bemail'];
                $sphone = $_POST['bphone'];
            }

            $trans_info1 = new TransInfo();

            $trans_info1->first_name = $bfirstname;
            $trans_info1->last_name = $blastname;
            $trans_info1->company_name = '';
            $trans_info1->address = $baddr;
            $trans_info1->city = $bcity;
            $trans_info1->country = $bcountry;
            $trans_info1->state = $bcounty;
            $trans_info1->zip_code = $bpostcode;
            $trans_info1->email = $bemail;
            $trans_info1->type = '2';
            $trans_info1->phone = $bphone;

            $trans_info1->save();

            $trans_info2 = new TransInfo();

            $trans_info2->first_name = $sfirstname;
            $trans_info2->last_name = $slastname;
            $trans_info2->company_name = '';
            $trans_info2->address = $saddr;
            $trans_info2->city = $scity;
            $trans_info2->country = $scountry;
            $trans_info2->state = $scounty;
            $trans_info2->zip_code = $spostcode;
            $trans_info2->email = $semail;
            $trans_info2->phone = $sphone;
            $trans_info2->type = '1';

            $trans_info2->save();


            $order = new Order();

            $order->shipping_info = $trans_info2->id;
            $order->billing_info = $trans_info1->id;
            $order->order_total = $cart['rtotal'];
            $order->order_subtotal = $cart['total'];
            $order->shipping_cost = $cart['rtotal'] - $cart['total'];
            $order->notes = $notes;
            $order->client = $user->id;
            $shipping_option = ShippingOption::model()->findByPk($cart['shipping']);
            $order->shipping_option = $shipping_option->name;
            $order->status = 1;

            $order->save();

            $order_number = new OrderNumber();
            $order_number->save();

            $temp_str = str_pad((string)$order_number->id, 9, '0', STR_PAD_LEFT);

            $order_number->char_number = 'T' . $temp_str;

            $order->order_number = 'T' . $temp_str;
            $order->save();

            foreach($cart['products'] as $key=>$product){
                $temp_prod = new ProductOrder();
                $temp_prod->product = $product['pid'];
                $temp_prod->ord = $order->id;

                if($product['sid'] != 'no_size'){
                    $temp_size = Size::model()->findByPk($product['sid']);
                    $temp_size = $temp_size->name;
                }
                else{
                    $temp_size = '--';
                }
                $temp_prod->psize = $temp_size;

                if($product['cid'] != 'no_color'){
                    $temp_color = Color::model()->findByPk($product['cid']);
                    $temp_color = $temp_color->name;
                }
                else{
                    $temp_color = '--';
                }
                $temp_prod->pcolor = $temp_color;
                $temp_prod->quantity = $product['qty'];
                $temp_prod->price = $product['total'];
                $temp_prod->save();
            }

            $shipp_addr_criteria = new CDbCriteria();
            $shipp_addr_criteria->compare('type','1');
            $shipp_addr_criteria->compare('client',$user->id);

            $ship_addr = ClientAddress::model()->find($shipp_addr_criteria);

            if(!$ship_addr){
                $new_client_addr = new ClientAddress();
                $new_client_addr->type = '1';
                $new_client_addr->first_name = $sfirstname;
                $new_client_addr->last_name = $slastname;
                $new_client_addr->company_name = '';
                $new_client_addr->address = $saddr;
                $new_client_addr->city = $scity;
                $new_client_addr->country = $scountry;
                $new_client_addr->state = $scounty;
                $new_client_addr->zip_code = $spostcode;
                $new_client_addr->email = $semail;
                $new_client_addr->phone = $sphone;
                $new_client_addr->client = Yii::app()->user->id;
                $new_client_addr->save();
            }


            $billing_addr_criteria = new CDbCriteria();
            $billing_addr_criteria->compare('type','2');
            $billing_addr_criteria->compare('client',$user->id);

            $billing_addr = ClientAddress::model()->find($billing_addr_criteria);

            if(!$billing_addr){
                $new_client_addr = new ClientAddress();
                $new_client_addr->type = '2';
                $new_client_addr->first_name = $bfirstname;
                $new_client_addr->last_name = $blastname;
                $new_client_addr->company_name = '';
                $new_client_addr->address = $baddr;
                $new_client_addr->city = $bcity;
                $new_client_addr->country = $bcountry;
                $new_client_addr->state = $bcounty;
                $new_client_addr->zip_code = $bpostcode;
                $new_client_addr->email = $bemail;
                $new_client_addr->phone = $bphone;
                $new_client_addr->client = Yii::app()->user->id;
                $new_client_addr->save();
            }

            $total = $cart['rtotal'];
            $qty = $cart['counts'];

            $cart['products'] = array();
            $cart['counts'] = 0;
            $cart['total'] = 0;
            $cart['rtotal'] = 0;

            $session['cart'] = $cart;

            return $controller->redirect(array('/frontend/paypal/buy', 'order' => $order->primaryKey));
        }

        $country_criteria = new CDbCriteria();
        $country_criteria->compare('is_active',1);

        $countries = Country::model()->findAll($country_criteria);

        $country_arr = array();

        foreach($countries as $country){
            $country_arr[$country->id] = array('name'=>$country->name,'code'=>$country->code);
        }

        $products = $session['cart']['products'];

        $total = $session['cart']['total'];
        $rtotal = $session['cart']['rtotal'];
        $counts = $session['cart']['counts'];
        $pshipp = $session['cart']['shipping'];

        $checkout_page = CheckoutPage::model()->find();

        if(isset($checkout_page)){
            $breadcrumb_title = $checkout_page->breadcrumb_title;
            $billing_form_header = $checkout_page->billing_form_header;
            $shipping_form_header = $checkout_page->shipping_form_header;
            $your_order_header = $checkout_page->your_order_header;
            $cart_totals_header = $checkout_page->cart_totals_header;
            $subtotal_label = $checkout_page->subtotal_label;
            $shipping_label = $checkout_page->shipping_label;
            $total_label = $checkout_page->total_label;
            $accept_terms_text = $checkout_page->accept_terms_text;
            $terms_cond_link_text = $checkout_page->terms_cond_link_text;
            $checkout_button_text = $checkout_page->checkout_button_text;
        }
        else{
            $breadcrumb_title = 'Shopping Cart';
            $billing_form_header = 'Billing details';
            $shipping_form_header = 'Shipping details';
            $your_order_header = 'your order';
            $cart_totals_header = 'cart totals';
            $subtotal_label = 'subtotal';
            $shipping_label = 'shipping';
            $total_label = 'total';
            $accept_terms_text = 'accept the';
            $terms_cond_link_text = 'terms and conditions';
            $checkout_button_text = 'checkout';
        }


        $shipp_criteria = new CDbCriteria();
        $shipp_criteria->compare('is_active',1);

        $shippings = ShippingOption::model()->findAll($shipp_criteria);

        $shipp_arr =  array();

        foreach($shippings as $shipp){
            $shipp_arr[$shipp->id] = $shipp->name;
        }

        $user = Yii::app()->user;

        $shipp_addr_criteria = new CDbCriteria();
        $shipp_addr_criteria->compare('type','1');
        $shipp_addr_criteria->compare('client',$user->id);

        $ship_addr = ClientAddress::model()->find($shipp_addr_criteria);

        $have_shipp_addr = false;

        if(isset($ship_addr)){
            $sfirst_name = $ship_addr->first_name;
            $slast_name = $ship_addr->last_name;
            $scompany_name = $ship_addr->company_name;
            $saddress = $ship_addr->address;
            $scity = $ship_addr->city;
            $sstate = $ship_addr->state;
            $szip_code = $ship_addr->zip_code;
            $semail = $ship_addr->email;
            $sphone = $ship_addr->phone;
            $scountry = $ship_addr->country;
            $have_shipp_addr = true;
        }

        $billing_addr_criteria = new CDbCriteria();
        $billing_addr_criteria->compare('type','2');
        $billing_addr_criteria->compare('client',$user->id);

        $billing_addr = ClientAddress::model()->find($billing_addr_criteria);

        $have_billing_addr = false;

        if(isset($billing_addr)){
            $sfirst_name = $billing_addr->first_name;
            $slast_name = $billing_addr->last_name;
            $scompany_name = $billing_addr->company_name;
            $saddress = $billing_addr->address;
            $scity = $billing_addr->city;
            $sstate = $billing_addr->state;
            $szip_code = $billing_addr->zip_code;
            $semail = $billing_addr->email;
            $sphone = $billing_addr->phone;
            $scountry = $billing_addr->country;

            $have_shipp_addr = true;


            $bfirst_name = $billing_addr->first_name;
            $blast_name = $billing_addr->last_name;
            $bcompany_name = $billing_addr->company_name;
            $baddress = $billing_addr->address;
            $bcity = $billing_addr->city;
            $bstate = $billing_addr->state;
            $bzip_code = $billing_addr->zip_code;
            $bemail = $billing_addr->email;
            $bphone = $billing_addr->phone;
            $bcountry = $billing_addr->country;

            $have_billing_addr = true;
        }




        return $controller->render('checkout',
            array(
                'countries' => $country_arr,
                'products' => $products,
                'total' => $total,
                'rtotal' => $rtotal,
                'counts'=> $counts,
                'breadcrumb_title' => $breadcrumb_title,
                'billing_form_header' => $billing_form_header,
                'shipping_form_header' => $shipping_form_header,
                'your_order_header' => $your_order_header,
                'cart_totals_header' => $cart_totals_header,
                'subtotal_label' => $subtotal_label,
                'shipping_label' => $shipping_label,
                'total_label' => $total_label,
                'accept_terms_text' => $accept_terms_text,
                'terms_cond_link_text' => $terms_cond_link_text,
                'checkout_button_text' => $checkout_button_text,
                'shippings' => $shipp_arr,
                'pshipp' => $pshipp,

                'slast_name' => $slast_name,
                'sfirst_name' => $sfirst_name,
                'scompany_name' => $scompany_name,
                'saddress' => $saddress,
                'scity' => $scity,
                'sstate' => $sstate,
                'szip_code' => $szip_code,
                'semail' => $semail,
                'sphone' => $sphone,
                'scountry' => $scountry,

                'have_shipp_addr' => $have_shipp_addr,


                'bfirst_name' => $bfirst_name,
                'blast_name' => $blast_name,
                'bcompany_name' => $bcompany_name,
                'baddress' => $baddress,
                'bcity' => $bcity,
                'bstate' => $bstate,
                'bzip_code' => $bzip_code,
                'bemail' => $bemail,
                'bphone' => $bphone,
                'bcountry' => $bcountry,

                'have_billing_addr' => $have_billing_addr,
            )
        );

    }
}