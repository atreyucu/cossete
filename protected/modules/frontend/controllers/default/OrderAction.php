<?php

class OrderAction extends CAction
{
    public function run()
    {
        $controller = $this->getController();
        $controller->common_data['view'] = 'order';

        //if($_GET['id']){
            $order_id = $_GET['id'];

            $order = Order::model()->findByPk($order_id);

            $ship_addr = TransInfo::model()->findByPk($order->shipping_info);
            $billing_addr = TransInfo::model()->findByPk($order->billing_info);

            $products_criteria = new CDbCriteria();
            $products_criteria->compare('ord',$order->id);

            $product_id_list = ProductOrder::model()->findAll($products_criteria);

            $product_arr = array();

            foreach($product_id_list as $product){
                $real_prod = Product::model()->findByPk($product->product);
                $product_arr[$product->product] = array('name'=>$real_prod->name, 'size'=>$product->psize, 'color'=>$product->pcolor, 'qty'=> $product->quantity, 'price'=>$product->price);
            }



            if($ship_addr) {
                $country = Country::model()->findByPk($ship_addr->country);
                $ship_addr_arr = array('id'=>$ship_addr->id,'name' => $ship_addr->first_name . ' ' . $ship_addr->last_name,
                    'company_name' => $ship_addr->company_name,
                    'addr' => $ship_addr->address,
                    'country' => $ship_addr->city . '/' . $country->name . ' ' . $ship_addr->zip_code,
                    'phone'=> $ship_addr->phone
                );
            }

            if($billing_addr) {
                $country = Country::model()->findByPk($billing_addr->country);
                $billing_addr_arr = array('id'=>$billing_addr->id, 'name' => $billing_addr->first_name . ' ' . $billing_addr->last_name,
                    'company_name' => $billing_addr->company_name,
                    'addr' => $billing_addr->address,
                    'country' => $billing_addr->city . '/' . $country->name . ' ' . $billing_addr->zip_code
                );
            }


            if($order){
                $a = new DateTime($order->created);
                $date = $a->format('M,d Y');
                $order_arr = array('id'=>$order->id,'number'=>$order->order_number,'date'=> $date, 'total'=>$order->order_total, 'shiiping'=>$order->shipping_option, 'status'=>$order->status);
            }

            $session = Yii::app()->session;
            $cart = $session['cart'];

            $order_page = CheckoutOrderPage::model()->find();
            if($order_page){
                $page_title = $order_page->breadcrumb_title;
                $order_list_header = $order_page->order_list_header;
                
                $cart_details_header = $order_page->cart_details_header;
                $subtotal_label = $order_page->subtotal_label;
                $shipping_label = $order_page->shipping_label;
                $total_label = $order_page->total_label;
                $have_question_link_text = $order_page->have_question_link_text;
                $call_us_text = $order_page->call_us_text;
                $order_detail_header = $order_page->order_detail_header;
                $product_column_label = $order_page->product_column_label;
                $customer_detail_header = $order_page->customer_detail_header;

                $billing_address = $order_page->billing_address;
                $shipping_address = $order_page->shipping_address;
            }
            else{
                $page_title = 'orders details';
                $order_list_header = 'Order received';

                $cart_details_header = 'Cart Details';
                $subtotal_label = 'Subtotal';
                $shipping_label = 'Shipping ';
                $total_label = 'Total';
                $have_question_link_text = 'Have question?';
                $call_us_text = 'Call us at';
                $order_detail_header = 'Order details';
                $product_column_label = 'Please send your cheque to Store Name, Store Street, Store Twon, Store State/Country, Store Postcode';
                $customer_detail_header = 'Customer details';

                $billing_address = 'Billing address';
                $shipping_address = 'Shipping address';
            }


            $product_count = ProductOrder::model()->count('ord=:id_order', array(':id_order' => $order->id));
            $product_total = $order->order_subtotal;
            $product_rtotal = $order->order_total;
            $pshipp = $order->shipping_option;

            $shipp_criteria = new CDbCriteria();
            $shipp_criteria->compare('is_active',1);

            $shippings = ShippingOption::model()->findAll($shipp_criteria);

            $shipp_arr =  array();

            foreach($shippings as $shipp){
                $shipp_arr[$shipp->id] = $shipp->name;
            }

            $user = User::model()->findByPk(Yii::app()->user->id);

            return $controller->render('order',
                array(
                    'product_count' => $product_count,
                    'product_total' => $product_total,
                    'product_rtotal' => $product_rtotal,
                    'shipps' => $shipp_arr,
                    'pshipp' => $pshipp,
                    'order'=>$order_arr,
                    'billing'=>$billing_addr_arr,
                    'shipping'=>$ship_addr_arr,
                    'products'=>$product_arr,

                    'page_title' => $page_title,
                    'order_list_header' => $order_list_header,

                    'cart_details_header' => $cart_details_header,
                    'subtotal_label' => $subtotal_label,
                    'shipping_label' => $shipping_label,
                    'total_label' => $total_label,
                    'have_question_link_text' => $have_question_link_text,
                    'call_us_text' => $call_us_text,
                    'order_detail_header' => $order_detail_header,
                    'product_column_label' => $product_column_label,
                    'customer_detail_header' => $customer_detail_header,

                    'billing_address' => $billing_address,
                    'shipping_address' => $shipping_address,
                    'user_email'=>$user->email
                )
            );


        //}
        /*else{
            $controller->redirect('/');
        }*/

    }
}