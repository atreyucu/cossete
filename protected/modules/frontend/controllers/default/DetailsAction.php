<?php

class DetailsAction extends CAction
{
    public function run()
    {
        $controller = $this->getController();
        $controller->common_data['view'] = 'details';
        $user = Yii::app()->user;

        if(Yii::app()->request->isPostRequest){
            $bfirstname = $_POST['bfirstname'];
            $blastname = $_POST['blastname'];
//            $bcompany = $_POST['bcompany'];
            $baddr = $_POST['baddr'];
            $bcity = $_POST['bcity'];
            $bcounty = $_POST['bcounty'];
            $bcountry = $_POST['bcountry'];
            $bpostcode = $_POST['bpostcode'];
            $bemail = $_POST['bemail'];
            $bphone = $_POST['bphone'];

            if($_POST['diferent_addr']){
                $sfirstname = $_POST['sfirstname'];
                $slastname = $_POST['slastname'];
//                $scompany = $_POST['scompany'];
                $saddr = $_POST['saddr'];
                $scity = $_POST['scity'];
                $scounty = $_POST['scounty'];
                $scountry = $_POST['scountry'];
                $spostcode = $_POST['spostcode'];
                $semail = $_POST['semail'];
                $sphone = $_POST['sphone'];
            }
            else{
                $sfirstname = $_POST['bfirstname'];
                $slastname = $_POST['blastname'];
//                $scompany = $_POST['bcompany'];
                $saddr = $_POST['baddr'];
                $scity = $_POST['bcity'];
                $scounty = $_POST['bcounty'];
                $scountry = $_POST['bcountry'];
                $spostcode = $_POST['bpostcode'];
                $semail = $_POST['bemail'];
                $sphone = $_POST['bphone'];
            }

            $shipp_addr_criteria = new CDbCriteria();
            $shipp_addr_criteria->compare('type',1);
            $shipp_addr_criteria->compare('client',$user->id);

            $ship_addr = ClientAddress::model()->find($shipp_addr_criteria);
            $ship_addr->first_name = $sfirstname;
            $ship_addr->last_name = $slastname;
            $ship_addr->company_name = '';
            $ship_addr->address = $saddr;
            $ship_addr->city = $scity;
            $ship_addr->country = $scountry;
            $ship_addr->state = $scounty;
            $ship_addr->zip_code = $spostcode;
            $ship_addr->email = $semail;
            $ship_addr->phone = $sphone;
            $ship_addr->client = Yii::app()->user->id;
            $ship_addr->save();

            $billing_addr_criteria = new CDbCriteria();
            $billing_addr_criteria->compare('type', 1);
            $billing_addr_criteria->compare('client', $user->id);

            $billing_addr = ClientAddress::model()->find($shipp_addr_criteria);
            $billing_addr->first_name = $bfirstname;
            $billing_addr->last_name = $blastname;
            $billing_addr->company_name = '';
            $billing_addr->address = $baddr;
            $billing_addr->city = $bcity;
            $billing_addr->country = $bcountry;
            $billing_addr->state = $bcounty;
            $billing_addr->zip_code = $bpostcode;
            $billing_addr->email = $bemail;
            $billing_addr->phone = $bphone;
            $billing_addr->client = Yii::app()->user->id;
            $billing_addr->save();

        }

        $countries = Country::model()->findAll('is_active=1');
        $country_dto = array();
        foreach($countries as $country){
            $country_dto[$country->id] = array('name'=>$country->name,'code'=>$country->code);
        }

        $shipp_addr_criteria = new CDbCriteria();
        $shipp_addr_criteria->compare('type','1');
        $shipp_addr_criteria->compare('client',$user->id);

        $ship_addr = ClientAddress::model()->find($shipp_addr_criteria);

        $have_shipp_addr = false;

        if(isset($ship_addr)){
            $sfirst_name = $ship_addr->first_name;
            $slast_name = $ship_addr->last_name;
            $scompany_name = $ship_addr->company_name;
            $saddress = $ship_addr->address;
            $scity = $ship_addr->city;
            $sstate = $ship_addr->state;
            $szip_code = $ship_addr->zip_code;
            $semail = $ship_addr->email;
            $sphone = $ship_addr->phone;
            $scountry = $ship_addr->country;
            $have_shipp_addr = true;
        }

        $billing_addr_criteria = new CDbCriteria();
        $billing_addr_criteria->compare('type','2');
        $billing_addr_criteria->compare('client',$user->id);

        $billing_addr = ClientAddress::model()->find($billing_addr_criteria);

        $have_billing_addr = false;

        if(isset($billing_addr)){
            $sfirst_name = $billing_addr->first_name;
            $slast_name = $billing_addr->last_name;
            $scompany_name = $billing_addr->company_name;
            $saddress = $billing_addr->address;
            $scity = $billing_addr->city;
            $sstate = $billing_addr->state;
            $szip_code = $billing_addr->zip_code;
            $semail = $billing_addr->email;
            $sphone = $billing_addr->phone;
            $scountry = $billing_addr->country;

            $have_shipp_addr = true;


            $bfirst_name = $billing_addr->first_name;
            $blast_name = $billing_addr->last_name;
            $bcompany_name = $billing_addr->company_name;
            $baddress = $billing_addr->address;
            $bcity = $billing_addr->city;
            $bstate = $billing_addr->state;
            $bzip_code = $billing_addr->zip_code;
            $bemail = $billing_addr->email;
            $bphone = $billing_addr->phone;
            $bcountry = $billing_addr->country;

            $have_billing_addr = true;
        }

        if(isset($checkout_page)){
            $billing_form_header = $checkout_page->billing_form_header;
            $shipping_form_header = $checkout_page->shipping_form_header;
        }
        else{
            $billing_form_header = 'Billing details';
            $shipping_form_header = 'Shipping details';
        }

        return $controller->render('details',
            array(
                'countries' => $country_dto,
                'billing_form_header' => $billing_form_header,
                'shipping_form_header' => $shipping_form_header,

                'slast_name' => $slast_name,
                'sfirst_name' => $sfirst_name,
                'scompany_name' => $scompany_name,
                'saddress' => $saddress,
                'scity' => $scity,
                'sstate' => $sstate,
                'szip_code' => $szip_code,
                'semail' => $semail,
                'sphone' => $sphone,
                'scountry' => $scountry,

                'have_shipp_addr' => $have_shipp_addr,


                'bfirst_name' => $bfirst_name,
                'blast_name' => $blast_name,
                'bcompany_name' => $bcompany_name,
                'baddress' => $baddress,
                'bcity' => $bcity,
                'bstate' => $bstate,
                'bzip_code' => $bzip_code,
                'bemail' => $bemail,
                'bphone' => $bphone,
                'bcountry' => $bcountry,

                'have_billing_addr' => $have_billing_addr,
            )
        );

    }
}