<?php

class TermCondAction extends CAction
{
    public function run()
    {
        $controller = $this->getController();
        $controller->common_data['view'] = 'terms';

        $terms = TermConds::model()->find();
        $page_title = 'Terms & Conditions';
        $description = false;
        if (isset($terms)){
            $page_title = $terms->breadcrumb_title;
            $description = $terms->tc_description;
        }

        return $controller->render('term',
            array(
                'title' => $page_title,
                'description' => $description
            )
        );

    }
}