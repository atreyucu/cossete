<?php

class MyOrdersAction extends CAction
{
    public function run()
    {
        $controller = $this->getController();
        $controller->common_data['view'] = 'myorders';

        $order_criteria = new CDbCriteria();
        $order_criteria->compare('client',Yii::app()->user->id);
        $order_criteria->order = 'created';

        $orders = Order::model()->findAll($order_criteria);
        $orders_dto = array();
        foreach($orders as $order){
            $temp = array();
            $temp['id'] = $order->id;
            $temp['number'] = $order->order_number;
            $temp['date'] = $order->created;
            $temp['status'] = $order->status;
            $temp['total'] = $order->order_total;
            $temp['items'] = ProductOrder::model()->count('ord=:id_order', array(':id_order' => $order->id));

            $orders_dto[] = $temp;
        }

        $billing = ClientAddress::model()->find('client=:id_user and type=2', array(':id_user' => Yii::app()->user->id));
        $billing_dto = array();
        if (isset($billing)){
            $billing_dto['name'] = $billing->first_name.' '.$billing->last_name;
            $billing_dto['address'] = $billing->address;
        }
        $shipping = ClientAddress::model()->find('client=:id_user and type=1', array(':id_user' => Yii::app()->user->id));
        $shipping_dto = array();
        if (isset($shipping)){
            $shipping_dto['name'] = $shipping->first_name.' '.$shipping->last_name;
            $shipping_dto['address'] = $shipping->address;
        }

        return $controller->render('myorders',
            array(
                'orders' => $orders_dto,
                'shipping' => $shipping_dto,
                'billing' => $billing_dto
            )
        );

    }
}