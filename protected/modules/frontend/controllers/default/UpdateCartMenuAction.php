<?php

class UpdateCartMenuAction extends CAction
{
    public function run()
    {
        $controller = $this->getController();
        $controller->layout = false;

        $session = Yii::app()->session;

        if(Yii::app()->request->isAjaxRequest){
            $controller->renderPartial('_cart_menu_part',array('products'=>$session['cart']['products'], 'subtotal'=>$session['cart']['total']));
        }
        else{
            $controller->renderText('');
        }

    }
}