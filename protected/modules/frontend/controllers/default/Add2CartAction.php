<?php

class Add2CartAction extends CAction
{
    public function run()
    {
        $controller = $this->getController();
        $controller->layout = false;

        $session = Yii::app()->session;

        $cart = $session['cart'];

        if(Yii::app()->request->isAjaxRequest && Yii::app()->request->isPostRequest){
            if(isset($_POST['product_id']) && isset($_POST['color_id']) && isset($_POST['size_id'])){
                $prod = Product::model()->findByPk($_POST['product_id']);
                if(array_key_exists($_POST['product_id'].$_POST['color_id'].$_POST['size_id'],$session['cart']['products'])){
                    $cart['products'][$_POST['product_id'].$_POST['color_id'].$_POST['size_id']]['qty'] += $_POST['quantity'];
                }
                else{
                    $cart['products'][$_POST['product_id'].$_POST['color_id'].$_POST['size_id']] = array(
                        'pid' => $_POST['product_id'],
                        'cid' => $_POST['color_id'],
                        'sid' => $_POST['size_id'],
                        'qty' =>  $_POST['quantity'],
                        'price'=> $prod->price,
                        'img'=>$prod->_listing_image->getFileUrl('thumb'),
                        'name'=>$prod->name,
                        'total'=>$prod->price*$_POST['quantity'],
                     );
                }

                $cart['counts'] += $_POST['quantity'];
                $cart['total'] += $prod->price*$_POST['quantity'];

                $shipping_obj = ShippingOption::model()->findByPk($cart['shipping']);

                $total = 0;

                if($shipping_obj->condition == 1){
                    if($shipping_obj->use_percent){
                        $total += $cart['total'] + $cart['total']*$shipping_obj->percent_amount/100;
                    }
                    else{
                        $total += $cart['total'] + $shipping_obj->fixed_amount;
                    }
                }
                else{
                    if($shipping_obj->use_percent){
                        foreach($cart['products'] as $key=>$value){
                            $total += $value['total']*$shipping_obj->percent_amount/100;
                        }
                        $total += $cart['total'];
                    }
                    else{
                        foreach($cart['products'] as $key=>$value){
                            $total += $value['total']*$shipping_obj->fixed_amount;
                        }
                        $total += $cart['total'];
                    }
                }

                $cart['rtotal'] = $total;

                $session['cart'] = $cart;
            }
            $response = array('success'=>1,'message'=>'The product was successfully added.','count'=> $session['cart']['counts'], 'total'=> $session['cart']['total'], 'rtotal' => $session['cart']['rtotal']);
            $json = json_encode($response);
            $controller->renderText($json);
        }
        else{
            $controller->renderText('');
        }

    }
}