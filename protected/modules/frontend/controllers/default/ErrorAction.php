<?php

class ErrorAction extends CAction
{
    public function run()
    {
        $controller = $this->getController();
        $controller->common_data['view'] = 'error';

        $error_page = ErrorPage::model()->find();
        $error_dto = array();
        $error_dto['page_title'] = 'Error 404';
        $error_dto['title'] = 'Page not found';
        $error_dto['message'] = "<p>We're sorry!</p>
                                 <p>We can't seem to find the page you're looking for.</p>";
        $error_dto['image'] = '/static/imgs/error-parallax.jpg';
        if (isset($error_page)) {
            $error_dto['page_title'] = $error_page->breadcrumb_title;
            $error_dto['title'] = $error_page->error_title;
            $error_dto['message'] = $error_page->error_message;
            $error_dto['image'] = $error_page->_background_image->getFileUrl('normal');
        }

        return $controller->render('error',
            array(
                'code' => Yii::app()->errorHandler->error['code'],
                'error' => $error_dto
            )
        );

    }
}