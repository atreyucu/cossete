<?php

class UpdateOrderTotalAction extends CAction
{
    public function run()
    {
        $controller = $this->getController();
        $controller->layout = false;

        $session = Yii::app()->session;
        
        $cart = $session['cart'];

        if(Yii::app()->request->isAjaxRequest && Yii::app()->request->isPostRequest){
            if(isset($_POST['shipping_id']) && isset($_POST['order_id'])) {
                $shipping_obj = ShippingOption::model()->findByPk($_POST['shipping_id']);

                $order = Order::model()->findByPk($_POST['order_id']);

                $total = 0;

                if($shipping_obj->condition == 1){
                    if($shipping_obj->use_percent){
                        $total += $order->order_total + $cart['total']*$shipping_obj->percent_amount/100;
                    }
                    else{
                        $total += $order->order_total + $shipping_obj->fixed_amount;
                    }
                }
                else{
                    if($shipping_obj->use_percent){
                        foreach(ProductOrder::model()->findAll('ord=:id_order', array(':id_order' => $order->id)) as $value){
                            $total += $value->price*$shipping_obj->percent_amount/100;
                        }
                        $total += $cart['total'];
                    }
                    else{
                        foreach(ProductOrder::model()->findAll('ord=:id_order', array(':id_order' => $order->id)) as $value){
                            $total += $value->price*$shipping_obj->fixed_amount;
                        }
                        $total += $cart['total'];
                    }
                }

                $order->shipping_option = $_POST['shipping_id'];

                $order->order_total = $total;

                $order->save();


                $response = array('success' => 1, 'message' => 'The product was successfully removed.', 'count' => ProductOrder::model()->count('ord=:id_order', array(':id_order' => $order->id)), 'total' => $order->order_subtotal, 'rtotal' => $order->order_total);
                $json = json_encode($response);
                $controller->renderText($json);
            }
            elseif(isset($_POST['shipping_id'])){
                $shipping_obj = ShippingOption::model()->findByPk($_POST['shipping_id']);
                $total = 0;

                if($shipping_obj->condition == 1){
                    if($shipping_obj->use_percent){
                        $total += $cart['total'] + $cart['total']*$shipping_obj->percent_amount/100;
                    }
                    else{
                        $total += $cart['total'] + $shipping_obj->fixed_amount;
                    }
                }
                else{
                    if($shipping_obj->use_percent){
                        foreach($cart['products'] as $key=>$value){
                            $total += $value['total']*$shipping_obj->percent_amount/100;
                        }
                        $total += $cart['total'];
                    }
                    else{
                        foreach($cart['products'] as $key=>$value){
                            $total += $value['total']*$shipping_obj->fixed_amount;
                        }
                        $total += $cart['total'];
                    }
                }

                if($cart['counts'] == 0){
                    $cart['rtotal'] = 0;
                }
                else{
                    $cart['rtotal'] = $total;
                }

                $cart['shipping'] = $_POST['shipping_id'];

                $session['cart'] = $cart;



                $response = array('success' => 1, 'message' => 'The product was successfully removed.', 'count' => $cart['counts'], 'total' => $cart['total'], 'rtotal' => $cart['rtotal']);
                $json = json_encode($response);
                $controller->renderText($json);
            }
        }
        else{
            $controller->renderText('');
        }

    }
}