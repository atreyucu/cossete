<?php

class ThanksAction extends CAction
{
    public function run()
    {
        $controller = $this->getController();
        $controller->common_data['view'] = 'thanks';

        $thanks = ThankPage::model()->find();
        $thanks_dto = array();
        $thanks_dto['page_title'] = 'Thanks';
        $thanks_dto['title'] = 'Thank You';
        $thanks_dto['subtitle'] = 'For using our services';
        $thanks_dto['image'] = '/static/imgs/error-parallax.jpg';
        if (isset($thanks)){
            $thanks_dto['page_title'] = $thanks->breadcrumb_title;
            $thanks_dto['title'] = $thanks->title;
            $thanks_dto['subtitle'] = $thanks->subtitle;
            $thanks_dto['image'] = $thanks->_background_image->getFileUrl('normal');
        }

        return $controller->render('thanks',
            array(
                'thank' => $thanks_dto
            )
        );

    }
}