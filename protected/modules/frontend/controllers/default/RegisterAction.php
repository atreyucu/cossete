<?php

class RegisterAction extends CAction
{
    public function run()
    {
        $controller = $this->getController();
        $controller->common_data['view'] = 'register';

        if (!Yii::app()->user->isGuest)
            $controller->redirect($controller->createUrl('/frontend/default/index'));

        if (isset($_POST['User'])) {
            $model = new User;
            $profile = new Profile;
            //$model->attributes = $_POST['User'];
            $model->superuser = 0;
            $model->status = 1;
            $model->is_client = 1;
            $model->password = $_POST['password'];
            $model->first_name = $_POST['firstname'];
            $model->last_name = $_POST['lastname'];
            $model->username = $_POST['email'];
            $model->activkey = UserModule::encrypting(microtime() . $model->password);
            $model->createtime = time();
            $model->lastvisit = time();
            $profile->firstname = $model->first_name;
            $profile->lastname = $model->last_name;
            if ($model->validate() && $profile->validate()) {
                $model->password = UserModule::encrypting($model->password);
                if ($model->save()) {
                    $profile->user_id = $model->id;
                    $profile->save();
                }
                //Yii::app()->user->setFlash('success', UserModule::t("Changes is saved."));
                $next = $controller->createUrl('/frontend/default/index');
                if ($_GET['next'])
                    $next = $_GET['next'];
                $controller->redirect($controller->createUrl('/frontend/default/login').'?next='.$next);
            }else {
                $result = User::model()->exists('username=:user', array(':user' => $model->username));
                if ($result)
                    Yii::app()->user->setFlash('error', "This user's name already exists.");
                else
                    Yii::app()->user->setFlash('error', "There was an error, please fill the form again");

                $controller->redirect($controller->createUrl('/frontend/default/register'));
            }

        }

        $register = RegisterPage::model()->find();
        $register_dto = array();
        $register_dto['page'] = 'My account';
        $register_dto['title'] = 'Register';
        $register_dto['subtitle'] = 'Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie
                                    consequat, vel illum dolore eu ';
        $register_dto['image'] = '/static/imgs/faqs-parallax.jpg';
        $register_dto['contact_title'] = "Have any questions we didn't answer?";
        $register_dto['contact_button'] = 'contact us';
        if (isset($register)){
            $register_dto['page'] = $register->breadcrumb_title;
            $register_dto['title'] = $register->form_title;
            $register_dto['subtitle'] = $register->form_subtitle;
            $register_dto['image'] = $register->_background_image->getFileUrl('normal');
            $register_dto['contact_title'] = $register->contact_title;
            $register_dto['contact_button'] = $register->contact_button;
        }

        return $controller->render('register',
            array(
                'register' => $register_dto
            )
        );

    }
}