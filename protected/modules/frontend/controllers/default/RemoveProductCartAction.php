<?php

class RemoveProductCartAction extends CAction
{
    public function run()
    {
        $controller = $this->getController();
        $controller->layout = false;

        $session = Yii::app()->session;

        $cart = $session['cart'];

        if(Yii::app()->request->isAjaxRequest && Yii::app()->request->isPostRequest){
            if(isset($_POST['product_id'])) {
                if (array_key_exists($_POST['product_id'],$cart['products'])) {
                    $temp_qty = $cart['products'][$_POST['product_id']]['qty'];
                    $temp_price = $cart['products'][$_POST['product_id']]['price'];
                    unset($cart['products'][$_POST['product_id']]);
                    $cart['counts'] -= $temp_qty;
                    $cart['total'] -= $temp_price * $temp_qty;

                    $shipping_obj = ShippingOption::model()->findByPk($cart['shipping']);

                    $total = 0;

                    if($shipping_obj->condition == 1){
                        if($shipping_obj->use_percent){
                            $total += $cart['total'] + $cart['total']*$shipping_obj->percent_amount/100;
                        }
                        else{
                            $total += $cart['total'] + $shipping_obj->fixed_amount;
                        }
                    }
                    else{
                        if($shipping_obj->use_percent){
                            foreach($cart['products'] as $key=>$value){
                                $total += $value['total']*$shipping_obj->percent_amount/100;
                            }
                            $total += $cart['total'];
                        }
                        else{
                            foreach($cart['products'] as $key=>$value){
                                $total += $value['total']*$shipping_obj->fixed_amount;
                            }
                            $total += $cart['total'];
                        }
                    }

                    if($cart['counts'] == 0){
                        $cart['rtotal'] = 0;
                    }
                    else{
                        $cart['rtotal'] = $total;
                    }

                    $session['cart'] = $cart;

                } else {
                    $response = array('success' => 0, 'message' => 'Ups! Something wrong removing the product.', 'count' => $session['cart']['counts'], 'total' => $session['cart']['total'], 'rtotal' => $session['cart']['rtotal']);
                    $json = json_encode($response);
                    $controller->renderText($json);
                }
                $response = array('success' => 1, 'message' => 'The product was successfully removed.', 'count' => $session['cart']['counts'], 'total' => $session['cart']['total'], 'rtotal' => $session['cart']['rtotal']);
                $json = json_encode($response);
                $controller->renderText($json);
            }
        }
        else{
            $controller->renderText('');
        }

    }
}