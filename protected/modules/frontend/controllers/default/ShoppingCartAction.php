<?php

class ShoppingCartAction extends CAction
{
    public function run()
    {
        $controller = $this->getController();
        $controller->common_data['view'] = 'cart';

        $shopping_page = ShoppingCartPage::model()->find();

        if(isset($shopping_page)){
            $breadcrumb_title = $shopping_page->breadcrumb_title;
            $product_column_label = $shopping_page->product_column_label;
            $price_column_label = $shopping_page->price_column_label;
            $quantity_column_label = $shopping_page->quantity_column_label;
            $total_column_label = $shopping_page->total_column_label;
            $checkout_form_header = $shopping_page->checkout_form_header;
            $subtotal_label = $shopping_page->subtotal_label;
            $shipping_label = $shopping_page->shipping_label;
            $total_label = $shopping_page->total_label;
            $have_question_link_text = $shopping_page->have_question_link_text;
            $call_us_text = $shopping_page->call_us_text;
            $checkout_button_text = $shopping_page->checkout_button_text;
        }
        else{
            $breadcrumb_title = 'Shopping Cart';
            $product_column_label = 'product';
            $price_column_label = 'price';
            $quantity_column_label = 'quantity';
            $total_column_label = 'total';
            $checkout_form_header = 'cart totals';
            $subtotal_label = 'Subtotal';
            $shipping_label = 'Shipping and Handling';
            $total_label = 'Order Total';
            $have_question_link_text = 'Have question?';
            $call_us_text = 'Call us at';
            $checkout_button_text = 'checkout';
        }

        $session = Yii::app()->session;

        $product_arr = $session['cart']['products'];
        $product_count = $session['cart']['counts'];
        $product_total = $session['cart']['total'];
        $product_rtotal = $session['cart']['rtotal'];
        $pshipp = $session['cart']['shipping'];

        $shipp_criteria = new CDbCriteria();
        $shipp_criteria->compare('is_active',1);

        $shippings = ShippingOption::model()->findAll($shipp_criteria);

        $shipp_arr =  array();

        foreach($shippings as $shipp){
            $shipp_arr[$shipp->id] = $shipp->name;
        }

        return $controller->render('cart',
            array(
                'breadcrumb_title' => $breadcrumb_title,
                'product_column_label' => $product_column_label,
                'price_column_label' => $price_column_label,
                'quantity_column_label' => $quantity_column_label,
                'total_column_label' => $total_column_label,
                'checkout_form_header' => $checkout_form_header,
                'subtotal_label' => $subtotal_label,
                'shipping_label' => $shipping_label,
                'total_label' => $total_label,
                'have_question_link_text' => $have_question_link_text,
                'call_us_text' => $call_us_text,
                'checkout_button_text' => $checkout_button_text,
                'products' => $product_arr,
                'product_count' => $product_count,
                'product_total' => $product_total,
                'product_rtotal' => $product_rtotal,
                'shipps' => $shipp_arr,
                'pshipp' => $pshipp
            )
        );

    }
}