<?php

class PaypalController extends Controller
{
	public function actionBuy($order){
        $session = Yii::app()->session;

        /** @var Order $order */
        $order = Order::model()->findByAttributes(array(
            'id'=>$order,
            'client'=>Yii::app()->user->id
        ));

        if ($order){
            if ($order->status == Order::STATUS_PAYED){
                Yii::app()->user->setFlash('error',  t('This order has already been payed.'));
                $this->redirect(array('/frontend/default/error'));
            }elseif ($order->status == Order::STATUS_PENDING){
                $paymentInfo['Order']['theTotal'] = $order->order_total;
                $paymentInfo['Order']['description'] = 'Payment of order ' . $order->order_number;
                $paymentInfo['Order']['quantity'] = 1;
            }
        }else{
            Yii::app()->user->setFlash('error',  t('This request is invalid.'));
            $this->redirect(array('/frontend/default/error'));
        }

		// call paypal 
		$result = Yii::app()->Paypal->SetExpressCheckout($paymentInfo); 
		//Detect Errors 
		if(!Yii::app()->Paypal->isCallSucceeded($result)){ 
			if(Yii::app()->Paypal->apiLive === true){
				//Live mode basic error message
				$error = 'We were unable to process your request. Please try again later';
				$error = $result['L_LONGMESSAGE0'];
			}else{
				//Sandbox output the actual error message to dive in.
				$error = $result['L_LONGMESSAGE0'];
			}
            Yii::app()->user->setFlash('error',  $error);
            $this->redirect(array('/frontend/default/error'));
		}else {
			// send user to paypal 
			$token = urldecode($result["TOKEN"]);

            $session[$token] = $order->primaryKey;
			
			$payPalURL = Yii::app()->Paypal->paypalUrl.$token; 
			$this->redirect($payPalURL); 
		}
	}

	public function actionConfirm()
	{
        $session = Yii::app()->session;

        $token = trim($_GET['token']);
		$payerId = trim($_GET['PayerID']);

        /** @var Order $order */
        $order = Order::model()->findByAttributes(array(
            'id'=>$session[$token],
            'client'=>Yii::app()->user->id
        ));

		$result = Yii::app()->Paypal->GetExpressCheckoutDetails($token);

		$result['PAYERID'] = $payerId;
		$result['TOKEN'] = $token;
		$result['ORDERTOTAL'] = $order->order_total;

		//Detect errors
		if(!Yii::app()->Paypal->isCallSucceeded($result)){
			if(Yii::app()->Paypal->apiLive === true){
				//Live mode basic error message
				$error = 'We were unable to process your request. Please try again later';
				$error = $result['L_LONGMESSAGE0'];
			}else{
				//Sandbox output the actual error message to dive in.
				$error = $result['L_LONGMESSAGE0'];
			}
            Yii::app()->user->setFlash('error',  $error);
            $this->redirect(array('/frontend/default/error'));
		}else{
			$paymentResult = Yii::app()->Paypal->DoExpressCheckoutPayment($result);
			//Detect errors
			if(!Yii::app()->Paypal->isCallSucceeded($paymentResult)){
				if(Yii::app()->Paypal->apiLive === true){
					//Live mode basic error message
					$error = 'We were unable to process your request. Please try again later';
					$error = $result['L_LONGMESSAGE0'];
				}else{
					//Sandbox output the actual error message to dive in.
					$error = $paymentResult['L_LONGMESSAGE0'];
				}
                Yii::app()->user->setFlash('error',  $error);
                $this->redirect(array('/frontend/default/error'));
			}else{
				//payment was completed successfully
                $order->status = 2;
                $order->save();

                $user = User::model()->findByPk(Yii::app()->user->id);

                $shipping_criteria = new CDbCriteria();
                $shipping_criteria->compare('type','1');
                $shipping_criteria->compare('client',$user->id);

                $shipp_addr = ClientAddress::model()->find($shipping_criteria);


                $billing_criteria = new CDbCriteria();
                $billing_criteria->compare('type','2');
                $billing_criteria->compare('client',$user->id);

                $billing_addr = ClientAddress::model()->find($billing_criteria);

                $message_admin = '<h2>Thanks to buy in Cosette.com</h2>';
                $order_detail = t("Your order number is: ").$order->order_number. '<br>';


                $product_list_part = '<br><h3>'.t("Details: ").'</h3><br>';
                $product_list_part .= '<table border="0">';

                $product_list_part .= '<tr>
                <td>Product</td>
                <td>Color</td>
                <td>Size</td>
                <td>Quantity</td>
                <td>Price</td>
                </tr>';

                /** @var ProductOrder $product */
                foreach($order->productOrders as $product){
                    $product_list_part .= '<tr>';
                    $product_list_part .= '<td>'.$product->productItem->name.'</td>';
                    $product_list_part .= '<td>'.$product->pcolor.'</td>';
                    $product_list_part .= '<td>'.$product->psize.'</td>';
                    $product_list_part .= '<td>'.$product->quantity.'</td>';
                    $product_list_part .= '<td>'. '$' .$product->productItem->price.'</td>';
                    $product_list_part .= '</tr>';
                }

                $product_list_part .= '</table><br>';
                $product_list_part .= '<h3>Total: $' . $order->order_total . '</h3>';


                $company_info = CompanyInfo::model()->find();

                if(isset($company_info)){
                    $from_email = $company_info->email;
                    $from_name = $company_info->name;

                    $subject = 'Thanks to buy in Cosette.com';

                    $to_email = $user->email;
                }
                else{
                    $from_email = 'hello@cossettecossete.com';
                    $from_name = 'Cossete';

                    $subject = 'Thanks to buy in Cosette.com';

                    $to_email = $user->email;
                }


                $message= $message_admin.$order_detail.$product_list_part. '<br><br>';

                $mail = new YiiMailer('contact', array('message' => nl2br($message), 'name' => $from_email, 'description' => 'New order notification'));


                //set properties
                $mail->setFrom($from_email, $from_name);
                $mail->setSubject($subject);
                $mail->setTo($to_email);

                if ($mail->send()) {
                    Yii::app()->user->setFlash('success',t('Thanks for contact us. We answers you soon as possible.'));
                    $this->redirect(array('/frontend/default/thanks'));
                } else {
                    Yii::app()->user->setFlash('error',t('Error sending email: ').$mail->getError());
                    $this->redirect(array('/frontend/default/error'));
                }


                //TODO ESTO DE ACA PARA ABAJO PARA QUE ESTA ???


                $message_admin = '<h2>Order notification</h2>';
                $order_detail = t("A new order has been generated with number: ").$order->order_number. '<br>';

                $client_part = '<br><h3>'.t("Client Details: ").'</h3><br>';
                $client_part = '<br>'.t("Name: "). ' ' . $user->first_name . ' ' . $user->last_name .'<br>';
                $client_part = '<br>'.t("Email: "). $user->email .'<br>';
                $client_part = '<br>'.t("Phone: "). $billing_addr->phone .'<br>';
                $client_part = '<br>'.t("Shipping Address: "). $shipp_addr->address . ' ' . $shipp_addr->city .'<br>';
                $client_part = '<br>'.t("Billing Address: "). $billing_addr->address . ' ' . $billing_addr->city .'<br>';


                $product_list_part = '<br><h3>'.t("Order Details: ").'</h3><br>';
                $product_list_part .= '<table border="0">';

                $product_list_part .= '<tr>
                <td>Product</td>
                <td>Color</td>
                <td>Size</td>
                <td>Quantity</td>
                <td>Price</td>
                </tr>';

                $product_criteria = new CDbCriteria();
                $product_criteria->compare('ord',$order->id);

                $product_id_list = ProductOrder::model()->findAll($product_criteria);

                foreach($product_id_list as $product){
                    $real_prod = Product::model()->findByPk($product->product);
                    $product_list_part .= '<tr>';
                    $product_list_part .= '<td>'.$real_prod->name.'</td>';
                    $product_list_part .= '<td>'.$product->pcolor.'</td>';
                    $product_list_part .= '<td>'.$real_prod->psize.'</td>';
                    $product_list_part .= '<td>'.$real_prod->quantity.'</td>';
                    $product_list_part .= '<td>'. '$' .$real_prod->price.'</td>';
                    $product_list_part .= '</tr>';
                }

                $product_list_part .= '</table><br>';
                $product_list_part .= '<h3>Total: $' . $order->order_total . '</h3>';

                $message2 = $message_admin.$client_part.$order_detail.$product_list_part. '<br><br>';

                $mail2 = new YiiMailer('contact', array('message' => nl2br($message), 'name' => $from_email, 'description' => 'New order notification'));

                $mail2->setFrom($from_email, $from_name);
                $mail2->setSubject($subject);
                $mail2->setTo($from_email);

                if ($mail->send()) {
                    Yii::app()->user->setFlash('success',t('Thanks for contact us. We answers you soon as possible.'));
                    return true;
                } else {
                    Yii::app()->user->setFlash('error',t('Error sending email: ').$mail->getError());
                    return false;
                }


				$this->redirect(array('/frontend/default/thanks'));
			}

		}
	}
        
    public function actionCancel()
	{
		//The token of the cancelled payment typically used to cancel the payment within your application
        $this->redirect(array('/frontend/default/myorders'));
	}
	
	public function actionDirectPayment(){ 
		$paymentInfo = array('Member'=> 
			array( 
				'first_name'=>'name_here', 
				'last_name'=>'lastName_here', 
				'billing_address'=>'address_here', 
				'billing_address2'=>'address2_here', 
				'billing_country'=>'country_here', 
				'billing_city'=>'city_here', 
				'billing_state'=>'state_here', 
				'billing_zip'=>'zip_here' 
			), 
			'CreditCard'=> 
			array( 
				'card_number'=>'number_here', 
				'expiration_month'=>'month_here', 
				'expiration_year'=>'year_here', 
				'cv_code'=>'code_here' 
			), 
			'Order'=> 
			array('theTotal'=>1.00) 
		); 

	   /* 
		* On Success, $result contains [AMT] [CURRENCYCODE] [AVSCODE] [CVV2MATCH]  
		* [TRANSACTIONID] [TIMESTAMP] [CORRELATIONID] [ACK] [VERSION] [BUILD] 
		*  
		* On Fail, $ result contains [AMT] [CURRENCYCODE] [TIMESTAMP] [CORRELATIONID]  
		* [ACK] [VERSION] [BUILD] [L_ERRORCODE0] [L_SHORTMESSAGE0] [L_LONGMESSAGE0]  
		* [L_SEVERITYCODE0]  
		*/ 
	  
		$result = Yii::app()->Paypal->DoDirectPayment($paymentInfo); 
		
		//Detect Errors 
		if(!Yii::app()->Paypal->isCallSucceeded($result)){ 
			if(Yii::app()->Paypal->apiLive === true){
				//Live mode basic error message
				$error = 'We were unable to process your request. Please try again later';
			}else{
				//Sandbox output the actual error message to dive in.
				$error = $result['L_LONGMESSAGE0'];
			}
			echo $error;
			
		}else { 
			//Payment was completed successfully, do the rest of your stuff
		}

		Yii::app()->end();
	} 
}