<?php

class DefaultController extends CController
{
    public $common_data;

    public function init()
    {
        $this->common_data = $this->getVariablesArray();
    }

    private function getVariablesArray()
    {
        $categories = ProductCategory::model()->findAll(array('condition' => 'is_active=1 and show_in_menu=1',
            'order' => 'priority'));
        $categories_dto = array();
        foreach($categories as $category) {
            $temp = array();
            $temp['id'] = $category->id;
            $temp['name'] = $category->name;
            $temp['position'] = $category->position;

            $categories_dto[] = $temp;
        }

        $social = SocialLinks::model()->find();

        $company = CompanyInfo::model()->find();
        $company_dto = array();
        $company_dto['email'] = 'hello@cossettecossete.com';
        $company_dto['address'] = '4556 NT 2Th St. Miami, FL 33144.';
        $company_dto['phone'] = '859.747.5466';
        $company_dto['website'] = 'cossettecossette.com';
        $company_dto['logo'] = '/static/imgs/brand-logo.png';
        if (isset($company)){
            $company_dto['email'] = $company->email;
            $company_dto['address'] = $company->addr;
            $company_dto['phone'] = $company->phone1;
            $website = explode('http://', $company->website);
            $company_dto['website'] = $website[0];
            $company_dto['logo'] = $company->_company_logo->getFileUrl('normal');
        }

        $footer = Footer::model()->find();
        $footer_dto = array();
        $footer_dto['image'] = '/static/imgs/footer-logo.png';
        $footer_dto['about'] = 'about';
        $footer_dto['contact'] = 'contact';
        $footer_dto['faq'] = 'faq';
        $footer_dto['term'] = 'term & conditions';
        if (isset($footer)){
            $footer_dto['image'] = $footer->_footer_logo->getFileUrl('normal');
            $footer_dto['about'] = $footer->about_label;
            $footer_dto['contact'] = $footer->contact_label;
            $footer_dto['faq'] = $footer->faq_label;
            $footer_dto['term'] = $footer->tc_label;
        }

        return array(
            'categories' => $categories_dto,
            'social' => $social,
            'company' => $company_dto,
            'footer' => $footer_dto
        );
    }

    public function filters() {
        return array(
            'accessControl',
        );
    }

    public function accessRules() {
        return array(
            array('deny',
                'actions'=>array(
                    'myorders',
                    'orders',
                    'checkout',
                    'details'
                ),
                'users'=>array('?'),
            ),
            array('allow',
                'actions'=>array('index',
                    'add2cart',
                    'category',
                    'product',
                    'about',
                    'contact',
                    'error',
                    'faqs',
                    'login',
                    'recover',
                    'register',
                    'shopping_cart',
                    'term_cond',
                    'thanks',
                ),
                'users'=>array('*'),
            ),
        );
    }

    public function actions()
    {
        $this->layout = '/layout/main';
        $actions_path = 'application.modules.frontend.controllers.default';
        return array(
            'index' => $actions_path.'.IndexAction',
            'category' => $actions_path.'.CategoryAction',
            'product' => $actions_path.'.ProductAction',
            'about' => $actions_path.'.AboutAction',
            'checkout' => $actions_path.'.CheckoutAction',
            'orders' => $actions_path.'.OrderAction',
            'contact' => $actions_path.'.ContactAction',
            'error' => $actions_path.'.ErrorAction',
            'faqs' => $actions_path.'.FaqsAction',
            'login' => $actions_path.'.LoginAction',
            'myorders' => $actions_path.'.MyOrdersAction',
            'recover' => $actions_path.'.RecoverAction',
            'register' => $actions_path.'.RegisterAction',
            'shopping_cart' => $actions_path.'.ShoppingCartAction',
            'term_cond' => $actions_path.'.TermCondAction',
            'thanks' => $actions_path.'.ThanksAction',
            'add2cart' => $actions_path.'.Add2CartAction',
            'updatecart' => $actions_path.'.UpdateCartAction',
            'removeproductcart' => $actions_path.'.RemoveProductCartAction',
            'updateqtyproductcart' => $actions_path.'.UpdateQtyProductCartAction',
            'updateordertotal' => $actions_path.'.UpdateOrderTotalAction',
            'updatecarmenu' => $actions_path.'.UpdateCartMenuAction',
            'details' => $actions_path.'.DetailsAction',
            'search' => $actions_path.'.SearchAction',
        );
    }



}