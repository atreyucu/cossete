<div class="content-header">
    <div class="container">
        <div class="flex-wrapper">
            <h1 class="page-title text-uppercase"><?php echo $breadcrumb_title?></h1>
            <ol class="breadcrumb">
                <li><a href="<?php echo $this->createUrl('/')?>" class="hvr-underline-from-center-white">Home</a></li>
            </ol>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
            <form method="post" class="login-form" action="/user/login">
                <div class="section-title bordered">
                    <h2 class="title text-uppercase"><?php echo $form_title?></h2>
                </div>
                <div class="form-group">
                    <p><?php echo $form_subtitle?></p>
                </div>
                <div role="alert" class="site-alerts alert alert-success fade in" id="alert_msg" style="display: none">
                    <button type="button" data-dismiss="alert" aria-label="Close" aria-hidden="true" class="close">&times;</button><strong>Alert text here...!</strong>
                </div>
                <div class="form-group">
                    <label for="username"><?php echo $username_label?></label>
                    <input type="text" id="username" required class="form-control input-lg" name="UserLogin[username]">
                </div>
                <div class="form-group">
                    <label for="password"><?php echo $password_label?></label>
                    <input type="password" id="password" required class="form-control input-lg" name="UserLogin[password]">
                </div>

                <div class="form-group">
                    Do you want an account? Register <a href="<?php echo $this->createUrl('/frontend/default/register')?>?next=<?php echo $next?>" class="hvr-underline-from-center colored">here</a>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-default btn-lg site-btn"><?php echo $send_button_label?></button>
                </div>
            </form>
        </div>
    </div>
</div>
<div data-parallax="scroll" data-image-src="<?php echo $background_image?>" class="parallax-section login-parallax">
    <div class="faqs-banner text-center">
        <h2><?php echo $contact_title?></h2><a href="<?php $this->createUrl('/frontend/default/contact')?>" role="button" class="btn btn-default btn-lg site-btn"><?php echo $contact_button?></a>
    </div>
</div>