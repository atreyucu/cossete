<div class="content-header">
    <div class="container">
        <div class="flex-wrapper">
            <h1 class="page-title text-uppercase">My account</h1>
            <ol class="breadcrumb">
                <li><a href="home.html" class="hvr-underline-from-center-white">Home</a></li>
            </ol>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
            <form id="checkoutForm" method="post" class="register-form" action="<?php echo $this->createUrl('/frontend/default/details')?>" style="display: inline-block;width: 700px;max-width: 100%;text-align: left;font-size: 16px;color: #333;">
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                        <div class="section-title bordered side-title">
                            <h1 class="title text-uppercase"><?php echo $billing_form_header?></h1>
                        </div>
                        <div class="row">

                            <?php if($have_billing_addr):?>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                    <label><?php echo Yii::t('checkout','Country')?></label>
                                    <select class="chosen-select form-control" name="bcountry" required>
                                        <?php $index=1?>
                                        <?php foreach($countries as $key=>$country):?>
                                            <option id="<?php echo $key?>" <?php if($index == 1 || $key == $bcountry):?>selected<?php endif;?> value="<?php echo $key?>"><?php echo $country['name']?></option>
                                            <?php $index++?>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                                    <label><?php echo Yii::t('checkout','First Name')?>:</label>
                                    <input type="text" class="form-control" name="bfirstname" value="<?php echo $bfirst_name?>" required>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                                    <label><?php echo Yii::t('checkout','Last Name')?>:</label>
                                    <input type="text" class="form-control" name="blastname" value="<?php echo $blast_name?>" required>
                                </div>
<!--                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">-->
<!--                                    <label>--><?php //echo Yii::t('checkout','Company Name')?><!--:</label>-->
<!--                                    <input type="text" class="form-control" name="bcompany" value="--><?php //echo $bcompany_name?><!--" required>-->
<!--                                </div>-->
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                    <label><?php echo Yii::t('checkout','Address:')?>:</label>
                                    <input type="text" class="form-control" name="baddr" value="<?php echo $baddress?>" required>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                    <label><?php echo Yii::t('checkout','Town / City')?>:</label>
                                    <input type="text" class="form-control" name="bcity" value="<?php echo $bcity?>" required>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                                    <label><?php echo Yii::t('checkout','State / County')?>:</label>
                                    <input type="text" class="form-control" name="bcounty" value="<?php echo $bstate?>" required>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                                    <label><?php echo Yii::t('checkout','Post Code / ZIP')?>:</label>
                                    <input type="text" class="form-control" name="bpostcode" value="<?php echo $szip_code?>>" required>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                                    <label><?php echo Yii::t('checkout','Email')?>:</label>
                                    <input type="email" class="form-control" name="bemail" value="<?php echo $bemail?>" required type="email">
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                                    <label><?php echo Yii::t('checkout','Phone')?>:</label>
                                    <input type="tel" class="form-control" name="bphone" value="<?php echo $bphone?>" required>
                                </div>
                            <?php else:?>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                    <label><?php echo Yii::t('checkout','Country')?></label>
                                    <select class="chosen-select form-control" name="bcountry" required>
                                        <?php $index=1?>
                                        <?php foreach($countries as $key=>$country):?>
                                            <option id="<?php echo $key?>" <?php if($index == 1):?>selected<?php endif;?> value="<?php echo $key?>"><?php echo $country['name']?></option>
                                            <?php $index++?>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                                    <label><?php echo Yii::t('checkout','First Name')?>:</label>
                                    <input type="text" class="form-control" name="bfirstname" required>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                                    <label><?php echo Yii::t('checkout','Last Name')?>:</label>
                                    <input type="text" class="form-control" name="blastname" required>
                                </div>
<!--                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">-->
<!--                                    <label>--><?php //echo Yii::t('checkout','Company Name')?><!--:</label>-->
<!--                                    <input type="text" class="form-control" name="bcompany" required>-->
<!--                                </div>-->
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                    <label><?php echo Yii::t('checkout','Address:')?>:</label>
                                    <input type="text" class="form-control" name="baddr" required>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                    <label><?php echo Yii::t('checkout','Town / City')?>:</label>
                                    <input type="text" class="form-control" name="bcity" required>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                                    <label><?php echo Yii::t('checkout','State / County')?>:</label>
                                    <input type="text" class="form-control" name="bcounty" required>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                                    <label><?php echo Yii::t('checkout','Post Code / ZIP')?>:</label>
                                    <input type="text" class="form-control" name="bpostcode" required>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                                    <label><?php echo Yii::t('checkout','Email')?>:</label>
                                    <input type="email" class="form-control" name="bemail" required type="email">
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                                    <label><?php echo Yii::t('checkout','Phone')?>:</label>
                                    <input type="tel" class="form-control" name="bphone" required>
                                </div>
                            <?php endif?>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                <label><?php echo Yii::t('checkout','Ship to a diferent address')?>?</label>
                                <div class="checkbox">
                                    <label>
                                        <input id="diferent_addr" type="checkbox" name="diferent_addr" <?php if($have_shipp_addr):?>checked="checked"<?php endif;?>><?php echo Yii::t('checkout','Confirm different address')?>
                                    </label>
                                </div>
                            </div>

                            <div id="shipping_addr">

                                <div class="section-title bordered side-title">
                                    <h1 class="title text-uppercase"><?php echo $shipping_form_header?></h1>
                                </div>


                                <div class="row" style="padding-left: 10px;">
                                    <?php if($have_shipp_addr):?>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                            <label><?php echo Yii::t('checkout','Country')?></label>
                                            <select class="chosen-select form-control ship_cmp" name="scountry" required>
                                                <?php $index=1?>
                                                <?php foreach($countries as $key=>$country):?>
                                                    <option id="<?php echo $key?>" <?php if($index == 1 || $key == $scountry):?>selected<?php endif;?> value="<?php echo $key?>"><?php echo $country['name']?></option>
                                                    <?php $index++?>
                                                <?php endforeach;?>
                                            </select>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                                            <label><?php echo Yii::t('checkout','First Name')?>:</label>
                                            <input type="text" class="form-control ship_cmp" name="sfirstname" value="<?php echo $sfirst_name?>" required>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                                            <label><?php echo Yii::t('checkout','Last Name')?>:</label>
                                            <input type="text" class="form-control ship_cmp" name="slastname" value="<?php echo $slast_name?>" required>
                                        </div>
<!--                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">-->
<!--                                            <label>--><?php //echo Yii::t('checkout','Company Name')?><!--:</label>-->
<!--                                            <input type="text" class="form-control ship_cmp" name="scompany" value="--><?php //echo $scompany_name?><!--" required>-->
<!--                                        </div>-->
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                            <label><?php echo Yii::t('checkout','Address:')?>:</label>
                                            <input type="text" class="form-control ship_cmp" name="saddr" value="<?php echo $saddress?>" required>
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                            <label><?php echo Yii::t('checkout','Town / City')?>:</label>
                                            <input type="text" class="form-control ship_cmp" name="scity" value="<?php echo $scity?>" required>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                                            <label><?php echo Yii::t('checkout','State / County')?>:</label>
                                            <input type="text" class="form-control ship_cmp" name="scounty" value="<?php echo $sstate?>" required>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                                            <label><?php echo Yii::t('checkout','Post Code / ZIP')?>:</label>
                                            <input type="text" class="form-control ship_cmp" name="spostcode" value="<?php echo $szip_code?>>" required>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                                            <label><?php echo Yii::t('checkout','Email')?>:</label>
                                            <input type="email" class="form-control ship_cmp" name="semail" value="<?php echo $semail?>" required type="email">
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                                            <label><?php echo Yii::t('checkout','Phone')?>:</label>
                                            <input type="tel" class="form-control ship_cmp" name="sphone" value="<?php echo $sphone?>" required>
                                        </div>
                                    <?php else:?>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                            <label><?php echo Yii::t('checkout','Country')?></label>
                                            <select class="chosen-select form-control ship_cmp" name="scountry">
                                                <?php $index=1?>
                                                <?php foreach($countries as $key=>$country):?>
                                                    <option id="<?php echo $key?>" <?php if($index == 1):?>selected<?php endif;?> value="<?php echo $key?>"><?php echo $country['name']?></option>
                                                    <?php $index++?>
                                                <?php endforeach;?>
                                            </select>
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                                            <label><?php echo Yii::t('checkout','First Name')?>:</label>
                                            <input type="text" class="form-control ship_cmp" name="sfirstname">
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                                            <label><?php echo Yii::t('checkout','Last Name')?>:</label>
                                            <input type="text" class="form-control ship_cmp" name="slastname">
                                        </div>
<!--                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">-->
<!--                                            <label>--><?php //echo Yii::t('checkout','Company Name')?><!--:</label>-->
<!--                                            <input type="text" class="form-control ship_cmp" name="scompany">-->
<!--                                        </div>-->
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                            <label><?php echo Yii::t('checkout','Address:')?>:</label>
                                            <input type="text" class="form-control ship_cmp" name="saddr">
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                            <label><?php echo Yii::t('checkout','Town / City')?>:</label>
                                            <input type="text" class="form-control ship_cmp" name="scity">
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                                            <label><?php echo Yii::t('checkout','State / County')?>:</label>
                                            <input type="text" class="form-control ship_cmp" name="scounty">
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                                            <label><?php echo Yii::t('checkout','Post Code / ZIP')?>:</label>
                                            <input type="text" class="form-control ship_cmp" name="spostcode">
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                                            <label><?php echo Yii::t('checkout','Email')?>:</label>
                                            <input type="email" class="form-control ship_cmp" name="semail" type="email">
                                        </div>
                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                                            <label><?php echo Yii::t('checkout','Phone')?>:</label>
                                            <input type="tel" class="form-control ship_cmp" name="sphone">
                                        </div>
                                    <?php endif?>
                                </div>

                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                <button type="submit" class="btn btn-default btn-lg site-btn">save</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
