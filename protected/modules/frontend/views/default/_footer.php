<footer class="page-footer">
    <div class="container">
        <div class="text-center socials-wrapper">
            <ul class="list-inline footer-socials">
                <li><a href="<?php echo $social->twitter?>"><i class="fa fa-twitter hover-transition"></i></a></li>
                <li><a href="<?php echo $social->facebook?>"><i class="fa fa-facebook hover-transition"></i></a></li>
                <li><a href="<?php echo $social->google?>"><i class="fa fa-google-plus hover-transition"></i></a></li>
                <li><a href="<?php echo $social->linkedin?>"><i class="fa fa-linkedin hover-transition"></i></a></li>
            </ul>
        </div>
        <div class="menu-wrapper text-center">
            <ul class="list-inline footer-menu">
                <li><a href="<?php echo $this->createUrl('/frontend/default/about')?>" class="menu-link hvr-underline-from-center"><?php echo $footer['about']?></a></li>
                <li><a href="<?php echo $this->createUrl('/frontend/default/contact')?>" class="menu-link hvr-underline-from-center"><?php echo $footer['contact']?></a></li>
                <li><a href="<?php echo $this->createUrl('/frontend/default/faqs')?>" class="menu-link hvr-underline-from-center"><?php echo $footer['faq']?></a></li>
                <li><a href="<?php echo $this->createUrl('/frontend/default/term_cond')?>" class="menu-link hvr-underline-from-center"><?php echo $footer['term']?></a></li>
            </ul>
        </div>
        <div class="logo-wrapper text-center"><a href="" class="footer-logo"><img src="<?php echo $footer['image']?>" class="img-responsive"></a>
            <div class="text-center"><a href="mailto:<?php echo $company['email']?>" class="footer-email hvr-underline-from-center"><?php echo $company['email']?></a></div>
        </div>
    </div>
    <div class="dark-strip text-center"><a href="" class="back-top visible-xs-block"><i class="fa fa-arrow-up fa-inverse hover-transition"></i></a>
        <div class="address-info pull-left">
            <p><?php echo $company['address']?> <a href="" class="hvr-underline-from-center-white"><?php echo $company['phone']?></a></p>
        </div><a href="" class="back-top hidden-xs"><i class="fa fa-arrow-up fa-inverse hover-transition"></i></a>
        <div class="copyright-info pull-right">
            <p><a href="http://<?php echo $company['website']?>" class="hvr-underline-from-center-white"><?php echo $company['website']?></a> &copy;<?php echo date('Y')?>. All right reserved.</p>
        </div>
    </div>
</footer>