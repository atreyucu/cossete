<div class="content-header">
    <div class="container">
        <div class="flex-wrapper">
            <h1 class="page-title text-uppercase">
                <?php for($i=count($categories)-1; $i >= 0; $i--) : ?>
                    <?php echo $categories[$i]['name']?>
                <?php endfor;?> Collection
            </h1>
            <ol class="breadcrumb">
                <li><a href="<?php echo $this->createUrl('/frontend/default/index')?>" class="hvr-underline-from-center-white">Home</a></li>
                <?php for($i=count($categories)-1; $i >= 0; $i--) : ?>
                    <li><a href="<?php echo $this->createUrl('/frontend/default/category', array('id'=>$categories[$i]['id']))?>" class="hvr-underline-from-center-white"><?php echo $categories[$i]['name']?></a></li>
                <?php endfor;?>

            </ol>
        </div>
    </div>
</div>
<div class="container">
    <div role="alert" class="site-alerts alert alert-success fade in" id="not_line" style="display: none">
        <button type="button" class="close" onclick="$('#not_line').hide()">&times;</button><strong>The product was successfully added.</strong><a href="<?php echo $this->createUrl('/frontend/default/category', array('id' => $product['category']))?>" class="hvr-underline-from-center alert-link">Keep buying</a><a href="<?php echo $this->createUrl('/frontend/default/shopping_cart')?>" class="hvr-underline-from-center alert-link">See cart</a>
    </div>
    <div role="alert" class="site-alerts alert alert-danger fade in" id="error_message" style="display: none">
        <button type="button" class="close" onclick="$('#error_message').hide()">&times;</button><strong>The product was successfully added.</strong>
    </div>
    <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-4 needed-margin easyzoom-wrapper">
            <div class="product-item easyzoom easyzoom--overlay">
                <a href="<?php echo $product['zoom_image']?>"><img src="<?php echo $product['image']?>" class="custom-responsive"></a>
                <div class="sale-badge">
                    <?php if ($product['has_label']): ?>
                        <img src="<?php echo $product['label']?>">
                    <?php endif?>
                </div>
            </div>
        </div>
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3 needed-margin col-lg-push-5 image-link-banner">
            <div class="product-banner"><a href=""><img src="<?php echo $product_page['right_banner']?>" class="custom-responsive"></a><a href=""><img src="<?php echo $product_page['left_banner']?>" class="custom-responsive"></a></div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-5 needed-margin col-lg-pull-3">
            <h2 class="product-name"><?php echo $product['name']?></h2>
            <div class="product-description">
                <?php echo $product['description']?>
            </div>
            <p class="has-questions">
                <a href="<?php echo $this->createUrl('/frontend/default/faqs')?>" class="hvr-underline-from-center colored">
                    Have question?
                </a>
                Call us at <a href="tel:<?php echo $this->common_data['company']['phone']?>" class="hvr-underline-from-center"><?php echo $this->common_data['company']['phone']?></a></p>
            <form method="post" class="product-form">
                <div class="product-details">
                    <div class="text-left">
                        <div class="small">Price:</div>
                        <div class="price">$<?php echo $product['price']?></div>
                    </div>
                    <?php if(count($colors) > 0):?>
                    <div class="text-left">
                        <div class="small">Colors:</div>
                        <div class="colors">
                            <select class="chosen-select form-control">
                                <option value="0">Select a color</option>
                                <?php foreach($colors as $color) :?>
                                    <option value="<?php echo $color['id']?>"><?php echo $color['name']?></option>
                                <?php endforeach; ?>

                            </select>
                        </div>
                    </div>
                    <?php endif;?>
                </div>
                <div class="product-details">
                    <div class="text-left">
                        <div class="small">Quantity:</div>
                        <div class="amount">
                            <input type="number" min="1" class="form-control" value="1">
                        </div>
                    </div>
                    <?php if(count($sizes)>0):?>
                    <div class="text-left">
                        <div class="small">Size:</div>
                        <div class="sizes">
                            <ul class="list-inline">
                                <?php foreach($sizes as $size) :?>
                                    <li><span id="<?php echo $size['id']?>"><?php echo $size['name']?></span></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                    <?php endif;?>
                </div>
                <div class="product-details">
                    <div class="text-left">
                        <div class="small">P. Code: <strong><?php echo $product['code']?></strong></div>

                        <button id="add2cart" type="button" class="btn btn-default site-btn" onclick="add2Cart('<?php echo $product['id']?>')"><?php echo $product_page['cart']?></button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="dark-title product-dark-title">
    <div class="line-divider"></div>
    <h3 class="text-center text-uppercase"><?php echo $product_page['relates_text']?></h3>
</div>
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="product-list related-products">
                <?php foreach($relates as $prelate) :?>
                    <div class="item-wrapper">
                        <div class="product-item"><a href="<?php echo $this->createUrl('/frontend/default/product', array('id' => $prelate['id']))?>"><img src="<?php echo $prelate['image']?>" class="custom-responsive"></a>
                            <div class="sale-badge">
                                <?php if ($prelate['has_label']): ?>
                                    <img src="<?php echo $prelate['label']?>">
                                <?php endif?>

                            </div>
                        </div>
                    </div>
                <?php endforeach?>
            </div>
        </div>
    </div>
</div>