<div class="pop-up-departamentos d-close" style="overflow: visible; display: none;">
    <ul>
        <li>
            <a style="cursor: pointer"
               href="<?php echo $this->createUrl('//offers/index') ?>"><?php echo Yii::t('frontend', 'Todos') ?></a>
        </li>
        <?php
        $criteriaCategories = new CDbCriteria();
        $criteriaCategories->with = array('categoryCountries');
        $criteriaCategories->compare('categoryCountries.country_id', multicountry()->country->id);
        $productCategories = ProductTypes::model()->roots()->active()->frontend()->findAll($criteriaCategories);


        /** @var ProductTypesGroup $category */
        foreach ($productCategories as $category): ?>

            <li>
                <a href="<?php echo $this->createUrl('//offers/index', array('category' => $category->primaryKey)) ?>"><?php echo Yii::t('frontend', $category->name) ?></a>
                <i class="fa fa-angle-right"></i>

                <div class="sub">
                    <ul class="col-md-6 col-sm-6 col-xs-6">
                        <li class="sub-title">
                            <?php echo GxHtml::valueEx($category) ?>
                        </li>
                        <?php $subCategory = $category->descendants(1)->active()->findAll($criteriaCategories) ?>
                        <?php foreach ($subCategory as $type): ?>
                            <?php if ($type->active == 1 && $type->is_show_frontend == 1) : ?>
                                <li>
                                    <a href="<?php echo $this->createUrl('//offers/index', array('category' => $type->primaryKey)) ?>"><?php echo GxHtml::valueEx($type) ?></a>
                                </li>
                            <?php endif ?>
                        <?php endforeach ?>
                    </ul>
                    <?php echo CHtml::image($category->image->getFileUrl('normal'), $category->name, array('class' => 'foto')) ?>
                </div>
            </li>
        <?php endforeach ?>
    </ul>
    <?php echo CHtml::image(Yii::app()->baseUrl . '/static/images/pico-blanco.png', 'arrow', array('class' => 'pico')) ?>
</div>