<div class="search-panel">
    <form method="get" class="input-wrapper" action="<?php echo $this->createUrl('/frontend/default/search')?>">
        <div class="input-group">
            <input type="text" name="q" placeholder="Search for..." class="form-control input-lg"><span class="input-group-btn">
            <button type="submit" class="btn btn-default btn-lg"><i class="fa fa-search"></i></button></span>
        </div>
    </form><a href="" role="button" id="search-close" class="pull-right"><i class="fa fa-times fa-2x"></i></a>
</div>

<header class="page-navigation">
    <div class="container-fluid">
        <div class="logo-wrapper"><a href="<?php echo $this->createUrl('/frontend/default/index')?>" class="brand-logo"><img src="<?php echo $company['logo']?>" class="img-responsive"></a></div><a href="" class="menu-trigger pull-left visible-xs-block visible-sm-block"><i class="fa fa-bars fa-2x fa-inverse"></i></a>
        <ul class="navigation list-inline">
            <li <?php if ($view == 'home'):?>class="active"<?php endif?>><a href="<?php echo $this->createUrl('/frontend/default/index')?>" class="nav-link hover-transition">home</a></li>
            <?php foreach($categories as $category) :?>
                <li <?php if ($view == $category['name']):?>class="active"<?php endif?>><a href="<?php echo $this->createUrl('/frontend/default/category', array('id'=>$category['id']))?>" class="nav-link hover-transition"><?php echo $category['name']?></a></li>
            <?php endforeach;?>
        </ul>
        <div class="sign-search">
            <?php if(Yii::app()->user->isGuest):?>
                <div class="sign-wrapper">
                    <form id="form-signin" method="get" action="<?php echo $this->createUrl('/frontend/default/login')?>">
                        <a id="signin" style="cursor: pointer" class="sign-trigger text-uppercase hover-transition">
                            <?php echo Yii::t('header','sign in')?>
                        </a>
                        <input type="hidden" name="next" value="<?php echo Yii::app()->request->getUrl()?>">
                    </form>
                </div>
                <div class="search-wrapper"><a href="" class="search-btn hover-transition"><i class="fa fa-search"></i></a></div>
            <?php else: ?>
                <div class="sign-wrapper loggued"><a href="" class="signed text-uppercase hover-transition"><i class="fa fa-user"></i> Hello</a></div>
                <div class="user-options">
                    <p>Welcome <strong><?php echo Yii::app()->user->name?> </strong> <a href="<?php echo $this->createUrl('/user/logout')?>" class="logout-btn">(<?php echo Yii::t('home','log out')?>)</a></p>
                    <div class="btns-wrapper"><a href="<?php echo $this->createUrl('/frontend/default/myorders')?>" class="btn btn-default site-btn"><?php echo Yii::t('home','My Orders')?></a><a href="<?php echo $this->createUrl('/frontend/default/details')?>" class="btn btn-default site-btn"><?php echo Yii::t('home','Edit Account')?></a></div>
                </div>
                <div class="search-wrapper"><a href="" class="search-btn hover-transition"><i class="fa fa-search"></i></a></div>
            <?php endif?>
        </div>
        <div class="socials-cart">
            <ul class="nav-socials list-inline hidden-sm hidden-xs">
                <li><a href="<?php echo $social->twitter?>"><i class="fa fa-twitter fa-inverse hover-transition"></i></a></li>
                <li><a href="<?php echo $social->facebook?>"><i class="fa fa-facebook fa-inverse hover-transition"></i></a></li>
                <li><a href="<?php echo $social->google?>"><i class="fa fa-google-plus fa-inverse hover-transition"></i></a></li>
                <li><a href="<?php echo $social->linkedin?>"><i class="fa fa-linkedin fa-inverse hover-transition"></i></a></li>
            </ul>
            <div class="shop-cart">
                <p class="text-uppercase"><i class="fa fa-shopping-cart fa-inverse fa-flip-horizontal"></i><strong> <?php echo $counts?> item</strong>(usd <span id="price"><?php $subtotal?></span>)</p>
            </div>
            <div class="cart-content">
                <div id="cart_list">
                    <div class="cart-list">
                        <?php foreach($products as $key=>$prod):?>
                            <div id="div<?php echo $key?>" class="order-item"><a role="button" class="delete-btn" onclick="removeFromCart('<?php echo $key?>')"><i class="fa fa-times"></i></a>
                                <div class="related-image"><a href="<?php echo $this->createUrl('/frontend/default/product', array('id' => $prod['pid'])) ?>"><img src="<?php echo $prod['img']?>" class="img-responsive"></a></div>
                                <div class="related-info">
                                    <div class="header-text"><?php echo $prod['name']?> (<strong><?php echo $prod['qty']?></strong>)</div>
                                    <div class="price">$<?php echo $prod['total']?></div>
                                </div>
                            </div>
                        <?php endforeach;?>
                    </div>
                    <div class="text-center subtotal">
                        <p>Subtotal: <strong><span id="subtotal_span"><?php echo $subtotal?></span></strong></p>
                    </div>
                </div>

                <div class="btns-wrapper"><a id="btn_up_view_cart" href="<?php echo $this->createUrl('/frontend/default/shopping_cart')?>" class="btn btn-default site-btn" <?php if(count($products)==0):?> disabled="disabled" <?php endif;?>><?php echo Yii::t('home','view cart')?></a><a id="btn_up_go_checkout" href="<?php echo $this->createUrl('/frontend/default/checkout')?>" class="btn btn-default site-btn" <?php if(count($products)==0):?> disabled="disabled" <?php endif;?>><?php echo Yii::t('home','checkout')?></a></div>

            </div>
        </div>
    </div>
</header>