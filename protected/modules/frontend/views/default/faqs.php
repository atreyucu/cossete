<div class="content-header">
    <div class="container">
        <div class="flex-wrapper">
            <h1 class="page-title text-uppercase"><?php echo $faq_page['page_title'] ?></h1>
            <ol class="breadcrumb">
                <li><a href="#" class="hvr-underline-from-center-white">Home</a></li>
                <li class="active"><a>FAQs</a></li>
            </ol>
        </div>
    </div>
</div>
<div class="container faqs-page">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="faqs-text">
                <?php echo $faq_page['description'] ?>
            </div>
        </div>
    </div>

    <?php foreach($faqs_categories as $category) :?>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class="section-title clear-margin">
                    <h2 class="title text-uppercase"><?php echo $category['name']?></h2>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <div role="tablist" aria-multiselectable="true" id="accordion-<?php echo $category['id']?>" class="faqs-accordion panel-group">
                    <?php foreach($category['faqs'] as $faq) :?>
                        <div class="panel panel-default">
                            <div role="tab" id="question-<?php echo $faq['id']?>" class="panel-heading">
                                <h4 class="panel-title"><a href="#collapse-<?php echo $faq['id']?>" role="button" data-toggle="collapse"
                                                           data-parent="#accordion-<?php echo $category['id']?>"><?php echo $faq['question']?><i
                                            class="fa fa-plus pull-right"></i></a></h4>
                            </div>
                            <div role="tabpanel" aria-labelledby="#question-<?php echo $faq['id']?>" id="collapse-<?php echo $faq['id']?>" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <?php echo $faq['reply']?>
                                </div>
                            </div>
                        </div>
                    <?php endforeach?>

                </div>
            </div>
        </div>
    <?php endforeach?>

</div>
<div class="faqs-banner-wrapper"
     style="background: url('<?php echo $faq_page["contact_image"] ?>?1438019086') no-repeat scroll center center / cover  rgba(0, 0, 0, 0)">
    <div class="faqs-banner text-center">
        <h2><?php echo $faq_page["contact_title"] ?></h2><a
            href="<?php echo $this->createUrl('/frontend/default/contact') ?>" role="button"
            class="btn btn-default btn-lg site-btn"><?php echo $faq_page["contact_button"] ?></a>
    </div>
</div>