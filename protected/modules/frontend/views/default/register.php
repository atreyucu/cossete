<div class="content-header">
    <div class="container">
        <div class="flex-wrapper">
            <h1 class="page-title text-uppercase"><?php echo $register['page']?></h1>
            <ol class="breadcrumb">
                <li><a href="#" class="hvr-underline-from-center-white">Home</a></li>
            </ol>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
            <form method="post" class="register-form" action="/frontend/default/register" id="signupForm">
                <fieldset>
                <div class="section-title bordered">
                    <h2 class="title text-uppercase"><?php echo $register['title']?></h2>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                    <p>
                        <?php echo $register['subtitle']?>
                    </p>
                </div>

                <?php $error_value = Yii::app()->user->getFlash('error');?>
                <?php if(isset($error_value)):?>
                    <div role="alert" class="site-alerts alert alert-success fade in">
                        <button type="button" data-dismiss="alert" aria-label="Close" aria-hidden="true" class="close">&times;</button><strong><?php echo $error_value?></strong>
                    </div>
                <?php endif;?>

                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                    <label for="first-name">First Name:</label>
                    <input type="text" id="firstname" name="firstname"
                           class="form-control" onchange="$('#profile_name').val(this.value);">
                </div>
                <input type="hidden" id="profile_name" name="Profile[firstname]" required="required" class="form-control">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                    <label for="last-name">Last Name:</label>
                    <input type="text" id="lastname" name="lastname"
                           class="form-control" onchange="$('#profile_lastname').val(this.value);">
                </div>
                <input type="hidden" id="profile_lastname" name="Profile[lastname]" class="form-control">

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                    <label for="email-add">Email Address:</label>
                    <input type="email" id="email" name="email" class="form-control">
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                    <label for="first-pass">Password:</label>

                    <input type="password" name="password" class="form-control" id="password">
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                    <label for="second-pass">Retype Password:</label>

                    <input type="password" class="form-control" id="confirm_password" name="confirm_password">
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                    <button type="submit" id="register_button" class="btn btn-default btn-lg site-btn">send</button>
                </div>
                </fieldset>
            </form>
        </div>
    </div>
</div>
<div data-parallax="scroll" data-image-src="<?php echo $register['image']?>" class="parallax-section login-parallax">
    <div class="faqs-banner text-center">
        <h2><?php echo $register['contact_title']?></h2><a href="<?php echo $this->createUrl('/frontend/default/contact')?>" role="button" class="btn btn-default btn-lg site-btn"><?php echo $register['contact_button']?></a>
    </div>
</div>
