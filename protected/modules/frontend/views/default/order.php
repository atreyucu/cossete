<div class="content-header">
    <div class="container">
        <div class="flex-wrapper">
            <h1 class="page-title text-uppercase"><?php echo $page_title?></h1>
            <ol class="breadcrumb">
                <li><a href="<?php echo $this->createUrl('/')?>" class="hvr-underline-from-center-white">Home</a></li>
                <li class="active"><a>Orders</a></li>
            </ol>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
            <div class="section-title bordered side-title">
                <h2 class="title text-uppercase"><?php echo $order_list_header?></h2>
            </div>
            <div class="table-responsive">
                <table id="orders-table" class="table table-hover">
                    <thead>
                    <tr>
                        <th><?php echo Yii::t('frontend_order','Order')?></th>
                        <th><?php echo Yii::t('frontend_order','Date')?></th>
                        <th><?php echo Yii::t('frontend_order','Total')?></th>
                        <th><?php echo Yii::t('frontend_order','Payment Method')?></th>
                        <th><?php echo Yii::t('frontend_order','Status')?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="order"><?php echo $order['number']?></td>
                        <td class="date"><?php echo $order['date']?></td>
                        <td class="total">$<?php echo $order['total']?></td>
                        <td class="payment">Paypal</td>
                        <td class="payment"><?php if($order['status'] == '1'):?>Pending<?php else:?>Completed><?php endif;?></td>
                    </tr>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="4">
                            <p class="text-right pull-right">
                                <?php echo $product_column_label?>
                            </p>
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <div class="section-title bordered side-title">
                <h2 class="title text-uppercase"><?php echo $order_detail_header?></h2>
            </div>
            <div class="table-responsive">
                <table id="order-details" class="table table-hover">
                    <thead>
                    <tr>
                        <th class="products"><?php echo Yii::t('frontend_order','Product')?></th>
                        <th class="products"><?php echo Yii::t('frontend_order','size')?></th>
                        <th class="products"><?php echo Yii::t('frontend_order','color')?></th>
                        <th class="total"><?php echo Yii::t('frontend_order','Price')?></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($products as $product):?>
                        <tr>
                            <td class="products"><?php echo $product['name']?></td>
                            <td class="products"><?php echo $product['size']?></td>
                            <td class="products"><?php echo $product['color']?></td>
                            <td class="total">$<?php echo $product['price']?></td>
                        </tr>
                    <?php endforeach;?>
                    </tbody>
                </table>
            </div>
            <div class="section-title bordered side-title">
                <h2 class="title text-uppercase"><?php echo $customer_detail_header?></h2>
            </div>
            <div class="customer-details">
                <div class="details">
                    <p><strong><?php echo Yii::t('frontend_order','Full Name')?>:</strong><?php echo Yii::app()->user->name?></p>
                    <p><strong><?php echo Yii::t('frontend_order','Email')?>:</strong><a href="mailto:<?php echo $user_email?>" class="hvr-underline-from-center"><?php echo $user_email?></a></p>
                    <p><strong><?php echo Yii::t('frontend_order','Phone')?>:</strong><a href="tel:<?php echo $shipping['phone']?>" class="hvr-underline-from-center"><?php echo $shipping['phone']?></a></p>
                </div>
                <div class="billing-detail">
                    <div class="underline-subtitle text-uppercase"><?php echo $billing_address?></div>
                    <div class="address">
                        <?php echo $billing['name']?>
                        <?php echo $billing['company_name']?>
                        <?php echo $billing['addr']?>
                        <?php echo $billing['country']?>

                    </div>
                </div>
                <div class="billing-detail">
                    <div class="underline-subtitle text-uppercase"><?php echo $shipping_address?></div>
                    <div class="address">
                        <?php echo $shipping['name']?>
                        <?php echo $shipping['company_name']?>
                        <?php echo $shipping['addr']?>
                        <?php echo $shipping['country']?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
            <div class="section-title bordered side-title">
                <h2 class="title text-uppercase"><?php echo  $cart_details_header?></h2>
            </div>
            <ul class="cart-totals list-unstyled">
                <li>
                    <div class="caption-text"><?php echo $subtotal_label?></div>
                    <div class="price" id="subtotal_price">$<?php echo $product_total?></div>
                </li>
                <li>
                    <div class="caption-text"><?php echo $shipping_label?></div>
                    <div class="price-text">
                        <div class="colors">
                            <select id="shipping_select" class="chosen-select form-control" onchange="updateShippingRealOrder('<?php echo $order['id']?>')" <?php if($order['status'] == '2'):?> disabled="disabled" <?php endif;?>>
                                <?php foreach($shipps as $key=>$shipp):?>
                                    <option id="<?php echo $key?>" <?php if($key == $pshipp):?> selected <?php endif;?>><?php echo $shipp?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="caption-text"><?php echo Yii::t('frontend_order','Payment Method')?></div>
                    <div class="price-text">Paypal</div>
                </li>
                <li>
                    <div class="caption-text"><?php echo $total_label?></div>
                    <div class="price" id="total_price">$<?php echo $product_rtotal?></div>
                </li>
            </ul>
            <p class="has-questions"><a target="_blank" href="<?php echo $this->createUrl('/frontend/default/faqs')?>" class="hvr-underline-from-center colored">Have question?</a>Call us at <a href="tel:18597475466" class="hvr-underline-from-center">1.859.747.5466</a></p>
<!--            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group text-center">-->
<!--                --><?php //if($product_total>0):?><!-- <a href="--><?php //echo $this->createUrl('/frontend/default/checkout')?><!--" style="cursor: pointer;"  class="btn btn-primary site-btn">--><?php //echo Yii::t('frontend_order','checkout')?><!--</a> --><?php //endif?>
<!--            </div>-->

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group text-center">
                <?php if($order['status'] == '1'):?><a style="cursor: pointer;"  class="btn btn-primary site-btn" href="<?php echo $this->createUrl('/frontend/paypal/buy', array('order' => $order['id']))?>"><?php echo Yii::t('order_frontend','Checkout')?></a><?php endif;?>
            </div>
        </div>
    </div>
</div>