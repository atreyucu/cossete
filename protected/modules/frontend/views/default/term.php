<div class="content-header">
    <div class="container">
        <div class="flex-wrapper">
            <h1 class="page-title text-uppercase"><?php echo $title?></h1>
            <ol class="breadcrumb">
                <li><a href="#" class="hvr-underline-from-center-white">Home</a></li>
                <li class="active"><a>Terms</a></li>
            </ol>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 term-page">
            <?php if (!$description) : ?>
                <p>
                    Proin tempor metus ut risus. Nulla dolor. Vestibulum diam nisl, aliquet tempor, mattis id,
                    varius id, nibh. Quisque vel diam id urna dapibus sollicitudin. Cras eget neque eu leo
                    sollicitudin dignissim. Nunc nibh nulla, placerat vitae, adipiscing consectetuer id diam.
                    Vestibulum ante ipsum primis in faucibus orci luctus et <a href="" class="hvr-underline-from-center">ultrices posuere</a> cubilia Curae;
                    Curabitur lobi.
                </p>
                <p>
                    Proin tempor metus ut risus. Nulla dolor. Vestibulum diam nisl, aliquet tempor, mattis id,
                    varius id, nibh. Quisque vel diam id urna dapibus sollicitudin. Cras eget neque eu leo
                    sollicitudin dignissim. Nunc nibh nulla, placerat vitae, adipiscing consectetuer id diam.
                    Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia
                </p>
                <p>
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt
                    ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci
                    tation ullamcorper suscipit.

                </p>
                <h4 class="term-subtitle">Proin tempor metus ut risus. Nulla dolor. Vestibulum diam nisl.</h4>
                <p>
                    Proin tempor metus ut risus. Nulla dolor. Vestibulum diam nisl, aliquet tempor, mattis id,
                    varius id, nibh. Quisque vel diam id urna dapibus sollicitudin. Cras eget neque eu leo
                    sollicitudin dignissim. Nunc nibh nulla, placerat vitae, adipiscing consectetuer id diam.
                    Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia

                </p>
                <h4 class="term-subtitle">Proin tempor metus ut risus. Nulla dolor. Vestibulum diam nisl, aliquet tempor, mattis id.</h4>
                <p>
                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt
                    ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci
                    tation ullamcorper suscipit.
                </p>
                <p>
                    Proin tempor metus ut risus. Nulla dolor. Vestibulum diam nisl, aliquet tempor, mattis id,
                    varius id, nibh. Quisque vel diam id urna dapibus sollicitudin. Cras eget neque eu leo
                    sollicitudin dignissim. Nunc nibh nulla, placerat vitae, adipiscing consectetuer id diam.
                    Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia

                </p>
                <h3 class="term-title">Vestibulum diam nisl, aliquet tempor, mattis id.</h3>
                <ul class="term-list">
                    <li>Proin tempor metus ut risus. Nulla dolor. Vestibulum diam nisl, aliquet tempor, mattis id.</li>
                    <li>Vestibulum diam nisl, aliquet tempor, <a href="" class="hvr-underline-from-center">mattis id</a>.</li>
                    <li>Nulla dolor. Vestibulum diam nisl, aliquet tempor, mattis id.</li>
                    <li>Proin tempor metus ut risus. Nulla dolor. Vestibulum diam nisl, aliquet tempor, mattis id.</li>
                    <li>Nulla dolor. Vestibulum diam nisl, aliquet tempor, mattis id.</li>
                </ul>
            <?php else : ?>
                <?php echo $description?>
            <?php endif; ?>
        </div>
    </div>
</div>