<div class="content-header">
    <div class="container">
        <div class="flex-wrapper">
            <h1 class="page-title text-uppercase"><?php echo $breadcrumb_title?></h1>
            <ol class="breadcrumb">
                <li><a href="<?php echo $this->createUrl('/')?>" class="hvr-underline-from-center-white"><?php echo Yii::t('cart_frontend','Home') ?></a></li>
                <li class="active"><a>Cart</a></li>
            </ol>
        </div>
    </div>
</div>
<div class="container">
    <form method="post" class="shopping-cart-form">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                <div id="div_cart_list" class="table-responsive">
                    <?php if(count($products)>0):?>
                        <table id="my-orders" class="table table-hover my-cart">
                            <thead>
                            <tr>
                                <th></th>
                                <th><?php echo $product_column_label?></th>
                                <th><?php echo $price_column_label?></th>
                                <th><?php echo $quantity_column_label?></th>
                                <th><?php echo $total_column_label?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach($products as $key=>$product):?>
                            <tr id="tr<?php echo $key?>">
                                <td class="close-btn"><i class="fa i fa fa-times-circle-o" onclick="removeFromCart('<?php echo $key?>')"></i></td>
                                <td class="product"><img src="<?php echo $product['img']?>"><?php echo $product['name']?></td>
                                <td class="price" id="price-<?php echo $key?>">$<?php echo $product['price']?></td>
                                <td class="amount">
                                    <input id="qty-<?php echo $key?>" type="number" min="1" class="form-control" onchange="updateqtyCart('<?php echo $key?>','qty-<?php echo $key?>')" value="<?php echo $product['qty']?>">
                                </td>
                                <td class="total-amount" id="total-<?php echo $key?>">$<?php echo $product['total']?></td>
                            </tr>
                            <?php endforeach;?>
                            </tbody>
                        </table>
                    <?php else:?>
                        <div style="font-weight: bold; text-align: center; margin-top: 30px;">
                        <h3><?php echo Yii::t('cart_frontend','Your cart is empty.')?></h3>
                        </div>
                    <?php endif;?>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
                <div class="section-title bordered side-title">
                    <h2 class="title text-uppercase"><?php echo $checkout_form_header?></h2>
                </div>
                <ul class="cart-totals list-unstyled">
                    <li>
                        <div class="caption-text"><?php echo $subtotal_label?></div>
                        <div class="price" id="subtotal_price">$<?php echo $product_total?></div>
                    </li>
                    <li>
                        <div class="caption-text"><?php echo $shipping_label?></div>
                        <div class="price-text">
                                <div class="colors">
                                    <select id="shipping_select" class="chosen-select form-control" onchange="updateShiping()">
                                        <?php foreach($shipps as $key=>$shipp):?>
                                            <option id="<?php echo $key?>" <?php if($key == $pshipp):?> selected <?php endif;?>><?php echo $shipp?></option>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                        </div>
                    </li>
                    <li>
                        <div class="caption-text"><?php echo $total_label?></div>
                        <div class="price" id="total_price">$<?php echo $product_rtotal?></div>
                    </li>
                </ul>
                <p class="has-questions"><a target="_blank" href="<?php echo $this->createUrl('/frontend/default/faqs')?>" class="hvr-underline-from-center colored">Have question?</a>Call us at <a href="tel:18597475466" class="hvr-underline-from-center">1.859.747.5466</a></p>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group text-center">
                    <?php if($product_total>0):?> <a href="<?php echo $this->createUrl('/frontend/default/checkout')?>" style="cursor: pointer;"  class="btn btn-primary site-btn"><?php echo $checkout_button_text?></a>  <?php else:?> <a style="cursor: pointer;"  class="btn btn-primary site-btn"><?php echo $checkout_button_text?></a> <?php endif?>
                </div>
            </div>
        </div>
    </form>
</div>

<?php $this->widget('application.modules.frontend.components.AlsoLike'); ?>