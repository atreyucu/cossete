<div class="content-header">
    <div class="container">
        <div class="flex-wrapper">
            <h1 class="page-title text-uppercase"><?php echo $breadcrumb_title?></h1>
            <ol class="breadcrumb">
                <li><a href="<?php $this->createUrl('/')?>" class="hvr-underline-from-center-white"><?php echo Yii::t('checkout','Home')?></a></li>
                <li class="active"><a>Cart</a></li>
            </ol>
        </div>
    </div>
</div>
<div class="container">
    <div id="msg_alert" role="alert" class="site-alerts alert alert-success fade in" style="display: none">
        <button type="button" data-dismiss="alert" aria-label="Close" aria-hidden="true" class="close">&times;</button><strong>Alert text here...!</strong>
    </div>
    <form method="post" class="check-out-form" action="/frontend/default/checkout" id="checkoutForm">
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8">
                <div class="section-title bordered side-title">
                    <h1 class="title text-uppercase"><?php echo $billing_form_header?></h1>
                </div>
                <div class="row">

                    <?php if($have_billing_addr):?>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                            <label><?php echo Yii::t('checkout','Country')?></label>
                            <select class="chosen-select form-control" name="bcountry" required>
                                <?php $index=1?>
                                <?php foreach($countries as $key=>$country):?>
                                    <option id="<?php echo $key?>" <?php if($index == 1 || $key == $bcountry):?>selected<?php endif;?> value="<?php echo $key?>"><?php echo $country['name']?></option>
                                    <?php $index++?>
                                <?php endforeach;?>
                            </select>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                            <label><?php echo Yii::t('checkout','First Name')?>:</label>
                            <input type="text" class="form-control" id="bfirstname" name="bfirstname" value="<?php echo $bfirst_name?>">
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                            <label><?php echo Yii::t('checkout','Last Name')?>:</label>
                            <input type="text" class="form-control" id="blastname" name="blastname" value="<?php echo $blast_name?>">
                        </div>
<!--                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">-->
<!--                            <label>--><?php //echo Yii::t('checkout','Company Name')?><!--:</label>-->
<!--                            <input type="text" class="form-control" id="bcompany" name="bcompany" value="--><?php //echo $bcompany_name?><!--">-->
<!--                        </div>-->
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                            <label><?php echo Yii::t('checkout','Address:')?>:</label>
                            <input type="text" class="form-control" id="baddr" name="baddr" value="<?php echo $baddress?>">
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                            <label><?php echo Yii::t('checkout','Town / City')?>:</label>
                            <input type="text" class="form-control" name="bcity" value="<?php echo $bcity?>">
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                            <label><?php echo Yii::t('checkout','State / County')?>:</label>
                            <input type="text" class="form-control" name="bcounty" value="<?php echo $bstate?>">
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                            <label><?php echo Yii::t('checkout','Post Code / ZIP')?>:</label>
                            <input type="text" class="form-control" name="bpostcode" value="<?php echo $szip_code?>">
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                            <label><?php echo Yii::t('checkout','Email')?>:</label>
                            <input type="email" class="form-control" name="bemail" value="<?php echo $bemail?>" type="email">
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                            <label><?php echo Yii::t('checkout','Phone')?>:</label>
                            <input type="tel" class="form-control" name="bphone" value="<?php echo $bphone?>">
                        </div>
                    <?php else:?>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                            <label><?php echo Yii::t('checkout','Country')?></label>
                            <select class="chosen-select form-control" name="bcountry" >
                                <?php $index=1?>
                                <?php foreach($countries as $key=>$country):?>
                                    <option id="<?php echo $key?>" <?php if($index == 1):?>selected<?php endif;?> value="<?php echo $key?>"><?php echo $country['name']?></option>
                                    <?php $index++?>
                                <?php endforeach;?>
                            </select>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                            <label><?php echo Yii::t('checkout','First Name')?>:</label>
                            <input type="text" class="form-control" name="bfirstname" >
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                            <label><?php echo Yii::t('checkout','Last Name')?>:</label>
                            <input type="text" class="form-control" name="blastname" >
                        </div>
<!--                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">-->
<!--                            <label>--><?php //echo Yii::t('checkout','Company Name')?><!--:</label>-->
<!--                            <input type="text" class="form-control" name="bcompany" >-->
<!--                        </div>-->
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                            <label><?php echo Yii::t('checkout','Address:')?>:</label>
                            <input type="text" class="form-control" name="baddr" >
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                            <label><?php echo Yii::t('checkout','Town / City')?>:</label>
                            <input type="text" class="form-control" name="bcity" >
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                            <label><?php echo Yii::t('checkout','State / County')?>:</label>
                            <input type="text" class="form-control" name="bcounty" >
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                            <label><?php echo Yii::t('checkout','Post Code / ZIP')?>:</label>
                            <input type="text" class="form-control" name="bpostcode" >
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                            <label><?php echo Yii::t('checkout','Email')?>:</label>
                            <input type="email" class="form-control" name="bemail"  type="email">
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                            <label><?php echo Yii::t('checkout','Phone')?>:</label>
                            <input type="tel" class="form-control" name="bphone" >
                        </div>
                    <?php endif?>
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                        <label><?php echo Yii::t('checkout','Ship to a diferent address')?>?</label>
                        <div class="checkbox">
                            <label>
                                <input id="diferent_addr" type="checkbox" name="diferent_addr" <?php if($have_shipp_addr):?>checked="checked"<?php endif;?>><?php echo Yii::t('checkout','Confirm different address')?>
                            </label>
                        </div>
                    </div>

                    <div id="shipping_addr">

                        <div class="section-title bordered side-title">
                            <h1 class="title text-uppercase"><?php echo $shipping_form_header?></h1>
                        </div>


                        <div class="row" style="padding-left: 10px;">
                            <?php if($have_shipp_addr):?>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                    <label><?php echo Yii::t('checkout','Country')?></label>
                                    <select class="chosen-select form-control ship_cmp" name="scountry" >
                                        <?php $index=1?>
                                        <?php foreach($countries as $key=>$country):?>
                                            <option id="<?php echo $key?>" <?php if($index == 1 || $key == $scountry):?>selected<?php endif;?> value="<?php echo $key?>"><?php echo $country['name']?></option>
                                            <?php $index++?>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                                    <label><?php echo Yii::t('checkout','First Name')?>:</label>
                                    <input type="text" class="form-control ship_cmp" name="sfirstname" value="<?php echo $sfirst_name?>">
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                                    <label><?php echo Yii::t('checkout','Last Name')?>:</label>
                                    <input type="text" class="form-control ship_cmp" name="slastname" value="<?php echo $slast_name?>" >
                                </div>
<!--                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">-->
<!--                                    <label>--><?php //echo Yii::t('checkout','Company Name')?><!--:</label>-->
<!--                                    <input type="text" class="form-control ship_cmp" name="scompany" value="--><?php //echo $scompany_name?><!--" >-->
<!--                                </div>-->
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                    <label><?php echo Yii::t('checkout','Address:')?>:</label>
                                    <input type="text" class="form-control ship_cmp" name="saddr" value="<?php echo $saddress?>" >
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                    <label><?php echo Yii::t('checkout','Town / City')?>:</label>
                                    <input type="text" class="form-control ship_cmp" name="scity" value="<?php echo $scity?>" >
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                                    <label><?php echo Yii::t('checkout','State / County')?>:</label>
                                    <input type="text" class="form-control ship_cmp" name="scounty" value="<?php echo $sstate?>" >
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                                    <label><?php echo Yii::t('checkout','Post Code / ZIP')?>:</label>
                                    <input type="text" class="form-control ship_cmp" name="spostcode" value="<?php echo $szip_code?>" >
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                                    <label><?php echo Yii::t('checkout','Email')?>:</label>
                                    <input type="email" class="form-control ship_cmp" name="semail" value="<?php echo $semail?>"  type="email">
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                                    <label><?php echo Yii::t('checkout','Phone')?>:</label>
                                    <input type="tel" class="form-control ship_cmp" name="sphone" value="<?php echo $sphone?>" >
                                </div>
                            <?php else:?>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                    <label><?php echo Yii::t('checkout','Country')?></label>
                                    <select class="chosen-select form-control ship_cmp" name="scountry">
                                        <?php $index=1?>
                                        <?php foreach($countries as $key=>$country):?>
                                            <option id="<?php echo $key?>" <?php if($index == 1):?>selected<?php endif;?> value="<?php echo $key?>"><?php echo $country['name']?></option>
                                            <?php $index++?>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                                    <label><?php echo Yii::t('checkout','First Name')?>:</label>
                                    <input type="text" class="form-control ship_cmp" name="sfirstname">
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                                    <label><?php echo Yii::t('checkout','Last Name')?>:</label>
                                    <input type="text" class="form-control ship_cmp" name="slastname">
                                </div>
<!--                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">-->
<!--                                    <label>--><?php //echo Yii::t('checkout','Company Name')?><!--:</label>-->
<!--                                    <input type="text" class="form-control ship_cmp" name="scompany">-->
<!--                                </div>-->
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                    <label><?php echo Yii::t('checkout','Address:')?>:</label>
                                    <input type="text" class="form-control ship_cmp" name="saddr">
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                    <label><?php echo Yii::t('checkout','Town / City')?>:</label>
                                    <input type="text" class="form-control ship_cmp" name="scity">
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                                    <label><?php echo Yii::t('checkout','State / County')?>:</label>
                                    <input type="text" class="form-control ship_cmp" name="scounty">
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                                    <label><?php echo Yii::t('checkout','Post Code / ZIP')?>:</label>
                                    <input type="text" class="form-control ship_cmp" name="spostcode">
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                                    <label><?php echo Yii::t('checkout','Email')?>:</label>
                                    <input type="email" class="form-control ship_cmp" name="semail" type="email">
                                </div>
                                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 form-group">
                                    <label><?php echo Yii::t('checkout','Phone')?>:</label>
                                    <input type="tel" class="form-control ship_cmp" name="sphone">
                                </div>
                            <?php endif?>
                        </div>

                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                        <label><?php echo Yii::t('checkout','Other Notes')?>:</label>
                        <textarea rows="8" class="form-control" name="notes"></textarea>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                <div class="section-title bordered side-title">
                    <h2 class="title text-uppercase"><?php echo $your_order_header?></h2>
                </div>
                <div class="orders-list">
                    <?php foreach($products as $key=>$prod):?>
                        <div class="order-item" id="<?php echo $key?>">
                            <div class="related-image"><a href="<?php echo $this->createUrl('/frontend/default/product', array('id' => $prod['pid'])) ?>"><img src="<?php echo $prod['img']?>" class="img-responsive"></a></div>
                            <div class="related-info">
                                <div class="header-text"><a style="color: #000000" href="<?php echo $this->createUrl('/frontend/default/product', array('id' => $prod['pid'])) ?>"><?php echo $prod['name']?></a> (<strong><?php echo $prod['qty']?></strong>)</div>
                                <div class="price">$<?php echo $prod['price']?></div>
                            </div>
                        </div>
                    <?php endforeach;?>

                </div>
                <div class="section-title bokrdered side-title">
                    <h2 class="title text-uppercase" style="margin-left: 10px;"><?php echo $cart_totals_header?></h2>
                </div>
                <ul class="cart-totals list-unstyled">
                    <li>
                        <div class="caption-text"><?php echo $subtotal_label?></div>
                        <div class="price"  id="subtotal_price">$<?php echo $total?></div>
                    </li>
                    <li>
                        <div class="caption-text"><?php echo $shipping_label?></div>
                        <div class="price-text">
                            <select id="shipping_select" class="chosen-select" onchange="updateShiping()">
                                <?php foreach($shippings as $key=>$shipp):?>
                                    <option id="<?php echo $key?>" <?php if($key == $pshipp):?> selected <?php endif;?>><?php echo $shipp?></option>
                                <?php endforeach;?>
                            </select>
                        </div>
                    </li>
                    <li>
                        <div class="caption-text"><?php echo $total_label?></div>
                        <div class="price" id="total_price">$<?php echo $rtotal?></div>
                    </li>
                </ul>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                    <div class="accept-terms text-center checkbox">
                        <label>
                            <input id="accept_terms" type="checkbox"><?php echo $accept_terms_text?> <a href="<?php echo $this->createUrl('/frontend/default/term_cond')?>" class="hvr-underline-from-center"><?php echo $terms_cond_link_text?></a>
                        </label>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group text-center">
                    <button id="btn_send" disabled="disabled" type="submit" class="btn btn-primary site-btn"><?php echo $checkout_button_text?></button>
                </div>
            </div>
        </div>
    </form>
</div>