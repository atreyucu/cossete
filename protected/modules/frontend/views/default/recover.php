<div class="content-header">
    <div class="container">
        <div class="flex-wrapper">
            <h1 class="page-title text-uppercase">My account</h1>
            <ol class="breadcrumb">
                <li><a href="#" class="hvr-underline-from-center-white">Home</a></li>
            </ol>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
            <form method="post" class="recover-form">
                <div class="section-title bordered">
                    <h2 class="title text-uppercase">Recover Password</h2>
                </div>
                <div class="form-group">
                    <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu</p>
                </div>
                <div role="alert" class="site-alerts alert alert-success fade in">
                    <button type="button" data-dismiss="alert" aria-label="Close" aria-hidden="true" class="close">&times;</button><strong>Alert text here...!</strong>
                </div>
                <div class="form-group">
                    <label for="username">Username or email address *</label>
                    <input type="text" id="username" required class="form-control input-lg">
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-default btn-lg site-btn">reset</button>
                </div>
            </form>
        </div>
    </div>
</div>
<div data-parallax="scroll" data-image-src="/static/imgs/faqs-parallax.jpg" class="parallax-section login-parallax">
    <div class="faqs-banner text-center">
        <h2>Have any questions we didn't answer?</h2><a href="<?php echo $this->createUrl('/frontend/default/contact')?>" role="button" class="btn btn-default btn-lg site-btn">contact us</a>
    </div>
</div>