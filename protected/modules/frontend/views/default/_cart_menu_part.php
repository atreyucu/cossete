<?php foreach($products as $key=>$prod):?>
    <div id="div<?php echo $key?>" class="order-item"><a role="button" class="delete-btn" onclick="removeFromCart('<?php echo $key?>')"><i class="fa fa-times"></i></a>
        <div class="related-image"><a href="<?php echo $this->createUrl('/frontend/default/product', array('id' => $prod['pid'])) ?>"><img src="<?php echo $prod['img']?>" class="img-responsive"></a></div>
        <div class="related-info">
            <div class="header-text"><?php echo $prod['name']?> (<strong><?php echo $prod['qty']?></strong>)</div>
            <div class="price">$<?php echo $prod['total']?></div>
        </div>
    </div>
<?php endforeach;?>

<div class="text-center subtotal">
    <p>Subtotal: <strong><span id="subtotal_span"><?php echo $subtotal?></span></strong></p>
</div>