<div class="content-header">
    <div class="container">
        <div class="flex-wrapper">
            <h1 class="page-title text-uppercase"><?php echo $contact['title']?></h1>
            <ol class="breadcrumb">
                <li><a href="#" class="hvr-underline-from-center-white">Home</a></li>
                <li class="active"><a><?php echo $contact['title']?></a></li>
            </ol>
        </div>
    </div>
</div>

<?php if($contact['use_map']) : ?>
    <input value="<?php echo $contact['longitud']?>" id="longitude" hidden="hidden">
    <input value="<?php echo $contact['latitude']?>" id="latitude" hidden="hidden">
    <input value="14" id="zoom" hidden="hidden">

    <div class="map-wrapper" id="map_contact"></div>
<?php else : ?>
    <div class="map-wrapper"><img src="<?php echo $contact['map']?>" class="custom-responsive"></div>
<?php endif; ?>

<div class="container">

    <div role="alert" class="site-alerts alert alert-success fade in" style="display: none">
        <button type="button" data-dismiss="alert" aria-label="Close" aria-hidden="true" class="close">&times;</button><strong>Alert text here...!</strong>
    </div>

    <div class="row">
        <form method="post" action="/frontend/default/contact" id="contactForm">
            <div class="col-xs-12 col-sm-8 col-md-8 col-lg-8 col-lg-push-4 col-md-push-4 col-sm-push-4">
                <div class="section-title bordered side-title">
                    <h2 class="title text-uppercase"><?php echo $contact['form_title']?></h2>
                </div>
                <div role="form" method="post" class="form contact-form">
                    <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <label for="contact-name">Your Name:</label>
                        <input type="text" id="name" name="name" class="form-control">
                    </div>
                    <div class="form-group col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <label for="contact-email">Your Email:</label>
                        <input type="email" required="required" id="email" name="email" class="form-control">
                    </div>
                    <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <label for="contact-message">Subject:</label>
                        <textarea rows="8" id="contact-message" name="message" class="form-control"></textarea>
                    </div>
                    <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <button type="submit" class="btn btn-default btn-lg site-btn">send</button>
                    </div>
                </div>
            </div>
        </form>
        <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 col-lg-pull-8 col-md-pull-8 col-sm-pull-8">
            <div class="section-title bordered side-title">
                <h2 class="title text-uppercase"><?php echo $contact['address_title']?></h2>
            </div>
            <div class="address-text">
                <?php echo $contact['address_description']?>
            </div>
            <p class="contact-address"><?php echo $contact['address']?></p>
            <a href="" class="contact-phone hvr-underline-from-center"><?php echo $contact['phone']?></a>
            <div class="section-title">
                <h2 class="title text-uppercase"><?php echo $contact['social_title']?></h2>
            </div>
            <div class="socials-wrapper">
                <ul class="list-inline footer-socials">
                    <li><a href="<?php echo $social->twitter?>"><i class="fa fa-twitter hover-transition text-center"></i></a></li>
                    <li><a href="<?php echo $social->facebook?>"><i class="fa fa-facebook hover-transition text-center"></i></a></li>
                    <li><a href="<?php echo $social->google?>"><i class="fa fa-google-plus hover-transition text-center"></i></a></li>
                    <li><a href="<?php echo $social->linkedin?>"><i class="fa fa-linkedin hover-transition text-center"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>


<?php $this->widget('application.modules.frontend.components.AlsoLike'); ?>