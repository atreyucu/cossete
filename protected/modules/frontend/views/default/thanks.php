<div class="content-header">
    <div class="container">
        <div class="flex-wrapper">
            <h1 class="page-title text-uppercase"><?php echo $thank['page_title']?></h1>
            <ol class="breadcrumb">
                <li><a href="#" class="hvr-underline-from-center-white">Home</a></li>
            </ol>
        </div>
    </div>
</div>
<div data-parallax="scroll" data-image-src="<?php echo $thank['image']?>" class="parallax-section error-parallax text-center">
    <div class="error-parallax-banner thank-text">
        <h1 class="text-capitalize"><?php echo $thank['title']?></h1>
        <p><?php echo $thank['subtitle']?></p>
    </div>
</div>
<div class="dark-title error-dark-title">
    <div class="line-divider"></div>
    <h3 class="text-center text-uppercase"></h3>
</div>