<div id="main-carousel">

    <?php if (count($galleries) == 0):?>
        <div class="main-slide"><img src="/static/imgs/main-slide-01.jpg" class="custom-responsive">
            <div class="slide-text text-center">
                <p class="cursive">finally started</p>
                <p class="big">SUMMER COLLECTION</p>
                <p class="fancy"><img src="/static/imgs/line-left.png" class="left">WOMEN BOUTIQUE<img src="/static/imgs/line-right.png" class="right"></p>
            </div>
        </div>
        <div class="main-slide"><img src="/static/imgs/main-slide-01.jpg" class="custom-responsive">
            <div class="slide-text text-center">
                <p class="cursive">finally started</p>
                <p class="big">SUMMER COLLECTION</p>
                <p class="fancy"><img src="/static/imgs/line-left.png" class="left">WOMEN BOUTIQUE<img src="/static/imgs/line-right.png" class="right"></p>
            </div>
        </div>
        <div class="main-slide"><img src="/static/imgs/main-slide-01.jpg" class="custom-responsive">
            <div class="slide-text text-center">
                <p class="cursive">finally started</p>
                <p class="big">SUMMER COLLECTION</p>
                <p class="fancy"><img src="/static/imgs/line-left.png" class="left">WOMEN BOUTIQUE<img src="/static/imgs/line-right.png" class="right"></p>
            </div>
        </div>
    <?php else : ?>
        <?php foreach($galleries as $gallery) : ?>
            <div class="main-slide"><img alt="<?php echo $gallery['alt']?>" src="<?php echo $gallery['image']?>" class="custom-responsive">
                <div class="slide-text text-center">
                    <p class="cursive"><?php echo $gallery['t_title']?></p>
                    <p class="big"><?php echo $gallery['m_title']?></p>
                    <p class="fancy"><img src="/static/imgs/line-left.png" class="left"><?php echo $gallery['b_title']?><img src="/static/imgs/line-right.png" class="right"></p>
                </div>
            </div>
        <?php endforeach; ?>
    <?php endif; ?>

</div>
<div class="container full-width">
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 seasons-trend clear-padding">
        <div class="big-trend-carousel">
            <?php if (count($trends) == 0):?>
                <div class="trend-item"><img src="/static/imgs/trend-01.jpg" class="custom-responsive"></div>
                <div class="trend-item"><img src="/static/imgs/trend-02.jpg" class="custom-responsive"></div>
                <div class="trend-item"><img src="/static/imgs/trend-03.jpg" class="custom-responsive"></div>
                <div class="trend-item"><img src="/static/imgs/trend-04.jpg" class="custom-responsive"></div>
            <?php else : ?>
                <?php foreach($trends as $trend) : ?>
                    <div class="trend-item">
                        <img alt="<?php echo $trend['alt']?>" src="<?php echo $trend['image']?>" class="custom-responsive">
                        <div class="hover-content text-center">
                            <div class="name"><?php echo $trend['name']?></div><a href="<?php echo $this->createUrl('/frontend/default/search', array('t'=>$trend['tag_id'])) ?>" class="see-more text-uppercase hvr-underline-from-center"><?php echo Yii::t('index_frontend','See more')?></a>
                        </div>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>

        </div>
        <div class="wrapper">
            <div class="header text-uppercase text-center">Season's trends</div>
            <div class="small-trend-carousel">
                <?php if (count($trends) == 0):?>
                    <div class="nav-item"><img src="/static/imgs/trend-02.jpg" class="custom-responsive"></div>
                    <div class="nav-item"><img src="/static/imgs/trend-03.jpg" class="custom-responsive"></div>
                    <div class="nav-item"><img src="/static/imgs/trend-04.jpg" class="custom-responsive"></div>
                    <div class="nav-item"><img src="/static/imgs/trend-01.jpg" class="custom-responsive"></div>
                <?php else : ?>
                    <?php $counter = 0?>
                    <?php for($i = 1; $i < count($trends); $i++) : ?>
                        <div class="nav-item">
                            <img alt="<?php echo $trends[$i]['alt']?>" src="<?php echo $trends[$i]['image']?>" class="custom-responsive">
                            <div class="hover-content text-center">
                                <div class="name"><?php echo $trend['name']?></div><a href="<?php echo $this->createUrl('/frontend/default/search', array('t'=>$trend['tag_id'])) ?>" class="see-more text-uppercase hvr-underline-from-center"><?php echo Yii::t('index_frontend','See more')?></a>
                            </div>
                        </div>
                    <?php endfor; ?>
                    <div class="nav-item">
                        <img alt="<?php echo $trends[0]['alt']?>" src="<?php echo $trends[0]['image']?>" class="custom-responsive">

                        <div class="hover-content text-center">
                            <div class="name"><?php echo $trends[0]['name']?></div><a href="<?php echo $this->createUrl('/frontend/default/search', array('t'=>$trends[0]['tag_id'])) ?>" class="see-more text-uppercase hvr-underline-from-center"><?php echo Yii::t('index_frontend','See more')?></a>
                        </div>
                    </div>
                <?php endif; ?>



            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 clear-padding">
        <div class="fancy-banners">

            <?php if (count($categories_images) == 0):?>
                <div class="banner"><a href=""><img src="/static/imgs/fancy-banner-01.jpg" class="custom-responsive"></a></div>
                <div class="banner"><a href=""><img src="/static/imgs/fancy-banner-02.jpg" class="custom-responsive"></a></div>
                <div class="banner"><a href=""><img src="/static/imgs/fancy-banner-03.jpg" class="custom-responsive"></a></div>
            <?php else : ?>
                <?php foreach($categories_images as $category) : ?>
                    <div class="banner">
                        <a href="<?php echo $this->createUrl('/frontend/default/category', array('id'=>$category['id'])) ?>">
                            <img src="<?php echo $category['image']?>" class="custom-responsive">
                        </a>
                    </div>
                <?php endforeach; ?>
            <?php endif; ?>

        </div>
    </div>
</div>
<div class="row">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 tabs-section">
<!-- TAB NAVIGATION-->
<ul role="tablist" class="list-inline text-center">
    <?php $count = 0 ?>
    <?php foreach($tags as $tag) :?>
        <li role="presentation" <?php if ($count == 0) :?>class="active"<?php endif?>>
            <a href="#<?php echo $tag['id']?>" role="tab" data-toggle="tab" aria-controls="most-review" class="btn btn-default btn-lg site-btn <?php if ($count != 0) :?>inverse<?php endif?> text-uppercase">
                <?php echo $tag['name']?>
            </a>
        </li>
        <?php $count += 1 ?>
    <?php endforeach; ?>
</ul>
<!-- TAB CONTENT-->
<div class="tab-content">
    <?php $count = 0 ?>
    <?php foreach($tags as $tag) :?>
        <div id="<?php echo $tag['id']?>" role="tabpanel" class="<?php if ($count == 0) :?>active in<?php endif?> tab-pane fade">
            <div class="product-list <?php echo $classes[$count]?>">
                <?php foreach($tag['products'] as $product) :?>
                    <div class="item-wrapper">
                        <div class="product-item">
                            <a href="<?php echo $this->createUrl('/frontend/default/product', array('id' => $product['id']))?>">
                                <img src="<?php echo $product['image']?>" class="custom-responsive">
                            </a>
                            <div class="sale-badge">
                                <?php if ($product['has_label']): ?>
                                    <img src="<?php echo $product['label']?>">
                                <?php endif?>

                            </div>
                            <div class="hover-content">
                                <div class="name text-center"><?php echo $product['name']?></div>
                                <div class="content"><a href="<?php echo $this->createUrl('/frontend/default/product', array('id' => $product['id']))?>" class="small-btn hover-transition">add to cart</a>
                                    <ul class="list-inline">
                                        <li class="price"><?php echo $product['price']?></li>
                                        <li><i class="fa fa-star-o favorite"></i></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>

            </div>
        </div>
        <?php $count += 1 ?>
    <?php endforeach; ?>

</div>
</div>
</div>
<div data-parallax="scroll" data-image-src="<?php echo $home_category['image']?>" class="parallax-section home-parallax-section">
    <div class="parallax-banner text-center home-parallax-banner">
        <div class="top-strip"></div>
        <div class="fancy-subtitle"><?php echo $home_category['title']?></div>
        <div class="main-text"><?php echo $home_category['subtitle']?></div>
        <div class="site-logo"><a href=""><img src="<?php echo $home_category['logo']?>"></a></div>
        <div class="description">
            <?php echo $home_category['description']?>
        </div><a href="<?php echo $home_category['button_url']?>" class="site-btn btn-inverted btn btn-default btn-lg"><?php echo $home_category['button']?></a>
    </div>
</div>
</div>

