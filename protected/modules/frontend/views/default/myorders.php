<div class="content-header">
    <div class="container">
        <div class="flex-wrapper">
            <h1 class="page-title text-uppercase">Orders</h1>
            <ol class="breadcrumb">
                <li><a href="#" class="hvr-underline-from-center-white">Home</a></li>
                <li class="active"><a>My Orders</a></li>
            </ol>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="table-responsive table-margin-top">
                <table id="my-orders" class="table table-hover">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Order</th>
                        <th>Date</th>
                        <th>Status</th>
                        <th>Total</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($orders as $order):?>
                        <tr>
                            <td class="close-btn"><i class="fa i fa fa-times-circle-o"></i></td>
                            <td class="order"><?php echo $order['number']?></td>
                            <td class="date"><?php echo $order['date']?></td>
                            <td class="status"><?php if($order['status'] == '1'):?>Pending<?php else:?>Completed<?php endif;?></td>
                            <td class="total">
                                <p class="amount">$<?php echo $order['total']?></p>
                                <p class="small">for <?php echo $order['items']?> item</p>
                            </td>
                            <td class="view-btn"><a href="<?php echo $this->createUrl('/frontend/default/orders', array('id' => $order['id']))?>" class="site-btn">View</a></td>
                            <?php if($order['status'] == '1'):?><td class="view-btn"><a href="<?php echo $this->createUrl('/frontend/paypal/buy', array('order' => $order['id']))?>" class="site-btn">Checkout</a></td><?php endif;?>
                        </tr>
                    <?php endforeach?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="section-title bordered side-title">
                <h2 class="title text-uppercase">My addresses</h2>
            </div>
            <div class="customer-details">
                <?php if(count($billing) > 0):?>
                    <div class="billing-detail">
                        <div class="underline-subtitle text-uppercase">Billing Address</div>
                        <div class="address">
                            <?php echo $billing['name']?>
                            <?php echo $billing['address']?>
                        </div>
                    </div>
                <?php endif;?>
                <?php if(count($shipping) > 0):?>
                    <div class="billing-detail">
                        <div class="underline-subtitle text-uppercase">Shipping Address</div>
                        <div class="address">
                            <?php echo $shipping['name']?>
                            <?php echo $shipping['address']?>
                        </div>
                    </div>
                <?php endif;?>

            </div>
        </div>
    </div>
</div>