<div class="content-header">
    <div class="container">
        <div class="flex-wrapper">
            <h1 class="page-title text-uppercase"><?php echo $category_name ?></h1>
            <ol class="breadcrumb">
                <li><a href="<?php echo $this->createUrl('/frontend/default/index') ?>"
                       class="hvr-underline-from-center-white">Home</a></li>
                <li class="active text-capitalize"><a><?php echo $category_name ?></a></li>
            </ol>
        </div>
    </div>
</div>
<div class="container">
    <div role="alert" class="site-alerts alert alert-success fade in" style="display: none">
        <button type="button" data-dismiss="alert" aria-label="Close" aria-hidden="true" class="close">&times;</button>
        <strong>Alert text here...!</strong>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-9 col-lg-8 needed-margin">
            <div class="sorting-details">
                <?php if ($count > 0) : ?>
                    <p>Showing <?php echo ($initial + 1) . '-' . $end . ' of ' . $count ?> results</p>

                    <div class="sort-filter">
                        <form method="get" id="sort_by">
                            <select name="o" class="form-control chosen-select" id="sort_select">
                                <option <?php if ($o == 'ASC'):?>selected<?php endif?> value="ASC">Ascending by Price</option>
                                <option <?php if ($o == 'DESC'):?>selected<?php endif?> value="DESC">Descesding by Price</option>
                            </select>
                        </form>

                    </div>
                <?php endif; ?>
            </div>
            <div class="row product-page-list">
                <?php if (count($products) == 0) :?>
                    <div role="alert" class="site-alerts alert alert-success fade in">
                        <strong>SORRY, NO RESULTS FOUND</strong>
                    </div>
                <?php else : ?>
                    <?php foreach ($products as $product) : ?>
                        <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                            <div class="product-item">
                                <a href="<?php echo $this->createUrl('/frontend/default/product', array('id' => $product['id'])) ?>">
                                    <img alt="<?php echo $product['image'] ?>" src="<?php echo $product['image'] ?>"
                                         class="custom-responsive">
                                </a>

                                <div class="sale-badge">
                                    <?php if ($product['has_label']): ?>
                                        <img src="<?php echo $product['label'] ?>">
                                    <?php endif ?>

                                </div>
                                <div class="hover-content">
                                    <div class="name text-center"><?php echo $product['name'] ?></div>
                                    <div class="content"><a href="<?php echo $this->createUrl('/frontend/default/product', array('id' => $product['id']))?>" class="small-btn hover-transition">add to cart</a>
                                        <ul class="list-inline">
                                            <li class="price">$<?php echo $product['price'] ?></li>
                                            <li><i class="fa fa-star-o favorite"></i></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4" style="display: none">
                        <div class="product-item"><a href=""><img src="imgs/related-product-02.jpg" class="custom-responsive"></a>
                            <div class="sale-badge"><img src="imgs/sale-badge.png"></div>
                            <div class="hover-content">
                                <div class="name text-center">Class Dark Dress</div>
                                <div class="content"><a href="" class="small-btn hover-transition">add to cart</a>
                                    <ul class="list-inline">
                                        <li class="price">$15.99</li>
                                        <li><i class="fa fa-star-o favorite"></i></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>

            </div>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-right">
                    <?php $this->widget('CLinkPager', array('pages' => $pages)) ?>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-3 col-lg-4 needed-margin">
            <h4 class="sidebar-title text-uppercase"><?php echo $category_page['tree_label']?></h4>

            <div id="side-accordion" role="tablist" aria-multiselectable="true" class="panel-group">
                <?php foreach($categories as $category) :?>
                    <?php if ($category['products'] != 0) :?>
                        <div class="panel panel-default">
                            <div role="tab" id="category-<?php echo $category['id']?>" class="panel-heading">
                                <h4 class="panel-title">
                                    <a href="#item-list-<?php echo $category['id']?>" role="button" data-toggle="collapse"
                                       data-parent="#side-accordion"><?php echo $category['name']?>
                                        <span class="item-count">(<?php echo $category['products']?>)</span>
                                    </a>
                                </h4>
                            </div>
                            <?php if (count($category['children']) > 0) :?>
                                <div role="tabpanel" aria-labelledby="#category-<?php echo $category['id']?>" id="item-list-<?php echo $category['id']?>" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <ul class="list-unstyled">
                                            <?php foreach($category['children'] as $child) :?>
                                                <li><?php echo $child['name']?><span class="item-count">(<?php echo $child['products']?>)</span></li>
                                            <?php endforeach?>
                                        </ul>
                                    </div>
                                </div>
                            <?php endif?>
                        </div>
                    <?php endif; ?>
                <?php endforeach?>
            </div>
            <div class="price-filter">
                <form method="get" class="range-slider" action="<?php echo $this->createUrl('/frontend/default/search')?>">
                    <div class="form-group text-label"><?php echo $category_page['filter_label']?>:</div>
                    <div class="form-group slider"></div>
                    <div data-min="10" data-max="1000" class="form-group text-right range"><span
                            class="price-range"></span>
                    </div>
                    <input id="q" name="q" type="hidden" value="<?php echo $_GET['q']?>">
                    <input id="b" name="b" type="hidden" value="<?php echo $b?>">
                    <input id="f" name="f" type="hidden" value="<?php echo $f?>">
                    <input id="o" name="o" type="hidden" value="<?php echo $o?>">
                    <div class="form-group text-center">
                        <button type="submit" id="button_filter" class="btn btn-default site-btn"><?php echo $category_page['filter_button_label']?></button>
                    </div>
                </form>
            </div>
            <div class="product-banner hidden-xs"><a href=""><img src="<?php echo $category_page['right_image1']?>"
                                                                  class="custom-responsive"></a><a href=""><img
                        src="<?php echo $category_page['right_image2']?>" class="custom-responsive"></a></div>
        </div>
    </div>
</div>
<div data-parallax="scroll" data-image-src="<?php echo $category_page['main_category_image']?>"
     class="parallax-section product-parallax">
    <div class="parallax-banner text-center product-parallax-banner">
        <div class="top-strip"></div>
        <div class="fancy-subtitle"><?php echo $category_page['main_category_title']?></div>
        <div class="main-text"><?php echo $category_page['main_category_subtitle']?></div>
        <div class="site-logo"><a href=""><img src="<?php echo $category_page['small_logo']?>"></a></div>
        <div class="description">
            <?php echo $category_page['main_category_description']?>
        </div>
        <a href="<?php echo $category_page['main_category']?>" class="site-btn btn-inverted btn btn-default btn-lg"><?php echo $category_page['main_category_button']?></a>
    </div>
</div>
