<div class="content-header">
    <div class="container">
        <div class="flex-wrapper">
            <h1 class="page-title text-uppercase"><?php echo $about['title']?></h1>
            <ol class="breadcrumb">
                <li><a href="#" class="hvr-underline-from-center-white">Home</a></li>
                <li class="active"><a>About Us</a></li>
            </ol>
        </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="section-title">
                <h2 class="title text-uppercase text-center"><?php echo $about['description_title']?></h2>
            </div>
            <div class="about-text text-center">
                <?php echo $about['description']?>
            </div>
        </div>
    </div>
</div>
<div class="about-banner">
    <div class="banner-info">
        <div class="parallax-banner text-center">
            <div class="top-strip"></div>
            <div class="fancy-subtitle"><?php echo $about['section1_title']?></div>
            <div class="main-text"><?php echo $about['section1_subtitle']?></div>
            <div class="site-logo"><a href=""><img src="<?php echo $about['section1_logo']?>"></a></div>
            <div class="description text-center">
                <p><?php echo $about['section1_address']?></p><a href="tel:<?php echo $about['section1_phone']?>" class="hvr-underline-from-center-white"><?php echo $about['section1_phone']?></a>
            </div>
        </div>
    </div>
    <div class="banner-images">
        <div class="main-banner"><img src="<?php echo $about['section1_main_banner']?>" class="custom-responsive"></div>
        <div class="small-banner">
            <div class="banner"><img src="<?php echo $about['section1_banner1']?>" class="custom-responsive"></div>
            <div class="banner"><img src="<?php echo $about['section1_banner2']?>" class="custom-responsive"></div>
        </div>
    </div>
</div>
<div class="fancy-woman">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="pretty-woman hidden-xs"><img src="/static/imgs/pretty-woman.png" class="img-responsive"></div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                <div class="section-title">
                    <h2 class="title text-uppercase"><?php echo $about['section2_title']?></h2>
                    <h4 class="subtitle"><?php echo $about['section2_subtitle']?></h4>
                </div>
                <div class="about-founder">
                    <?php echo $about['section2_description']?>
                    <a href="" class="hvr-underline-from-center"><?php echo $about['section2_link1']?></a>
                    <a href="" class="hvr-underline-from-center"><?php echo $about['section2_link2']?></a>
                </div>
            </div>
        </div>
    </div>
    <div class="section-bg">
        <div class="strip"></div>
    </div>
</div>