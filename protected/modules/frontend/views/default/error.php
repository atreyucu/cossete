<div class="content-header">
    <div class="container">
        <div class="flex-wrapper">
            <h1 class="page-title text-uppercase">Error <?php echo $code?></h1>
            <ol class="breadcrumb">
                <li><a href="<?php echo $this->createUrl('/frontend/default/index')?>" class="hvr-underline-from-center-white">Home</a></li>
            </ol>
        </div>
    </div>
</div>
<div data-parallax="scroll" data-image-src="<?php echo $error['image']?>" class="parallax-section error-parallax text-center">
    <div class="error-parallax-banner">
        <h3 class="text-capitalize"><?php echo $error['title']?></h3>
        <?php echo $error['message']?>
    </div>
</div>

<?php $this->widget('application.modules.frontend.components.AlsoLike'); ?>