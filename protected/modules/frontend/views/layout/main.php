<!DOCTYPE html>
<html>
<head>
    <?php $this->widget('application.modules.seo.widgets.SeoPageWidget'); ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php
        Yii::app()->clientScript->scriptMap = array(
            'bootstrap.min.css' => false,
            'font-awesome.min.css' => false,
            'bootstrap-yii.css' => false,
            'jquery-ui-bootstrap.css' => false,
            'jquery.min.js'=>false,
            'jquery.js'=>false,
            'bootstrap-noconflict.js' => false,
            'bootbox.min.js' => false,
            'notify.min.js' => false,
            'bootstrap.min.js' => false,
        );
    ?>

    <link rel="stylesheet" type="text/css" href="/static/css/vendors/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/static/css/vendors/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="/static/css/vendors/easyzoom.css">
    <link rel="stylesheet" type="text/css" href="/static/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="/static/css/chosen.css">
    <link rel="stylesheet" type="text/css" href="/static/slick/slick.css">
    <link rel="stylesheet" type="text/css" href="/static/slick/slick-theme.css">
    <link rel="stylesheet" type="text/css" href="/static/css/styles.css">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

   <?php $session = Yii::app()->session;?>

    <?php $this->renderPartial('_header',
        array(
            'categories' => $this->common_data['categories'],
            'view' => $this->common_data['view'],
            'social' => $this->common_data['social'],
            'products' => $session['cart']['products'],
            'subtotal' => $session['cart']['total'],
            'total' => $session['cart']['rtotal'],
            'counts' => $session['cart']['counts'],
            'company' => $this->common_data['company'],
        ));
    ?>

    <?php echo $content?>

    <?php $this->renderPartial('_footer',
        array(
            'social' => $this->common_data['social'],
            'company' => $this->common_data['company'],
            'footer' => $this->common_data['footer'],
        ));
    ?>

    <script src="/static/js/jquery-1.11.1.js"></script>
    <script src="/static/js/jquery.validate.js"></script>
    <script src="/static/js/bootstrap.min.js"></script>
    <script src="/static/js/jquery-ui.min.js"></script>
    <script src="/static/js/parallax.min.js"></script>
    <script src="/static/js/easyzoom.js"></script>
    <script src="/static/slick/slick.min.js"></script>
    <script src="/static/js/chosen.jquery.min.js"></script>
    <script src="/static/js/site-scripts.js"></script>

    <?php //if (Yii::app()->controller->action->id == 'contact') : ?>
    <script src="/static/js/functions.js"></script>
    <?php //endif; ?>
    <script src="/static/js/common_cart.js"></script>

</body>
</html>