<?php
/**
 * Created by PhpStorm.
 * User: ramdy
 * Date: 25/06/15
 * Time: 14:16
 */

class AlsoLike extends CWidget {

    public function init(){

    }

    public function run(){

        $criteria = new CDbCriteria();
        $criteria->limit = 12;
        $criteria->order = 'visit_count DESC';
        $products = Product::model()->findAll($criteria);
        $products_dto = array();
        foreach ($products as $product) {
            $temp = array();
            $temp['id'] = $product->id;
            $temp['image'] = $product->_listing_image->getFileUrl('normal');
            $label = $product->label0;
            if (isset($label)) {
                $temp['label'] = $product->label0->_icon->getFileUrl('normal');
                $temp['has_label'] = true;
            }
            else
                $temp['has_label'] = false;

            $products_dto[] = $temp;
        }

        return $this->render('alsolike', array(
            'products' => $products_dto
        ));
    }
} 