<div class="dark-title">
    <div class="line-divider"></div>
    <h3 class="text-center text-uppercase">you may also like</h3>
</div>
<div class="container">
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="product-list related-products">
                <?php foreach($products as $product) :?>
                    <div class="item-wrapper">
                        <div class="product-item"><a href=""><img src="<?php echo $product['image']?>" class="custom-responsive"></a>
                            <div class="sale-badge">
                                <?php if ($product['has_label']): ?>
                                    <img src="<?php echo $product['label']?>">
                                <?php endif?>

                            </div>
                        </div>
                    </div>
                <?php endforeach?>
            </div>
        </div>
    </div>
</div>