<?php

class m150819_183235_2015_08_19_adding_client_data extends CDbMigration
{

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
        $this->execute("ALTER TABLE users ADD first_name VARCHAR(70) NULL COMMENT '{\"label\":\"First name\"}' AFTER id;");
        $this->execute("ALTER TABLE users ADD last_name VARCHAR(100) NULL COMMENT '{\"label\":\"Last name\"}' AFTER id;");
        $this->execute("ALTER TABLE users ADD is_client tinyint(1) NULL default 0 COMMENT '{\"label\":\"Is client\"}' AFTER id;");
        $this->execute("ALTER TABLE `order` CHANGE `client` `client` INT( 11 ) NULL DEFAULT NULL ;");

        $this->createTable(
            'client_address', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "breadcrumbs"=>array("tablename"=>"Client","submenu"=>"Address")))."'",
                'first_name' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"First name"))."'",
                'last_name' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Last name"))."'",
                'company_name' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Company"))."'",
                'address' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Company"))."'",
                'city' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"City"))."'",
                'state' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"State"))."'",
                'zip_code' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Zip code"))."'",
                'email' => 'varchar(255) COMMENT '."'".migratetionComment(array("label"=>"Email address", "type" => "@email"))."'",
                'phone' => 'varchar(255) COMMENT '."'".migratetionComment(array("label"=>"Phone", "type" => "@phone"))."'",


                'type' => 'varchar(255) NOT NULL COMMENT '."'".migratetionComment(array("type"=> "dropdown", "label"=>"Order status", "data"=> array(1 => "Shipping", 2 => "Billing")))."'",

                'country' => 'varchar(50)',
                'client' => 'int(11)',

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');

        $this->execute('SET FOREIGN_KEY_CHECKS=0;');

        $this->addForeignKey('order_client_fk1', 'order', 'client', 'users', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('client_addr_fk1', 'client_address', 'client', 'users', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('client_addr_country_fk1', 'client_address', 'country', 'country', 'id', 'CASCADE', 'CASCADE');
	}

	public function safeDown()
	{
	}

}