<?php

class m150828_130448_2015_08_26_paypal_change extends CDbMigration
{

	public function safeUp()
	{
        $this->execute("ALTER TABLE paypal_configuration ADD signature_key TEXT NULL COMMENT '{\"label\":\"signature key\"}' AFTER password;");
	}

	public function safeDown()
	{
        $this->dropColumn('paypal_configuration','signature_key');
	}

}