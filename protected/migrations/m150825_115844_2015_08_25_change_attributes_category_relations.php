<?php

class m150825_115844_2015_08_25_change_attributes_category_relations extends CDbMigration
{

	public function safeUp()
	{
        $this->execute('SET FOREIGN_KEY_CHECKS=0;');

        $this->dropForeignKey('size_category_fk1', 'size');
        $this->dropForeignKey('color_category_fk1', 'color');

        $this->dropColumn('size','category');
        $this->dropColumn('color','category');

        $this->createTable(
            'color_category', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL',
                'color' => 'varchar(50)',
                'category' => 'varchar(50)',

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
                'UNIQUE KEY product_order_uk1 (category,color)',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');


        $this->createTable(
            'size_category', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL',
                'size' => 'varchar(50)',
                'category' => 'varchar(50)',

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
                'UNIQUE KEY product_order_uk1 (category,size)',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');


        $this->addForeignKey('size_category_fk1', 'size_category', 'size', 'size', 'id', 'CASCADE', 'CASCADE' );
        $this->addForeignKey('size_category_fk2', 'size_category', 'category', 'product_category', 'id', 'CASCADE', 'CASCADE');

        $this->addForeignKey('color_category_fk1', 'color_category', 'color', 'color', 'id', 'CASCADE', 'CASCADE' );
        $this->addForeignKey('color_category_fk2', 'color_category', 'category', 'product_category', 'id', 'CASCADE', 'CASCADE');
	}

	public function safeDown()
	{

    }
}