<?php

class m150828_010325_2015_08_26_product_zoom_image extends CDbMigration
{

	public function safeUp()
	{
        $this->execute("ALTER TABLE product ADD detail_zoom_image TEXT NULL COMMENT '{\"type\":\"recipeImg6\",\"label\":\"Detail zoom image\",\"dimensions\":\"600,900\"}' AFTER id;");
	}

	public function safeDown()
	{
        $this->dropColumn('product','detail_zoom_image');
	}

}
