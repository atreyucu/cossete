<?php

class m150817_151822_2015_08_17_trends_image_order extends CDbMigration
{

	public function safeUp()
	{
        $this->execute("ALTER TABLE trend ADD img_order INT(11) DEFAULT 1 NULL COMMENT '{\"label\":\"Trend Order\"}' AFTER id;");
	}

	public function safeDown()
	{
        $this->dropColumn('trend','img_order');
	}
}