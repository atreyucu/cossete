<?php

class m150814_150756_2015_08_11_add_about_us_elements extends CDbMigration
{

	public function safeUp()
	{
        $this->execute("ALTER TABLE aboutus_page ADD description_title TEXT NULL COMMENT '{\"label\":\"Description title\"}' AFTER id;");
        $this->execute("ALTER TABLE aboutus_page ADD section2_link1_text TEXT NULL COMMENT '{\"label\":\"Section2 link1 text\"}' AFTER id;");
        $this->execute("ALTER TABLE aboutus_page ADD section2_link2_text TEXT NULL COMMENT '{\"label\":\"Section2 link2 text\"}' AFTER id;");
	}

	public function safeDown()
	{
        $this->dropColumn('aboutus_page','description_title');
        $this->dropColumn('aboutus_page','section2_link1_text');
        $this->dropColumn('aboutus_page','section2_link2_text');
	}

}