<?php

class m150828_160736_2015_08_26_product_order_change extends CDbMigration
{

	public function safeUp()
	{
        $this->execute("ALTER TABLE product_order ADD quantity int(11) NULL AFTER id;");
        $this->execute("ALTER TABLE product_order ADD price DOUBLE NULL AFTER id;");
        $this->execute("ALTER TABLE product_order ADD psize VARCHAR(255) NULL AFTER id;");
        $this->execute("ALTER TABLE product_order ADD pcolor VARCHAR(255) NULL AFTER id;");
	}

	public function safeDown()
	{
	}
}