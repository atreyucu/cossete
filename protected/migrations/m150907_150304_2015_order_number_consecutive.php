<?php

class m150907_150304_2015_order_number_consecutive extends CDbMigration
{
	public function safeUp()
	{
        $this->createTable('order_number',array(
            "`id` int(11) NOT NULL AUTO_INCREMENT,
             `char_number` varchar(10) NOT NULL,
             PRIMARY KEY (`id`)"
        ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');

	}

	public function safeDown()
	{
        $this->dropTable('order_number');
	}
}