<?php

class m150812_174130_2015_08_11_add_home_banner_order extends CDbMigration
{

	public function safeUp()
	{
        $this->execute("ALTER TABLE home_banner_gallery ADD img_order INT(11) DEFAULT 1 NULL COMMENT '{\"label\":\"Image Order\"}' AFTER id;");
	}

	public function safeDown()
	{
        $this->dropColumn('home_banner_gallery','img_order');
	}
}