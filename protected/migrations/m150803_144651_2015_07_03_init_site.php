<?php

class m150803_144651_2015_07_03_init_site extends CDbMigration
{

	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
        $this->createTable(
            'company_info', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "rows"=> 1, "breadcrumbs"=>array("tablename"=>"General","submenu"=>"Company information")))."'",
                'company_logo' => 'varchar(255) COMMENT '."'".migratetionComment(array("type"=> "recipeImg1", "label"=>"Company logo", "dimensions" => "184,99"))."'",
                'company_small_logo' => 'varchar(255) COMMENT '."'".migratetionComment(array("type"=> "recipeImg2", "label"=>"Company small logo", "dimensions" => "114,61"))."'",
                'name' => 'varchar(255) COMMENT '."'".migratetionComment(array("label"=>"Company name"))."'",
                'email' => 'varchar(255) COMMENT '."'".migratetionComment(array("label"=>"Email address", "type" => "@email"))."'",
                'website' => "varchar(255) DEFAULT NULL COMMENT '{\"type\" : \"@url\", \"label\" : \"Company website\"}'",
                'phone1' => 'varchar(255) COMMENT '."'".migratetionComment(array("label"=>"phone1", "type" => "@phone"))."'",
                'phone2' => 'varchar(255) COMMENT '."'".migratetionComment(array("label"=>"phone2", "type" => "@phone"))."'",

                'addr' => 'Text NULL COMMENT '."'".migratetionComment(array("label"=>"Address"))."'",
                'contact_person' => 'varchar(255) COMMENT '."'".migratetionComment(array("label"=>"Contact Person"))."'",


                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');


        $this->createTable(
            'main_menu', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "rows"=> 1, "breadcrumbs"=>array("tablename"=>"General","submenu"=>"Main menu")))."'",
                'home_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Home label"))."'",
                'sales_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Sales label"))."'",
                'signin_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Signin label"))."'",

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');


        $this->createTable(
            'footer', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "rows"=> 1, "breadcrumbs"=>array("tablename"=>"General","submenu"=>"Footer")))."'",
                'footer_logo' => 'varchar(255) COMMENT '."'".migratetionComment(array("type"=> "recipeImg1", "label"=>"Footer Logo", "dimensions" => "184,99"))."'",
                'about_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"About label"))."'",

                'contact_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Contact label"))."'",


                'faq_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"FAQ label"))."'",


                'tc_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"T&C label"))."'",

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');


        $this->createTable(
            'social_links', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "rows"=> 1, "breadcrumbs"=>array("tablename"=>"General","submenu"=>"Social Media")))."'",

                'twitter' => "varchar(255) DEFAULT NULL COMMENT '{\"type\" : \"@url\", \"label\" : \"Facebook url\"}'",
                'facebook' => "varchar(255) DEFAULT NULL COMMENT '{\"type\" : \"@url\", \"label\" : \"Twitter url\"}'",
                'google' => "varchar(255) DEFAULT NULL COMMENT '{\"type\" : \"@url\", \"label\" : \"Google url\"}'",
                'linkedin' => "varchar(255) DEFAULT NULL COMMENT '{\"type\" : \"@url\", \"label\" : \"Linkedin url\"}'",

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');

        $this->createTable(
            'contactus_page', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "rows"=> 1, "breadcrumbs"=>array("tablename"=>"Pages","submenu"=>"Contact Us")))."'",

                'breadcrumb_title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Breadcrumb title"))."'",
                'form_title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Contact us form title"))."'",
                'social_title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Social integration title"))."'",
                'address_title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Address title"))."'",
                'addr_description' => 'Text NULL COMMENT '."'".migratetionComment(array("label"=>"Address description", "type"=> "@redactor"))."'",
                'send_button_text' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Send button text"))."'",
                'also_like_title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Also like section title"))."'",


                'use_map' => 'tinyint(1) NULL COMMENT '."'".migratetionComment(array("label"=>"Use map view"))."'",
                'longitude' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Longitude"))."'",
                'latitude' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Latitude"))."'",
                'map_image' => 'varchar(255) COMMENT '."'".migratetionComment(array("type"=> "recipeImg1", "label"=>"Map image", "dimensions" => "1599,578"))."'",

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');


        $this->createTable(
            'aboutus_page', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "rows"=> 1, "breadcrumbs"=>array("tablename"=>"Pages","submenu"=>"About Us")))."'",

                'breadcrumb_title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Breadcrumb title"))."'",

                'page_description' => 'Text NULL COMMENT '."'".migratetionComment(array("label"=>"Page description", "type"=> "@redactor"))."'",

                'section1_title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Section1 title"))."'",
                'section1_subtitle' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Section1 subtitle"))."'",
                'section1_about_main_banner' => 'varchar(255) COMMENT '."'".migratetionComment(array("type"=> "recipeImg1", "label"=>"Section 1 about main banner image ", "dimensions" => "800,600"))."'",
                'section1_about_banner1' => 'varchar(255) COMMENT '."'".migratetionComment(array("type"=> "recipeImg2", "label"=>"Section 1 about banner image1 ", "dimensions" => "800,300"))."'",
                'section1_about_banner2' => 'varchar(255) COMMENT '."'".migratetionComment(array("type"=> "recipeImg3", "label"=>"Section 1 about banner image2 ", "dimensions" => "800,300"))."'",

                'section2_title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Section 2 title"))."'",
                'section2_subtitle' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Section 2 subtitle"))."'",
                'section2_description' => 'Text NULL COMMENT '."'".migratetionComment(array("label"=>"Section 2 description", "type"=> "@redactor"))."'",
                'section2_image' => 'varchar(255) COMMENT '."'".migratetionComment(array("type"=> "recipeImg4", "label"=>"Section 2 image", "dimensions" => "605,644"))."'",


                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');


        $this->createTable(
            'error_page', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "rows"=> 1, "breadcrumbs"=>array("tablename"=>"Page","submenu"=>"Error Page")))."'",
                'background_image' => 'varchar(255) COMMENT '."'".migratetionComment(array("type"=> "recipeImg1", "label"=>"Background image", "dimensions" => "1600,566"))."'",
                'breadcrumb_title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Breadcrumb title"))."'",
                'error_title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Title"))."'",
                'error_message' => 'Text NULL COMMENT '."'".migratetionComment(array("label"=>"Message text", "type"=> "@redactor"))."'",
                'also_like_title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Also like section title"))."'",

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');

        $this->createTable(
            'term_conds', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "rows"=> 1, "breadcrumbs"=>array("tablename"=>"Pages","submenu"=>"Error Page")))."'",
                'breadcrumb_title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Breadcrumb title"))."'",
                'tc_description' => 'Text NULL COMMENT '."'".migratetionComment(array("label"=>"Terms and conditions", "type"=> "@redactor"))."'",

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');

        $this->createTable(
            'login_page', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "rows"=> 1, "breadcrumbs"=>array("tablename"=>"Pages","submenu"=>"Faqs Page")))."'",
                'breadcrumb_title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Breadcrumb title"))."'",

                'form_title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Form title"))."'",
                'form_subtitle' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Form subtitle"))."'",
                'username_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Username label"))."'",
                'password_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Password label"))."'",
                'send_button_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Send button label"))."'",

                'background_image' => 'varchar(255) COMMENT '."'".migratetionComment(array("type"=> "recipeImg1", "label"=>"Contact us section background image", "dimensions" => "1600,552"))."'",
                'contact_title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Contact us section title"))."'",
                'contact_button' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Contact us section button text"))."'",

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');


        $this->createTable(
            'register_page', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "rows"=> 1, "breadcrumbs"=>array("tablename"=>"Pages","submenu"=>"Faqs Page")))."'",
                'breadcrumb_title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Breadcrumb title"))."'",

                'form_title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Form title"))."'",
                'form_subtitle' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Form subtitle"))."'",

                'firstname_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Firstname label"))."'",
                'lastname_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Lastname label"))."'",
                'username_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Username label"))."'",
                'password_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Password label"))."'",
                'retype_password_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Retype password label"))."'",
                'email_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Email address label"))."'",
                'address_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Address label"))."'",
                'state_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"State label"))."'",
                'phone_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Phone label"))."'",
                'send_button_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Send button label"))."'",

                'background_image' => 'varchar(255) COMMENT '."'".migratetionComment(array("type"=> "recipeImg1", "label"=>"Contact us section background image", "dimensions" => "1600,552"))."'",
                'contact_title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Contact us section title"))."'",
                'contact_button' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Contact us section button text"))."'",

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');


        $this->createTable(
            'recover_page', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "rows"=> 1, "breadcrumbs"=>array("tablename"=>"Pages","submenu"=>"Faqs Page")))."'",
                'breadcrumb_title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Breadcrumb title"))."'",

                'form_title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Form title"))."'",
                'form_subtitle' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Form subtitle"))."'",
                'username_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Username label"))."'",

                'send_button_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Reset button label"))."'",

                'background_image' => 'varchar(255) COMMENT '."'".migratetionComment(array("type"=> "recipeImg1", "label"=>"Contact us section background image", "dimensions" => "1600,552"))."'",
                'contact_title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Contact us section title"))."'",
                'contact_button' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Contact us section button text"))."'",

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');


        $this->createTable(
            'faqs_page', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "rows"=> 1, "breadcrumbs"=>array("tablename"=>"Pages","submenu"=>"Faqs Page")))."'",
                'breadcrumb_title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Breadcrumb title"))."'",

                'description_text' => 'Text NULL COMMENT '."'".migratetionComment(array("label"=>"Description text", "type"=> "@redactor"))."'",

                'background_image' => 'varchar(255) COMMENT '."'".migratetionComment(array("type"=> "recipeImg1", "label"=>"Contact us section background image", "dimensions" => "1600,552"))."'",
                'contact_title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Contact us section title"))."'",
                'contact_button' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Contact us section button text"))."'",

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');

        $this->createTable(
            'faq_category', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "breadcrumbs"=>array("tablename"=>"Faqs","submenu"=>"Category")))."'",
                'name' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Name"))."'",

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');

        $this->createTable(
            'faq', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "breadcrumbs"=>array("tablename"=>"Faqs","submenu"=>"Question")))."'",
                'title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Title"))."'",

                'question_text' => 'Text NULL COMMENT '."'".migratetionComment(array("label"=>"Question text"))."'",
                'reply_text' => 'Text NULL COMMENT '."'".migratetionComment(array("label"=>"Reply text"))."'",
                'category' => 'varchar(50)',

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');


        $this->createTable(
            'shipping_option', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "breadcrumbs"=>array("tablename"=>"Shop","submenu"=>"Shippings options")))."'",
                'name' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Option name"))."'",

                'use_percent' => 'tinyint(1) NULL COMMENT '."'".migratetionComment(array("label"=>"Use percentage value"))."'",
                'fixed_amount' => 'float NULL COMMENT '."'".migratetionComment(array("label"=>"Fixed amount", "type"=>"currency"))."'",
                'percent_amount' => 'float NULL COMMENT '."'".migratetionComment(array("label"=>"Percent", ))."'",



                'condition' => 'varchar(255) NOT NULL COMMENT '."'".migratetionComment(array("type"=> "dropdown", "label"=>"Apply?", "data"=> array("1" => "By order", "2" => "By product")))."'",

                'is_active' => 'tinyint(1) NULL COMMENT '."'".migratetionComment(array("label"=>"Active"))."'",
                'is_default' => 'tinyint(1) NULL COMMENT '."'".migratetionComment(array("label"=>"Default option"))."'",

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');


        $this->createTable(
            'country', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "breadcrumbs"=>array("tablename"=>"Shop","submenu"=>"Countries")))."'",
                'name' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Name"))."'",

                'code' => 'varchar(50) NULL COMMENT'."'".migratetionComment(array("label"=>"Country code"))."'",
                'is_active' => 'tinyint(1) NULL COMMENT '."'".migratetionComment(array("label"=>"Active"))."'",

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');


        $this->createTable(
            'country_shipping_option', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL',
                'country' => 'varchar(50)',
                'shipping_option' => 'varchar(50)',

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
                'UNIQUE KEY country_shipping_option_uk1 (country,shipping_option)',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');


        $this->createTable(
            'product_category', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "breadcrumbs"=>array("tablename"=>"Shop","submenu"=>"Product categories")))."'",
                'name' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Name"))."'",
                'is_active' => 'tinyint(1) NULL COMMENT '."'".migratetionComment(array("label"=>"Active"))."'",
                'show_in_menu' => 'tinyint(1) NULL COMMENT '."'".migratetionComment(array("label"=>"Show in main menu"))."'",
                'position' => 'tinyint(11) NULL COMMENT '."'".migratetionComment(array("label"=>"Position in main menu"))."'",
                'priority'=> 'tinyint(11)',

                'parent_id' => 'tinyint(10) NULL COMMENT '."'".migratetionComment(array("label"=>"Parent category"))."'",

                'show_in_home' => 'tinyint(1) NULL COMMENT '."'".migratetionComment(array("label"=>"Show in home page"))."'",
                'home_image' => 'varchar(255) COMMENT '."'".migratetionComment(array("type"=> "recipeImg1", "label"=>"Home image", "dimensions" => "528,206"))."'",
                'position' => 'varchar(255) NOT NULL COMMENT '."'".migratetionComment(array("type"=> "dropdown", "label"=>"Position in the home", "data"=> array("1" => "Position 1", "2" => "Position 2", "3" => "Position 3")))."'",

                'root' => 'tinyint(10)',
                'lft' => 'tinyint(10)',
                'rgt' => 'tinyint(10)',
                'level' => 'tinyint(10)',


                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');


        $this->createTable(
            'tag', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "breadcrumbs"=>array("tablename"=>"Shop","submenu"=>"Product tags")))."'",
                'name' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Name"))."'",
                'is_active' => 'tinyint(1) NULL COMMENT '."'".migratetionComment(array("label"=>"Active"))."'",
                'show_in_home' => 'tinyint(1) NULL COMMENT '."'".migratetionComment(array("label"=>"Show in home page"))."'",

                'position' => 'varchar(255) NOT NULL COMMENT '."'".migratetionComment(array("type"=> "dropdown", "label"=>"Position in the home", "data"=> array(1 => "Position 1", 2 => "Position 2", 3 => "Position 3")))."'",
                'priority'=> 'tinyint(11)',

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');


        $this->createTable(
            'product_tag', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL',
                'tag' => 'varchar(50)',
                'product' => 'varchar(50)',

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
                'UNIQUE KEY product_tag_uk1 (tag,product)',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');


        $this->createTable(
            'label', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "breadcrumbs"=>array("tablename"=>"Shop","submenu"=>"Product labels")))."'",
                'name' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Label name"))."'",
                'icon' => 'varchar(255) COMMENT '."'".migratetionComment(array("type"=> "recipeImg1", "label"=>"Label icon", "dimensions" => "61,60"))."'",


                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');



        $this->createTable(
            'size', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "breadcrumbs"=>array("tablename"=>"Shop","submenu"=>"Sizes")))."'",
                'name' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Name"))."'",
                'is_active' => 'tinyint(1) NULL COMMENT '."'".migratetionComment(array("label"=>"Active"))."'",

                'category' => 'varchar(50)',

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');

        $this->createTable(
            'color', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "breadcrumbs"=>array("tablename"=>"Shop","submenu"=>"Colors")))."'",
                'name' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Name"))."'",
                'is_active' => 'tinyint(1) NULL COMMENT '."'".migratetionComment(array("label"=>"Active"))."'",

                'category' => 'varchar(50)',

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');


        $this->createTable(
            'product', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "breadcrumbs"=>array("tablename"=>"Shop","submenu"=>"Product")))."'",
                'name' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Product name"))."'",
                'description' => 'Text NULL COMMENT '."'".migratetionComment(array("label"=>"Product description", "type"=> "@redactor"))."'",
                'price' => 'float NULL COMMENT '."'".migratetionComment(array("label"=>"Price", "type"=>"currency"))."'",
                'visit_count' => 'int(11) NULL COMMENT '."'".migratetionComment(array("label"=>"Visit count"))."'",
                'code' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Product code"))."'",

                'category' => 'varchar(50)',
                'label' => 'varchar(50)',

                'listing_image' => 'varchar(255) COMMENT '."'".migratetionComment(array("type"=> "recipeImg1", "label"=>"Listing image", "dimensions" => "260,390"))."'",
                'detail_image' => 'varchar(255) COMMENT '."'".migratetionComment(array("type"=> "recipeImg2", "label"=>"Detail image", "dimensions" => "360,540"))."'",
                'tag_home_image' => 'varchar(255) COMMENT '."'".migratetionComment(array("type"=> "recipeImg3", "label"=>"Home image(Tag section)", "dimensions" => "275,412"))."'",


                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');



        $this->createTable(
            'product_size', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL',
                'size' => 'varchar(50)',
                'product' => 'varchar(50)',

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
                'UNIQUE KEY product_size_uk1 (size,product)',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');



        $this->createTable(
            'product_color', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL',
                'color' => 'varchar(50)',
                'product' => 'varchar(50)',

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
                'UNIQUE KEY product_color_uk1 (color,product)',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');


        $this->createTable(
            'trend', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "breadcrumbs"=>array("tablename"=>"Shop","submenu"=>"Trend")))."'",
                'title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Trend title"))."'",
                'position' => 'varchar(255) NOT NULL COMMENT '."'".migratetionComment(array("type"=> "dropdown", "label"=>"Position trend list", "data"=> array(1 => "Position 1", 2 => "Position 2", 3 => "Position 3", 4 => "Position 4")))."'",
                'home_image' => 'varchar(255) COMMENT '."'".migratetionComment(array("type"=> "recipeImg1", "label"=>"Trend image", "dimensions" => "600,900"))."'",

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');


        $this->createTable(
            'trend_tag', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL',
                'tag' => 'varchar(50)',
                'trend' => 'varchar(50)',

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
                'UNIQUE KEY trend_tag_uk1 (tag,trend)',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');



        $this->createTable(
            'home_page', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "rows"=> 1, "breadcrumbs"=>array("tablename"=>"Pages","submenu"=>"Home Page")))."'",


                'main_category_image' => 'varchar(255) COMMENT '."'".migratetionComment(array("type"=> "recipeImg1", "label"=>"Main category image", "dimensions" => "100,562"))."'",
                'main_category_title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Main category title"))."'",
                'main_category_subtitle' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Main category subtitle"))."'",
                'main_category_description_text' => 'Text NULL COMMENT '."'".migratetionComment(array("label"=>"Main category description", "type"=> "@redactor"))."'",
                'main_category_button' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Main category button text"))."'",

                'category' => 'varchar(50)',

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');



        $this->createTable(
            'home_banner_gallery', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "breadcrumbs"=>array("tablename"=>"Pages","submenu"=>"Home Gallery")))."'",
                'gallery_image' => 'varchar(255) NOT NULL COMMENT '."'".migratetionComment(array("type"=> "recipeImg1", "label"=>"Image alt", "dimensions" => "1600,802"))."'",

                'home_page' => 'varchar(50) NOT NULL',

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');


        $this->createTable(
            'product_list_page', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "rows"=> 1, "breadcrumbs"=>array("tablename"=>"Pages","submenu"=>"Home Page")))."'",
                'breadcrumb_title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Breadcrumb title"))."'",
                'items_per_page' => 'int(11) NULL COMMENT '."'".migratetionComment(array("label"=>"Items per page"))."'",

                'categories_title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Categories tree header label"))."'",
                'filter_title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Filter by header label"))."'",
                'filter_button_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Filter button label"))."'",


                'litle_banner_image1' => 'varchar(255) COMMENT '."'".migratetionComment(array("type"=> "recipeImg1", "label"=>"Right banner image 1", "dimensions" => "275,200"))."'",
                'litle_banner_image2' => 'varchar(255) COMMENT '."'".migratetionComment(array("type"=> "recipeImg2", "label"=>"Right banner image 2", "dimensions" => "275,200"))."'",


                'main_category_image' => 'varchar(255) COMMENT '."'".migratetionComment(array("type"=> "recipeImg3", "label"=>"Main category image", "dimensions" => "1600,562"))."'",
                'main_category_title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Main category title"))."'",
                'main_category_subtitle' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Main category subtitle"))."'",
                'main_category_description_text' => 'Text NULL COMMENT '."'".migratetionComment(array("label"=>"Main category description", "type"=> "@redactor"))."'",
                'main_category_button' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Main category button text"))."'",

                'category' => 'varchar(50)',

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');



        $this->createTable(
            'product_page', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "rows"=> 1, "breadcrumbs"=>array("tablename"=>"Pages","submenu"=>"Home Page")))."'",
                'breadcrumb_title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Breadcrumb title"))."'",

                'litle_banner_image1' => 'varchar(255) COMMENT '."'".migratetionComment(array("type"=> "recipeImg1", "label"=>"Right banner image 1", "dimensions" => "275,200"))."'",
                'litle_banner_image2' => 'varchar(255) COMMENT '."'".migratetionComment(array("type"=> "recipeImg2", "label"=>"Right banner image 2", "dimensions" => "275,200"))."'",

                'related_product_text' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Related product header title"))."'",
                'addcart_button_text' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Add to cart button text"))."'",

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');


        $this->createTable(
            'order', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "breadcrumbs"=>array("tablename"=>"Shop","submenu"=>"Order")))."'",
                'order_number' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Order number"))."'",
                'order_subtotal' => 'float NULL COMMENT '."'".migratetionComment(array("label"=>"Order Subtotal", "type"=>"currency"))."'",
                'shipping_cost' => 'float NULL COMMENT '."'".migratetionComment(array("label"=>"Shipping cost", "type"=>"currency"))."'",
                'order_total' => 'float NULL COMMENT '."'".migratetionComment(array("label"=>"Order Total", "type"=>"currency"))."'",
                'status' => 'varchar(255) NOT NULL COMMENT '."'".migratetionComment(array("type"=> "dropdown", "label"=>"Order status", "data"=> array(1 => "Pending", 2 => "Completed")))."'",


                'shipping_info' => 'varchar(50)',
                'billing_info' => 'varchar(50)',
                'client' => 'varchar(50)',

                'notes' => 'text NOT NULL COMMENT '."'".migratetionComment(array("label"=>"Order status"))."'",

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');


        $this->createTable(
            'trans_info', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "breadcrumbs"=>array("tablename"=>"Shop","submenu"=>"Transaction information")))."'",
                'first_name' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"First name"))."'",
                'last_name' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Last name"))."'",
                'company_name' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Company"))."'",
                'address' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Company"))."'",
                'city' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"City"))."'",
                'state' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"State"))."'",
                'zip_code' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Zip code"))."'",
                'email' => 'varchar(255) COMMENT '."'".migratetionComment(array("label"=>"Email address", "type" => "@email"))."'",
                'phone' => 'varchar(255) COMMENT '."'".migratetionComment(array("label"=>"Phone", "type" => "@phone"))."'",


                'type' => 'varchar(255) NOT NULL COMMENT '."'".migratetionComment(array("type"=> "dropdown", "label"=>"Order status", "data"=> array(1 => "Shipping", 2 => "Billing")))."'",

                'country' => 'varchar(50)',

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');

        $this->createTable(
            'product_order', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL',
                'product' => 'varchar(50)',
                'ord' => 'varchar(50)',

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
                'UNIQUE KEY product_order_uk1 (product,ord)',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');


        $this->createTable(
            'my_orders_page', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "rows"=> 1, "breadcrumbs"=>array("tablename"=>"Pages","submenu"=>"Home Page")))."'",
                'breadcrumb_title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Breadcrumb title"))."'",

                'order_column_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Order column label"))."'",
                'date_column_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Date column label"))."'",
                'status_column_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Status column label"))."'",
                'total_column_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Total column label"))."'",

                'billing_address' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Billing address header text"))."'",
                'shipping_address' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Shipping address header text"))."'",

                'view_button_text' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Checkout button text"))."'",


                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');


        $this->createTable(
            'checkout_order_page', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "rows"=> 1, "breadcrumbs"=>array("tablename"=>"Pages","submenu"=>"Home Page")))."'",
                'breadcrumb_title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Breadcrumb title"))."'",

                'order_list_header' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Order list header"))."'",
                'order_column_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Order column label"))."'",
                'day_column_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Day column label"))."'",
                'total_column_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Total column label"))."'",


                'cart_details_header' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Cart details header"))."'",
                'subtotal_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Subtotal label"))."'",
                'shipping_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Shipping label"))."'",
                'total_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Total label"))."'",
                'have_question_link_text' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Have question link text"))."'",
                'call_us_text' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Call us text"))."'",

                'order_detail_header' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Order details header"))."'",
                'product_column_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Product column label"))."'",
                'product_total_column_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Product total column label"))."'",

                'customer_detail_header' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Customer details header"))."'",
                'name_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Name label"))."'",
                'email_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Email label"))."'",
                'phone_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Phone label"))."'",

                'billing_address' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Billing address header text"))."'",
                'shipping_address' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Shipping address header text"))."'",

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');


        $this->createTable(
            'shopping_cart_page', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "rows"=> 1, "breadcrumbs"=>array("tablename"=>"Pages","submenu"=>"Home Page")))."'",
                'breadcrumb_title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Breadcrumb title"))."'",

                'product_column_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Product column label"))."'",
                'price_column_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Price column label"))."'",
                'quantity_column_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Quantity column label"))."'",
                'total_column_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Total column label"))."'",
                'checkout_form_header' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Checkout form header text"))."'",
                'subtotal_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Subtotal label"))."'",
                'shipping_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Shipping label"))."'",
                'total_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Total label"))."'",
                'have_question_link_text' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Have question link text"))."'",
                'call_us_text' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Call us text"))."'",


                'you_may_also_text' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"You may also like label"))."'",
                'checkout_button_text' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Checkout button text"))."'",


                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');



        $this->createTable(
            'checkout_page', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "rows"=> 1, "breadcrumbs"=>array("tablename"=>"Pages","submenu"=>"Home Page")))."'",
                'breadcrumb_title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Breadcrumb title"))."'",


                'billing_form_header' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Billing form header text"))."'",
                'shipping_form_header' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Shipping form header text"))."'",
                'your_order_header' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Your order header text"))."'",

                'cart_totals_header' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Cart total header text"))."'",
                'subtotal_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Subtotal label"))."'",
                'shipping_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Shipping label"))."'",
                'total_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Total label"))."'",
                'accept_terms_text' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Accept terms text"))."'",
                'terms_cond_link_text' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Terms & conditions link text"))."'",
                'checkout_button_text' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Checkout button text"))."'",

                'country_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Country label"))."'",
                'firstname_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Firstname label"))."'",
                'lastname_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Lastname label"))."'",
                'city_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Town/City label"))."'",
                'state_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"State/County label"))."'",
                'zipcode_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Zip code label"))."'",
                'email_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Email label"))."'",
                'phone_label' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Phone label"))."'",
                'use_shipping_question_text' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Use different shipping address question text"))."'",
                'checkbox_text' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Checkbox text"))."'",
                'other_notes_text' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Other notest text"))."'",



                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');







        $this->execute('SET FOREIGN_KEY_CHECKS=0;');

        $this->addForeignKey('faqs_category_id_fk1', 'faq', 'category', 'faq_category', 'id', 'CASCADE', 'CASCADE');

        $this->addForeignKey('country_shipping_option_fk1', 'country_shipping_option', 'country', 'country', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('country_shipping_option_fk2', 'country_shipping_option', 'shipping_option', 'shipping_option', 'id', 'CASCADE', 'CASCADE');

        $this->addForeignKey('product_tag_fk1', 'product_tag', 'tag', 'tag', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('product_tag_fk2', 'product_tag', 'product', 'product', 'id', 'CASCADE', 'CASCADE');

        $this->addForeignKey('trend_tag_fk1', 'trend_tag', 'tag', 'tag', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('trend_tag_fk2', 'trend_tag', 'trend', 'trend', 'id', 'CASCADE', 'CASCADE');

        $this->addForeignKey('product_label_fk1', 'product', 'label', 'label', 'id', 'CASCADE', 'CASCADE');


        $this->addForeignKey('product_size_fk1', 'product_size', 'product', 'product', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('product_size_fk2', 'product_size', 'size', 'size', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('product_color_fk1', 'product_color', 'product', 'product', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('product_color_fk2', 'product_color', 'color', 'color', 'id', 'CASCADE', 'CASCADE');


        $this->addForeignKey('product_category_fk1', 'product', 'category', 'product_category', 'id', 'CASCADE', 'CASCADE');

        $this->addForeignKey('size_category_fk1', 'size', 'category', 'product_category', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('color_category_fk1', 'color', 'category', 'product_category', 'id', 'CASCADE', 'CASCADE');


        $this->addForeignKey('home_banner_gallery_fk1', 'home_banner_gallery', 'home_page', 'home_page', 'id', 'CASCADE', 'CASCADE');

        $this->addForeignKey('home_page_category_fk1', 'home_page', 'category', 'product_category', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('product_list_page_category_fk1', 'product_list_page', 'category', 'product_category', 'id', 'CASCADE', 'CASCADE');


        $this->addForeignKey('order_trans_info_fk1', 'order', 'shipping_info', 'trans_info', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('order_trans_info_fk2', 'order', 'billing_info', 'trans_info', 'id', 'CASCADE', 'CASCADE');


        $this->addForeignKey('trans_info_country_fk1', 'trans_info', 'country', 'country', 'id', 'CASCADE', 'CASCADE');


        $this->addForeignKey('product_order_fk1', 'product_order', 'product', 'product', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('product_order_fk2', 'product_order', 'ord', 'order', 'id', 'CASCADE', 'CASCADE');

    }

	public function safeDown()
	{
	}
}