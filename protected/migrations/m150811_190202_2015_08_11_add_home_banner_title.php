<?php

class m150811_190202_2015_08_11_add_home_banner_title extends CDbMigration
{

	public function safeUp()
	{
        $this->execute("ALTER TABLE home_banner_gallery ADD title TEXT NULL COMMENT '{\"label\":\"Slide Title\"}' AFTER id;");
        $this->execute("ALTER TABLE home_banner_gallery ADD middle_title TEXT NULL COMMENT '{\"label\":\"Slide middle title\"}' AFTER id;");
        $this->execute("ALTER TABLE home_banner_gallery ADD bottom_title TEXT NULL COMMENT '{\"label\":\"Slide bottom title\"}' AFTER id;");
	}

	public function safeDown()
	{
        $this->dropColumn('home_banner_gallery','title');
        $this->dropColumn('home_banner_gallery','middle_title');
        $this->dropColumn('home_banner_gallery','bottom_title');
	}

}