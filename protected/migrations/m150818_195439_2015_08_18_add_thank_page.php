<?php

class m150818_195439_2015_08_18_add_thank_page extends CDbMigration
{

	public function safeUp()
	{
        $this->createTable(
            'thank_page', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "rows"=> 1, "breadcrumbs"=>array("tablename"=>"Page","submenu"=>"Thank You Page")))."'",
                'background_image' => 'varchar(255) COMMENT '."'".migratetionComment(array("type"=> "recipeImg1", "label"=>"Background image", "dimensions" => "1600,566"))."'",
                'breadcrumb_title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Page title"))."'",
                'title' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Title"))."'",
                'subtitle' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"SubTitle"))."'",

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');
	}

	public function safeDown()
	{
        $this->dropTable('thank_page');
	}

}