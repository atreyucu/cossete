<?php

class m150811_154737_2015_08_11_change_main_category extends CDbMigration
{

	public function safeUp()
	{
        $this->execute('ALTER TABLE `product_category` CHANGE `parent_id` `parent_id` VARCHAR( 50 ) NULL DEFAULT NULL COMMENT \'{"label":"Parent category"}\';');
	}

	public function safeDown()
	{
        $this->execute('ALTER TABLE `product_category` CHANGE `parent_id` `parent_id` tinyint(11) NULL DEFAULT NULL COMMENT \'{"label":"Parent category"}\';');
	}

}