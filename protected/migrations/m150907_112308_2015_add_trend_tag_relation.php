<?php

class m150907_112308_2015_add_trend_tag_relation extends CDbMigration
{
	public function safeUp()
	{
        $this->execute("ALTER TABLE trend ADD tag_id VARCHAR(50) NULL COMMENT '{\"label\":\"Tag\"}' AFTER id;");

        $this->execute('SET FOREIGN_KEY_CHECKS=0;');

        $this->addForeignKey('tend_tag_id_fk2', 'trend', 'tag_id', 'tag', 'id', 'CASCADE', 'CASCADE');
	}

	public function safeDown()
	{
	}
}