<?php

class m150828_190639_2015_08_26_order_store_shipp_type extends CDbMigration
{

	public function safeUp()
	{
        $this->execute("ALTER TABLE `order` ADD shipping_option VARCHAR(255) NULL AFTER id;");
	}

	public function safeDown()
	{
	}
}