<?php

class m150817_150147_2015_08_17_trends_home_relation extends CDbMigration
{

	public function safeUp()
	{
        $this->execute("ALTER TABLE trend ADD home_page VARCHAR(50) NULL COMMENT '{\"label\":\"Home page\"}' AFTER id;");

        $this->execute('SET FOREIGN_KEY_CHECKS=0;');

        $this->addForeignKey('tend_home_page_id_fk1', 'trend', 'home_page', 'home_page', 'id', 'CASCADE', 'CASCADE');
	}

	public function safeDown()
	{
	}

}