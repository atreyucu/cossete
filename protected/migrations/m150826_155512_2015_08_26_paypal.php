<?php

class m150826_155512_2015_08_26_paypal extends CDbMigration
{
	public function safeUp()
	{
        $this->createTable(
            'paypal_configuration', array(
                'id' => 'varchar(50) PRIMARY KEY NOT NULL COMMENT '."'".migratetionComment(array("type"=>"@uid", "rows"=> 1, "breadcrumbs"=>array("tablename"=>"Administration","submenu"=>"Paypal")))."'",
                'username' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Username (Paypal account)"))."'",

                'password' => 'varchar(255) NULL COMMENT '."'".migratetionComment(array("label"=>"Password"))."'",

                'created'=> 'datetime default null',
                'updated'=> 'datetime default null',
                'owner'=> 'varchar(100) default null',
            ), 'ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;');

	}

	public function safeDown()
	{
        $this->dropTable('paypal_configuration');

	}
}