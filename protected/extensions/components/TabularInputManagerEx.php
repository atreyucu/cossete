<?php
/**
 * Author : Yusnel Rojas García <yrojass@gmail.com>
 * User   : one
 * Date   : 8/7/12
 * Time   : 12:48 PM
 */
Yii::import('ext.components.TabularInputManager');
class TabularInputManagerEx extends TabularInputManager{

    protected $model;
    public $dependantAttr;

    public $_deleteOldItems = true;
    public $_fetchItems = true;
    public $_setUnsafeAttribute = true;
    public $_dependantAttributeDefaultValue = 0;
    public $_primaryKeyAttribute = 'id';

    public function __construct($modelName=null, $params = array()){
        if ($modelName){
            $this->class = $modelName;
            foreach($params as $key=>$value){
                $this->{$key} = $value;
            }
        }
    }

    public function setUnsafeAttribute($item, $model)
    {
        if ($this->_setUnsafeAttribute && !empty($this->dependantAttr)) {
            $item->{$this->dependantAttr} = $model->getPrimaryKey();
        }
    }

    public function validate($model)
    {
        $valid=true;
        /** @var $item Client */
        foreach ($this->_items as $i=>$item){

            if ($item->isNewRecord){
                if ($this->dependantAttr){
                    $item->{$this->dependantAttr} = $this->_dependantAttributeDefaultValue;
                }
            }
            //we want to validate all tags, even if there are errors
            $valid=$item->validate() && $valid;
//            if (!$valid)ddump($item->getErrors());
        }
        return $valid;

    }


    /**
     * @param CActiveRecord $model
     * @param $itemsPk
     */
    public function deleteOldItems($model, $itemsPk){
        if ($this->_deleteOldItems && !empty($this->dependantAttr)) {
            $q = new CDbCriteria();
            $q->addNotInCondition($model->getTableSchema()->primaryKey, $itemsPk);
            $q->compare($this->dependantAttr, $model->getPrimaryKey());
            call_user_func(array($this->class, 'model'))->deleteAll($q);
        }
    }

    public function fetchItems(){
        if ($this->_fetchItems && !empty($this->dependantAttr)) {
            $elements = call_user_func(array($this->class, 'model'))->findAll(array(
                'condition' => '( ' . $this->dependantAttr . ' = :modelID )',
                'params' => array(
                    ':modelID' => $this->model->getPrimaryKey()
                )
            ));

            foreach ($elements as $row) {
                $row->setScenario($this->updateScenario);
                $this->_items[$row->id] = $row;
            }
        }
    }

    public function fetchItemsOrdered($field){
        if ($this->_fetchItems && !empty($this->dependantAttr)) {
            $elements = call_user_func(array($this->class, 'model'))->findAll(array(
                'condition' => '( ' . $this->dependantAttr . ' = :modelID )',
                'params' => array(
                    ':modelID' => $this->model->getPrimaryKey()
                ),
                'order'=> $field . ' ASC',
            ));

            foreach ($elements as $row) {
                $row->setScenario($this->updateScenario);
                $this->_items[$row->id] = $row;
            }
        }
    }

    public function findByAttributes($attrs){
        foreach($this->getItems() as $item){
            $ok = true;
            foreach($attrs as $key=>$value){
                $ok = $ok && ( $item->$key == $value);
                if (!$ok)continue;
            }
            if ($ok)return $item;
        }
        return null;
    }

    public function getOrCreate($attributes){
        $item = $this->findByAttributes($attributes);

        if (null === $item){
            $item = new $this->class($this->newScenario);
        }
        $item->setAttributes($attributes);
        return $item;
    }

    /**
     * @static
     * @param $model
     * @return ChannelProductManager
     */
    public static function load($model, $__CLASS__=__CLASS__)
    {
        $instance = new $__CLASS__();
        $instance->model = $model;
        $instance->fetchItems();
        return $instance;
    }

    public function setDeleteOldItems($deleteOldItems)
    {
        $this->_deleteOldItems = $deleteOldItems;
    }

    public function setFetchItems($fetchItems)
    {
        $this->_fetchItems = $fetchItems;
    }

    public function setSetUnsafeAttribute($setUnsafeAttribute)
    {
        $this->_setUnsafeAttribute = $setUnsafeAttribute;
    }

    public function setPrimaryKeyAttribute($primaryKeyAttribute)
    {
        $this->_primaryKeyAttribute = $primaryKeyAttribute;
    }

    public function setDependantAttributeDefaultValue($dependantAttributeDefaultValue)
    {
        $this->_dependantAttributeDefaultValue = $dependantAttributeDefaultValue;
    }
}