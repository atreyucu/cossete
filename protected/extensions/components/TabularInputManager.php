<?php
/**
 * TabularInputManager is a class of utility for manage tabular input.
 * it supplies all utlity necessary for create, save models in tabular input
 */
abstract class TabularInputManager extends CComponent
{
    /**
     * @var array the items on wich to work
     */
    public $_items = array();

    public $newScenario = 'insert';

    public $updateScenario = 'update';

    /**
     * $var string the classname of the items
     */
    protected $class;

    protected $_lastNew=0;

    protected $_errors=array();	// attribute name => array of errors

    public function isNewItem($key){
        return substr($key, 0, 1)=='n';
    }

    /**
	 * Main function of the class.
	 * load the items from db and applys modification
	 * @param model the model on wich keywords belongs to
	 */
	public function manage($data)
	{
        // var for last new record
		$this->_lastNew=0;
		$classname=$this->class;
		$this->_items=array();
        foreach((array)$data as $i=>$item_post)
		{	// if is ordred a deletion, we jump to the next one element
			if (($i==='command')||($i==='id'))
				continue;
			if (isset($data['command'])&&isset($data["id"]))
				if (($data['command']==="delete")&&($data["id"]===$i))
					continue;
			
			// if the code is like 'nxxx' is a new record
			if ($this->isNewItem($i))
			{ 
				// create of new record
				$item=new $classname();
                $item->setScenario($this->newScenario);
				// rember of the last one code
				$this->_lastNew=substr($i, 1);
			} 
			else{ // load from db
				$item=CActiveRecord::model($this->class)->findByPk($i);
                $item->setScenario($this->updateScenario);
            }
		
			$this->_items[$i]=$item;
			if(isset($data[$i]))
				$item->attributes=$data[$i];
		} // add of new keyword
		if (isset($data['command']))
			if ($data['command']=="add")
			{
                $newId='n'.($this->_lastNew+1);
				$item=new $classname();
				$this->_items[$newId]=$item;
			}
	}
	
	public function getLastNew()
	{
		return $this->_lastNew;
	}
	
	/**
	 * retrive the items
	 * @return array the items loaded
	 */
	public function getItems()
	{
		if (is_array($this->_items))
			return ($this->_items);
		else 
			return array();
	}

	/**
	 * Validates items
	 * @return boolean weather the validation is successfully
	 */
	public function validate($model)
	{
		$valid=true;
		foreach ($this->_items as $i=>$item){
		//FIXME:check this again
            $this->setUnsafeAttribute($item, $model);
		    //we want to validate all tags, even if there are errors
			 $valid=$item->validate() && $valid;
        }
		return $valid;
	
	}
	
	/**
	 * saves the tags on db and delete not needed tags
	 * @param photograph the photograph on wich tags belongs to
	 */
	public function save($model)
	{
        $itemOk=array();
        foreach ((array)$this->_items as $i=>$item)
        {
            $this->setUnsafeAttribute($item, $model);
            $item->save();
            $itemOk[]=$item->primaryKey;
        }
        if (!$model->isNewRecord)
            $this->deleteOldItems($model, $itemOk);
        return count((array)$this->_items) === count($itemOk);
	}
	
	/**
	 * set the unsafe attributes in the items, usually the primary key of the model
	 * @param model the item to save
	 * @param model the model on wich the items belongs to
	 */
	public abstract function setUnsafeAttribute($item, $model);
	
	/**
	 * Delete the items that the user deleted
	 * @param model the model on wich the items belongs to
	 * @param array the primary keys to preserve
	 */
	public abstract function deleteOldItems($model, $itemsPk);
	
	
	/**
	 * Create a tagManager and load the existent tags
	 * @param photograph the photograph on wich tags belongs to
	 * @return tagManager the newly created tagManager
	 */
	public abstract static function load($model);


    /**
     * Returns a value indicating whether there is any validation error.
     * @param string $attribute attribute name. Use null to check all attributes.
     * @return boolean whether there is any error.
     */
    public function hasErrors($attribute=null)
    {
        if($attribute===null)
            return $this->_errors!==array();
        else
            return isset($this->_errors[$attribute]);
    }

    /**
     * Returns the errors for all attribute or a single attribute.
     * @param string $attribute attribute name. Use null to retrieve errors for all attributes.
     * @return array errors for all attributes or the specified attribute. Empty array is returned if no error.
     */
    public function getErrors($attribute=null)
    {
        if($attribute===null)
            return $this->_errors;
        else
            return isset($this->_errors[$attribute]) ? $this->_errors[$attribute] : array();
    }

    /**
     * Returns the first error of the specified attribute.
     * @param string $attribute attribute name.
     * @return string the error message. Null is returned if no error.
     */
    public function getError($attribute)
    {
        return isset($this->_errors[$attribute]) ? reset($this->_errors[$attribute]) : null;
    }

    /**
     * Adds a new error to the specified attribute.
     * @param string $attribute attribute name
     * @param string $error new error message
     */
    public function addError($attribute,$error)
    {
        $this->_errors[$attribute][]=$error;
    }

    /**
     * Adds a list of errors.
     * @param array $errors a list of errors. The array keys must be attribute names.
     * The array values should be error messages. If an attribute has multiple errors,
     * these errors must be given in terms of an array.
     * You may use the result of {@link getErrors} as the value for this parameter.
     */
    public function addErrors($errors)
    {
        foreach($errors as $attribute=>$error)
        {
            if(is_array($error))
            {
                foreach($error as $e)
                    $this->_errors[$attribute][]=$e;
            }
            else
                $this->_errors[$attribute][]=$error;
        }
    }

    /**
     * Removes errors for all attributes or a single attribute.
     * @param string $attribute attribute name. Use null to remove errors for all attribute.
     */
    public function clearErrors($attribute=null)
    {
        if($attribute===null)
            $this->_errors=array();
        else
            unset($this->_errors[$attribute]);
    }

}
