<?php
/**
 * Created by PhpStorm.
 * User: pablo
 * Date: 5/25/14
 * Time: 6:40 PM
 */
Yii::import('ext.JCarousel.JCarousel');
class JCarouselModal extends JCarousel{


    public function registerClientScript()
    {
        $id=$this->getId();

        $options = array();
        if($this->vertical===true)
            $options['vertical'] = true;
        if ($this->rtl===true)
            $options['rtl'] = true;
        if (is_int($this->start))
            $options['start'] = $this->start;
        if (is_int($this->offset))
            $options['offset'] = $this->offset;
        if (is_int($this->size))
            $options['size'] = $this->size;
        if (is_int($this->scroll))
            $options['scroll'] = $this->scroll;
        if (is_int($this->visible))
            $options['visible'] = $this->visible;
        if (isset($this->animation))
            $options['animationa'] = $this->animation;
        if (is_string($this->easing))
            $options['easing'] = $this->easing;
        if (is_int($this->auto))
            $options['auto'] = $this->auto;
        if (is_string($this->wrap))
            $options['wrap'] = $this->wrap;
        if (isset($this->initCallback))
            $options['initCallback'] = $this->initCallback;
        if (isset($this->setupCallback))
            $options['setupCallback'] = $this->setupCallback;
        if (isset($this->itemLoadCallback))
            $options['itemLoadCallback'] = $this->itemLoadCallback;
        if (isset($this->itemFirstInCallback))
            $options['itemFirstInCallback'] = $this->itemFirstInCallback;
        if (isset($this->itemFirstOutCallback))
            $options['itemFirstOutCallback'] = $this->itemFirstOutCallback;
        if (isset($this->itemLastInCallback))
            $options['itemLastInCallback'] = $this->itemLastInCallback;
        if (isset($this->itemLastOutCallback))
            $options['itemLastOutCallback'] = $this->itemLastOutCallback;
        if (isset($this->itemVisibleInCallback))
            $options['itemVisibleInCallback'] = $this->itemVisibleInCallback;
        if (isset($this->itemVisibleOutCallback))
            $options['itemVisibleOutCallback'] = $this->itemVisibleOutCallback;
        if (isset($this->animationStepCallback))
            $options['animationStepCallback'] = $this->animationStepCallback;
        if (isset($this->buttonNextCallback))
            $options['buttonNextCallback'] = $this->buttonNextCallback;
        if (isset($this->buttonPrevCallback))
            $options['buttonPrevCallback'] = $this->buttonPrevCallback;
        if (isset($this->buttonNextHTML))
            $options['buttonNextHTML'] = $this->buttonNextHTML;
        if (isset($this->buttonPrevHTML))
            $options['buttonPrevHTML'] = $this->buttonPrevHTML;
        if (isset($this->buttonNextEvent))
            $options['buttonNextEvent'] = $this->buttonNextEvent;
        if (isset($this->buttonPrevEvent))
            $options['buttonPrevEvent'] = $this->buttonPrevEvent;
        if (is_int($this->itemFallbackDimension))
            $options['itemFallbackDimension'] = $this->itemFallbackDimension;

        Yii::app()->getClientScript()->registerCssFile($this->baseScriptUrl.'/jquery.fancybox.css');

        $options=CJavaScript::encode($options);
        $cs=Yii::app()->getClientScript();
        $cs->registerCoreScript('jquery');
        $cs->registerScriptFile($this->baseScriptUrl.'/jquery.jcarousel.min.js',CClientScript::POS_END);
        //fancybox
        $cs->registerScriptFile($this->baseScriptUrl.'/jquery.fancybox.js',CClientScript::POS_END);
        $cs->registerScriptFile($this->baseScriptUrl.'/jquery.fancybox.pack.js',CClientScript::POS_END);

        $cs->registerScriptFile("$('.fancybox').fancybox({
            overlay : {
                css : {
                    'background' : 'rgba(238,238,238,0.85)'
                }
            },
            padding: 0,

            openEffect : 'elastic',
            openSpeed  : 350,

            closeEffect : 'elastic',
            closeSpeed  : 150,

            closeClick : true

        });",CClientScript::POS_END);



        $cs->registerScript(__CLASS__.'#'.$id,"jQuery('#$id').jcarousel($options);");

        if ($this->target) {
            $clickCallback = $this->clickCallback ? $this->clickCallback : '$(target).html(imageElement);';
            Yii::app()->clientScript->registerScript(__CLASS__.'#'.$id.'-clicker', '
				$("#'.$id.' a").click(function() {
					var itemSrc = $(this).attr("href");
					var itemClass = $(this).attr("class");
					var itemAlt = $(this).children("img").attr("alt");
					var itemTitle = $(this).children("img").attr("title");
					var titleAttr = "";
					if (itemTitle !== "") {
						titleAttr = "title=\""+itemTitle+"\"";
					}
					var target = "#'.$this->target.'";
					var imageElement = "<img src=\""+itemSrc+"\" alt=\""+itemAlt+"\" "+titleAttr+" />";

					$.fancybox({
                         "autoScale": true,
                         "transitionIn": "elastic",
                         "transitionOut": "elastic",
                         "speedIn": 500,
                         "speedOut": 300,
                         "autoDimensions": true,
                         "centerOnScroll": true,
                         "href" : "#'.$this->target.'"
                    });
					'
                .$clickCallback.
                'return false;
            });
        ');
        }
    }



}