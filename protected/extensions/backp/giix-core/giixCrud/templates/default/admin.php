<?php
/**
 * The following variables are available in this template:
 * @var $this GiixCrudCode
 */
?>
<?php
echo "
<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
 ?>";
?>


<?php echo "<?php\n"; ?>
<?php
$submenu = null;
 foreach($this->tableSchema->columns as $column){

    $comment = $column->comment;
    $comment_json = json_decode($comment, 6);

    if(isset($comment_json))
    {
        if(array_key_exists('breadcrumbs', $comment_json))
        {

            $breadcrumbs_temp = $comment_json['breadcrumbs'];
            if(array_key_exists('submenu', $breadcrumbs_temp))
            {

                $submenu = $breadcrumbs_temp["submenu"];
                break;
            }


        }
    }
 }


?>

$this->title = $model->adminNames[3];
$this->breadcrumbs = array(
    $model->adminNames[3]<?php if($submenu != null) echo ",'".$submenu."'" ?>
);

?>

<?php echo '<?php'; ?> $this->widget('application.extensions.bootstrap.widgets.TbGridView', array(
	'id' => '<?php echo $this->class2id($this->modelClass); ?>-grid',
	'dataProvider' => $model->search(),
	//'filter' => $model,
	'columns' => array(
<?php
$count = 0;
foreach ($this->tableSchema->columns as $column) {
	if (++$count == 7)
		echo "\t\t/*\n";
    if($column->name!='id' && $column->name!='created' && $column->name!='updated' && $column->name!='owner')
	    echo "\t\t" . $this->generateGridViewColumn($this->modelClass, $column).",\n";
}
if ($count >= 7)
	echo "\t\t*/\n";
?>
    array(
        'class' => 'application.extensions.bootstrap.widgets.TbButtonColumn',
        'buttons' => array('delete' => array('visible' => (user()->isAdmin) ? 'true' : 'false')),
        'htmlOptions'=>array('class'=>'action-column'),
        'headerHtmlOptions'=>array('class'=>'action-column'),
        'footerHtmlOptions'=>array('class'=>'action-column'),
        'deleteConfirmation'=>Yii::t('admin','Are you sure want to delete this item'),
        'header'=>Yii::t('admin','Actions'),
	),
    ),
)); ?>

<?php //ddump($this->tableSchema)?>
<div class="form-actions">
<?php
    $rows_amount = false;
    foreach($this->tableSchema->columns as $column):

    $comment = $column->comment;
    $json = json_decode($comment, 6);
//    dump($comment);
    if(isset($json))
    {
        if(array_key_exists('rows', $json))
        {
            echo "<?php if(\$model->count() < "; echo $json['rows']."): ?>\n";
            $rows_amount = $json['rows'];
            break;
        }
    }


    ?>

<?php endforeach?>
<?php if($rows_amount > 0):?>
    <p>Up to <?php echo $rows_amount;?> row(s).</p>
<?php endif?>
<?php echo "<?php if(user()->isAdmin):?>\n";?>

    <?php
        echo "<?php echo CHtml::link(Yii::t('admin',TbHtml::icon('glyphicon glyphicon-plus'). 'Add item'),array('create'),array('class'=>'btn btn-default'));?>"
     ?>

<?php echo '<?php endif?>'?>

<?php if($rows_amount > 0){
    echo "<?php endif?>\n";
}

?>
</div>
<div id="success"  class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <p><?php echo "<?php echo Yii::t('admin','The data was save with success');?>";?></p>
            </div>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>

