<?php
echo "
<?php
/**
 * The following code was generated automatically using GiixCrudCode
 * This generator was improve by iReevo Team
 */
 ?>";
?>

<?php $ajax = ($this->enable_ajax_validation) ? 'true' : 'false'; ?>

<?php echo '<?php '; ?>
$form = $this->beginWidget('application.extensions.bootstrap.widgets.TbActiveForm', array(
    'id' => '<?php echo $this->class2id($this->modelClass); ?>-form',
<?php if ($this->enable_ajax_validation): ?>
    'enableClientValidation'=>true,
<?php endif; ?>

));
<?php echo '?>'; ?>

<?php foreach ($this->tableSchema->columns as $column): ?>
<?php if (!$column->autoIncrement && ($column->name!='id' && $column->name!='created' && $column->name!='updated' && $column->name!='owner')): ?>
    <?php echo "<?php " . $this->generateActiveField($this->modelClass, $column) . "; ?>\n"; ?>
<?php endif; ?>
<?php endforeach; ?>
<?php
echo "<div class='form-actions'>";
echo "   <a href='<?php echo app()->request->urlReferrer;?>' class='btn btn-default'><span class='glyphicon glyphicon-arrow-left'></span><?php echo t('Back');?></a>";
echo "   <?php \$this->widget(
        'application.extensions.bootstrap.widgets.TbButton',
        array(
            'buttonType' => 'submit',
            'context' => 'primary',
            'icon'=> 'glyphicon glyphicon-saved',
            'label' => Yii::t('admin','Save item')
        )
    ); ?>
    <?php \$this->widget(
        'application.extensions.bootstrap.widgets.TbButton',
        array(
            'buttonType' => 'reset',
            'context' => 'warning',
            'icon'=> 'glyphicon glyphicon-remove',
            'label' => Yii::t('admin','Reset form')
        )
    ); ?>
    <?php \$this->endWidget(); ?>
    <?php if(isset(\$model->id)){
       // echo CHtml::link(Yii::t('admin',TbHtml::icon('glyphicon glyphicon-remove'). 'Remove item'),array('delete','id'=>\$model->id),array('class'=>'btn btn-danger'));
    }?>";
echo "</div>";
?>
