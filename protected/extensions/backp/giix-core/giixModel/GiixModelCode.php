<?php

/**
 * GiixModelCode class file.
 *
 * @author Rodrigo Coelho <rodrigo@giix.org>
 * @link http://giix.org/
 * @copyright Copyright &copy; 2010-2011 Rodrigo Coelho
 * @license http://giix.org/license/ New BSD License
 */
Yii::import('system.gii.generators.model.ModelCode');
Yii::import('ext.giix-core.helpers.*');

/**
 * GiixModelCode is the model for giix model generator.
 *
 * @author Rodrigo Coelho <rodrigo@giix.org>
 */
class GiixModelCode extends ModelCode {

	/**
	 * @var string The (base) model base class name.
	 */
	public $baseClass = 'GxActiveRecord';
	/**
	 * @var string The path of the base model.
	 */
	public $baseModelPath;
	/**
	 * @var string The base model class name.
	 */
	public $baseModelClass;

    /**
     * Generates and returns the view source code line
     * to create the CGridView column definition.
     * @param string $modelClass The model class name.
     * @param CDbColumnSchema $column The column.
     * @return string The source code line for the column definition.
     */
    public function generateGridViewColumn($modelClass, $column) {
        if (!$column->isForeignKey) {
            // Boolean or bit.
            if (strtoupper($column->dbType) == 'TINYINT(1)'
                || strtoupper($column->dbType) == 'BIT'
                || strtoupper($column->dbType) == 'BOOL'
                || strtoupper($column->dbType) == 'BOOLEAN') {
                return "array(
                	'class' => 'ext.yExt.YBooleanColumn',
					'name' => '{$column->name}',
				)";
            } else if(stripos($column->dbType, 'int') !== false && strpos($column->comment,'@order')!==false){
                return "'{$column->name}'";
            } else
                return "'{$column->name}'";
        } else { // FK.
            // Find the related model for this column.
            list($relationName, $relatedModelClass) = $this->findRelation($modelClass, $column);
            return "array(
                    'class' => 'ext.yExt.YRelatedColumn',
					'relation'=>'{$relationName}',
				)";
        }
    }

    /**
     * Generates and returns the view source code line
     * to create the CGridView column definition.
     * @param string $modelClass The model class name.
     * @param CDbColumnSchema $column The column.
     * @return string The source code line for the column definition.
     */
    public function generateDetailViewColumn($modelClass, $column) {
        if (!$column->isForeignKey) {
            // Boolean or bit.
            if (strtoupper($column->dbType) == 'TINYINT(1)'
                || strtoupper($column->dbType) == 'BIT'
                || strtoupper($column->dbType) == 'BOOL'
                || strtoupper($column->dbType) == 'BOOLEAN') {
                return "'{$column->name}:boolean'";
            } else if(stripos($column->dbType, 'int') !== false && strpos($column->comment,'@order')!==false){
                return "'{$column->name}'";
            } else
                return "'{$column->name}'";
        } else { // FK.
            // Find the related model for this column.
            list($relationName, $relatedModelClass) = $this->findRelation($modelClass, $column);
            return "array(
                    'name'=>'{$column->name}',
                    'type'=>'raw',
                    'value'=>CHtml::link(\$this->{$relationName}, ycm()->url_view(\$this->{$relationName})),
				)";
        }
    }

    /**
     * Finds the relation of the specified column.
     * Note: There's a similar method in the class GxActiveRecord.
     * @param string $modelClass The model class name.
     * @param CDbColumnSchema $column The column.
     * @return array The relation. The array will have 3 values:
     * 0: the relation name,
     * 1: the relation type (will always be GxActiveRecord::BELONGS_TO),
     * 2: the foreign key (will always be the specified column),
     * 3: the related active record class name.
     * Or null if no matching relation was found.
     */
    public function findRelation($modelClass, $column) {
        if (!$column->isForeignKey)
            return null;
        $relations = $this->getRelationsData($modelClass);
        // Find the relation for this attribute.
        foreach ($relations as $relationName => $relation) {
            // For attributes on this model, relation must be BELONGS_TO.
            if ($relation[0] == GxActiveRecord::BELONGS_TO && $relation[3] == $column->name) {
                return array(
                    $relationName, // the relation name
                    $relation[1] // the related active record class name
                );
            }
        }
        // None found.
        return null;
    }




    public function generateWidget($column) {

        $comment = $column->comment;
        $comment_json = json_decode($comment, 6);
        $type = null;

        if(isset($comment_json))
        {
            if(array_key_exists('type', $comment_json))
            {
                $type = $comment_json['type'];
            }
        }
        if ($column->isForeignKey) {
            if (strpos($column->comment,'@select2')!==false)
                return "array('{$column->name}', 'select2')";

            if (strpos($column->comment,'@dropDown')!==false)
                return "array('{$column->name}', 'dropDown')";

            if (strpos($column->comment,'@radioButton')!==false)
                return "array('{$column->name}', 'radioButton')";

            if (strpos($column->comment,'@btngroup')!==false)
                return "array('{$column->name}', 'btngroup')";

            return "array('{$column->name}', 'chosen')";



        }

        if (strtoupper($column->dbType) == 'TINYINT(1)'
            || strtoupper($column->dbType) == 'BIT'
            || strtoupper($column->dbType) == 'BOOL'
            || strtoupper($column->dbType) == 'BOOLEAN') {
            return "array('{$column->name}', 'boolean')";
        }else if ($column->type==='double') {

            return "array('{$column->name}', 'currency')";
        } else if ($column->type==='integer') {
            if (strpos($column->comment,'@datetime')!==false)
                return "array('{$column->name}', 'datetime')";

            if (strpos($column->comment,'@date')!==false)
                return "array('{$column->name}', 'date')";

            if (strpos($column->comment,'@order')!==false)
                return "array('{$column->name}', 'hide')";




            return "array('{$column->name}', 'spinner')";
        } else if (strtoupper($column->dbType) == 'DATETIME') {
            return "array('{$column->name}', 'datetime')";
        } else if (strtoupper($column->dbType) == 'DATE') {
            return "array('{$column->name}', 'date')";
        }else if (strtoupper($column->dbType) == 'TIME') {
            return "array('{$column->name}', 'time')";
        } else if (stripos($column->dbType, 'text') !== false and (stripos($column->comment, "@redactor") !== false or stripos($type, "@redactor") !== false)) { // Start of CrudCode::generateActiveField code.
            if (strpos($column->comment,'@redactor')!==false or stripos($type, "@redactor") !== false)
                return "array('{$column->name}', 'wysiwyg')";

            if (strpos($column->comment,'@html')!==false or stripos($type, "@html") !== false)
                return "array('{$column->name}', 'wysiwyg')";

            if (strpos($column->comment,'@html5')!==false or  stripos($type, "@html5") !== false)
                return "array('{$column->name}', 'html5')";

            if (strpos($column->comment,'@markdown')!==false or stripos($type, "@markdown") !== false)
                return "array('{$column->name}', 'markdown')";

            if (strpos($column->comment,'@ckeditor')!==false or stripos($type, "@ckeditor") !== false)
                return "array('{$column->name}', 'ckeditor')";

            return "array('{$column->name}', 'textArea')";
        } else {
            if (strpos($column->comment,'@mask')!==false or stripos($type, "@mask") !== false) {
                $matches = array();
                preg_match('/\@mask (?P<mask>.+)/', $column->comment, $matches);
                $mask = $matches['mask'];
                return "array('{$column->name}', 'mask', 'mask'=>'$mask')";
            }

            if (strpos($column->comment,'@color')!==false or stripos($type, "@color") !== false) {
                return "array('{$column->name}', 'color')";
            }

            if (strpos($column->comment,'@file')!==false or stripos($type, "@file") !== false) {
                return "array('{$column->name}', 'file')";
            }

            if (strpos($column->comment,'@image')!==false or stripos($type, "@image") !== false) {
                return "array('{$column->name}', 'image')";
            }

            if (strpos($column->comment,'@dateRange')!==false or stripos($type, "@dateRange") !== false) {
                return "array('{$column->name}', 'dateRange')";
            }

            $passwordI18n = 'password';
            $passwordI18n = (isset($passwordI18n) && $passwordI18n !== '') ? '|' . $passwordI18n : '';
            $pattern = '/^(password|pass|passwd|passcode' . $passwordI18n . ')$/i';
            if (preg_match($pattern, $column->name))
                return "array('{$column->name}', 'password')";
            return "array('{$column->name}', 'textField')";
        } // End of CrudCode::generateActiveField code.
    }

    public function getOrderColumn() {
        foreach ($this->getTableSchema($this->tableName)->columns as $column){
            if (stripos($column->dbType, 'int') !== false && strpos($column->comment,'@order')!==false) {
                return $column;
            }
        }
        return false;
    }

    public function getDates(){
        $dates = array();
        foreach ($this->getTableSchema($this->tableName)->columns as $column){
            if (stripos($column->dbType, 'int') !== false && strpos($column->comment,'@date')!==false && strpos($column->comment,'@datetime')===false) {
                $dates[] = $column->name;
            }
        }
        return implode(',', $dates);
    }

    public function getDateTimes(){
        $dates = array();
        foreach ($this->getTableSchema($this->tableName)->columns as $column){
            if (stripos($column->dbType, 'int') !== false && strpos($column->comment,'@datetime')!==false) {
                $dates[] = $column->name;
            }
        }
        return implode(',', $dates);
    }

    public function getCurrencies(){
        $currencies = array();
        foreach ($this->getTableSchema($this->tableName)->columns as $column){
            if ($column->type==='double') {
                $currencies[] = $column->name;
            }
        }
        return implode(',', $currencies);
    }

	/**
	 * Prepares the code files to be generated.
	 * #MethodTracker
	 * This method is based on {@link ModelCode::prepare}, from version 1.1.7 (r3135). Changes:
	 * <ul>
	 * <li>Generates the base model.</li>
	 * <li>Provides the representing column for the table.</li>
	 * <li>Provides the pivot class names for MANY_MANY relations.</li>
	 * </ul>
	 */
	public function prepare() {
		if (($pos = strrpos($this->tableName, '.')) !== false) {
			$schema = substr($this->tableName, 0, $pos);
			$tableName = substr($this->tableName, $pos + 1);
		} else {
			$schema = '';
			$tableName = $this->tableName;
		}
		if ($tableName[strlen($tableName) - 1] === '*') {
			$tables = Yii::app()->db->schema->getTables($schema);
			if ($this->tablePrefix != '') {
				foreach ($tables as $i => $table) {
					if (strpos($table->name, $this->tablePrefix) !== 0)
						unset($tables[$i]);
				}
			}
		}
		else
			$tables=array($this->getTableSchema($this->tableName));

		$this->files = array();
		$templatePath = $this->templatePath;

		$this->relations = $this->generateRelations();

		foreach ($tables as $table) {
			$tableName = $this->removePrefix($table->name);
			$className = $this->generateClassName($table->name);

			// Generate the pivot model data.
			$pivotModels = array();
			if (isset($this->relations[$className])) {
				foreach ($this->relations[$className] as $relationName => $relationData) {
					if (preg_match('/^array\(self::MANY_MANY,.*?,\s*\'(.+?)\(/', $relationData, $matches)) {
						// Clean the table name if needed.
						$pivotTableName = str_replace(array('{', '}'), '', $matches[1]);
						$pivotModels[$relationName] = $this->generateClassName($pivotTableName);
					}
				}
			}

			$params = array(
				'tableName' => $schema === '' ? $tableName : $schema . '.' . $tableName,
				'modelClass' => $className,
				'columns' => $table->columns,
				'labels' => $this->generateLabelsEx($table, $className),
				'rules' => $this->generateRules($table),
				'relations' => isset($this->relations[$className]) ? $this->relations[$className] : array(),
				'representingColumn' => $this->getRepresentingColumn($table), // The representing column for the table.
				'pivotModels' => $pivotModels, // The pivot models.
			);
			// Setup base model information.
			$this->baseModelPath = $this->modelPath . '._base';
			$this->baseModelClass = 'Base' . $className;
			// Generate the model.
			$this->files[] = new CCodeFile(
							Yii::getPathOfAlias($this->modelPath . '.' . $className) . '.php',
							$this->render($templatePath . DIRECTORY_SEPARATOR . 'model.php', $params)
			);
			// Generate the base model.
			$this->files[] = new CCodeFile(
							Yii::getPathOfAlias($this->baseModelPath . '.' . $this->baseModelClass) . '.php',
							$this->render($templatePath . DIRECTORY_SEPARATOR . '_base' . DIRECTORY_SEPARATOR . 'basemodel.php', $params)
			);
		}
	}

	/**
	 * Lists the template files.
	 * #MethodTracker
	 * This method is based on {@link ModelCode::requiredTemplates}, from version 1.1.7 (r3135). Changes:
	 * <ul>
	 * <li>Includes the base model.</li>
	 * </ul>
	 * @return array A list of required template filenames.
	 */
	public function requiredTemplates() {
		return array(
			'model.php',
			'_base' . DIRECTORY_SEPARATOR . 'basemodel.php',
		);
	}

	/**
	 * Generates the labels for the table fields and relations.
	 * By default, the labels for the FK fields and for the relations is null. This
	 * will cause them to be represented by the related model label.
	 * #MethodTracker
	 * This method is based on {@link ModelCode::generateLabels}, from version 1.1.7 (r3135). Changes:
	 * <ul>
	 * <li>Default label for FKs is null.</li>
	 * <li>Creates entries for the relations. The default label is null.</li>
	 * </ul>
	 * @param CDbTableSchema $table The table definition.
	 * @param string $className The model class name.
	 * @return array The labels.
	 * @see GxActiveRecord::label
	 * @see GxActiveRecord::getRelationLabel
	 */
	public function generateLabelsEx($table, $className) {
		$labels = array();
		// For the fields.
		foreach ($table->columns as $column) {
            $comment = $column->comment;
            $comment_json = json_decode($comment, 6);
            $label_temp = null;
            if(isset($comment_json))
            {
                if(array_key_exists('label', $comment_json))
                {
                    $label_temp = $comment_json['label'];
                }
                $type = null;
                if(array_key_exists('type', $comment_json))
                {
                    if(stripos($comment_json['type'], 'recipeImg') !== false)
                    {
                        $label_temp = $label_temp.' alt';
                    }
                }

            }
			if ($column->isForeignKey) {
                $name = explode('_', $column->name);
                if($name[count($name)-1] == 'id')
                    unset($name[count($name)-1]);
                $name = implode(' ', $name);
                $name = ucwords($name);
                $label = "\$this->t_label('{$name}')";
			} else {
				$label = ucwords(trim(strtolower(str_replace(array('-', '_'), ' ', preg_replace('/(?<![A-Z])[A-Z]/', ' \0', $column->name)))));
				$label = preg_replace('/\s+/', ' ', $label);

				if (strcasecmp(substr($label, -3), ' id') === 0)
					$label = substr($label, 0, -3);
				if ($label === 'Id')
					$label = 'ID';

				$label = "\$this->t_label('{$label}')";
			}
            if($label_temp != null)
                $labels[$column->name] = "\$this->t_label('$label_temp')";
            else
			    $labels[$column->name] = $label;
		}
		// For the relations.
		$relations = $this->getRelationsData($className);
		if (isset($relations)) {
			foreach (array_keys($relations) as $relationName) {
				$labels[$relationName] = null;
			}
		}

		return $labels;
	}

    public function parent_generateRules_modificado($table)
    {
        $rules=array();
        $required=array();
        $integers=array();
        $boolean=array();
        $numerical=array();
        $length=array();
        $safe=array();

        $date=array();
        $datetime=array();
        $file=array();
        $image=array();
        $url=array();
        $email=array();


         //LA VARIABLE $TYPE ES EL TIPO QUE SE ESPECIFICA EN EL COMMENT DE LA COLUMNA EN EL SQL
        foreach($table->columns as $column)
        {
            $comment = $column->comment;
            $json = json_decode($comment, 6);

            $type = '';
            if(isset($json))
            {
                if(array_key_exists('type', $json))
                {
                    $type = $json['type'];
                }
            }

            if($column->autoIncrement)
                continue;
            $r=!$column->allowNull && $column->defaultValue===null;
            if($r && !(strtoupper($column->dbType) == 'TINYINT(1)'
                || strtoupper($column->dbType) == 'BIT'
                || strtoupper($column->dbType) == 'BOOL'
                || strtoupper($column->dbType) == 'BOOLEAN'))
                $required[]=$column->name;
            if($column->type==='integer' && strtoupper($column->dbType) != 'TINYINT(1)'){
                if (strpos($column->comment,'@datetime')!==false) {
                    $datetime[] = $column->name;
                }else if (strpos($column->comment,'@date')!==false or $type == '@date') {
                    $date[] = $column->name;
                }
                else {
                    $integers[] = $column->name;
                }

            }else if (strtoupper($column->dbType) == 'TINYINT(1)'
                || strtoupper($column->dbType) == 'BIT'
                || strtoupper($column->dbType) == 'BOOL'
                || strtoupper($column->dbType) == 'BOOLEAN')
                $boolean[]=$column->name;
            elseif($column->type==='double')
                $numerical[]=$column->name;
            elseif($column->type==='string' && $column->size>0)
                $length[$column->size][]=$column->name;
            elseif(!$column->isPrimaryKey && !$r)
                $safe[]=$column->name;

            if($column->type==='string' && (strpos($column->comment,'@image' or $type== '@image'))!==false){
                $image[] = $column->name;
            }
            if($column->type==='string' && (strpos($column->comment,'@file' or $type== '@file'))!==false){
                $file[] = $column->name;
            }
            if($column->type==='string' && ((strpos($column->comment,'@url' or $type== '@file'))!==false) || $type=='url'){
                $url[] = $column->name;
            }
            if($column->type==='string' && (strpos($column->comment,'@email' or $type== '@file'))!==false){
                $email[] = $column->name;
            }
        }
        if($datetime!==array())
            $rules[]="array('".implode(', ',$datetime)."', 'date', 'format'=>'yyyy-MM-dd HH:mm:ss')";
        if($date!==array())
            $rules[]="array('".implode(', ',$date)."', 'date', 'format'=>'yyyy-MM-dd')";
        if($required!==array())
            $rules[]="array('".implode(', ',$required)."', 'required')";
        if($integers!==array())
            $rules[]="array('".implode(', ',$integers)."', 'numerical', 'integerOnly'=>true)";
        if($boolean!==array())
            $rules[]="array('".implode(', ',$boolean)."', 'boolean')";
        if($numerical!==array())
            $rules[]="array('".implode(', ',$numerical)."', 'numerical')";
//            $rules[]="array('".implode(', ',$numerical)."', 'numerical', 'numberPattern' => \"/{\$this->currency->pathern}/\")";

        if($image!==array()){
            $rules[]="array('".implode(', ',$image)."', 'file', 'on'=>'insert', 'allowEmpty'=>true, 'types'=>'jpg,jpeg,gif,png', 'maxSize'=>1024*1024*6)";
            $rules[]="array('".implode(', ',$image)."', 'file', 'on'=>'update', 'allowEmpty'=>true, 'types'=>'jpg,jpeg,gif,png', 'maxSize'=>1024*1024*6)";
        }

        if($file!==array()){
            $rules[]="array('".implode(', ',$file)."', 'file', 'on'=>'insert', 'allowEmpty'=>true, 'types'=>'jpg,jpeg,gif,png,gz,tar,zip,pdf,doc,docx,xls,xlsx,ppt,pptx,pps,ppsx', 'maxSize'=>1024*1024*6)";
            $rules[]="array('".implode(', ',$file)."', 'file', 'on'=>'update', 'allowEmpty'=>true, 'types'=>'jpg,jpeg,gif,png,gz,tar,zip,pdf,doc,docx,xls,xlsx,ppt,pptx,pps,ppsx', 'maxSize'=>1024*1024*6)";
        }

        if($length!==array())
        {
            foreach($length as $len=>$cols)
                $rules[]="array('".implode(', ',$cols)."', 'length', 'max'=>$len)";
        }
        if($safe!==array())
            $rules[]="array('".implode(', ',$safe)."', 'safe')";

        if($url!==array())
            $rules[]="array('".implode(', ',$url)."', 'url')";

        if($email!==array())
            $rules[]="array('".implode(', ',$email)."', 'email')";

        return $rules;
    }

	/**
	 * Generates the rules for table fields.
	 * #MethodTracker
	 * This method overrides {@link ModelCode::generateRules}, from version 1.1.7 (r3135). Changes:
	 * <ul>
	 * <li>Adds the rule to fill empty attributes with null.</li>
	 * </ul>
	 * @param CDbTableSchema $table The table definition.
	 * @return array The rules for the table.
	 */
	public function generateRules($table) {
		$rules = array();
		$null = array();
		foreach ($table->columns as $column) {
			if ($column->autoIncrement)
				continue;
			if (!(!$column->allowNull && $column->defaultValue === null))
				$null[] = $column->name;
		}
		if ($null !== array())
			$rules[] = "array('" . implode(', ', $null) . "', 'default', 'setOnEmpty' => true, 'value' => null)";

		return array_merge($this->parent_generateRules_modificado($table), $rules);
	}

	/**
	 * Selects the representing column of the table.
	 * The "representingColumn" method is the responsible for the
	 * string representation of the model instance.
	 * @param CDbTableSchema $table The table definition.
	 * @return string|array The name of the column as a string or the names of the columns as an array.
	 * @see GxActiveRecord::representingColumn
	 * @see GxActiveRecord::__toString
	 */
	protected function getRepresentingColumn($table) {
		$columns = $table->columns;
		// If this is not a MANY_MANY pivot table
		if (!$this->isRelationTable($table)) {
			// First we look for a string, not null, not pk, not fk column, not original number on db.
			foreach ($columns as $name => $column) {
				if ($column->type === 'string' && !$column->allowNull && !$column->isPrimaryKey && !$column->isForeignKey && stripos($column->dbType, 'int') === false)
					return $name;
			}
			// Then a string, not null, not fk column, not original number on db.
			foreach ($columns as $name => $column) {
				if ($column->type === 'string' && !$column->allowNull && !$column->isForeignKey && stripos($column->dbType, 'int') === false)
					return $name;
			}
			// Then the first string column, not original number on db.
			foreach ($columns as $name => $column) {
				if ($column->type === 'string' && stripos($column->dbType, 'int') === false)
					return $name;
			}
		} // If the appropriate column was not found or if this is a MANY_MANY pivot table.
		// Then the pk column(s).
		$pk = $table->primaryKey;
		if ($pk !== null) {
			if (is_array($pk))
				return $pk;
			else
				return (string) $pk;
		}
		// Then the first column.
		return reset($columns)->name;
	}

	/**
	 * Finds the related class of the specified column.
	 * @param string $className The model class name.
	 * @param CDbColumnSchema $column The column.
	 * @return string The related class name. Or null if no matching relation was found.
	 */
	public function findRelatedClass($className, $column) {
		if (!$column->isForeignKey)
			return null;

		$relations = $this->getRelationsData($className);

        if ($relations) {
		foreach ($relations as $relation) {
			// Must be BELONGS_TO.
			if (($relation[0] === GxActiveRecord::BELONGS_TO) && ($relation[3] === $column->name))
				return $relation[1];
		}
        }
		// None found.
		return null;
	}

	/**
	 * Finds the relation data for all the relations of the specified model class.
	 * @param string $className The model class name.
	 * @return array An array of arrays with the relation data.
	 * The array will have one array for each relation.
	 * The key is the relation name. There are 5 values:
	 * 0: the relation type,
	 * 1: the related active record class name,
	 * 2: the joining (pivot) table (note: it may come with curly braces) (if the relation is a MANY_MANY, else null),
	 * 3: the local FK (if the relation is a BELONGS_TO or a MANY_MANY, else null),
	 * 4: the remote FK (if the relation is a HAS_ONE, a HAS_MANY or a MANY_MANY, else null).
	 * Or null if the model has no relations.
	 */
	public function getRelationsData($className) {
		if (!empty($this->relations))
			$relations = $this->relations;
		else
			$relations = $this->generateRelations();

		if (!isset($relations[$className]))
			return null;

		$result = array();
		foreach ($relations[$className] as $relationName => $relationData) {
			$result[$relationName] = $this->getRelationData($className, $relationName, $relations);
		}
		return $result;
	}

	/**
	 * Finds the relation data of the specified relation name.
	 * @param string $className The model class name.
	 * @param string $relationName The relation name.
	 * @param array $relations An array of relations for the models
	 * in the format returned by {@link ModelCode::generateRelations}. Optional.
	 * @return array The relation data. The array will have 3 values:
	 * 0: the relation type,
	 * 1: the related active record class name,
	 * 2: the joining (pivot) table (note: it may come with curly braces) (if the relation is a MANY_MANY, else null),
	 * 3: the local FK (if the relation is a BELONGS_TO or a MANY_MANY, else null),
	 * 4: the remote FK (if the relation is a HAS_ONE, a HAS_MANY or a MANY_MANY, else null).
	 * Or null if no matching relation was found.
	 */
	public function getRelationData($className, $relationName, $relations = array()) {
		if (empty($relations)) {
			if (!empty($this->relations))
				$relations = $this->relations;
			else
				$relations = $this->generateRelations();
		}

		if (isset($relations[$className]) && isset($relations[$className][$relationName]))
			$relation = $relations[$className][$relationName];
		else
			return null;

		$relationData = array();
		if (preg_match("/^array\(([\w:]+?),\s?'(\w+)',\s?'([\w\s\(\),]+?)'\)$/", $relation, $matches_base)) {
			$relationData[1] = $matches_base[2]; // the related active record class name

			switch ($matches_base[1]) {
				case 'self::BELONGS_TO':
					$relationData[0] = GxActiveRecord::BELONGS_TO; // the relation type
					$relationData[2] = null;
					$relationData[3] = $matches_base[3]; // the local FK
					$relationData[4] = null;
					break;
				case 'self::HAS_ONE':
					$relationData[0] = GxActiveRecord::HAS_ONE; // the relation type
					$relationData[2] = null;
					$relationData[3] = null;
					$relationData[4] = $matches_base[3]; // the remote FK
					break;
				case 'self::HAS_MANY':
					$relationData[0] = GxActiveRecord::HAS_MANY; // the relation type
					$relationData[2] = null;
					$relationData[3] = null;
					$relationData[4] = $matches_base[3]; // the remote FK
					break;
				case 'self::MANY_MANY':
					if (preg_match("/^((?:{{)?\w+(?:}})?)\((\w+),\s?(\w+)\)$/", $matches_base[3], $matches_manymany)) {
						$relationData[0] = GxActiveRecord::MANY_MANY; // the relation type
						$relationData[2] = $matches_manymany[1]; // the joining (pivot) table
						$relationData[3] = $matches_manymany[2]; // the local FK
						$relationData[4] = $matches_manymany[3]; // the remote FK
					}
					break;
			}

			return $relationData;
		} else
			return null;
	}

	/**
	 * Returns the message to be displayed when the newly generated code is saved successfully.
	 * #MethodTracker
	 * This method overrides {@link CCodeModel::successMessage}, from version 1.1.7 (r3135). Changes:
	 * <ul>
	 * <li>Custom giix success message.</li>
	 * </ul>
	 * @return string The message to be displayed when the newly generated code is saved successfully.
	 */
	public function successMessage() {
     /*   <ul style="list-style-type: none; padding-left: 0;">
	<li><img src="http://giix.org/icons/love.png"> Show how you love giix on <a href="http://www.yiiframework.com/forum/index.php?/topic/13154-giix-%E2%80%94-gii-extended/">the forum</a> and on its <a href="http://www.yiiframework.com/extension/giix">extension page</a></li>
	<li><img src="http://giix.org/icons/vote.png"> Upvote <a href="http://www.yiiframework.com/extension/giix">giix</a></li>
	<li><img src="http://giix.org/icons/powered.png"> Show everybody that you are using giix in <a href="http://www.yiiframework.com/forum/index.php?/topic/19226-powered-by-giix/">Powered by giix</a></li>
	<li><img src="http://giix.org/icons/donate.png"> <a href="http://giix.org/">Donate</a></li>
</ul>*/

		return <<<EOM
<p><strong>Sweet!</strong></p>

<p><b>Power by iReevo</b></p>
<p style="margin: 2px 0; position: relative; text-align: right; top: -15px; color: #668866;">icons by <a href="http://www.famfamfam.com/lab/icons/silk/" style="color: #668866;">famfamfam.com</a></p>
EOM;
	}

}