<?php
/**
 * This is the template for generating the model class of a specified table.
 * - $this: the ModelCode object
 * - $tableName: the table name for this class (prefix is already removed if necessary)
 * - $modelClass: the model class name
 * - $columns: list of table columns (name=>CDbColumnSchema)
 * - $labels: list of attribute labels (name=>label)
 * - $rules: list of validation rules
 * - $relations: list of relations (name=>relation declaration)
 * - $representingColumn: the name of the representing column for the table (string) or
 *   the names of the representing columns (array)
 */
?>
<?php echo "<?php\n"; ?>

/**
 * This is the model base class for the table "<?php echo $tableName; ?>".
 * It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "<?php echo $modelClass; ?>".
 * This code was improve iReevo Team
 * Columns in table "<?php echo $tableName; ?>" available as properties of the model,
<?php if(!empty($relations)): ?>
 * followed by relations of table "<?php echo $tableName; ?>" available as properties of the model.
<?php else: ?>
 * and there are no model relations.
<?php endif; ?>
 *
<?php foreach($columns as $column): ?>
 * @property <?php echo (stripos($column->dbType, 'int') !== false && strpos($column->comment,'@date')!==false?'string':$column->type).' $'.$column->name."\n"; ?>
<?php endforeach; ?>
 *
<?php
$seo=0;
$one_many= array();
$many_many=array();
foreach(array_keys($relations) as $name): ?>
 * @property <?php
	$relationData = $this->getRelationData($modelClass, $name);
	$relationType = $relationData[0];
	$relationModel = $relationData[1];

	switch($relationType) {
		case GxActiveRecord::BELONGS_TO:
		case GxActiveRecord::HAS_ONE:
			echo $relationModel;

			break;
		case GxActiveRecord::HAS_MANY:
		case GxActiveRecord::MANY_MANY:
			echo $relationModel . '[]';
			break;
		default:
			echo 'mixed';
	}
    if(GxActiveRecord::HAS_MANY){
        $one_many[]= $relationModel;
    }
    if(GxActiveRecord::MANY_MANY){
        $many_many[]= $relationModel;
    }
    if($relationModel=='SeoUrl'){
        $seo=1;
    }
	echo ' $' . $name . "\n";
	?>
<?php endforeach; ?>
 * @property Date2TimeBehavior $date2time
 * @property CurrencyBehavior $currency
 * @property ImageARBehavior $imageAR

 */
abstract class <?php echo $this->baseModelClass; ?> extends <?php echo $this->baseClass; ?> {
<?php if($seo==1){?>
    //SEO
    public $seo_url;
    public $seo_title;
    public $seo_description;
    public $seo_keywords;
<?php }?>
<?php if(count($many_many)>0){?>
// many to many relationship
    <?php foreach($many_many as $item){?>
        public $<?php echo $item;?>;
    <?php }?>
<?php }?>

<?php
//OBTENIENDO DATOS PARA LLENAR LA PARTE DE LAS IMAGENES


$images_data = array();
$files_data = array();
foreach($columns as $column)
{
    $comment = $column->comment;
    $comment_json = json_decode($comment, 6);
    $type = 'recipe';
    $dimensions = '100,100';
    $allowEmpty = true;
    $label = 'Recipe Image';
    if(isset($comment_json))
    {
        if(array_key_exists('type', $comment_json))
        {
            $type = $comment_json['type'];
            if(array_key_exists('label', $comment_json))
            {
                $label = $comment_json['label'];
            }
            if(array_key_exists('allowEmpty', $comment_json))
            {
                $allowEmpty = $comment_json['allowEmpty'];
            }
            if(stripos($type, 'recipeImg') !== false)
            {

                if(array_key_exists('dimensions', $comment_json))
                {
                    $dimensions = $comment_json['dimensions'];
                }


                elseif(array_key_exists('allowempty', $comment_json))
                {
                    $allowEmpty = $comment_json['allowempty'];
                }
                $images_data[] = array('type'=> $type, 'dimensions'=> $dimensions, 'allowEmpty'=>$allowEmpty,
                            'label'=>$label, 'name'=> $column->name);
            }
            elseif(stripos($type, '@file') !== false)
            {
                $files_data[] = array('type' => $type, 'label' => $label, 'allowEmpty' => $allowEmpty,
                    'name' => $column->name);
            }

        }

    }

}
if(sizeof($images_data) > 0)
{
    @mkdir('/images/' . $modelClass, 0777, false);
}
?>
/* si tiene una imagen pa subir con ImageARBehavior, descomente la linea siguiente
// public $recipeImg;

    /**
    * Behaviors.
    * @return array
    */
<?php foreach($images_data as $key=> $images):?>
    public $<?php echo $images["type"]?>;
<?php endforeach?>
<?php foreach($files_data as $key=> $file):?>
    public $<?php echo $file["name"]?>;
<?php endforeach?>
    function behaviors() {
        return CMap::mergeArray(parent::behaviors(), array(
                <?php foreach($images_data as $key=> $images):
                    $key = $key + 1;?>
                '_<?php echo $images['name']?>' => array(
                    'class' => 'ImageARBehavior',
                    'attribute' => '<?php echo $images['type']?>', // this must exist
                    'extension' => 'jpg,gif,png', // possible extensions, comma separated
                    'prefix' => 'img<?php echo $key?>_',
                    'relativeWebRootFolder' => '/images/<?php echo $modelClass; ?>',
                    'formats' => array(
                    // create a thumbnail for used in the view datails
                    'thumb' => array(
                    'suffix' => '_thumb',
                    'process' => array('resize' => array(50, 50)),
                    ),
                    'normal' => array(
                    'suffix' => '_normal',
                    'process' => array('resize' => array(<?php echo $images['dimensions']?>, 1)),
                    ),
                    // and override the default :
                    ),

                    'defaultName' => 'default', // when no file is associated, this one is used
                            // defaultName need to exist in the relativeWebRootFolder path, and prefixed by prefix,
                            // and with one of the possible extensions. if multiple formats are used, a default file must exist
                            // for each format. Name is constructed like this :
                            //     {prefix}{name of the default file}{suffix}{one of the extension}
                ),
                <?php endforeach?>


                <?php foreach($files_data as $key=>$file):?>
    '<?php echo '_'.$file['name']?>' => array(
            'class' => 'FileARBehavior',
            'attribute' => '<?php echo $file['name']?>', // this must exist
            'extension' => 'pdf', // possible extensions, comma separated
            'prefix' => '<?php echo $file['name']?>_',
            'relativeWebRootFolder' => '/uploads',
            'formats' => array(
                'normal' => array(
                    'suffix' => '_1',
                ),

            )),

                <?php endforeach?>
                'files' => array(
                     'class'=>'application.modules.ycm.behaviors.FileBehavior',
                ),
                'date2time' => array(
                    'class' => 'ycm.behaviors.Date2TimeBehavior',
                    'attributes'=>'<?php echo $this->getDates(); ?>',
                    'format'=>'Y-m-d',
                ),
                'datetime2time' => array(
                    'class' => 'ycm.behaviors.Date2TimeBehavior',
                    'attributes'=>'<?php echo $this->getDateTimes(); ?>',
                    'format'=>'Y-m-d H:i:s',
                ),
                'currency' => array(
                    'class' => 'ycm.behaviors.CurrencyBehavior',
                    'attributes'=>'<?php echo $this->getCurrencies(); ?>',
                ),
                <?php if($this->orderColumn):?>
                'order' => array(
                    'class' => 'ycm.behaviors.OrderBehavior',
                    'attr'=>'<?php echo $this->orderColumn->name; ?>',
                ),
                <?php endif; ?>
            ));
    }


	public static function model($className=__CLASS__) {
		return parent::model($className);
	}

	public function tableName() {
		return '<?php echo $tableName; ?>';
	}

	public static function label($n = 1) {
		return self::model()->t_model('<?php echo $modelClass; ?>|<?php echo $this->pluralize($modelClass); ?>', $n);
	}

	public static function representingColumn() {
<?php if (is_array($representingColumn)): ?>
		return array(
<?php foreach($representingColumn as $representingColumn_item): ?>
			'<?php echo $representingColumn_item; ?>',
<?php endforeach; ?>
		);
<?php else: ?>
		return '<?php echo $representingColumn; ?>';
<?php endif; ?>
	}

	public function rules() {
		return array(
<?php foreach($rules as $rule): ?>
			<?php echo $rule.",\n"; ?>
<?php endforeach; ?>

/* descomente las lineas siguientes si quiere subir una image con ImageARBehavior*/
<?php foreach($images_data as $image):?>
    <?php
    $allowEmpty = 'true';
    if(!$image['allowEmpty'])
        $allowEmpty = 'false';
    ?>
array('<?php echo $image['type']?>', 'file', 'on'=>'insert', 'allowEmpty'=><?php echo $allowEmpty?>, 'types'=>'jpg,jpeg,gif,png,JPG,GIF,JPEG,PNG', 'maxSize'=>1024*1024*6),
array('<?php echo $image['type']?>', 'file', 'on'=>'update', 'allowEmpty'=>true, 'types'=>'jpg,jpeg,gif,png,JPG,GIF,JPEG,PNG', 'maxSize'=>1024*1024*6),
array('<?php echo $image['type']?>', 'safe'),
<?php endforeach?>
<?php foreach($files_data as $file):?>
    <?php $allowEmpty = 'true';
    if(!$file['allowEmpty'])
        $allowEmpty = 'false';
    ?>
        array('<?php echo $file['name']?>', 'file', 'on'=>'insert,update', 'allowEmpty' => true,'types' => 'pdf'),
            array('<?php echo $file['name']?>', 'file', 'on'=>'insert', 'allowEmpty' => <?php echo $allowEmpty?>,'types' => 'pdf'),
<?php endforeach?>


			array('<?php echo implode(', ', array_keys($columns)); ?>', 'safe', 'on'=>'search'),
        <?php if($seo): ?>
            array('seo_url, seo_title, seo_keywords, seo_description', 'safe'),
        <?php endif?>
<?php if(count($many_many)>0): ?>
    array('<?php echo implode(', ', $many_many); ?>', 'safe'),
<?php endif?>
		);
	}

	public function relations() {
		return array(
<?php foreach($relations as $name=>$relation): ?>
			<?php if($name=='SeoUrl'){
                echo "'seo' => {$relation},\n";
            }
            else{
                echo "'{$name}' => {$relation},\n";
            } ?>
<?php endforeach; ?>
		);
	}

	public function pivotModels() {
		return array(
<?php foreach($pivotModels as $relationName=>$pivotModel): ?>
			<?php echo "'{$relationName}' => '{$pivotModel}',\n"; ?>
<?php endforeach; ?>
		);
	}

	public function attributeLabels() {
		return array(
<?php foreach($labels as $name=>$label): ?>
<?php if($label === null): ?>
			<?php /*echo "'{$name}' => null,\n";*/ ?>
<?php else: ?>
			<?php echo "'{$name}' => Yii::t('".$modelClass."','{$name}'),\n"; ?>
<?php endif; ?>
<?php endforeach; ?>
<?php foreach($images_data as $image):?>
    <?php echo "'".$image['type']."' => Yii::t('".$modelClass."','".$image['label']."'),\n"?>
<?php endforeach?>

<?php if ($seo==1):?>
    'seo_url_id' => Yii::t('SEO',Seo Url ID'),
    'seo_url' => Yii::t('SEO','SEO URL'),
    'seo_title' => Yii::t('SEO','SEO Title'),
    'seo_keywords' => Yii::t('SEO','SEO Keywords'),
    'seo_description' => Yii::t('SEO','SEO Description'),
<?php endif?>
		);
	}

	public function search() {
		$criteria = new CDbCriteria;

<?php foreach($columns as $name=>$column): ?>
<?php $partial = ($column->type==='string' and !$column->isForeignKey); ?>
		$criteria->compare('<?php echo $name; ?>', $this-><?php echo $name; ?><?php echo $partial ? ', true' : ''; ?>);
<?php endforeach; ?>

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
            <?php if($this->orderColumn):?>
                'sort' => array(
                    'defaultOrder' => 't.<?php echo $this->orderColumn->name; ?>',
                ),
            <?php endif; ?>
		));
	}
}