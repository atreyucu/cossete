<?php
Yii::import('zii.widgets.CBreadcrumbs');
class IRBreadcrumb extends CBreadcrumbs
{
    public $separator = ' ';
    public $htmlOptions = array('id' => 'breadcrumb');
    public $homeLink = array('site/index');
    public $inactiveLinkTemplate = '<a href="#" class="current">{label}</a>';

    public function init()
    {
        $this->homeLink = CHtml::link('<i class="glyphicon glyphicon-home"></i>' . Yii::t('admin', 'Home'), '/site/index', array(
                'class' => 'tip-bottom',
                'title' => Yii::t('admin', 'Go to Home')
            ));
        parent::init();
    }
}