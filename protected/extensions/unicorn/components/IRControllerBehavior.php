<?php

class IRControllerBehavior extends CBehavior
{
    public $title = null;
    public $icon = 'th';
    public $breadcrumbs = array();
    public $sideMenu = array();
    public $userMenu = array();
    public $contentButtons = array();

    public $menu = array();

    public function renderFormFooter()
    {
        $this->owner->renderPartial('//helpers/form-footer');
    }


}