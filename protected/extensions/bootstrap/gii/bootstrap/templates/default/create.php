<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 */
?>
<?php
echo "<?php\n";
$label = $this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs=array(
	'$label'=>array('index'),
	'Create',
);\n";
?>

<!--$this->menu=array(
array('label'=>'List <?php /*echo $this->modelClass; */?>','url'=>array('index')),
array('label'=>'Manage <?php /*echo $this->modelClass; */?>','url'=>array('admin')),
);-->
?>

<h1>Create <?php echo $this->modelClass; ?></h1>

<?php echo "<?php echo \$this->renderPartial('_form', array('model'=>\$model)); ?>"; ?>

<div class="button-menu" style="text-align: right">
    <a href="index" class="btn btn-default"><span class="glyphicon glyphicon-list"></span> <?php echo t('Listar')?></a>
    <a href="admin" class="btn btn-default"><span class="glyphicon glyphicon-briefcase"></span> <?php echo t('Administrar')?></a>
</div>

