<!DOCTYPE html>
<html lang="<?php echo app()->language;?>">
<head>
    <meta charset="UTF-8" />



    <?php $this->widget('application.modules.seo.widgets.SeoPageWidget'); ?>
    <?php
    cs()->registerCssFile(thu('css/unicorn.login.css'));
    cs()->registerCssFile(thu('css/jquery.gritter.css'));


    ?>
</head>
<body>




<div id="logo" style="text-align: center; font-size: 25px; font-weight: bold; color: white">
    <?php
    $entity=CompanyInfo::model()->find();
    if(isset($entity)){
        if(isset($entity->_company_logo)) {
            echo CHtml::image($entity->_company_logo->getFileUrl('normal'),$entity->company_logo);
        }
        else {
            echo $entity->name;
        }
    }
    else {
        echo app()->name;
    }?>

</div>
<div id="loginbox">
    <?php echo $content; ?>
</div>

<?php
cs()->registerScriptFile(thu('js/jquery.gritter.js'), CClientScript::POS_END);
cs()->registerScriptFile(thu('js/unicorn.login.js'), CClientScript::POS_END);
//cs()->registerScriptFile(thu('js/jquery.ias.js'), CClientScript::POS_END);

?>
<?php
$flashes = user()->flashes;
foreach($flashes as $key => $message){
    $icons = param('flash.icons');
    $icon = $icons[$key];
    cs()->registerScript('gritter_flash', <<<JS
    $.gritter.add({
        text:	'<i class="icon-$icon"></i> $message',
        sticky: false
    });
JS
        , CClientScript::POS_READY);

}
?>
</body>
</html>
