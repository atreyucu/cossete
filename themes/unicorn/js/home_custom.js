function swap_contents(tr1,tr2){
    if(tr1.length != 0){
        var temp_value = tr1.children('td').last().prev().children('input[type=hidden]').val();
        tr1.children('td').last().prev().children('input[type=hidden]').val(tr2.children('td').last().prev().children('input[type=hidden]').val());
        tr2.children('td').last().prev().children('input[type=hidden]').val(temp_value);

        var temp_html = tr1.html();
        tr1.html(tr2.html());
        tr2.html(temp_html);
    }
}

function add_gall(){
    if($("#homeGallTable tbody").children("tr").length > 1){
        $("#homeGallTable tbody").children("tr").last().children().last().prev().children("input[type=hidden]").val(parseInt($("#homeGallTable tbody").children("tr").last().prev().children().last().prev().children("input[type=hidden]").val()) + 1);
    }
    else{
        $("#homeGallTable tbody").children("tr").last().children().last().prev().children("input[type=hidden]").val(1);
    }
}

function add_trend(){
    if($("#homeTrendTable tbody").children("tr").length > 1){
        $("#homeTrendTable tbody").children("tr").last().children().last().prev().children("input[type=hidden]").val(parseInt($("#homeTrendTable tbody").children("tr").last().prev().children().last().prev().children("input[type=hidden]").val()) + 1);
    }
    else{
        $("#homeTrendTable tbody").children("tr").last().children().last().prev().children("input[type=hidden]").val(1);
    }
}